#!/bin/bash

DAILY_BACKUP_FOLDER=GAT_BACKUP
MONTHLY_BACKUP_FOLDER=MONTHLY_GAT_BACKUP

TIMESTAMP=$(date +"%Y-%m-%d_%H:%M")
MONTHLY_BACKUP_TIME=$(date +"%d")

BACKUP_DIR="/root/backup/$TIMESTAMP"

echo "$TIMESTAMP";
echo "Preparing backup files...";
mkdir -p "$BACKUP_DIR"
zip -r services.zip /root/services -q
mv services.zip $BACKUP_DIR

size=$(du -s $BACKUP_DIR | awk '{ print $1}')

if [ $size -lt "1000000" ]
then
    echo "Cannot zip the backup file!"
else 
    start=$(date +"%Y-%m-%d_%H:%M:%S")
    echo "Starting uploading backup at: $start";

    if [ "20" = $MONTHLY_BACKUP_TIME ]
    then
        rclone move $BACKUP_DIR "remote:$MONTHLY_BACKUP_FOLDER/$TIMESTAMP" >> /var/log/rclone.log 2>&1
        rclone -q --min-age 3M delete "remote:$MONTHLY_BACKUP_FOLDER" #Remove all backups older than 3 months
        rclone -q --min-age 3M rmdirs "remote:$MONTHLY_BACKUP_FOLDER" #Remove all empty folders older than 3 months
    else
        rclone move $BACKUP_DIR "remote:$DAILY_BACKUP_FOLDER/$TIMESTAMP" >> /var/log/rclone.log 2>&1
        rclone -q --min-age 2d delete "remote:$DAILY_BACKUP_FOLDER" #Remove all backups older than 2 days
        rclone -q --min-age 2d rmdirs "remote:$DAILY_BACKUP_FOLDER" #Remove all empty folders older than 2 days
    fi

    # Clean up
    rclone cleanup "remote:" #Cleanup Trash
    rm -rf $BACKUP_DIR

    end=$(date +"%Y-%m-%d_%H:%M:%S")
    echo "Finished at: $end";
fi

echo "Backup's size: $size"
echo '';
