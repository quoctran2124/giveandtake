// To backup the whole source code and the database (in other word, backup the services folder) to google drive => need to install rclone 
and use it to connect to google drive 

// After connecting to google drive successfully, allocate files and folders as follows:
- gat: /var/ 		(a shell script that helps us to backup)
- backup.log: /var/log/ (a log file for the backup process)

// To automatically backup everyday, use "crontab"
// Create a crontab script:
EDITOR=nano crontab -e

// Copy and paste:
0 10 * * * sh /var/gat/gat.sh >> /var/log/backup.log 2>&1

// Press Ctrl+O, Enter to save and Ctrl+X to exit
// crontab script will be saved at "/var/spool/cron/crontabs/"