\---services
    \---projects
        +---giveandtake
        |   |   .gitignore
        |   |   README.md
        |   |   
        |   +---APKs
        |   |   |   giveandtake.apk
        |   |   |   
        |   |   \---Keystore
        |   |       \---chovanhan
        |   |               chovanhan.keyInfo
        |   |               chovanhan.keystore
        |   |               
        |   +---Software
        |   |   |   GiveAndTake.sln
        |   |   |   
        |   |   +---GiveAndTake.Core
        |   |   |   |   App.cs
        |   |   |   |   AppConstants.cs
        |   |   |   |   AppEnum.cs
        |   |   |   |   GiveAndTake.Core.csproj
        |   |   |   |   
        |   |   |   +---Exceptions
        |   |   |   |       AppException.cs
        |   |   |   |       
        |   |   |   +---Extensions
        |   |   |   |       IDictionaryExt.cs
        |   |   |   |       
        |   |   |   +---Helpers
        |   |   |   |   |   ColorHelper.cs
        |   |   |   |   |   DebouncerHelper.cs
        |   |   |   |   |   EmailHelper.cs
        |   |   |   |   |   IDeviceInfo.cs
        |   |   |   |   |   ISystemHelper.cs
        |   |   |   |   |   JsonHelper.cs
        |   |   |   |   |   MenuOptionHelper.cs
        |   |   |   |   |   TimeHelper.cs
        |   |   |   |   |   
        |   |   |   |   \---Interface
        |   |   |   |           IEmailHelper.cs
        |   |   |   |           IUrlHelper.cs
        |   |   |   |           
        |   |   |   +---Locales
        |   |   |   |       vi.txt
        |   |   |   |       
        |   |   |   +---Models
        |   |   |   |       ApiNotificationResponse.cs
        |   |   |   |       ApiPostsResponse.cs
        |   |   |   |       ApiRequestsResponse.cs
        |   |   |   |       BaseResponse.cs
        |   |   |   |       BaseUser.cs
        |   |   |   |       Category.cs
        |   |   |   |       CreatePost.cs
        |   |   |   |       DataModel.cs
        |   |   |   |       EditPost.cs
        |   |   |   |       ErrorResponse.cs
        |   |   |   |       IDataModel.cs
        |   |   |   |       Image.cs
        |   |   |   |       LoginResponse.cs
        |   |   |   |       Notification.cs
        |   |   |   |       Pagination.cs
        |   |   |   |       PopupListParam.cs
        |   |   |   |       Post.cs
        |   |   |   |       PostImage.cs
        |   |   |   |       PostResponse.cs
        |   |   |   |       ProvinceCitiesResponse.cs
        |   |   |   |       ProvinceCity.cs
        |   |   |   |       PushNotificationUserInformation.cs
        |   |   |   |       Request.cs
        |   |   |   |       RequestResponse.cs
        |   |   |   |       Response.cs
        |   |   |   |       SortFilter.cs
        |   |   |   |       StatusObj.cs
        |   |   |   |       User.cs
        |   |   |   |       
        |   |   |   +---Services
        |   |   |   |       ILoadingOverlayService.cs
        |   |   |   |       IManagementService.cs
        |   |   |   |       LoadingOverlayService.cs
        |   |   |   |       ManagementService.cs
        |   |   |   |       RestClient.cs
        |   |   |   |       
        |   |   |   \---ViewModels
        |   |   |       |   AboutViewModel.cs
        |   |   |       |   CreatePostViewModel.cs
        |   |   |       |   LoginViewModel.cs
        |   |   |       |   NotificationIconViewModel.cs
        |   |   |       |   PhotoCollectionViewModel.cs
        |   |   |       |   PhotoTemplateViewModel.cs
        |   |   |       |   PostDetailViewModel.cs
        |   |   |       |   PostImageViewModel.cs
        |   |   |       |   PostItemViewModel.cs
        |   |   |       |   RequestDetailViewModel.cs
        |   |   |       |   RequestItemViewModel.cs
        |   |   |       |   RequestsViewModel.cs
        |   |   |       |   ResponseViewModel.cs
        |   |   |       |   UserProfileViewModel.cs
        |   |   |       |   
        |   |   |       +---Base
        |   |   |       |       BaseMyRequestDetailViewModel.cs
        |   |   |       |       BaseViewModel.cs
        |   |   |       |       DetailViewModel.cs
        |   |   |       |       MasterViewModel.cs
        |   |   |       |       
        |   |   |       +---Popup
        |   |   |       |   |   LoadingOverlayViewModel.cs
        |   |   |       |   |   PopupCreateRequestViewModel.cs
        |   |   |       |   |   PopupExtensionOptionViewModel.cs
        |   |   |       |   |   PopupItemViewModel.cs
        |   |   |       |   |   PopupListViewModel.cs
        |   |   |       |   |   PopupMessageViewModel.cs
        |   |   |       |   |   PopupNotificationViewModel.cs
        |   |   |       |   |   PopupResponseViewModel.cs
        |   |   |       |   |   PopupViewModel.cs
        |   |   |       |   |   PopupWarningResponseViewModel.cs
        |   |   |       |   |   PopupWarningViewModel.cs
        |   |   |       |   |   
        |   |   |       |   \---MyRequest
        |   |   |       |           MyRequestApprovedViewModel.cs
        |   |   |       |           MyRequestPendingViewModel.cs
        |   |   |       |           MyRequestReceivedViewModel.cs
        |   |   |       |           
        |   |   |       \---TabNavigation
        |   |   |               ConversationViewModel.cs
        |   |   |               HomeViewModel.cs
        |   |   |               NotificationItemViewModel.cs
        |   |   |               NotificationViewModel.cs
        |   |   |               ProfileViewModel.cs
        |   |   |               TabNavigationViewModel.cs
        |   |   |               
        |   |   +---GiveAndTake.Droid
        |   |   |   |   Application.cs
        |   |   |   |   chovanhan.keystore
        |   |   |   |   GiveAndTake.Droid.csproj
        |   |   |   |   google-services.json
        |   |   |   |   LinkerPleaseInclude.cs
        |   |   |   |   Setup.cs
        |   |   |   |   SplashScreen.cs
        |   |   |   |   
        |   |   |   +---Assets
        |   |   |   |       AboutAssets.txt
        |   |   |   |       
        |   |   |   +---Controls
        |   |   |   |       CustomButton.cs
        |   |   |   |       CustomCardView.cs
        |   |   |   |       CustomCircleImageView.cs
        |   |   |   |       CustomImageView.cs
        |   |   |   |       CustomMvxCachedImageView.cs
        |   |   |   |       CustomProfileItem.cs
        |   |   |   |       CustomRecyclerView.cs
        |   |   |   |       CustomSquareRoundedImageView.cs
        |   |   |   |       CustomTextView.cs
        |   |   |   |       ImageSliderAdapter.cs
        |   |   |   |       
        |   |   |   +---Converters
        |   |   |   |       BoolToViewStatesValueConverter.cs
        |   |   |   |       
        |   |   |   +---Helpers
        |   |   |   |       ColorHelper.cs
        |   |   |   |       DeviceInfo.cs
        |   |   |   |       DimensionHelper.cs
        |   |   |   |       FilterHelper.cs
        |   |   |   |       ImageHelper.cs
        |   |   |   |       KeyboardHelper.cs
        |   |   |   |       SytemHelper.cs
        |   |   |   |       UrlHelper.cs
        |   |   |   |       
        |   |   |   +---Properties
        |   |   |   |       AndroidManifest.xml
        |   |   |   |       AssemblyInfo.cs
        |   |   |   |       
        |   |   |   +---Resources
        |   |   |   |   |   AboutResources.txt
        |   |   |   |   |   
        |   |   |   |   +---drawable
        |   |   |   |   |       btn_delete_photo.png
        |   |   |   |   |       cancel_button_bg.xml
        |   |   |   |   |       cancel_button_bg__normal_state.xml
        |   |   |   |   |       cancel_button_bg__pressed_state.xml
        |   |   |   |   |       cancel_button_text.xml
        |   |   |   |   |       category_bg.xml
        |   |   |   |   |       category_button.xml
        |   |   |   |   |       category_button_default.png
        |   |   |   |   |       category_button_selected.png
        |   |   |   |   |       chat.png
        |   |   |   |   |       comment.png
        |   |   |   |   |       conversation_off.png
        |   |   |   |   |       conversation_on.png
        |   |   |   |   |       default_avatar.png
        |   |   |   |   |       default_post.png
        |   |   |   |   |       display_number_image_bg.xml
        |   |   |   |   |       Extention.png
        |   |   |   |   |       facebook_button.png
        |   |   |   |   |       gallery_button.png
        |   |   |   |   |       google_button.png
        |   |   |   |   |       header_bar.png
        |   |   |   |   |       heart_off.png
        |   |   |   |   |       home_off.png
        |   |   |   |   |       home_on.png
        |   |   |   |   |       icon_profile.xml
        |   |   |   |   |       item_count.axml
        |   |   |   |   |       left_button_bg.xml
        |   |   |   |   |       left_button_bg_normal.xml
        |   |   |   |   |       left_button_bg_pressed.xml
        |   |   |   |   |       list_bg.xml
        |   |   |   |   |       location_button.xml
        |   |   |   |   |       location_button_default.png
        |   |   |   |   |       location_button_selected.png
        |   |   |   |   |       location_logo.png
        |   |   |   |   |       LoginLogo.png
        |   |   |   |   |       login_background.png
        |   |   |   |   |       login_logo.png
        |   |   |   |   |       logoApp_withText.png
        |   |   |   |   |       logoSioux.png
        |   |   |   |   |       Multiphoto.png
        |   |   |   |   |       navigateLeft.png
        |   |   |   |   |       navigateRight.png
        |   |   |   |   |       new_post.png
        |   |   |   |   |       notification_off.png
        |   |   |   |   |       notification_on.png
        |   |   |   |   |       phone_contact.png
        |   |   |   |   |       popup_bg.xml
        |   |   |   |   |       popup_button_bg.xml
        |   |   |   |   |       popup_button_cancel_bg.xml
        |   |   |   |   |       popup_line.png
        |   |   |   |   |       PostPhoto.png
        |   |   |   |   |       post_divider.xml
        |   |   |   |   |       post_editText_bg.xml
        |   |   |   |   |       profile_off.png
        |   |   |   |   |       profile_on.png
        |   |   |   |   |       request_icon.xml
        |   |   |   |   |       request_number_bg.axml
        |   |   |   |   |       request_off.png
        |   |   |   |   |       request_on.png
        |   |   |   |   |       right_button_bg.xml
        |   |   |   |   |       right_button_bg_normal.xml
        |   |   |   |   |       right_button_bg_pressed.xml
        |   |   |   |   |       search_bg.xml
        |   |   |   |   |       setting.png
        |   |   |   |   |       sort_button.xml
        |   |   |   |   |       sort_button_default.png
        |   |   |   |   |       sort_button_selected.png
        |   |   |   |   |       spinner_button_bg.xml
        |   |   |   |   |       submit_button_bg.xml
        |   |   |   |   |       submit_button_bg__disable_state.xml
        |   |   |   |   |       submit_button_bg__normal_state.xml
        |   |   |   |   |       submit_button_bg__pressed_state.xml
        |   |   |   |   |       tab_navigation_icon_conversation.xml
        |   |   |   |   |       tab_navigation_icon_home.xml
        |   |   |   |   |       tab_navigation_icon_notification.xml
        |   |   |   |   |       tab_navigation_icon_profile.xml
        |   |   |   |   |       takePicture_button.png
        |   |   |   |   |       tony.png
        |   |   |   |   |       Top_logo.png
        |   |   |   |   |       
        |   |   |   |   +---drawable-hdpi
        |   |   |   |   |       facebook_button.png
        |   |   |   |   |       google_button.png
        |   |   |   |   |       login_background.png
        |   |   |   |   |       login_logo.png
        |   |   |   |   |       new_post.png
        |   |   |   |   |       splash_screen.png
        |   |   |   |   |       
        |   |   |   |   +---drawable-mdpi
        |   |   |   |   |       facebook_button.png
        |   |   |   |   |       google_button.png
        |   |   |   |   |       login_background.png
        |   |   |   |   |       login_logo.png
        |   |   |   |   |       new_post.png
        |   |   |   |   |       splash_screen.png
        |   |   |   |   |       
        |   |   |   |   +---drawable-xhdpi
        |   |   |   |   |       facebook_button.png
        |   |   |   |   |       google_button.png
        |   |   |   |   |       login_background.png
        |   |   |   |   |       login_logo.png
        |   |   |   |   |       new_post.png
        |   |   |   |   |       splash_screen.png
        |   |   |   |   |       
        |   |   |   |   +---drawable-xxhdpi
        |   |   |   |   |       facebook_button.png
        |   |   |   |   |       google_button.png
        |   |   |   |   |       login_background.png
        |   |   |   |   |       login_logo.png
        |   |   |   |   |       new_post.png
        |   |   |   |   |       splash_screen.png
        |   |   |   |   |       
        |   |   |   |   +---font
        |   |   |   |   |       sanfranciscodisplay_bold.otf
        |   |   |   |   |       sanfranciscodisplay_light.otf
        |   |   |   |   |       sanfranciscodisplay_medium.otf
        |   |   |   |   |       sanfranciscodisplay_regular.otf
        |   |   |   |   |       
        |   |   |   |   +---layout
        |   |   |   |   |       AboutView.axml
        |   |   |   |   |       BaseMyRequestDetailView.axml
        |   |   |   |   |       ConversationView.axml
        |   |   |   |   |       CreatePostView.axml
        |   |   |   |   |       FragmentImage.axml
        |   |   |   |   |       HomeView.axml
        |   |   |   |   |       LoadingOverlayView.axml
        |   |   |   |   |       LoginView.axml
        |   |   |   |   |       MasterView.axml
        |   |   |   |   |       MyPostTemplate.axml
        |   |   |   |   |       MyRequestApprovedView.axml
        |   |   |   |   |       MyRequestPendingView.axml
        |   |   |   |   |       MyRequestReceivedView.axml
        |   |   |   |   |       NotificationIconView.axml
        |   |   |   |   |       NotificationTemplate.axml
        |   |   |   |   |       NotificationView.axml
        |   |   |   |   |       PhotoCollectionView.axml
        |   |   |   |   |       PhotoTemplate.axml
        |   |   |   |   |       PopupCreateRequestRequestView.axml
        |   |   |   |   |       PopupCreateRequestView.axml
        |   |   |   |   |       PopupExtensionOptionView.axml
        |   |   |   |   |       PopupItemTemplate.axml
        |   |   |   |   |       PopupListView.axml
        |   |   |   |   |       PopupMessageView.axml
        |   |   |   |   |       PopupNotificationView.axml
        |   |   |   |   |       PopupReportView.axml
        |   |   |   |   |       PopupWarningView.axml
        |   |   |   |   |       PostDetailView.axml
        |   |   |   |   |       PostImageView.axml
        |   |   |   |   |       PostTemplate.axml
        |   |   |   |   |       ProfileView.axml
        |   |   |   |   |       RequestDetailView.axml
        |   |   |   |   |       RequestsView.axml
        |   |   |   |   |       RequestTemplate.axml
        |   |   |   |   |       ResponseView.axml
        |   |   |   |   |       SplashScreen.axml
        |   |   |   |   |       TabNavigation.axml
        |   |   |   |   |       TabNavigationIcon.axml
        |   |   |   |   |       UserProfileView.axml
        |   |   |   |   |       
        |   |   |   |   +---mipmap-hdpi
        |   |   |   |   |       ic_launcher.png
        |   |   |   |   |       ic_launcher_round.png
        |   |   |   |   |       
        |   |   |   |   +---mipmap-mdpi
        |   |   |   |   |       ic_launcher.png
        |   |   |   |   |       ic_launcher_round.png
        |   |   |   |   |       
        |   |   |   |   +---mipmap-xhdpi
        |   |   |   |   |       ic_launcher.png
        |   |   |   |   |       ic_launcher_round.png
        |   |   |   |   |       
        |   |   |   |   +---mipmap-xxhdpi
        |   |   |   |   |       ic_launcher.png
        |   |   |   |   |       ic_launcher_round.png
        |   |   |   |   |       
        |   |   |   |   +---mipmap-xxxhdpi
        |   |   |   |   |       ic_launcher.png
        |   |   |   |   |       ic_launcher_round.png
        |   |   |   |   |       
        |   |   |   |   +---values
        |   |   |   |   |       colors.xml
        |   |   |   |   |       dimens.xml
        |   |   |   |   |       ic_launcher_background.xml
        |   |   |   |   |       strings.xml
        |   |   |   |   |       styles.xml
        |   |   |   |   |       
        |   |   |   |   +---values-hdpi
        |   |   |   |   |       dimens.xml
        |   |   |   |   |       
        |   |   |   |   \---values-xxhdpi
        |   |   |   |           dimens.xml
        |   |   |   |           
        |   |   |   +---Services
        |   |   |   |       DroidFirebaseMessagingService.cs
        |   |   |   |       MyFirebaseIIDService.cs
        |   |   |   |       
        |   |   |   \---Views
        |   |   |       |   AboutView.cs
        |   |   |       |   CreatePostView.cs
        |   |   |       |   LoginView.cs
        |   |   |       |   PhotoCollectionView.cs
        |   |   |       |   PostDetailView.cs
        |   |   |       |   PostImageView.cs
        |   |   |       |   RequestDetailView.cs
        |   |   |       |   RequestsView.cs
        |   |   |       |   ResponseView.cs
        |   |   |       |   UserProfileView.cs
        |   |   |       |   
        |   |   |       +---Base
        |   |   |       |       BaseActivity.cs
        |   |   |       |       BaseFragment.cs
        |   |   |       |       BaseMyRequestDetailView.cs
        |   |   |       |       DetailView.cs
        |   |   |       |       DetailView.cs~Stashed changes
        |   |   |       |       DetailView.cs~Updated upstream
        |   |   |       |       MasterView.cs
        |   |   |       |       
        |   |   |       +---Popup
        |   |   |       |   |   LoadingOverlayView.cs
        |   |   |       |   |   PopupCreateRequestView.cs
        |   |   |       |   |   PopupExtensionOptionView.cs
        |   |   |       |   |   PopupListView.cs
        |   |   |       |   |   PopupMessageView.cs
        |   |   |       |   |   PopupNotificationView.cs
        |   |   |       |   |   PopupResponseView.cs
        |   |   |       |   |   PopupWarningResponseView.cs
        |   |   |       |   |   PopupWarningView.cs
        |   |   |       |   |   
        |   |   |       |   \---MyRequest
        |   |   |       |           MyRequestApprovedView.cs
        |   |   |       |           MyRequestPendingView.cs
        |   |   |       |           MyRequestReceivedView.cs
        |   |   |       |           
        |   |   |       \---TabNavigation
        |   |   |               ConversationView.cs
        |   |   |               HomeView.cs
        |   |   |               NotificationView.cs
        |   |   |               ProfileView.cs
        |   |   |               ScrollListener.cs
        |   |   |               TabNavigationView.cs
        |   |   |               
        |   |   \---GiveAndTake.iOS
        |   |       |   AppDelegate.cs
        |   |       |   Entitlements.plist
        |   |       |   GiveAndTake.iOS.csproj
        |   |       |   Info.plist
        |   |       |   LaunchScreen.storyboard
        |   |       |   LinkerPleaseInclude.cs
        |   |       |   Main.cs
        |   |       |   packages.config
        |   |       |   Setup.cs
        |   |       |   
        |   |       +---Assets.xcassets
        |   |       |   \---AppIcon.appiconset
        |   |       |           Contents.json
        |   |       |           Icon-App-20x20@1x[1].png
        |   |       |           Icon-App-20x20@2x[2].png
        |   |       |           Icon-App-20x20@2x[2]1.png
        |   |       |           Icon-App-20x20@3x[2].png
        |   |       |           Icon-App-29x29@1x[1].png
        |   |       |           Icon-App-29x29@2x[1].png
        |   |       |           Icon-App-29x29@2x[2].png
        |   |       |           Icon-App-29x29@3x[1].png
        |   |       |           Icon-App-40x40@1x[1].png
        |   |       |           Icon-App-40x40@2x[1].png
        |   |       |           Icon-App-40x40@2x[1]1.png
        |   |       |           Icon-App-40x40@3x[1].png
        |   |       |           Icon-App-60x60@2x[2].png
        |   |       |           Icon-App-60x60@3x[1].png
        |   |       |           Icon-App-76x76@1x[1].png
        |   |       |           Icon-App-76x76@2x[1].png
        |   |       |           Icon-App-83.5x83.5@2x[1].png
        |   |       |           ItunesArtwork@2x[1].png
        |   |       |           
        |   |       +---Controls
        |   |       |       AlphaUiButton.cs
        |   |       |       CustomMvxCachedImageView.cs
        |   |       |       CustomMvxImageView.cs
        |   |       |       CustomTextField.cs
        |   |       |       CustomTextView.cs
        |   |       |       CustomUIButton.cs
        |   |       |       CustomUIImageView.cs
        |   |       |       CustomUILabel.cs
        |   |       |       LoadingIndicator.cs
        |   |       |       PlaceholderTextView.cs
        |   |       |       PopupItemLabel.cs
        |   |       |       
        |   |       +---Converter
        |   |       |       InvertBoolValueConverter.cs
        |   |       |       
        |   |       +---Converters
        |   |       |       BoolToViewStatesValueConverter.cs
        |   |       |       StringToAttributedTextConverter.cs
        |   |       |       StringToUIColorValueConverter.cs
        |   |       |       
        |   |       +---CustomControls
        |   |       |       HeaderBar.cs
        |   |       |       
        |   |       +---CustomTextField.xcassets
        |   |       |   +---AppIcons.appiconset
        |   |       |   |       Contents.json
        |   |       |   |       
        |   |       |   \---LaunchImages.launchimage
        |   |       |           Contents.json
        |   |       |           
        |   |       +---Helpers
        |   |       |       ColorHelper.cs
        |   |       |       Constants.cs
        |   |       |       DeviceInfo.cs
        |   |       |       DimensionHelper.cs
        |   |       |       ImageHelper.cs
        |   |       |       Keys.cs
        |   |       |       MediaHelper.cs
        |   |       |       ResolutionHelper.cs
        |   |       |       SystemHelper.cs
        |   |       |       UIHelper.cs
        |   |       |       UrlHelper.cs
        |   |       |       
        |   |       +---Libraries
        |   |       |       ELCImagePicker.dll
        |   |       |       
        |   |       +---Properties
        |   |       |       AssemblyInfo.cs
        |   |       |       
        |   |       +---Resources
        |   |       |   |   LaunchScreen.xib
        |   |       |   |   
        |   |       |   +---Fonts
        |   |       |   |       SanFranciscoDisplay-Bold.otf
        |   |       |   |       SanFranciscoDisplay-Light.otf
        |   |       |   |       SanFranciscoDisplay-Medium.otf
        |   |       |   |       SanFranciscoDisplay-Regular.otf
        |   |       |   |       
        |   |       |   \---Images
        |   |       |           avt_off.png
        |   |       |           avt_off@2x.png
        |   |       |           avt_on.png
        |   |       |           avt_on@2x.png
        |   |       |           back_button.png
        |   |       |           btn_delete_photo.png
        |   |       |           btn_delete_photo@2x.png
        |   |       |           btn_delete_request_detail.png
        |   |       |           category_button_default.png
        |   |       |           category_button_selected.png
        |   |       |           chat.png
        |   |       |           comment.png
        |   |       |           contact_phone.png
        |   |       |           conversation_off.png
        |   |       |           conversation_off@2x.png
        |   |       |           conversation_on.png
        |   |       |           conversation_on@2x.png
        |   |       |           default_avatar.png
        |   |       |           default_post.png
        |   |       |           extension.png
        |   |       |           facebook_button.png
        |   |       |           gallery_button.png
        |   |       |           google_button.png
        |   |       |           header_bar.png
        |   |       |           heart_off.png
        |   |       |           home_off.png
        |   |       |           home_off@2x.png
        |   |       |           home_on.png
        |   |       |           home_on@2x.png
        |   |       |           iOS_BackButton.png
        |   |       |           location_button_default.png
        |   |       |           location_button_selected.png
        |   |       |           location_logo.png
        |   |       |           LoginLogo.png
        |   |       |           login_background.png
        |   |       |           login_logo.png
        |   |       |           logoApp_withText.png
        |   |       |           logoSioux.png
        |   |       |           Multiphoto.png
        |   |       |           my_avatar.png
        |   |       |           navigateLeft.png
        |   |       |           navigateRight.png
        |   |       |           new_post.png
        |   |       |           notification_off.png
        |   |       |           notification_off@2x.png
        |   |       |           notification_on.png
        |   |       |           notification_on@2x.png
        |   |       |           request_off.png
        |   |       |           request_on.png
        |   |       |           setting.png
        |   |       |           sort_button_default.png
        |   |       |           sort_button_selected.png
        |   |       |           splash_screen.png
        |   |       |           takePicture_button.png
        |   |       |           Top_logo.png
        |   |       |           
        |   |       \---Views
        |   |           |   AboutView.cs
        |   |           |   CreatePostView.cs
        |   |           |   LoginView.cs
        |   |           |   PhotoCollectionView.cs
        |   |           |   PostDetailView.cs
        |   |           |   PostImageView.cs
        |   |           |   RequestDetailView.cs
        |   |           |   RequestsView.cs
        |   |           |   ResponseView.cs
        |   |           |   SlideViewDataSource.cs
        |   |           |   SlideViewDelegate.cs
        |   |           |   UserProfileView.cs
        |   |           |   
        |   |           +---Base
        |   |           |       BaseMyRequestDetailView.cs
        |   |           |       BaseView.cs
        |   |           |       DetailView.cs
        |   |           |       MasterView.cs
        |   |           |       
        |   |           +---CollectionViewCells
        |   |           |       PhotoItemViewCell.cs
        |   |           |       
        |   |           +---CollectionViewSources
        |   |           |       PhotoItemViewSource.cs
        |   |           |       
        |   |           +---Popups
        |   |           |   |   LoadingOverlayView.cs
        |   |           |   |   PopupCreateRequestView.cs
        |   |           |   |   PopupExtensionOptionView.cs
        |   |           |   |   PopupListView.cs
        |   |           |   |   PopupMessageView.cs
        |   |           |   |   PopupResponseView.cs
        |   |           |   |   PopupView.cs
        |   |           |   |   PopupWarningResponseView.cs
        |   |           |   |   PopupWarningView.cs
        |   |           |   |   
        |   |           |   \---MyRequest
        |   |           |           MyRequestApprovedView.cs
        |   |           |           MyRequestPendingView.cs
        |   |           |           MyRequestReceivedView.cs
        |   |           |           
        |   |           +---TableViewCells
        |   |           |       MyPostItemViewCell.cs
        |   |           |       NotificationItemTableViewCell.cs
        |   |           |       PopupItemViewCell.cs
        |   |           |       PostItemViewCell.cs
        |   |           |       RequestItemViewCell.cs
        |   |           |       
        |   |           +---TableViewSources
        |   |           |       NotificationItemTableViewSource.cs
        |   |           |       PopupItemTableViewSource.cs
        |   |           |       PostItemTableViewSource.cs
        |   |           |       RequestItemTableViewSource.cs
        |   |           |       
        |   |           \---TabNavigation
        |   |                   ConversationView.cs
        |   |                   HomeView.cs
        |   |                   NotificationView.cs
        |   |                   ProfileView.cs
        |   |                   TabNavigationView.cs
        |   |                   
        |   \---Trunk
        |       +---deployment
        |       |   +---deployment
        |       |   |   +---env
        |       |   |   |   +---dev
        |       |   |   |   |   |   docker-compose.yml
        |       |   |   |   |   |   publish_api.sh
        |       |   |   |   |   |   publish_cms.sh
        |       |   |   |   |   |   README.md
        |       |   |   |   |   |   
        |       |   |   |   |   +---key
        |       |   |   |   |   |       id_rsa
        |       |   |   |   |   |       
        |       |   |   |   |   +---output
        |       |   |   |   |   |   +---api
        |       |   |   |   |   |   |   |   appsettings.json
        |       |   |   |   |   |   |   |   Dockerfile
        |       |   |   |   |   |   |   |   
        |       |   |   |   |   |   |   \---app
        |       |   |   |   |   |   \---cms
        |       |   |   |   |   \---volumes
        |       |   |   |   |       +---config
        |       |   |   |   |       |   \---cms
        |       |   |   |   |       |           config.js
        |       |   |   |   |       |           
        |       |   |   |   |       +---nginx-cms
        |       |   |   |   |       |   \---conf.d
        |       |   |   |   |       |           giveandtake-cms.conf
        |       |   |   |   |       |           
        |       |   |   |   |       \---proxy
        |       |   |   |   |           \---templates
        |       |   |   |   |                   nginx.tmpl
        |       |   |   |   |                   
        |       |   |   |   \---pro
        |       |   |   |       |   .gitignore
        |       |   |   |       |   docker-compose.yml
        |       |   |   |       |   publish_api.sh
        |       |   |   |       |   publish_cms.sh
        |       |   |   |       |   README.md
        |       |   |   |       |   
        |       |   |   |       +---key
        |       |   |   |       |       id_rsa
        |       |   |   |       |       
        |       |   |   |       +---output
        |       |   |   |       |   +---api
        |       |   |   |       |   |   |   appsettings.json
        |       |   |   |       |   |   |   Dockerfile
        |       |   |   |       |   |   |   
        |       |   |   |       |   |   \---app
        |       |   |   |       |   \---cms
        |       |   |   |       \---volumes
        |       |   |   |           |   publish_cms.sh
        |       |   |   |           |   
        |       |   |   |           +---config
        |       |   |   |           |   \---cms
        |       |   |   |           |           config.js
        |       |   |   |           |           
        |       |   |   |           +---nginx-cms
        |       |   |   |           |   \---conf.d
        |       |   |   |           |           giveandtake-cms.conf
        |       |   |   |           |           
        |       |   |   |           \---proxy
        |       |   |   |               \---templates
        |       |   |   |                       nginx.tmpl
        |       |   |   |                       
        |       |   |   +---mysql
        |       |   |   |   |   docker-compose.yml
        |       |   |   |   |   
        |       |   |   |   +---conf
        |       |   |   |   |       custom.cnf
        |       |   |   |   |       
        |       |   |   |   \---data
        |       |   |   \---webserver-configuration
        |       |   |       +---proxy-test
        |       |   |       |       docker-compose.yml
        |       |   |       |       
        |       |   |       \---traefik
        |       |   |               acme.json
        |       |   |               docker-compose.yml
        |       |   |               traefik.toml
        |       |   |               
        |       |   \---env
        |       |       +---development
        |       |       |   \---config
        |       |       |       +---api
        |       |       |       |       appsettings.json
        |       |       |       |       
        |       |       |       \---cms
        |       |       |               conf.json
        |       |       |               
        |       |       \---production
        |       |           \---config
        |       |               +---api
        |       |               |       appsettings.json
        |       |               |       
        |       |               \---cms
        |       |                       conf.json
        |       |                       
        |       +---Giveaway.API
        |       |   \---Giveaway
        |       |       +---API
        |       |       |   +---Giveaway.API
        |       |       |   \---Giveaway.API.Shared
        |       |       +---Giveaway.Data
        |       |       |   |   Giveaway.Data.csproj
        |       |       |   |   
        |       |       |   +---Enums
        |       |       |   \---Models
        |       |       +---Giveaway.Data.EF
        |       |       +---Giveaway.Service
        |       |       +---Giveaway.Util
        |       |       \---Test
        |       \---Giveaway.CMS
        \---GiveAndTakeLandingPage
            +---.well-known
            |       assetlinks.json
            |       
            +---SourceCode
            \---wordpress-docker-compose
                |   .env
                |   docker-compose.yml
                |   export.sh
                |   LICENSE
                |   README.md
                |   
                \---config
                    |   .htaccess
                    |   php.conf.ini
                    |   
                    \---apache2
                        \---sites-enabled
                                000-default.conf
                                
