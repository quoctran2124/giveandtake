import request from '../utils/request';

const ROOT_PATH = '/comment';

export async function fetchCommentList(params) {
  const { id } = params;
  const options = {
    method: 'GET',
    body: {
      limit: 1000,
    },
  };
  return request(`${ROOT_PATH}/list/${id}`, options);
}
