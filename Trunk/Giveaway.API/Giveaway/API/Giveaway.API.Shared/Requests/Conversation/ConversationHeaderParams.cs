﻿using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Requests.Conversation
{
	[DataContract]
	public class ConversationHeaderParams : BasePagingQueryRequest
	{
		[DataMember(Name = "status", EmitDefaultValue = false)]
		public string Status { get; set; }
	}
}