﻿using System;
using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Requests.Report
{
	[DataContract]
	public class CreateReportRequest
	{
		[DataMember(Name = "postId")]
		public Guid PostId { get; set; }

		[DataMember(Name = "userId")]
		public Guid UserId { get; set; }

		[DataMember(Name = "message")]
		public string Message { get; set; }
	}
}
