﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Giveaway.API.Shared.Requests.Comment
{
	public class CommentPostRequest
	{
		[DataMember(Name = "commentMessage")]
		public string CommentMessage { get; set; }

		[DataMember(Name = "userId")]
		public Guid UserId { get; set; }

		[DataMember(Name = "postId")]
		public Guid PostId { get; set; }
	}
}
