﻿using System;
using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Requests.Message
{
	[DataContract]
	public class MessageRequest : BasePagingQueryRequest
	{
		[DataMember(Name = "conversationId")]
		public Guid? ConversationId { get; set; }
	}
}