﻿using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Requests
{
	[DataContract]
	public class VersionRequest
	{
		[DataMember(Name = "version")]
		public int? Version { get; set; }
	}
}