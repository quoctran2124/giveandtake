﻿using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Requests.User
{
    [DataContract]
    public class CreateUserProfileRequest 
    {
        [DataMember(Name = "phoneNumber")]
        public string UserName { get; set; }

        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }
    }
}