﻿using System;
using Giveaway.Data.Models.Database;

namespace Giveaway.API.Shared.Extensions
{
	public static class ConversationExt
	{
		public static Conversation OfUser(this Conversation conversation, Guid? userId)
		{
			conversation.UserId = userId;
			return conversation;
		}

		public static UserConversation ToIsSeen(this UserConversation userConversation)
		{
			userConversation.IsSeen = true;
			return userConversation;
		}

		public static UserConversation ChangeActiveStatus(this UserConversation userConversation, bool isActive)
		{
			userConversation.IsActive = isActive;
			if (!isActive) 
			{
				userConversation.LastActiveTime = DateTimeOffset.UtcNow;
			}
			return userConversation;
		}
	}
}