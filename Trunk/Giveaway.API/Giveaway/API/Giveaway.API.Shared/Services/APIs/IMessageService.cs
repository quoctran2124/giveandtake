﻿using System;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Conversation;
using System.Collections.Generic;

namespace Giveaway.API.Shared.Services.APIs
{
	public interface IMessageService
	{
		PagingQueryResponse<MessageResponse> FetchMessages(IDictionary<string, string> @params, Guid currentUserId);
	}
}