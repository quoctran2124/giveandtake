﻿using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Report;
using System.Collections.Generic;
using Giveaway.API.Shared.Requests.Report;

namespace Giveaway.API.Shared.Services.APIs
{
	public interface IReportService
    {
        PagingQueryResponse<ReportCmsResponse> GetReporttForPaging(IDictionary<string, string> @params);
	    ReportAppResponse Create(CreateReportRequest reportRequest);
    }
}
