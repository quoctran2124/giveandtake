﻿using System;
using System.Collections.Generic;
using System.Text;
using Giveaway.API.Shared.Requests.Comment;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Comment;
using Giveaway.Data.Models.Database;

namespace Giveaway.API.Shared.Services.APIs
{
	public interface ICommentService
	{
		CommentPostResponse Create(CommentPostRequest requestPost);
		PagingQueryResponse<CommentPostResponse> GetRequestForPaging(string postId, IDictionary<string, string> @params);
		CommentPostResponse Update(string commentId,CommentPostRequest comment, Guid userId);
		bool Delete(string commentId, Guid userId);
	}
}
