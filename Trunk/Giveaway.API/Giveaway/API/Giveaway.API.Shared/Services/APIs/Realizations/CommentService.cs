﻿using AutoMapper;
using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Requests;
using Giveaway.API.Shared.Requests.Comment;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Comment;
using Giveaway.Data.EF.Exceptions;
using Giveaway.Data.Enums;
using Giveaway.Data.Models.Database;
using Giveaway.Util.Constants;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Giveaway.Data.EF.Extensions;
using DbService = Giveaway.Service.Services;

namespace Giveaway.API.Shared.Services.APIs.Realizations
{
	public class CommentService : ICommentService
	{
		private readonly DbService.ICommentService _commentService;
		private readonly INotificationService _notificationService;
		public CommentService(DbService.ICommentService commentService, INotificationService notificationService)
		{
			_commentService = commentService;
			_notificationService = notificationService;
		}

		public CommentPostResponse Create(CommentPostRequest commentPost)
		{
			var comment = Mapper.Map<Comment>(commentPost);
			_commentService.Create(comment, out var isSaved);

			if (isSaved == false)
			{
				throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);
			}

			comment = _commentService.Include(x => x.User).Include(x => x.Post).Find(comment.Id);

			// Send a notification to the user who requested and also save it to db
			_notificationService.Create(new Notification()
			{
				UserDisplayName = comment.User.DisplayName,
				TemptType = TemptNotificationType.Comment,
				Type = NotificationType.Comment,
				RelevantId = comment.PostId,
				SourceUserId = comment.UserId,
				DestinationUserId = comment.Post.UserId
			});

			return Mapper.Map<CommentPostResponse>(comment);
		}

		public PagingQueryResponse<CommentPostResponse> GetRequestForPaging(string postId, IDictionary<string, string> @params)
		{
			var request = @params.ToObject<BasePagingQueryRequest>();
			var comments = GetPagedRequests(postId, request, out var total);
			return new PagingQueryResponse<CommentPostResponse>
			{
				Data = comments,
				PageInformation = new PageInformation
				{
					Total = total,
					Page = request.Page,
					Limit = request.Limit
				}
			};
		}

		public CommentPostResponse Update(string commentId, CommentPostRequest commentApp, Guid userId)
		{
			var commentGuid = Guid.Parse(commentId);
			var comment = _commentService.FirstOrDefault(x => x.Id == commentGuid && x.EntityStatus == EntityStatus.Activated);
			if (comment.UserId == userId)
			{
				comment.CommentMessage = commentApp.CommentMessage;
				_commentService.Update(comment);
			}			
			return Mapper.Map<CommentPostResponse>(comment);
		}

		public bool Delete(string commentId, Guid userId)
		{
			var commentGuid = Guid.Parse(commentId);
			var comment = _commentService.Include(x => x.Post).FirstOrDefault(x => x.Id == commentGuid && x.EntityStatus == EntityStatus.Activated);
			if (comment == null)
			{
				throw new DataNotFoundException("Comment not founded");
			}

			if (comment.UserId == userId || comment.Post.UserId == userId)
			{
				comment.EntityStatus = EntityStatus.Deleted;
				var isSaved = _commentService.Update(comment);
				if (isSaved)
				{
					return true;
				}

				throw new InternalServerErrorException("Could not delete the comment");
			}

			throw new BadRequestException("Not Authorized");
		}

		private List<CommentPostResponse> GetPagedRequests(string postId, BasePagingQueryRequest request, out int total)
		{
			var comments = _commentService.Include(x => x.Post).Include(x => x.User)
				.Where(x => x.EntityStatus == EntityStatus.Activated);
			if (comments == null)
			{
				throw new BadRequestException(CommonConstant.Error.NotFound);
			}
			// Get list by postId
			if (!string.IsNullOrEmpty(postId))
			{
				try
				{
					Guid id = Guid.Parse(postId);
					comments = comments.Where(x => x.PostId == id);
				}
				catch
				{
					throw new BadRequestException(CommonConstant.Error.InvalidInput);
				}
			}

			comments = comments.OrderByDescending(x => x.CreatedTime);
			total = comments.Count();

			return comments
				.Skip(request.Limit * (request.Page - 1))
				.Take(request.Limit)
				.Select(x => Mapper.Map<CommentPostResponse>(x))
				.ToList();
		}
	}
}
