using AutoMapper;
using Giveaway.API.Shared.Constants;
using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Helpers;
using Giveaway.API.Shared.Models;
using Giveaway.API.Shared.Models.DTO;
using Giveaway.API.Shared.Requests;
using Giveaway.API.Shared.Requests.Post;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Post;
using Giveaway.Data.EF.Exceptions;
using Giveaway.Data.Enums;
using Giveaway.Data.Models.Database;
using Giveaway.Util.Constants;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Giveaway.Data.EF.Extensions;
using Giveaway.Data.Models;
using DbService = Giveaway.Service.Services;
namespace Giveaway.API.Shared.Services.APIs.Realizations
{
	public class PostService<T> : IPostService<T> where T : PostBaseResponse
	{
		private readonly DbService.IPostService _postService;
		private readonly DbService.IImageService _imageService;
		private readonly DbService.IRequestService _requestService;
		private readonly INotificationService _notificationService;
		private readonly DbService.IUserService _userService;
		private readonly DbService.ICategoryService _categoryService;
		private readonly DbService.IReportService _reportService;
		private readonly DbService.IBlockedUserService _blockService;
		private readonly DbService.IAppreciationService _appreciationService;
		public PostService(DbService.IPostService postService, DbService.IImageService imageService,
			DbService.IRequestService requestService, INotificationService notificationService,
			DbService.IUserService userService, DbService.ICategoryService categoryService,
			DbService.IReportService reportService, DbService.IBlockedUserService blockService, 
			DbService.IAppreciationService appreciationService)
		{
			_postService = postService;
			_imageService = imageService;
			_requestService = requestService;
			_notificationService = notificationService;
			_userService = userService;
			_categoryService = categoryService;
			_reportService = reportService;
			_blockService = blockService;
			_appreciationService = appreciationService;
		}

		public PagingQueryResponse<T> GetPostForPaging(IDictionary<string, string> @params, string userId, string currentUserId, bool isListOfSingleUser)
		{
			var request = @params.ToObject<PagingQueryPostRequest>();

			int total;
			var posts = isListOfSingleUser ? GetPagedPosts(userId, request, out total, currentUserId) : GetPagedPosts(null, request, out total, currentUserId);

			CheckIfCurrentUserRequested(currentUserId, posts);

			return new PagingQueryResponse<T>
			{
				Data = posts,
				PageInformation = new PageInformation
				{
					Total = total,
					Page = request.Page,
					Limit = request.Limit
				}
			};
		}

		public PagingQueryResponse<PostAppResponse> GetListRequestedPostOfUser(IDictionary<string, string> @params, string userId)
		{
			var request = @params.ToObject<PagingQueryPostRequest>();

			if (Guid.TryParse(userId, out var id))
			{
				var user = _userService.Include(x => x.BlockedUsers).Find(id);
				var posts = _postService.Include(x => x.Category)
					.Include(x => x.Images)
					.Include(x => x.ProvinceCity)
					.Include(x => x.User)
					.Include(x => x.Comments)
					.Include(x => x.Requests)
					.Include(x => x.User.BlockedUsers)
					.Where(x => x.Category.EntityStatus == EntityStatus.Activated &&
								x.Requests.Any(y => y.EntityStatus == EntityStatus.Activated && y.UserId == id && y.RequestStatus != RequestStatus.Rejected) &&
								x.Reports.All(r => !r.UserId.Equals(id)) &&
					            x.User.BlockedUsers.All(u => !u.BanId.Equals(id)) &&
					            user.BlockedUsers.All(u => !u.BanId.Equals(x.UserId)));


				//just get requests that have not deleted yet
				var postList = posts.ToList();
				foreach (var post in postList)
				{
					post.Requests = post.Requests.Where(x => x.EntityStatus == EntityStatus.Activated && x.RequestStatus != RequestStatus.Rejected).ToList();
				}

				var result = postList.Select(x => GenerateRequestedPostResponse(x, id));

				result = SortPostsByRequestStatus(result);

				result = result.Skip(request.Limit * (request.Page - 1))
					.Take(request.Limit).AsEnumerable();

				return new PagingQueryResponse<PostAppResponse>
				{
					Data = result.ToList(),
					PageInformation = new PageInformation
					{
						Total = posts.Count(),
						Page = request.Page,
						Limit = request.Limit
					}
				};
			}

			throw new BadRequestException(CommonConstant.Error.InvalidInput);
		}

		private static IEnumerable<PostAppResponse> SortPostsByRequestStatus(IEnumerable<PostAppResponse> result)
		{
			Dictionary<string, int> mapping = new Dictionary<string, int>()
				{
					{ RequestStatus.Pending.ToString(), 1 },
					{ RequestStatus.Approved.ToString(), 2 },
					{ RequestStatus.Received.ToString(), 3 }
				};
			result = result.OrderBy(x => mapping[x.RequestedPostStatus]).ThenByDescending(x => x.CreatedTime);
			return result;
		}

		public T GetDetail(Guid postId, string userId)
		{
            try
            {
                var post = _postService.Include(x => x.Category).Include(y => y.Images).Include(z => z.ProvinceCity)
                    .Include(x => x.User).Include(x => x.Requests).Include(x => x.Comments).Include(x => x.Appreciations)
					.FirstOrDefault(x => x.Id == postId && x.EntityStatus != EntityStatus.Deleted);


				//just get requests that have not deleted yet
				post.Requests = post.Requests.Where(x => x.EntityStatus == EntityStatus.Activated && x.RequestStatus != RequestStatus.Rejected).ToList();

				var postResponse = Mapper.Map<T>(post);

				if (typeof(T) == typeof(PostAppResponse) && !string.IsNullOrEmpty(userId))
				{
					if (Guid.TryParse(userId, out var id))
					{
						var postAppResponse = postResponse as PostAppResponse;

						var request = _requestService.FirstOrDefault(x =>
							x.EntityStatus == EntityStatus.Activated && x.RequestStatus != RequestStatus.Rejected && x.PostId == post.Id && x.UserId == id);
						var report = _reportService.Include(x => x.Post)
							.FirstOrDefault(x => x.EntityStatus != EntityStatus.Deleted && x.PostId == post.Id && x.UserId == id);
						if (request != null)
						{
							postAppResponse.RequestedPostStatus = request.RequestStatus.ToString();
							postAppResponse.IsCurrentUserRequested = true;
						}
						if (report != null)
						{
							postAppResponse.IsCurrentUserReported = true;
						}

						postAppResponse.IsCurrentUserAppreciated = post.Appreciations.Any(p => p.UserId.Equals(id));

						return postAppResponse as T;
					}

					throw new BadRequestException(CommonConstant.Error.InvalidInput);
				}

				return postResponse;
			}
			catch
			{
				throw new BadRequestException(CommonConstant.Error.NotFound);
			}
		}

		public PostAppResponse Create(PostRequest postRequest)
		{
			postRequest.Id = Guid.NewGuid();
			var post = Mapper.Map<Post>(postRequest);
			post.Images = null;

			_postService.Create(post, out var isPostSaved);

			if (isPostSaved)
			{
				if (postRequest.Images.Count != 0) CreateImage(postRequest);

				var postDb = _postService.Include(x => x.Category).Include(y => y.Images).Include(z => z.ProvinceCity).FirstAsync(x => x.Id == post.Id).Result;
				var postResponse = Mapper.Map<PostAppResponse>(postDb);

				return postResponse;
			}

			throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);
		}

		public PostAppResponse Update(Guid id, PostRequest postRequest)
		{
			var post = _postService.Include(x => x.Images).FirstOrDefault(x => x.Id == id);
			if (post == null)
			{
				throw new BadRequestException(CommonConstant.Error.NotFound);
			}

			try
			{
				List<Image> oldImages = post.Images.ToList();
				Mapper.Map(postRequest, post);
				bool updated = _postService.Update(post);

				if (updated)
				{
					UpdateImages(postRequest, oldImages);
				}
				else
				{
					throw new InternalServerErrorException("Internal Error");
				}

				var postDb = _postService.Include(x => x.Category).Include(y => y.Images).Include(z => z.ProvinceCity).FirstAsync(x => x.Id == post.Id).Result;
				var postResponse = Mapper.Map<PostAppResponse>(postDb);

				return postResponse;
			}
			catch
			{
				throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);
			}
		}

		public ResponseMessage AppreciatePost(Guid userId, Guid postId)
		{
			var appreciations = _appreciationService.Where(x => x.UserId.Equals(userId) && x.PostId.Equals(postId));
			if (appreciations.Any())
			{
				var isDeleted = _appreciationService.DeletePermanent(appreciations.First());
				return isDeleted
					? new ResponseMessage(HttpStatusCode.OK, data: "UnAppreciated Successfully")
					: new ResponseMessage(HttpStatusCode.BadRequest, CommonConstant.Error.NotFound);
			}

			var appreciation = _appreciationService.Create(new Appreciation { UserId = userId, PostId = postId }, out var isSaved);
			appreciation = _appreciationService.Include(x => x.User).Include(x => x.Post).Find(appreciation.Id);

			// Send a notification to an user who requested and also save it to db
			_notificationService.Create(new Notification()
			{
				UserDisplayName = appreciation.User.DisplayName,
				TemptType = TemptNotificationType.Appreciation,
				Type = NotificationType.Like,
				RelevantId = postId,
				SourceUserId = userId,
				DestinationUserId = appreciation.Post.UserId
			});

			return isSaved
				? new ResponseMessage(HttpStatusCode.OK, data: "Appreciated Successfully")
				: new ResponseMessage(HttpStatusCode.BadRequest, CommonConstant.Error.BadRequest);
		}

		public bool ChangePostStatus(Guid postId, Guid userId, StatusRequest request)
		{
			var post = _postService.Include(x => x.Requests).FirstOrDefault(x => x.Id == postId);
			bool updated = false;

			if (post == null)
			{
				throw new BadRequestException(CommonConstant.Error.NotFound);
			}

			var isStatus = Enum.TryParse(request.UserStatus, out PostStatus postStatus);
			if (isStatus)
			{
				post.PostStatus = postStatus;
				updated = _postService.Update(post);
				if (updated && postStatus == PostStatus.Gave)
				{
					RejectRequests(postId, userId);
				}
			}
			else
			{
				updated = _postService.UpdateStatus(postId, request.UserStatus) != null;
				if (updated)
				{
					RejectRequests(postId, userId);
				}
			}

			if (updated == false)
				throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);

			return updated;
		}

		#region Utils

		private PostAppResponse GenerateRequestedPostResponse(Post post, Guid userId)
		{
			var requestedPost = Mapper.Map<PostAppResponse>(post);
			if (requestedPost != null)
			{
				requestedPost.IsCurrentUserRequested = true;
				var request = post.Requests.FirstOrDefault(x => x.UserId == userId && x.EntityStatus == EntityStatus.Activated);

				requestedPost.RequestedPostStatus = request?.RequestStatus.ToString();
			}
			return requestedPost;
		}

		private void RejectRequests(Guid postId, Guid userId)
		{
			var requests = _requestService.Where(x =>
				x.PostId == postId && x.EntityStatus == EntityStatus.Activated && x.RequestStatus == RequestStatus.Pending).ToList();
			var user = _userService.Find(userId);

			if (requests.Any())
			{
				foreach (var item in requests)
				{
					item.RequestStatus = RequestStatus.Rejected;
				}

				_requestService.UpdateMany(requests, out var isSaved);
				if (isSaved == false)
					throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);

				Task.Run(async () =>
				   {
					   foreach (var request in requests)
					   {
						   // Send a notification to an user who is rejected and also save it to db
						   _notificationService.Create(new Notification()
						   {
							   UserDisplayName = user.DisplayName,
							   TemptType = TemptNotificationType.RequestRejected,
							   Type = NotificationType.Request,
							   RelevantId = request.Id,
							   SourceUserId = userId,
							   DestinationUserId = request.UserId
						   });
					   }
				   });
			}
		}

		private void CheckIfCurrentUserRequested(string currentUserId, List<T> posts)
		{
			if (typeof(T) == typeof(PostAppResponse) && !string.IsNullOrEmpty(currentUserId))
			{
				if (Guid.TryParse(currentUserId, out var id))
				{
					foreach (PostAppResponse post in posts as List<PostAppResponse>)
					{
						var request = _requestService.FirstOrDefault(x =>
							x.EntityStatus == EntityStatus.Activated && x.RequestStatus != RequestStatus.Rejected && x.PostId == post.Id && x.UserId == id);
						var report = _reportService.Include(x => x.Post)
							.FirstOrDefault(x => x.EntityStatus != EntityStatus.Deleted && x.PostId == post.Id &&  x.UserId == id);
						if (request != null)
						{
							post.IsCurrentUserRequested = true;
							post.RequestedPostStatus = request.RequestStatus.ToString();
						}
						if (report != null)
						{
							post.IsCurrentUserReported = true;
						}
					}
				}
				else
				{
					throw new BadRequestException(CommonConstant.Error.InvalidInput);
				}
			}
		}

		private void CreateImage(PostRequest post)
		{
			var imageBase64Requests = InitImageBase64Requests(post);
			var imagesDTO = ConvertFromBase64(imageBase64Requests);
			var imageDBs = InitListImageDB(post.Id, imagesDTO);

			_imageService.CreateMany(imageDBs, out var isImageSaved);

			if (!isImageSaved)
			{
				throw new InternalServerErrorException("Internal Error");
			}
		}

		private void UpdateImages(PostRequest postRequest, List<Image> oldImages)
		{
			// get retained images from request
			var retainedImages =
				postRequest.Images.Where(x => x.Image.Contains("https://") || x.Image.Contains("http://")).ToList();
			// get images needing to be deleted
			var deletedImages = oldImages.Except(
				// get retained images from database
				oldImages.Where(x => retainedImages.Any(y => y.Image == x.OriginalImage))
			).ToList();

			DeleteImages(deletedImages);

			postRequest.Images = postRequest.Images.Except(retainedImages).ToList();
			if (postRequest.Images.Count != 0) CreateImage(postRequest);
		}


		private void DeleteImages(List<Image> images)
		{
			foreach (var image in images)
			{
				_imageService.Delete(x => x.Id == image.Id, out var isSaved);

				if (isSaved == false)
					throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);
			}
		}

		private List<ImageBase64Request> InitImageBase64Requests(PostRequest post)
		{
			var requests = new List<ImageBase64Request>();
			foreach (var image in post.Images)
			{
				requests.Add(new ImageBase64Request()
				{
					Id = post.Id.ToString(),
					Type = "post",
					File = image.Image
				});
			}

			return requests;
		}

		private List<ImageDTO> ConvertFromBase64(List<ImageBase64Request> requests)
		{
			var images = new List<ImageDTO>();
			if (requests != null)
			{
				foreach (var request in requests)
				{
					var url = UploadImageHelper.PostBase64Image(request);
					if (url.Count > 1)
					{
						images.Add(new ImageDTO()
						{
							OriginalImage = url.ElementAt(0),
							ResizedImage = url.ElementAt(1),
						});
					}
				}

				return images;
			}

			return images;
		}

		private List<Image> InitListImageDB(Guid postId, List<ImageDTO> images)
		{
			var imageList = new List<Image>();
			foreach (var image in images)
			{
				imageList.Add(new Image()
				{
					Id = Guid.NewGuid(),
					PostId = postId,
					OriginalImage = image.OriginalImage,
					ResizedImage = image.ResizedImage
				});
			}

			return imageList;
		}

		private List<T> GetPagedPosts(string userId, PagingQueryPostRequest request, out int total, string currentUserId)
		{
			var posts = _postService.Include(x => x.Category)
				.Include(x => x.Images)
				.Include(x => x.ProvinceCity)
				.Include(x => x.User)
				.Include(x => x.User.BlockedUsers)
				.Include(x => x.Comments)
				.Include(x => x.Requests)
				.Include(x => x.Appreciations)
				.Where(x => x.Category.EntityStatus == EntityStatus.Activated);

			if (typeof(T) == typeof(PostAppResponse))
			{
				// Get post list for app
				posts = GetPostListForApp(userId, posts, currentUserId);
			}
			else
			{
				// Get post list for cms
				posts = posts.Where(x => x.EntityStatus != EntityStatus.Deleted);
			}

			//filter posts by properties
			posts = FilterPost(request, posts);
			posts = SortPosts(request, posts);

			//just get requests that have not deleted yet
			var postList = posts.ToList();
			foreach (var post in postList)
			{
				post.Requests = post.Requests.Where(x => x.EntityStatus == EntityStatus.Activated && x.RequestStatus != RequestStatus.Rejected).ToList();
				post.IsCurrentUserAppreciated = post.Appreciations.Any(p => p.UserId.ToString().Equals(currentUserId));
			}

			total = postList.Count();

			return postList
				.Skip(request.Limit * (request.Page - 1))
				.Take(request.Limit)
				.Select(Mapper.Map<T>)
				.ToList();
		}

		private IQueryable<Post> GetPostListForApp(string userId, IQueryable<Post> posts, string currentUserId)
		{
			posts = posts.Where(x => x.EntityStatus == EntityStatus.Activated);
			if (!string.IsNullOrEmpty(userId))
			{
				if (Guid.TryParse(userId, out var id))
				{
					if (userId.Equals(currentUserId))
					{
						// Get posts to show in my profile
						posts = posts.Where(x => x.UserId == id);
					}
					else
					{
						// Get posts to show in other profile
						posts = posts.Where(x => x.PostStatus == PostStatus.Giving && x.UserId == id);
					}
				}
				else
				{
					throw new BadRequestException(CommonConstant.Error.InvalidInput);
				}
			}
			else
			{
				if (Guid.TryParse(currentUserId, out var id))
				{
					//get only posts with giving status and not reported by the current user to show at the news feed
					var user = _userService.Include(x => x.BlockedUsers).Find(id);

					posts = posts.Where(x => x.PostStatus == PostStatus.Giving && 
					                         x.Reports.All(r => !r.UserId.Equals(id)) &&
											 x.User.BlockedUsers.All(u => !u.BanId.Equals(id)) &&
					                         user.BlockedUsers.All(u => !u.BanId.Equals(x.UserId)));
				}
				else
				{
					throw new BadRequestException(CommonConstant.Error.InvalidInput);
				}
			}
			return posts;
		}

		private IQueryable<Post> SortPosts(PagingQueryPostRequest request, IQueryable<Post> posts)
		{
			if (!string.IsNullOrEmpty(request.Order) && request.Order == AppConstant.ASC)
			{
				posts = posts.OrderBy(x => x.CreatedTime);
			}
			else
			{
				posts = posts.OrderByDescending(x => x.CreatedTime);
			}

			return posts;
		}

		private IQueryable<Post> FilterPost(PagingQueryPostRequest request, IQueryable<Post> posts)
		{
			if (!string.IsNullOrEmpty(request.Title))
			{
				posts = posts.Where(x => x.Title.Contains(request.Title));
			}
			if (!string.IsNullOrEmpty(request.ProvinceCityId))
			{
				posts = posts.Where(x => x.ProvinceCityId.Equals(Guid.Parse(request.ProvinceCityId)));
			}
			if (!string.IsNullOrEmpty(request.CategoryId))
			{
				if (!_categoryService.Find(Guid.Parse(request.CategoryId)).CategoryName.Contains("Tất cả"))
				{
					posts = posts.Where(x => x.CategoryId.Equals(Guid.Parse(request.CategoryId)));
				}
			}
			if (!string.IsNullOrEmpty(request.Keyword))
			{
				posts = posts.Where(x => x.Title.Contains(request.Keyword, StringComparison.OrdinalIgnoreCase) ||
										 x.Description.Contains(request.Keyword, StringComparison.OrdinalIgnoreCase) ||
										 x.Category.CategoryName.Contains(request.Keyword, StringComparison.OrdinalIgnoreCase) ||
										 x.User.DisplayName.Contains(request.Keyword, StringComparison.OrdinalIgnoreCase) ||
										 x.ProvinceCity.ProvinceCityName.Contains(request.Keyword, StringComparison.OrdinalIgnoreCase));
			}

			return posts;
		}

		#endregion
	}
}
