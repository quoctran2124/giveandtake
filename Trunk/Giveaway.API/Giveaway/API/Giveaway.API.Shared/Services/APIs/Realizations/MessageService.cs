﻿using AutoMapper;
using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Requests.Message;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Conversation;
using Giveaway.Data.EF.Exceptions;
using Giveaway.Service.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Giveaway.API.Shared.Services.APIs.Realizations
{
	public class MessageService : IMessageService
	{
		private readonly Service.Services.IMessageService _messageService;
		private readonly Service.Services.IUserConversationService _userConversationService;

		public MessageService(Service.Services.IMessageService messageService, IUserConversationService userConversationService)
		{
			_messageService = messageService;
			_userConversationService = userConversationService;
		}

		public PagingQueryResponse<MessageResponse> FetchMessages(IDictionary<string, string> @params,
			Guid currentUserId)
		{
			var request = @params.ToObject<MessageRequest>();

			if (request.ConversationId == null)
			{
				throw new BadRequestException("Missing conversationId");
			}

			var deletedDate = _userConversationService.Include(x => x.User)
				.FirstOrDefault(x => x.User.Id == currentUserId && x.ConversationId == request.ConversationId)
				?.DeletedDate;

			var messages = _messageService.Include(x => x.UserConversation).ThenInclude(x => x.User)
				.Where(x => x.UserConversation.ConversationId == request.ConversationId && x.CreatedTime > deletedDate);

			return new PagingQueryResponse<MessageResponse>
			{
				Data = messages.OrderByDescending(x => x.CreatedTime)
					.Skip(request.Limit * (request.Page - 1))
					.Take(request.Limit)
					.AsEnumerable()
					.Select(Mapper.Map<MessageResponse>),
				PageInformation = new PageInformation
				{
					Page = request.Page,
					Limit = request.Limit,
					Total = messages.Count()
				}
			};
		}
	}
}