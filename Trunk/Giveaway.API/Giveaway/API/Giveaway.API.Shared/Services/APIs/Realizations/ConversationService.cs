using AutoMapper;
using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Requests;
using Giveaway.API.Shared.Requests.Conversation;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Conversation;
using Giveaway.Data.EF.Exceptions;
using Giveaway.Data.EF.Helpers;
using Giveaway.Data.Enums;
using Giveaway.Data.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using DbService = Giveaway.Service.Services;

namespace Giveaway.API.Shared.Services.APIs.Realizations
{
	public class ConversationService : IConversationService
	{
		private readonly DbService.IConversationService _conversationService;
		private readonly DbService.IUserConversationService _userConversationService;


		public ConversationService(DbService.IConversationService conversationService,
			DbService.IUserConversationService userConversationService)
		{
			_conversationService = conversationService;
			_userConversationService = userConversationService;
		}

		public ConversationResponse Create(ConversationRequest conversationRequest)
		{
			var conversationId = _userConversationService.Include(x => x.User)
				.Where(x => x.User.Id == conversationRequest.UserId)
				.Select(x => x.ConversationId)
				.Intersect(_userConversationService.Include(x => x.User).Include(x => x.Conversation)
					.Where(x => x.User.Id == conversationRequest.FriendId)
					.Select(x => x.ConversationId))
				.FirstOrDefault();

			if (conversationId == null)
			{
				var conversation = _conversationService.Create(new Conversation(), out var isSaved);
				conversation.UserConversations = _userConversationService.CreateMany(new[]
				{
					new UserConversation
					{
						ConversationId = conversation.Id,
						UserId = conversationRequest.UserId
					},
					new UserConversation
					{
						ConversationId = conversation.Id,
						UserId = conversationRequest.FriendId
					}
				}, out isSaved).ToList();

				if (!isSaved)
				{
					throw new InternalServerErrorException("Cannot create conversation between the 2 users");
				}

				conversationId = conversation.Id;
			}

			return GetConversationDetail((Guid) conversationId, conversationRequest.UserId);
		}

		public ConversationResponse GetConversationDetail(Guid id, Guid currentUserId)
		{
			var conversation = _conversationService
				.Include(x => x.UserConversations).ThenInclude(x => x.User)
				.Include(x => x.UserConversations).ThenInclude(x => x.Messages)
				.Where(x => x.EntityStatus == EntityStatus.Activated)
				.FirstOrDefault(x => x.Id.Equals(id));

			if (conversation == null)
			{
				throw new BadRequestException("No conversation found!");
			}

			_userConversationService.Update(conversation.UserConversations
				.First(uc => uc.UserId == currentUserId)
				.ToIsSeen());

			return Mapper.Map<ConversationResponse>(conversation.OfUser(currentUserId));
		}

		public PagingQueryResponse<ConversationResponse> GetAllConversation(IDictionary<string, string> @params, Guid currentUserId)
		{
			var requestParams = @params.ToObject<ConversationHeaderParams>();

			var conversations = _conversationService
				.Include(x => x.UserConversations).ThenInclude(x => x.User)
				.Include(x => x.UserConversations).ThenInclude(x => x.Messages)
				.Where(x => x.EntityStatus == EntityStatus.Activated
				            && x.UserConversations.Any(uc => uc.UserId == currentUserId && uc.ConversationStatus == requestParams.Status.ToEnum(ConversationStatus.Allow))
				            && x.UserConversations.SelectMany(uc => uc.Messages).Any()
				            // filter with keyword
				            && (string.IsNullOrEmpty(requestParams.Keyword) || x.UserConversations.Any(uc =>
					                uc.User.DisplayName.ToUpper().Contains(requestParams.Keyword.ToUpper()) &&
					                uc.User.Id != currentUserId)));

			_userConversationService.UpdateMany(
				conversations.Select(c => c.UserConversations.First(uc => uc.UserId == currentUserId).ToIsSeen()),
				out _);

			var response =  conversations.Select(c => Mapper.Map<ConversationResponse>(c.OfUser(currentUserId)));

			return new PagingQueryResponse<ConversationResponse>
			{
				Data = response.ToList().Where(c => c.Messages.Any()).Skip(requestParams.Limit * (requestParams.Page - 1)).Take(requestParams.Limit),
				PageInformation = new PageInformation
				{
					Page = requestParams.Page,
					Limit = requestParams.Limit,
					Total = response.Count()
				}
			};
		}

		public bool UpdateStatus(Guid id, Guid currentUserId, StatusRequest status)
		{
			var userConversation = _userConversationService.Include(x => x.User)
				.First(uc => uc.EntityStatus == EntityStatus.Activated && uc.ConversationId == id && uc.User.Id == currentUserId);

			userConversation.ConversationStatus = status.UserStatus.ToEnum<ConversationStatus>();
			if (!_userConversationService.Update(userConversation))
			{
				throw new InternalServerErrorException($"Cannot update status for conversation id {id}");
			}

			return true;
		}

		public int GetConversationBadge(Guid currentUserId)
		{
			return _userConversationService.Include(x => x.User)
				.Count(uc => uc.EntityStatus == EntityStatus.Activated && uc.User.Id == currentUserId && !uc.IsSeen);
		}

		public bool DeleteConversation(Guid id, Guid currentUserId)
		{
			var userConversation = _userConversationService.Include(x => x.User).First(x =>
				x.EntityStatus == EntityStatus.Activated && x.ConversationId == id && x.User.Id == currentUserId);
			userConversation.DeletedDate = DateTimeOffset.Now;
			return _userConversationService.Update(userConversation);
		}
	}
}