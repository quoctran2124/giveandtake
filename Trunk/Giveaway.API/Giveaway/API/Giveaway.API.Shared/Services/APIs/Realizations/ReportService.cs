﻿using System;
using AutoMapper;
using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Requests.Report;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Report;
using Giveaway.Data.Enums;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Giveaway.Data.EF.Exceptions;
using Giveaway.Data.Models.Database;
using Giveaway.Util.Constants;

namespace Giveaway.API.Shared.Services.APIs.Realizations
{
    public class ReportService : IReportService
    {
        private readonly Service.Services.IReportService _reportService;

        public ReportService(Service.Services.IReportService reportService)
        {
            _reportService = reportService;
        }

        public PagingQueryResponse<ReportCmsResponse> GetReporttForPaging(IDictionary<string, string> @params)
        {
            var request = @params.ToObject<PagingQueryReportRequest>();
            var reports = GetPagedReports(request, out var total);
            return new PagingQueryResponse<ReportCmsResponse>
            {
                Data = reports,
                PageInformation = new PageInformation
                {
                    Total = total,
                    Page = request.Page,
                    Limit = request.Limit
                }
            };
        }

	    public ReportAppResponse Create(CreateReportRequest reportRequest)
	    {
		    var report = Mapper.Map<Report>(reportRequest);
			report.Id = Guid.NewGuid();

		    var reportDb = _reportService.Create(report, out var isSaved);

		    if (isSaved == false) throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);

			return Mapper.Map<ReportAppResponse>(reportDb);
	    }

	    #region Utils

        private List<ReportCmsResponse> GetPagedReports(PagingQueryReportRequest request, out int total)
        {
            var reports = _reportService.Include(x => x.Post).Include(x => x.User.WarningMessages)
	            .Where(x => x.EntityStatus != EntityStatus.Deleted)
	            .OrderByDescending(x => x.CreatedTime);

            total = reports.Count();

            return reports
                .Skip(request.Limit * (request.Page - 1))
                .Take(request.Limit)
                .Select(x => GererateReportCmsResponse(x))
                .ToList();
        }

	    private ReportCmsResponse GererateReportCmsResponse(Report report)
	    {
		    var reportResponse = Mapper.Map<ReportCmsResponse>(report);
		    reportResponse.WarningNumber = report.User.WarningMessages.Count;

		    return reportResponse;
	    }

		#endregion
	}
}
