﻿using Giveaway.API.Shared.Requests.Conversation;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Conversation;
using System;
using System.Collections.Generic;
using Giveaway.API.Shared.Requests;

namespace Giveaway.API.Shared.Services.APIs
{
	public interface IConversationService
	{
		ConversationResponse Create(ConversationRequest conversationRequest);
		ConversationResponse GetConversationDetail(Guid id, Guid currentUserId);
		PagingQueryResponse<ConversationResponse> GetAllConversation(IDictionary<string, string> @params, Guid currentUserId);
		bool UpdateStatus(Guid id, Guid currentUserId, StatusRequest status);
		int GetConversationBadge(Guid currentUserId);
		bool DeleteConversation(Guid id, Guid currentUserId);
	}
}