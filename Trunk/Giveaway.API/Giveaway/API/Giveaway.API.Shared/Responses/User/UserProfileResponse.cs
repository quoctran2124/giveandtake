using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Responses.User
{
	[DataContract]
	public class UserProfileResponse
	{
	    [DataMember(Name = "id")]
	    public Guid Id { get; set; }

		[DataMember(Name = "displayName")]
		public string DisplayName { get; set; }

	    [DataMember(Name="birthdate", EmitDefaultValue=false)]
	    [JsonProperty(PropertyName = "birthdate")]
	    public DateTime? BirthDate { get; set; }

	    [DataMember(Name="email", EmitDefaultValue=false)]
	    public string Email { get; set; }

	    [DataMember(Name= "avatarUrl", EmitDefaultValue=false)]
	    public string AvatarUrl { get; set; }

	    [DataMember(Name= "phoneNumber", EmitDefaultValue=false)]
	    public string PhoneNumber { get; set; }

	    [DataMember(Name="gender", EmitDefaultValue=false)]
		public string Gender { get; set; }

	    [DataMember(Name="username", EmitDefaultValue=false)]
		public string Username { get; set; }

	    [DataMember(Name="address", EmitDefaultValue=false)]
		public string Address { get; set; }

		[DataMember(Name="role", EmitDefaultValue=false)]
		public string[] Roles { get; set; }

	    [DataMember(Name = "status", EmitDefaultValue = false)]
        public string Status { get; set; }

		[DataMember(Name = "appreciationNumber")]
		public int AppreciationNumber { get; set; }

		[DataMember(Name = "memberType")]
		public string MemberType { get; set; }
	}
}
