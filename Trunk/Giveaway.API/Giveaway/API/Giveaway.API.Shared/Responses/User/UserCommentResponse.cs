﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Giveaway.API.Shared.Responses.User
{
	public class UserCommentResponse
	{
		[DataMember(Name = "id")]
		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[DataMember(Name = "displayName")]
		[JsonProperty(PropertyName = "displayName")]
		public string DisplayName { get; set; }

		[DataMember(Name = "avatarUrl")]
		[JsonProperty(PropertyName = "avatarUrl")]
		public string AvatarUrl { get; set; }

		[DataMember(Name = "memberType")]
		[JsonProperty(PropertyName = "memberType")]
		public string MemberType { get; set; }
	}
}
