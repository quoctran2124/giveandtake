﻿using System;
using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Responses.User
{
    [DataContract]
    public class ShortUserResponse
    {
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

		[DataMember(Name = "avatarUrl")]
	    public string AvatarUrl { get; set; }
    }
}
