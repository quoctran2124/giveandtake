﻿using Giveaway.API.Shared.Responses.User;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Responses.Conversation
{
	[DataContract]
	public class ConversationResponse
	{
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public Guid Id { get; set; }

		[DataMember(Name = "userConversationId", EmitDefaultValue = false)]
		public Guid? UserConversationId { get; set; }

		[DataMember(Name = "conversationStatus", EmitDefaultValue = false)]
		public string ConversationStatus { get; set; }

		[DataMember(Name = "friend", EmitDefaultValue = false)]
		public ShortUserResponse Friend { get; set; }

		[DataMember(Name = "lastMessage", EmitDefaultValue = false)]
		public MessageResponse LastMessage { get; set; }

		[DataMember(Name = "isRead", EmitDefaultValue = false)]
		public bool? IsRead { get; set; }

		[DataMember(Name = "isSeen", EmitDefaultValue = false)]
		public bool? IsSeen { get; set; }

		[DataMember(Name = "messages", EmitDefaultValue = false)]
		public List<MessageResponse> Messages { get; set; }
	}
}