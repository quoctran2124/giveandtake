﻿using System;
using System.Runtime.Serialization;
using Giveaway.API.Shared.Responses.User;

namespace Giveaway.API.Shared.Responses.Conversation
{
	[DataContract]
	public class MessageResponse
	{
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public Guid Id { get; set; }

		[DataMember(Name = "sender", EmitDefaultValue = false)]
		public ShortUserResponse Sender { get; set; }

		[DataMember(Name = "userConversationId", EmitDefaultValue = false)]
		public Guid? UserConversationId { get; set; }

		[DataMember(Name = "messageContent", EmitDefaultValue = false)]
		public string MessageContent { get; set; }

		[DataMember(Name = "createdTime", EmitDefaultValue = false)]
		public DateTimeOffset CreatedTime { get; set; }
	}
}