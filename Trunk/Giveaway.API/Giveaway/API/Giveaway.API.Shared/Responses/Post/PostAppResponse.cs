using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Responses.Post
{
	[DataContract]
    public class PostAppResponse : PostBaseResponse
    {
        [DataMember(Name = "commentCount", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "commentCount")]
        public int CommentCount { get; set; }

        [DataMember(Name = "requestCount", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "requestCount")]
        public int RequestCount { get; set; }

	    [DataMember(Name = "appreciationCount", EmitDefaultValue = false)]
	    [JsonProperty(PropertyName = "appreciationCount")]
	    public int AppreciationCount { get; set; }

	    [DataMember(Name = "isCurrentUserRequested", EmitDefaultValue = false)]
	    [JsonProperty(PropertyName = "isCurrentUserRequested")]
	    public bool IsCurrentUserRequested { get; set; }

	    [DataMember(Name = "isCurrentUserAppreciated", EmitDefaultValue = false)]
	    [JsonProperty(PropertyName = "isCurrentUserAppreciated")]
	    public bool IsCurrentUserAppreciated { get; set; }

	    [DataMember(Name = "requestedPostStatus", EmitDefaultValue = false)]
	    [JsonProperty(PropertyName = "requestedPostStatus")]
	    public string RequestedPostStatus { get; set; }

	    [DataMember(Name = "isCurrentUserReported", EmitDefaultValue = false)]
	    [JsonProperty(PropertyName = "isCurrentUserReported")]
	    public bool IsCurrentUserReported { get; set; }
	}
}
