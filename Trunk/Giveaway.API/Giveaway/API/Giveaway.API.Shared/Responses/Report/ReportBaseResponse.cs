﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Giveaway.API.Shared.Responses.Post;
using Giveaway.API.Shared.Responses.User;
using Newtonsoft.Json;

namespace Giveaway.API.Shared.Responses.Report
{
	public class ReportBaseResponse
	{
		[DataMember(Name = "id")]
		[JsonProperty(PropertyName = "id")]
		public Guid Id { get; set; }

		[DataMember(Name = "reportMessage")]
		[JsonProperty(PropertyName = "reportMessage")]
		public string Message { get; set; }

		[DataMember(Name = "createdTime")]
		[JsonProperty(PropertyName = "createdTime")]
		public DateTimeOffset CreatedTime { get; set; }

		[DataMember(Name = "updatedTime")]
		[JsonProperty(PropertyName = "updatedTime")]
		public DateTimeOffset UpdatedTime { get; set; }	
	}
}
