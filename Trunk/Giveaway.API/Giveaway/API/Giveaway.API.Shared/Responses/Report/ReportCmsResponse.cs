﻿using Giveaway.API.Shared.Responses.Post;
using Giveaway.API.Shared.Responses.User;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Responses.Report
{
	public class ReportCmsResponse : ReportBaseResponse
    {
        [DataMember(Name = "warningNumber")]
        [JsonProperty(PropertyName = "warningNumber")]
        public int WarningNumber { get; set; }

	    [DataMember(Name = "user")]
	    [JsonProperty(PropertyName = "user")]
	    public UserReportResponse User { get; set; }

	    [DataMember(Name = "post")]
	    [JsonProperty(PropertyName = "post")]
	    public PostReportResponse Post { get; set; }
	}
}
