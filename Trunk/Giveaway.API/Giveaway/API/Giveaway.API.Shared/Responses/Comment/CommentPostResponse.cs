﻿using System;
using System.Runtime.Serialization;
using Giveaway.API.Shared.Responses.Post;
using Giveaway.API.Shared.Responses.User;
using Newtonsoft.Json;

namespace Giveaway.API.Shared.Responses.Comment
{
	public class CommentPostResponse
	{
		[DataMember(Name = "id")]
		[JsonProperty(PropertyName = "id")]
		public Guid Id { get; set; }

		[DataMember(Name = "commentMessage")]
		[JsonProperty(PropertyName = "commentMessage")]
		public string CommentMessage { get; set; }

		[DataMember(Name = "createdTime")]
		[JsonProperty(PropertyName = "createdTime")]
		public DateTimeOffset CreatedTime { get; set; }

		[DataMember(Name = "updatedTime")]
		[JsonProperty(PropertyName = "updatedTime")]
		public DateTimeOffset UpdatedTime { get; set; }

		[DataMember(Name = "user")]
		[JsonProperty(PropertyName = "user")]
		public UserCommentResponse User { get; set; }
	}
}
