﻿using Giveaway.API.Shared.Models;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.Collections.Generic;
using System.IO;

namespace Giveaway.API.Shared.Helpers
{
	public class UploadImageHelper
	{
		public static List<string> PostBase64Image(ImageBase64Request request)
		{
			var urls = new List<string>();
			try
			{
				var bytes = Convert.FromBase64String(request.File);
				if (request.File.Length > 0)
				{
					var path = ContentHelper.GetPath(request.Type, request.Id);

					StreamWrite(bytes, path, out var fileName);

					string url = ContentHelper.GetImageUrl(request.Type, request.Id, fileName);
					urls.Add(url);

					string localUrl = ContentHelper.GetLocalImageUrl(request.Type, request.Id, fileName);

					using (Image<Rgba32> image = Image.Load(localUrl))
					{
						image.Mutate(x => x.Resize(image.Width / 2, image.Height / 2));
						fileName = "small_" + fileName;
						var filePath = Path.Combine(path, fileName);
						image.Save(filePath);
					}

					url = ContentHelper.GetImageUrl(request.Type, request.Id, fileName);
					urls.Add(url);

					return urls;
				}
			}
			catch (Exception e)
			{

			}

			return urls;
		}

		private static void StreamWrite(byte[] bytes, string path, out string fileName)
		{
			fileName = Guid.NewGuid() + ".jpg";
			var filePath = Path.Combine(path, fileName);
			if (bytes.Length > 0)
			{
				using (var stream = new FileStream(filePath, FileMode.Create))
				{
					stream.Write(bytes, 0, bytes.Length);
					stream.Flush();
				}
			}
		}
	}
}
