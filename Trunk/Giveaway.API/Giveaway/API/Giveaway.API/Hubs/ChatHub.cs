﻿using Giveaway.API.Shared.Extensions;
using Giveaway.Data.Enums;
using Giveaway.Data.Models.Database;
using Giveaway.Service.Services;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;
using INotificationService = Giveaway.API.Shared.Services.APIs.INotificationService;

namespace Giveaway.API.Hubs
{
	/// <inheritdoc />
	/// Dear my friends
	/// When I wrote this line, only god and I knew how it worked
	/// Now, only god knows it
	public class ChatHub : Hub
	{
		private readonly IMessageService _messageService;
		private readonly INotificationService _notificationService;
		private readonly IUserConversationService _userConversationService;

		/// <inheritdoc />
		public ChatHub(IMessageService messageService, IUserConversationService userConversationService, INotificationService notificationService)
		{
			_messageService = messageService;
			_notificationService = notificationService;
			_userConversationService = userConversationService;
		}

		/// <inheritdoc />
		public override Task OnConnectedAsync()
		{
			try
			{
				var conversationId = Guid.Parse(Context.GetHttpContext().Request.Headers["conversationId"]);
				var userConversationId = Guid.Parse(Context.GetHttpContext().Request.Headers["userConversationId"]);
				_userConversationService.Update(_userConversationService.FirstOrDefault(uc => uc.Id == userConversationId).ChangeActiveStatus(true));

				Groups.AddToGroupAsync(Context.ConnectionId, conversationId.ToString().ToUpper());
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}

			return base.OnConnectedAsync();
		}

		public override async Task OnDisconnectedAsync(Exception exception)
		{
			var conversationId = Context.GetHttpContext().Request.Headers["conversationId"];
			var userConversationId = Guid.Parse(Context.GetHttpContext().Request.Headers["userConversationId"]);
			await Groups.RemoveFromGroupAsync(Context.ConnectionId, conversationId.ToString().ToUpper());

			_userConversationService.Update(_userConversationService.FirstOrDefault(uc => uc.Id == userConversationId).ChangeActiveStatus(false));

			await base.OnDisconnectedAsync(exception);
		}

		public async Task SendMessage(Message messageRequest)
		{
			if (string.IsNullOrEmpty(messageRequest.MessageContent))
			{
				return;
			}

			await Clients.Group(messageRequest.ConversationId.ToString().ToUpper()).SendAsync("ReceiveMessage", messageRequest);
			_messageService.Create(new Message
			{
				UserConversationId = messageRequest.UserConversationId,
				MessageContent = messageRequest.MessageContent
			}, out _);

			var receiver = _userConversationService.FirstOrDefault(uc =>
				uc.ConversationId == messageRequest.ConversationId && uc.Id != messageRequest.UserConversationId);

			if (!receiver.IsActive && receiver.ConversationStatus == ConversationStatus.Allow)
			{
				receiver.IsSeen = false;
				_userConversationService.Update(receiver);

				// Send a notification to the user who requested and also save it to db
				_notificationService.PushNotification(new Notification
				{
					UserDisplayName = messageRequest.Sender.DisplayName,
					TemptType = TemptNotificationType.Chat,
					Type = NotificationType.Chat,
					RelevantId = messageRequest.ConversationId,
					SourceUserId = messageRequest.Sender.Id,
					DestinationUserId = messageRequest.ReceiverId
				});
			}
		}
	}
}