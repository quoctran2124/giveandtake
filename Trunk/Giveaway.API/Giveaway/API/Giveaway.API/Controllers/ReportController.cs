using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Report;
using Giveaway.API.Shared.Services.APIs;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Requests.Report;

namespace Giveaway.API.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/report")]
    public class ReportController : BaseController
    {
        private readonly IReportService _reportService;

        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        /// <summary>
        /// Get list report with params object that includes: page, limit, keyword 
        /// </summary>
        /// <param name="params"></param>
        /// <returns></returns>
        [HttpGet("list")]
        [Produces("application/json")]
        public PagingQueryResponse<ReportCmsResponse> GetList([FromHeader]IDictionary<string, string> @params)
        {
            return _reportService.GetReporttForPaging(@params);
        }
       
		/// <summary>
		/// Create post report
		/// </summary>
		/// <param name="reportRequest"></param>
		/// <returns></returns>
	    [HttpPost("createReport")]
	    [Produces("application/json")]
	    public ReportAppResponse Create([FromBody]CreateReportRequest reportRequest)
		{
			reportRequest.UserId = User.GetUserId();
		    return _reportService.Create(reportRequest);
	    }
	}
}