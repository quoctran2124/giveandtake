﻿using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Requests;
using Giveaway.API.Shared.Requests.Post;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Post;
using Giveaway.API.Shared.Services.APIs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Giveaway.Data.Models;

namespace Giveaway.API.Controllers
{
	/// <inheritdoc />
	[Produces("application/json")]
    [Route("api/v1/Post")]
    public class PostController : BaseController
    {
        private readonly IPostService<PostCmsResponse> _postCmsService;
        private readonly IPostService<PostAppResponse> _postService;

        public PostController(IPostService<PostCmsResponse> postCmsService, IPostService<PostAppResponse> postAppService)
        {
            _postCmsService = postCmsService;
            _postService = postAppService;
        }

        /// <summary>
        /// Get list post with params object that includes: page, limit, keyword, provinceCityId, categoryId, title
        /// </summary>
        /// <param name="params">page, limit, keyword, provinceCityId, categoryId, title</param>
        /// <returns>List post</returns>
        [HttpGet("cms/list")]
        [Produces("application/json")]
        public PagingQueryResponse<PostCmsResponse> GetListPostCMS([FromHeader]IDictionary<string, string> @params)
        {
            return _postCmsService.GetPostForPaging(@params, userId: null, currentUserId: null, isListOfSingleUser: false);
        }

        /// <summary>
        /// Get list post with params object that includes: page, limit, provinceCityId, categoryId, title, keyword(title, Description, CategoryName, UserName, ProvinceCityName)
        /// </summary>
        /// <param name="params"></param>
        /// <returns></returns>
        [HttpGet("app/list")]
        [Produces("application/json")]
        public PagingQueryResponse<PostAppResponse> GetListPostApp([FromHeader]IDictionary<string, string> @params)
        {
	        var currentUserId = GetUserId();
	        return _postService.GetPostForPaging(@params, userId: null, currentUserId, isListOfSingleUser: false);
        }

	    /// <summary>
        /// Get list post of an User with userId and params object that includes: page, limit, keyword, provinceCityId, categoryId, title
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="params">page, limit, keyword, provinceCityId, categoryId, title</param>
        /// <returns>List post</returns>
        [Authorize]
        [HttpGet("app/listPostOfUser/{userId}")]
        [Produces("application/json")]
        public PagingQueryResponse<PostAppResponse> GetListPostOfSingleUser(string userId, [FromHeader]IDictionary<string, string> @params)
	    {
		    var currentUserId = GetUserId();
            return _postService.GetPostForPaging(@params, userId, currentUserId, isListOfSingleUser: true);
        }

		/// <summary>
		/// Get all posts that the user requested
		/// </summary>
		/// <param name="params"></param>
		/// <returns></returns>
		[Authorize]
	    [HttpGet("app/listRequestedPostOfUser")]
	    [Produces("application/json")]
		public PagingQueryResponse<PostAppResponse> GetListRequestedPostOfUser([FromHeader]IDictionary<string, string> @params)
	    {
		    var userId = GetUserId();
			return _postService.GetListRequestedPostOfUser(@params, userId);
	    }

		/// <summary>
		/// Get detail of a post by id 
		/// </summary>
		/// <param name="postId"></param>
		/// <returns></returns>
		[HttpGet("app/detail/{postId}")]
        [Produces("application/json")]
        public PostAppResponse GetDetailApp(Guid postId)
        {
	        var userId = GetUserId();
			return _postService.GetDetail(postId, userId);
        }

        /// <summary>
        /// Get detail of a post by id 
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        [HttpGet("cms/detail/{postId}")]
        [Produces("application/json")]
        public PostCmsResponse GetDetailCms(Guid postId)
        {
            return _postCmsService.GetDetail(postId, null);
        }

        /// <summary>
        /// Create a post
        /// </summary>
        /// <param name="postRequest">page, limit, keyword, provinceCityId, categoryId, title</param>
        /// <returns>List post</returns>
        [Authorize]
        [HttpPost("app/create")]
        [Produces("application/json")]
        public PostAppResponse Create([FromBody]PostRequest postRequest)
        {
            postRequest.UserId = User.GetUserId();  
            return _postService.Create(postRequest);
        }

        /// <summary>
        /// Update a post
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="postRequest"></param>
        /// <returns></returns>
        [Authorize] 
        [HttpPut("app/update/{postId}")]
        [Produces("application/json")]
        public PostAppResponse Update(Guid postId, [FromBody]PostRequest postRequest)
        {
            postRequest.UserId = User.GetUserId();
            return _postService.Update(postId, postRequest);
        }

		/// <summary>
		/// Change status of a post: Giving, Gave, Received, Activated, Blocked, Deleted
		/// </summary>
		/// <param name="postId"></param>
		/// <param name="request"></param>
		/// <returns></returns>
		[Authorize]
        [HttpPut("status/{postId}")]
        [Produces("application/json")]
        public bool ChangePostStatus(Guid postId, [FromBody]StatusRequest request)
		{
			var userId = User.GetUserId();
            return _postService.ChangePostStatus(postId, userId, request);
        }

	    /// <summary>
	    /// Appreciate a Post
	    /// </summary>
	    /// <param name="postId"></param>
	    /// <returns></returns>
	    [Authorize]
	    [HttpPost("app/appreciate/{postId}")]
	    [Produces("application/json")]
	    public ResponseMessage AppreciatePost(Guid postId)
	    {
		    var userId = User.GetUserId();
		    return _postService.AppreciatePost(userId, postId);
	    }

		#region Utils

		private string GetUserId()
		{
			string userId = "";
			if (User.Identity.IsAuthenticated)
				userId = User.GetUserId().ToString();
			return userId;
		}

		#endregion
	}
}