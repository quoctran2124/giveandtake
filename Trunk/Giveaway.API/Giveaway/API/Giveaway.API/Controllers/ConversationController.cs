﻿using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Requests;
using Giveaway.API.Shared.Requests.Conversation;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Conversation;
using Giveaway.API.Shared.Services.APIs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Giveaway.API.Controllers
{
	/// <inheritdoc />
	/// <summary>
	/// Manage Conversations
	/// </summary>
	[Route("api/v1/conversations")]
	public class ConversationController : BaseController
	{
		private readonly IConversationService _conversationService;

		/// <inheritdoc />
		public ConversationController(IConversationService conversationService)
		{
			_conversationService = conversationService;
		}

		/// <summary>
		/// Create a new conversation
		/// </summary>
		/// <param name="conversationRequest"></param>
		/// <returns>BaseResponse</returns>
		[HttpPost]
		[Produces("application/json")]
		public ConversationResponse Create([FromBody]ConversationRequest conversationRequest)
		{
			return _conversationService.Create(conversationRequest);
		}

		/// <summary>
		/// Get conversations of a specific user
		/// </summary>
		/// <param name="id"></param>
		/// <param name="params"></param>
		/// <returns>BaseResponse</returns>
		[HttpGet("{id}")]
		[Authorize]
		[Produces("application/json")]
		public ConversationResponse GetConversationDetail(Guid id, [FromHeader]IDictionary<string, string> @params)
		{
			var currentUserId = User.GetUserId();
			return _conversationService.GetConversationDetail(id, currentUserId);
		}

		/// <summary>
		/// Change Status of a conversation : allow/block
		/// </summary>
		/// <param name="id"></param>
		/// <param name="status"></param>
		/// <returns>BaseResponse</returns>
		[HttpPut("{id}")]
		[Authorize]
		[Produces("application/json")]
		public bool UpdateStatus(Guid id, [FromBody]StatusRequest status)
		{
			var currentUserId = User.GetUserId();
			return _conversationService.UpdateStatus(id, currentUserId, status);
		}

		/// <summary>
		/// Get all conversations
		/// </summary>
		/// <param name="params"></param>
		/// <returns>BaseResponse</returns>
		[HttpGet]
		[Authorize]
		[Produces("application/json")]
		public PagingQueryResponse<ConversationResponse> GetAllConversation([FromHeader]IDictionary<string, string> @params)
		{
			var currentUserId = User.GetUserId();
			return _conversationService.GetAllConversation(@params, currentUserId);
		}

		/// <summary>
		/// Get conversation badge
		/// </summary>
		/// <returns>int</returns>
		[HttpGet("badge")]
		[Authorize]
		[Produces("application/json")]
		public int GetConversationBadge()
		{
			var currentUserId = User.GetUserId();
			return _conversationService.GetConversationBadge(currentUserId);
		}

		/// <summary>
		/// Delete Conversation
		/// </summary>
		/// <param name="id"></param>
		/// <returns>BaseResponse</returns>
		[HttpDelete("{id}")]
		[Authorize]
		[Produces("application/json")]
		public bool DeleteConversation(Guid id)
		{
			var currentUserId = User.GetUserId();
			return _conversationService.DeleteConversation(id, currentUserId);
		}
	}
}