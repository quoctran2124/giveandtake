﻿using System.Collections.Generic;
using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Requests.Comment;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Comment;
using Giveaway.API.Shared.Services.APIs;
using Giveaway.Data.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Giveaway.API.Controllers
{
	[Produces("application/json")]
	[Route("api/v1/comment")]
	public class CommentController : BaseController
	{
		private readonly ICommentService _commentService;

		public CommentController(ICommentService commentService)
		{
			_commentService = commentService;
		}

		/// <summary>
		/// Create a comment
		/// </summary>
		/// <param name="comment"></param>
		/// <returns></returns>
		[Authorize]
		[HttpPost("create")]
		[Produces("application/json")]
		public CommentPostResponse Create([FromBody]CommentPostRequest comment)
		{
			comment.UserId = User.GetUserId();
			return _commentService.Create(comment);
		}

		/// <summary>
		/// Get list request with params object that includes: page, limit, keyword, requestStatus
		/// </summary>
		/// <param name="postId"></param>
		/// <param name="params"></param>
		/// <returns></returns>
		[Authorize]
		[HttpGet("list/{postId}")]
		[Produces("application/json")]
		public PagingQueryResponse<CommentPostResponse> GetListByPostId(string postId, [FromHeader]IDictionary<string, string> @params)
		{			
			return _commentService.GetRequestForPaging(postId, @params);
		}

		/// <summary>
		/// Update a comment
		/// </summary>
		/// <param name="commentId"></param>
		/// <param name="comment"></param>
		/// <returns></returns>
		[Authorize]
		[HttpPut("edit/{commentId}")]
		[Produces("application/json")]
		public CommentPostResponse Update(string commentId, [FromBody]CommentPostRequest comment)
		{
			return _commentService.Update(commentId, comment, User.GetUserId());
		}

		/// <summary>
		/// Delete a comment
		/// </summary>
		/// <param name="commentId"></param>
		/// <returns></returns>
		[Authorize]
		[HttpDelete("delete/{commentId}")]
		[Produces("application/json")]
		public bool Delete(string commentId)
		{
			return _commentService.Delete(commentId, User.GetUserId());
		}
	}
}
