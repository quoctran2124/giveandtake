﻿using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Conversation;
using Giveaway.API.Shared.Services.APIs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Giveaway.API.Shared.Extensions;

namespace Giveaway.API.Controllers
{
	/// <inheritdoc />
	/// <summary>
	/// Manage Conversations
	/// </summary>
	[Route("api/v1/messages")]
	public class MessageController : Controller
	{
		private readonly IMessageService _messageService;

		/// <inheritdoc />
		public MessageController(IMessageService messageService)
		{
			_messageService = messageService;
		}

		/// <summary>
		/// Fetch messages of a conversation
		/// </summary>
		/// <param name="params"></param>
		/// <returns>PagingQueryResponse</returns>
		[HttpGet]
		[Authorize]
		[Produces("application/json")]
		public PagingQueryResponse<MessageResponse> FetchMessages([FromHeader]IDictionary<string, string> @params)
		{
			var currentUserId = User.GetUserId();
			return _messageService.FetchMessages(@params, currentUserId);
		}
	}
}