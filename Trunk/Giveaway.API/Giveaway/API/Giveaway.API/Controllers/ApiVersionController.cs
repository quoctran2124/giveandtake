﻿using Giveaway.API.Shared.Constants;
using Microsoft.AspNetCore.Mvc;

namespace Giveaway.API.Controllers
{
    /// <inheritdoc />
    [Produces("application/json")]
    [Route("api/v1/version")]
    public class ApiVersionController : BaseController
    {
        /// <summary>
        /// Get current version of the api service system
        /// </summary>
        /// <returns>int</returns>
        [HttpGet]
        public int GetApiVersion()
        {
            return AppConstant.Version;
        }
    }
}