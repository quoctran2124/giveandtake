﻿using Giveaway.Data.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Giveaway.Data.Models.Database
{
	[Table("Notification")]
	public class Notification : BaseEntity
	{
		[NotMapped]
		public string Message { get; set; }
		[Required]
		public Guid DestinationUserId { get; set; }
		[Required]
		public Guid SourceUserId { get; set; }
		public Guid RelevantId { get; set; }
		[Required]
		public NotificationType Type { get; set; }

		public TemptNotificationType TemptType { get; set; }

		[Required]
		public bool IsSeen { get; set; }
		[Required]
		public bool IsRead { get; set; }

		public string UserDisplayName { get; set; }
	}
}
