﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Giveaway.Data.Models.Database
{
	[Table("Message")]
	[DataContract]
	public class Message : BaseEntity
	{
		[DataMember(Name = "userConversationId")]
		public Guid UserConversationId { get; set; }

		[Required]
		[DataMember(Name = "messageContent")]
		public string MessageContent { get; set; }

		public virtual UserConversation UserConversation { get; set; }

		[NotMapped]
		[DataMember(Name = "sender", EmitDefaultValue = false)]
		public User Sender { get; set; }

		[NotMapped]
		[DataMember(Name = "conversationId", EmitDefaultValue = false)]
		public Guid ConversationId { get; set; }

		[NotMapped]
		[DataMember(Name = "userId", EmitDefaultValue = false)]
		public Guid? UserId { get; set; }

		[NotMapped]
		[DataMember(Name = "receiverId", EmitDefaultValue = false)]
		public Guid ReceiverId { get; set; }
	}
}