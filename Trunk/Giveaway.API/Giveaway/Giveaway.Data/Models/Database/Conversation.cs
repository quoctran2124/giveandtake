﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Giveaway.Data.Enums;

namespace Giveaway.Data.Models.Database
{
    [Table("Conversation")]
    public class Conversation : BaseEntity
    {
        public virtual List<UserConversation> UserConversations { get; set; }

		[NotMapped]
	    public Guid? UserId { get; set; }

		[NotMapped]
	    public Guid? UserConversationId => UserConversations?.FirstOrDefault(u => u.UserId == UserId)?.Id;

		[NotMapped]
	    public ConversationStatus? ConversationStatus => UserConversations?.FirstOrDefault(u => u.UserId == UserId)?.ConversationStatus;

		[NotMapped]
	    public IEnumerable<User> Users => UserConversations?.Where(uc => uc.User != null).Select(uc => uc.User);

	    [NotMapped]
	    public User Friend => Users?.FirstOrDefault(u => u.Id != UserId);

		[NotMapped]
	    public IEnumerable<Message> Messages =>
		    UserConversations?.SelectMany(uc => uc.Messages).Where(m => m.CreatedTime > DeletedDate).OrderByDescending(m => m.CreatedTime);

	    [NotMapped]
	    public Message LastMessage => Messages?.FirstOrDefault();

		[NotMapped]
		public bool? IsRead => UserConversations?.FirstOrDefault(u => u.UserId == UserId)?.LastActiveTime > LastMessage.CreatedTime;

	    [NotMapped]
		public bool? IsSeen => UserConversations?.FirstOrDefault(u => u.UserId == UserId)?.IsSeen;

	    [NotMapped]
	    public DateTimeOffset? DeletedDate => UserConversations?.FirstOrDefault(u => u.UserId == UserId)?.DeletedDate;
    }
}