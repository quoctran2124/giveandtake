﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Giveaway.Data.Enums
{
	public enum MemberType
	{
		Normal,
		Silver, 
		Gold,
		Platinum,
		Diamond
	}
}
