﻿namespace Giveaway.Data.Enums
{
	public enum ConversationStatus
	{
		Allow,
		Block,
		Deleted
	}
}