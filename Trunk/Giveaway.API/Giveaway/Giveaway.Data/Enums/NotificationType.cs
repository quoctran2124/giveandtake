﻿namespace Giveaway.Data.Enums
{
	public enum NotificationType
	{
		Like,
		Comment,
		Request,
		BlockedPost,
		Warning,
		Chat
	}

	public enum TemptNotificationType
	{
		Appreciation,
		RequestPending,
		RequestApproved,
		RequestRejected,
		ItemReceived,
		ItemCancelled,
		Comment,
		Closed,
		BlockedPost,
		Warning,
		Chat
	}
}
