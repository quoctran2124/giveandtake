﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Giveaway.Data.EF.Migrations
{
    public partial class UpdateUserConversationLastActiveTime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsRead",
                table: "UserConversation",
                newName: "IsActive");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastActiveTime",
                table: "UserConversation",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastActiveTime",
                table: "UserConversation");

            migrationBuilder.RenameColumn(
                name: "IsActive",
                table: "UserConversation",
                newName: "IsRead");
        }
    }
}
