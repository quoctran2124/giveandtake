﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Giveaway.Data.EF.Migrations
{
    public partial class UpdateConversationStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsRead",
                table: "UserConversation",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSeen",
                table: "UserConversation",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsRead",
                table: "UserConversation");

            migrationBuilder.DropColumn(
                name: "IsSeen",
                table: "UserConversation");
        }
    }
}
