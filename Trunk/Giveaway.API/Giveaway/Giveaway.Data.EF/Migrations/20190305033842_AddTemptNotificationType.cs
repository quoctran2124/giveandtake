﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Giveaway.Data.EF.Migrations
{
    public partial class AddTemptNotificationType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TemptType",
                table: "Notification",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TemptType",
                table: "Notification");
        }
    }
}
