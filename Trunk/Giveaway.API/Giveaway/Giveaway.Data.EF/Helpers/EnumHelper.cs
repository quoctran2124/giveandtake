﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Giveaway.Data.EF.Helpers
{
	public static class EnumHelper
	{
		public static string GetDisplayValue(Enum value)
		{
			var fieldInfo = value.GetType().GetRuntimeField(value.ToString());

			if (!(fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false) is DisplayAttribute[] descriptionAttributes))
			{
				return string.Empty;
			};

			return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Description : value.ToString();
		}

		public static T ToEnum<T>(this string value) where T : struct
		{
			return Enum.Parse<T>(value, true);
		}

		public static T ToEnum<T>(this string value, T defaultValue) where T : struct 
		{
			if (string.IsNullOrEmpty(value))
			{
				return defaultValue;
			}

			return Enum.TryParse<T>(value, true, out var result) ? result : defaultValue;
		}
	}
}
