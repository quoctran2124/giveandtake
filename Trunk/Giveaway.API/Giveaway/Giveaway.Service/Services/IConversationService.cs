﻿using Giveaway.Data.Models.Database;

namespace Giveaway.Service.Services
{
	public interface IConversationService : IEntityService<Conversation>
	{
		
	}

	public class ConversationService : EntityService<Conversation>, IConversationService
	{

	}
}