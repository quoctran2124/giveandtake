﻿
using Giveaway.Data.Models.Database;

namespace Giveaway.Service.Services
{
	public interface ICommentService : IEntityService<Comment>
	{
	}
	public class CommentService : EntityService<Comment>, ICommentService
	{

	}
}
