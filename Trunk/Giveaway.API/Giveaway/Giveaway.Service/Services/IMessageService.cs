﻿using Giveaway.Data.Models.Database;

namespace Giveaway.Service.Services
{
	public interface IMessageService : IEntityService<Message>
	{
		
	}

	public class MessageService : EntityService<Message>, IMessageService
	{

	}
}