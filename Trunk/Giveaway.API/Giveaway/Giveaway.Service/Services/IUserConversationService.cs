﻿using Giveaway.Data.Models.Database;

namespace Giveaway.Service.Services
{
	public interface IUserConversationService : IEntityService<UserConversation>
	{
	}

	public class UserConversationService : EntityService<UserConversation>, IUserConversationService
	{
	}
}