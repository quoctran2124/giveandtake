<?php
/**
 *	Kalium WordPress Theme
 *
 *	Laborator.co
 *	www.laborator.co
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Direct access not allowed.
}

// Footer texts
$footer_text = get_data( 'footer_text' );
$footer_text_right = get_data( 'footer_text_right' );

?>
<footer id="footer" role="contentinfo" <?php kalium_footer_class(); ?>>

<!--    --><?php
/*		// Display footer widgets, if enabled
		if ( get_data( 'footer_widgets' ) ) {
			get_template_part( 'tpls/footer-widgets' );
		}
	*/?>

	<?php if ( get_data( 'footer_bottom_visible' ) ) : ?>

	<div class="footer-bottom">
		
		<div class="container">

			<div class="footer-bottom-content">
				
				<?php /*if ( $footer_text_right ) : */?><!--

					<div class="footer-content-right">
							<?php /*echo do_shortcode( laborator_esc_script( $footer_text_right ) ); */?>
					</div>

					
				--><?php /*endif; */?>
                <div class="footer-content-right">
                    <div class="footer-text-right">
                        <br/>
                        <ul class="menu">
                            <!--<li style="display: inline-block;padding-right:5%">
                                <a href="#info" data-ps2id-api="true" target="_blank" class="_mPS2id-h mPS2id-highlight">
                                    <span>Giới thiệu</span>
                                </a>
                            </li>
                            <li style="display: inline-block;padding-right:5%">
                                <a href="#given" data-ps2id-api="true" target="_blank" class="_mPS2id-h mPS2id-highlight">
                                    <span>Người cho</span>
                                </a>
                            </li>
                            <li style="display: inline-block;padding-right:5%">
                                <a href="#taken" data-ps2id-api="true" target="_blank" class="_mPS2id-h mPS2id-highlight">
                                    <span>Người nhận</span>
                                </a>
                            </li>
                            <li style="display: inline-block;padding-right:5%">
                                <a href="#goal" data-ps2id-api="true" target="_blank" class="_mPS2id-h mPS2id-highlight">
                                    <span>Mục đích</span>
                                </a>
                            </li>
                            <li style="display: inline-block;">
                                <a href="#registration" data-ps2id-api="true" target="_blank" class="_mPS2id-h mPS2id-highlight">
                                    <span>Đăng kí</span>
                                </a>
                            </li>-->
                            <li style="display: inline-block;">
<!--                                <a href="#info" data-ps2id-api="true" target="_blank" class="_mPS2id-h mPS2id-highlight">
                                    <span><img src="../chovanhan/wp-content/uploads/2018/10/btm_backtotop.png"/></span>
                                </a>-->
                            </li>
                        </ul>


                    </div>
                    <br />
                    <div >
                        <p style="color:white; padding-top:15px"><span style="font-weight:100">Phát triển bởi </span><span style="font-weight:400">Sioux High Tech Software Ltd.</span></p>
                    </div>
                </div>

				<?php if ( $footer_text ) : ?>
				
					<div class="footer-content-left">
						
						<div class="copyrights site-info">
                            <div>
                                <a href="https://www.sioux.asia/"><img src="../wp-content/uploads/2019/04/sioux_logo.png" width="125px" height="49px" /></a>
                            </div>
                            <br />
<!--							<p style="color:white">--><?php //echo do_shortcode( laborator_esc_script( $footer_text ) ); ?><!--</p>-->
							<span style="color:white">Bản quyền © 2019 Sioux. Giữ toàn quyền.</span>
						</div>
						
					</div>
				
				<?php endif; ?>
			</div>

		</div>
		
	</div>
	
	<?php endif; ?>

</footer>