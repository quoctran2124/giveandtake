<?php
/**
 *	Kalium WordPress Theme
 *
 *	Laborator.co
 *	www.laborator.co
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Direct access not allowed.
}

// Get Menu Type To Use
$main_menu_type = get_data( 'main_menu_type' );
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset='utf-8'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<meta property="fb:app_id" content="643536022700414" />
	<meta property="al:android:package" content="com.sioux.giveandtake" />
	<meta property="al:android:app_name" content="Cho va Nhan" />
	<meta property="al:ios:app_store_id" content="1444250710" />
	<meta property="al:ios:app_name" content="Cho va Nhan" />
	<meta property="al:web:should_fallback" content="false" />
	<meta property="og:title" content="Cho Và Nhận" />
	<meta property="og:description" content="Cho và Nhận là một ứng dụng phi lợi nhuận nơi người dùng có thể chia sẻ miễn phí các vật phẩm tới cộng đồng, đồng thời có thể nhận được những thứ mà mình đang tìm kiếm. Hãy cho đi thật nhiều để được nhận lại nhiều hơn với Cho và Nhận!" />
	<meta property="og:image" content="https://chovanhan.asia/wp-content/uploads/2019/04/Splash-screen.png" />

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<?php

	/**
	 * Before header starts hooks
	 *
	 * @hooked woocommerce_demo_store - 10
	 */
	do_action( 'kalium_before_header' );
		
	if ( apply_filters( 'kalium_show_header', true ) ) :
		
		// Theme Borders
		if ( get_data( 'theme_borders' ) ) :
		
			get_template_part( 'tpls/borders' );
			
		endif;

		// Mobile Menu
		include locate_template( 'tpls/menu-mobile.php' );
		
		// Top Menu
		if ( $main_menu_type == 'top-menu' || get_data( 'menu_top_force_include' ) ) {
			include locate_template( 'tpls/menu-top.php' );
		}
				
		// Sidebar Menu
		if ( $main_menu_type == 'sidebar-menu' || get_data( 'menu_sidebar_force_include' ) ) {
			include locate_template( 'tpls/menu-sidebar.php' );
		}
		
	endif;
	?>

	<div class="wrapper" id="main-wrapper">

		<?php
		// Kalium Start Wrapper
		do_action( 'kalium_wrapper_start' );	
		
		// Show Header
		if ( apply_filters( 'kalium_show_header', true ) ):

			// Main Header
			get_template_part( 'tpls/header-main' );
			
		endif;
		?>
