<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *Simple Contact Form
 * @package WordPress
 */
define( 'WP_HOME', 'https://192.168.51.119/chovanhanweb' );
define( 'WP_SITEURL', 'https://192.168.51.119/chovanhanweb' );
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'chovanhan');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S0U+H8EGmc.n/(-%pBMn&x2[Ic(wZ*12-D-{{*u1|PBl.:ALjWY8O3*L.[xxVqiJ');
define('SECURE_AUTH_KEY',  'n!+{_hv#<?V(pk+hTKVAq$A-mYNyP9$5X:G|L4g!m~Cmg6z[|x!s_4.Lno^975~?');
define('LOGGED_IN_KEY',    '=,R#%Dr!Luo4f#%`7$/M+vj!:=fG)PPi5DyDWvxm0zz@;Kr3/L)XN2Zy3P^9W,%J');
define('NONCE_KEY',        'x!dDCZ<U#P!x6:Q8kz:vCByh^/tu|HKnq=Dlz3v3|.{PhTbW3=hC0>ti30A*Q!iF');
define('AUTH_SALT',        'tHlvY{%>VP8$Y4dpNsww~|He|5*Unh5wE6B3S<w8LNxdf/CQQ:5m-4`jB2+!<Y9O');
define('SECURE_AUTH_SALT', 'I?IXj/ZaX1@[J1#*FGoYr|xzCk3Y?e6{PjHDW)UxJ+8@^0Gmm1sosJ55_%/Ct~pi');
define('LOGGED_IN_SALT',   'k1Ny~@c/?Nu9=qGn{;8SZ3ng*&8[nvh>Pdp@_QQFvk7A?*WScbl<.Jr+f%JZh%(c');
define('NONCE_SALT',       'kO=?xm)jwt`yWYYEKfa{e`K*XPJ)@=tzM0!Wt,W+qh^U&WjAte2q;eZJ[oC|^Y>@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
