﻿using Foundation;
using UIKit;

namespace GiveAndTake.iOS.Interfaces
{
	public interface ITextViewDelegate
	{
		bool ShouldInteractWithUrl(UITextView textView, NSUrl url, NSRange characterRange);
	}
}