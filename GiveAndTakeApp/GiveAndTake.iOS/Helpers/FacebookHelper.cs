﻿using Facebook.CoreKit;
using Facebook.LoginKit;
using Facebook.ShareKit;
using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels;
using MvvmCross;
using MvvmCross.Navigation;
using UIKit;

namespace GiveAndTake.iOS.Helpers
{
	public class FacebookHelper : IFacebookHelper
	{
		public void ShareFacebookContent(string content, string contentUrl)
		{
			if (string.IsNullOrEmpty(contentUrl))
			{
				return;
			}

			var hashTag = new Hashtag()
			{
				StringRepresentation = AppConstants.GiveAndTakeHashtag
			};

			var linkContent = new ShareLinkContent()
			{
				Hashtag = hashTag,
				Quote = content
			};

			linkContent.SetContentUrl(NSUrl.FromString(contentUrl));
			var shareDialog = new ShareDialog();
			var window = UIApplication.SharedApplication.KeyWindow;
			var rootViewController = window.RootViewController;
			shareDialog.FromViewController = rootViewController;

			shareDialog.SetShareContent(linkContent);
			shareDialog.Mode = ShareDialogMode.FeedWeb;

			shareDialog.Show();
		}

		public bool IsLoggedIn() => AccessToken.CurrentAccessToken != null && !AccessToken.CurrentAccessToken.IsExpired;

		public User GetCurrentUser() => new User
		{
			FirstName = Profile.CurrentProfile.FirstName,
			LastName = Profile.CurrentProfile.LastName,
			DisplayName = Profile.CurrentProfile.Name,
			UserName = Profile.CurrentProfile.UserID,
			AvatarUrl = GetProfilePicture(Profile.CurrentProfile.UserID),
			SocialAccountId = Profile.CurrentProfile.UserID
		};

		public void LogOut()
		{
			new LoginManager().LogOut();
			Mvx.Resolve<IMvxNavigationService>().Navigate<LoginViewModel>();
		}

		private static string GetProfilePicture(string profileId) =>
			string.Format(AppConstants.FaceBookAvatarUrl, profileId);
	}
}