﻿using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.Models;

namespace GiveAndTake.iOS.Helpers
{
	public class StorageHelper : IStorageHelper
	{
		public void SaveLanguage(LanguageType language)
		{
			var nSUserDefaults = NSUserDefaults.StandardUserDefaults;
			nSUserDefaults.SetString(language.ToString(), AppConstants.DeviceLanguage);
			nSUserDefaults.Synchronize();
		}

        public void SaveCredentials(LoginResponse loginResponse)
        {
            var nSUserDefaults = NSUserDefaults.StandardUserDefaults;
            nSUserDefaults.SetBool(true, AppConstants.Authenticated);
            nSUserDefaults.SetString(JsonHelper.Serialize(loginResponse), AppConstants.Credentials);
            nSUserDefaults.Synchronize();
        }

        public LoginResponse GetCredentials()
        {
            var nSUserDefaults = NSUserDefaults.StandardUserDefaults;
            var authenticated = nSUserDefaults.BoolForKey(AppConstants.Authenticated);
            if (!authenticated)
            {
                return null;
            }

            return JsonHelper.Deserialize<LoginResponse>(nSUserDefaults.StringForKey(AppConstants.Credentials));
        }

        public void DeleteCredentials()
        {
            var nSUserDefaults = NSUserDefaults.StandardUserDefaults;
            nSUserDefaults.RemoveObject(AppConstants.Authenticated);
            nSUserDefaults.RemoveObject(AppConstants.Credentials);
            nSUserDefaults.Synchronize();
        }
    }
}