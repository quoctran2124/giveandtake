﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DevKit.Xamarin.ImageKit;
using DevKit.Xamarin.ImageKit.Abstractions;
using Foundation;
using GMImagePicker;
using Photos;
using UIKit;

namespace GiveAndTake.iOS.Helpers
{
	public static class MediaHelper
	{
		public static List<byte[]> Images;
		private static byte[] _bytes;
		private const int NewImageQualityPercentage = 80;
		private const int NewImageScaledPercentage = 50;
		private const string GalleryTitle = "Gallery";

		public static async Task<List<byte[]>> OpenGallery(int maximumImageCount = -1)
		{
			Images=new List<byte[]>();
			var mainTcs = new TaskCompletionSource<List<byte[]>>();

			var hostViewController = GetHostViewController();

			var picker = new GMImagePickerController
			{
				ShowCameraButton = false,
				AllowsMultipleSelection = true,
				MediaTypes = new[] { PHAssetMediaType.Image },
				GridSortOrder = SortOrder.Descending
			};

			picker.Title = GalleryTitle;

			if (maximumImageCount > 0)
			{
				picker.ShouldSelectAsset += (sender, args) => args.Cancel = picker.SelectedAssets.Count >= maximumImageCount;
			}


			picker.FinishedPickingAssets += async (object sender, MultiAssetEventArgs args) =>
			{

				foreach (var asset in args.Assets)
				{
					var nsUrl = await Task.Run<NSUrl>(() =>
					{
						var tcs = new TaskCompletionSource<NSUrl>();
						asset.RequestContentEditingInput(new PHContentEditingInputRequestOptions(), (contentEditingInput, requestStatusInfo) =>
						{
							var url = contentEditingInput.FullSizeImageUrl;
							tcs.SetResult(url);
						});

						return tcs.Task;
					});

					// we cannot process url's starting with file://, remove it
					var stringUrl = nsUrl.ToString();
					var urlPrefex = "file://";
					if (stringUrl.StartsWith(urlPrefex, StringComparison.InvariantCultureIgnoreCase))
					{
						stringUrl = stringUrl.Substring(urlPrefex.Length);
					}

					using (var stream = File.OpenRead(stringUrl))
					{
						var image = UIImage.LoadFromData(NSData.FromStream(stream));
						using (NSData imageData = image.AsJPEG())
						{
							Byte[] myByteArray = new Byte[imageData.Length];
							System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, myByteArray, 0, Convert.ToInt32(imageData.Length));
							_bytes = myByteArray;
						}
					}

					byte[] scaledImage =
						await CrossImageResizer.Current.ScaleImageAsync(_bytes, NewImageScaledPercentage, ImageFormat.PNG);
					byte[] lessQualityImage = await CrossImageResizer.Current.ReduceJPGQualityAsync(scaledImage, NewImageQualityPercentage);
					Images.Add(lessQualityImage);
				}

				mainTcs.SetResult(Images);
			};

			await hostViewController.PresentViewControllerAsync(picker, true);

			return await mainTcs.Task;
		}

		private static UIViewController GetHostViewController()
		{
			UIViewController viewController = null;
			var window = UIApplication.SharedApplication.KeyWindow;
			if (window == null)
				throw new InvalidOperationException("There's no current active window");

			if (window.WindowLevel == UIWindowLevel.Normal)
				viewController = window.RootViewController;

			if (viewController == null)
			{
				window = UIApplication.SharedApplication.Windows.OrderByDescending(w => w.WindowLevel).FirstOrDefault(w => w.RootViewController != null && w.WindowLevel == UIWindowLevel.Normal);
				if (window == null)
					throw new InvalidOperationException("Could not find current view controller");
				else
					viewController = window.RootViewController;
			}

			while (viewController.PresentedViewController != null)
				viewController = viewController.PresentedViewController;

			return viewController;
		}
	}
}