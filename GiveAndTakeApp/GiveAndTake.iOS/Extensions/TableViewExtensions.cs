﻿using Foundation;
using UIKit;

namespace GiveAndTake.iOS.Extensions
{
	public static class TableViewExtensions
	{
		public static void TryScrollToRow(this UITableView tableView, int row, int section, bool animated)
		{
			if (tableView != null && section < tableView.NumberOfSections() && row < tableView.NumberOfRowsInSection(section))
			{
				tableView.ScrollToRow(NSIndexPath.FromRowSection(row, section), UITableViewScrollPosition.Top, animated);
			}
		}
	}
}