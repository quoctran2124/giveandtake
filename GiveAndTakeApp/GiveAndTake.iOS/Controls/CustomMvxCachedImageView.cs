﻿using FFImageLoading;
using FFImageLoading.Cross;

namespace GiveAndTake.iOS.Controls
{
	public class CustomMvxCachedImageView : MvxCachedImageView
	{
		private string _imageUrl;
		public string ImageUrl
		{
			get => _imageUrl;
			set
			{
				_imageUrl = string.IsNullOrEmpty(value) ? ErrorPlaceholderImagePath : value;
				ImagePath = _imageUrl;
			}
		}

		private string _base64String;
		public string Base64String
		{
			get => _base64String;
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					return;
				}

				_base64String = value;
				ImageService.Instance.LoadBase64String($"data:image/png;base64,{value}").Into(this);
			}
		}
	}
}