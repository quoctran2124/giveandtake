﻿using GiveAndTake.iOS.Helpers;
using UIKit;

namespace GiveAndTake.iOS.Controls
{
    public sealed class CheckboxButton : UIImageView
    {
        private bool _checked;
        public bool Checked
        {
            get => _checked;
            set
            {
                _checked = value;
                Image = new UIImage(_checked ? ImageHelper.CheckedCheckbox : ImageHelper.UnCheckedCheckbox);
            }
        }

        public CheckboxButton()
        {
            UserInteractionEnabled = true;
            TranslatesAutoresizingMaskIntoConstraints = false;
            BackgroundColor = ColorHelper.Transparent;
            Checked = false;
        }
    }
}