﻿
using GiveAndTake.iOS.Interfaces;
using Foundation;
using UIKit;

namespace GiveAndTake.iOS.Controls
{
	public class DisabledInteractionTextViewDelegate : UITextViewDelegate
	{
		private readonly ITextViewDelegate _textViewDelegate;

		public DisabledInteractionTextViewDelegate(ITextViewDelegate textViewDelegate)
		{
			_textViewDelegate = textViewDelegate;
		}

		public override bool ShouldInteractWithUrl(UITextView textView, NSUrl url, NSRange characterRange)
		{
			return _textViewDelegate.ShouldInteractWithUrl(textView, url, characterRange);
		}
	}
}
