﻿using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;

namespace GiveAndTake.iOS.Controls
{
	public class DisabledInteractionTextView : UITextView
	{
		public override bool CanPerform(Selector action, NSObject withSender)
		{

			if (action == new Selector("paste:") ||
			    action == new Selector("cut:") ||
			    action == new Selector("copy:") ||
			    action == new Selector("select:") ||
			    action == new Selector("selectAll:") ||
			    action == new Selector("delete:") ||
			    action == new Selector("makeTextWritingDirectionLeftToRight:") ||
			    action == new Selector("makeTextWritingDirectionRightToLeft:") ||
			    action == new Selector("toggleBoldface:") ||
			    action == new Selector("toggleItalics:") ||
			    action == new Selector("toggleUnderline:"))
			{
				return false;
			}

			return base.CanPerform(action, withSender);
		}

		public override UITextRange SelectedTextRange { get => null; }

		public override CGRect GetCaretRectForPosition(UITextPosition position)
		{
			return CGRect.Empty;
		}

		public override UITextSelectionRect[] GetSelectionRects(UITextRange range)
		{
			return null;
		}
	}
}