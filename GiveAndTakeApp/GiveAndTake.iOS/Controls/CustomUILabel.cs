﻿using GiveAndTake.Core;
using GiveAndTake.iOS.Helpers;
using UIKit;

namespace GiveAndTake.iOS.Controls
{
	public class CustomUILabel : UILabel
	{
		private bool _activated;
		public bool Activated
		{
			get => _activated;
			set
			{
				_activated = value;
				TextColor = value ? UIColor.White : UIColor.White.ColorWithAlpha(0.5f);
			}
		}
	}
}