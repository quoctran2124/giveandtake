﻿using CoreGraphics;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen,
		ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class ProfileEditorView : BaseView
	{
		private UIView _profileView;
		private CustomMvxCachedImageView _avatarView;
		private UILabel _userNameLabel;
		private UITextField _userDisplayNameTextField;
		private UIImageView _cameraButton;
		private AlphaUiButton _btnCancel;
		private AlphaUiButton _btnSubmit;

		protected override void InitView()
		{
			View.AddGestureRecognizer(new UITapGestureRecognizer(HideKeyboard)
			{
				CancelsTouchesInView = false
			});

			HeaderBar.BackButtonIsShown = true;

			_profileView = UIHelper.CreateView(DimensionHelper.ProfileEditSize, ResolutionHelper.Width, ColorHelper.Line);

			View.Add(_profileView);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_profileView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, ResolutionHelper.StatusHeight + DimensionHelper.HeaderBarHeight),
				NSLayoutConstraint.Create(_profileView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0)
			});

			_avatarView = UIHelper.CreateCustomImageView(DimensionHelper.ProfileSize, DimensionHelper.ProfileSize,
				ImageHelper.DefaultAvatar, DimensionHelper.ProfileSize / 2);

			_profileView.Add(_avatarView);

			_profileView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_avatarView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _profileView,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_avatarView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _profileView,
					NSLayoutAttribute.CenterY, 1, 0)
			});

			_cameraButton = UIHelper.CreateImageView(DimensionHelper.PopupButtonHeight,
				DimensionHelper.PopupButtonHeight, ImageHelper.Camera, DimensionHelper.PopupButtonHeight / 2);

			_profileView.Add(_cameraButton);

			_profileView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_cameraButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _avatarView,
					NSLayoutAttribute.Top, 1, DimensionHelper.AvatarBigSize),
				NSLayoutConstraint.Create(_cameraButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _avatarView,
					NSLayoutAttribute.Left, 1, DimensionHelper.AvatarBigSize)
			});

			_userNameLabel = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);

			View.Add(_userNameLabel);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_userNameLabel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _profileView,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
				NSLayoutConstraint.Create(_userNameLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, DimensionHelper.MediumMargin)
			});

			_userDisplayNameTextField = UIHelper.CreateTextField(DimensionHelper.DropDownButtonHeight, 0,
				ColorHelper.LightGray, ColorHelper.Gray, DimensionHelper.RoundCorner, DimensionHelper.MediumTextSize, FontType.Light);

			var paddingView = new UIView(new CGRect(0, 0, 15, _userDisplayNameTextField.Frame.Height));
			_userDisplayNameTextField.LeftView = paddingView;
			_userDisplayNameTextField.LeftViewMode = UITextFieldViewMode.Always;

			View.Add(_userDisplayNameTextField);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_userDisplayNameTextField, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _userNameLabel,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_userDisplayNameTextField, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_userDisplayNameTextField, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MediumMargin)
			});

			_btnCancel = UIHelper.CreateAlphaButton(0, DimensionHelper.CreatePostButtonHeight,
				ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
				UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
				true, true, FontType.Light);

			View.Add(_btnCancel);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _userDisplayNameTextField,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, - DimensionHelper.DefaultMargin),
			});

			_btnSubmit = UIHelper.CreateAlphaButton(0, DimensionHelper.CreatePostButtonHeight,
				UIColor.White, UIColor.White, DimensionHelper.MediumTextSize,
				ColorHelper.LightBlue, ColorHelper.DarkBlue, ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);

			View.Add(_btnSubmit);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _btnCancel,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.DefaultMargin),
			});
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var set = this.CreateBindingSet<ProfileEditorView, ProfileEditorViewModel>();

			set.Bind(HeaderBar)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);

			set.Bind(_avatarView)
				.For(v => v.ImageUrl)
				.To(vm => vm.AvatarUrl);

			set.Bind(_avatarView)
				.For(v => v.Base64String)
				.To(vm => vm.Base64String);

			set.Bind(_avatarView.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowFullSizeAvatarImageCommand);

			set.Bind(_userNameLabel)
				.To(vm => vm.UserNameLabel);

			set.Bind(_userDisplayNameTextField)
				.To(vm => vm.UserName);

			set.Bind(_btnCancel)
				.For("Title")
				.To(vm => vm.BtnCancelTitle);

			set.Bind(_btnSubmit)
				.For("Title")
				.To(vm => vm.BtnSaveTitle);

			set.Bind(_cameraButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.CameraButtonClickCommand);

			set.Bind(_btnCancel.Tap())
				.For(v => v.Command)
				.To(vm => vm.BackPressedCommand);

			set.Bind(_btnSubmit.Tap())
				.For(v => v.Command)
				.To(vm => vm.SaveCommand);

			set.Apply();
		}

		private void HideKeyboard() => KeyboardHelper.HideKeyboard();
	}
}