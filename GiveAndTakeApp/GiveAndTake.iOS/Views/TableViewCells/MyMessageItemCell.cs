﻿using System;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;

namespace GiveAndTake.iOS.Views.TableViewCells
{
	public class MyMessageItemCell : MvxTableViewCell
	{
		private UILabel _message;

		public MyMessageItemCell(IntPtr handle) : base(handle)
		{
			InitView();
			CreateBinding();
		}

		private void CreateBinding()
		{
			var set = this.CreateBindingSet<MyMessageItemCell, MessageItemViewModel>();

			set.Bind(_message)
				.To(vm => vm.MessageContent);

			set.Apply();
		}

		private void InitView()
		{
			var messageView = UIHelper.CreateView(0, 0, ColorHelper.Gray, DimensionHelper.DefaultCornerRadius);

			ContentView.AddSubview(messageView);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(messageView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Top, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(messageView, NSLayoutAttribute.Width, NSLayoutRelation.LessThanOrEqual,
					ContentView, NSLayoutAttribute.Width, 1,
					-DimensionHelper.MessageMarginLeft - DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(messageView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(messageView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MarginNormal),
			});

			_message = UIHelper.CreateLabel(UIColor.White, DimensionHelper.PostDetailNormalTextSize);

			messageView.AddSubview(_message);
			messageView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_message, NSLayoutAttribute.Top, NSLayoutRelation.Equal, messageView,
					NSLayoutAttribute.Top, 1, DimensionHelper.TextPaddingShort),
				NSLayoutConstraint.Create(_message, NSLayoutAttribute.Left, NSLayoutRelation.Equal, messageView,
					NSLayoutAttribute.Left, 1, DimensionHelper.TextPaddingMedium),
				NSLayoutConstraint.Create(_message, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, messageView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.TextPaddingShort),
				NSLayoutConstraint.Create(_message, NSLayoutAttribute.Right, NSLayoutRelation.Equal, messageView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.TextPaddingMedium),
			});
		}
	}
}