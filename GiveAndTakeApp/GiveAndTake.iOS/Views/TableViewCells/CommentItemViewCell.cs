﻿using System;
using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using UIKit;

namespace GiveAndTake.iOS.Views.TableViewCells
{
	[Register(nameof(CommentItemViewCell))]
	public class CommentItemViewCell : MvxTableViewCell
	{
		private CustomMvxCachedImageView _avatarIcon;
		private UILabel _userName;
		private UILabel _dateCreated;
		private UILabel _commentContent;
		private UIView _showMoreOptionViewTouch;
		private UIImageView _showMoreOptionImg;

		public CommentItemViewCell(IntPtr handle) : base(handle)
		{
			InitViews();
			CreateBinding();
		}

		private void CreateBinding()
		{
			var set = this.CreateBindingSet<CommentItemViewCell, CommentItemViewModel>();

			set.Bind(_avatarIcon.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowUserProfileCommamd);

			set.Bind(_avatarIcon)
				.For(v => v.ImageUrl)
				.To(vm => vm.AvatarUrl);

			set.Bind(_userName)
				.For(v => v.Text)
				.To(vm => vm.UserName);

			set.Bind(_dateCreated)
				.For(v => v.Text)
				.To(vm => vm.CreatedTime);

			set.Bind(_commentContent)
				.For(v => v.Text)
				.To(vm => vm.CommentMessage);

			set.Bind(_showMoreOptionImg)
				.For("Visibility")
				.To(vm => vm.IsOptionsMenuShown)
				.WithConversion("InvertBool");

			set.Bind(_showMoreOptionViewTouch)
				.For("Visibility")
				.To(vm => vm.IsOptionsMenuShown)
				.WithConversion("InvertBool");

			set.Bind(_showMoreOptionViewTouch.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowMenuPopupCommand);

			set.Apply();
		}

		private void InitViews()
		{

			_avatarIcon = UIHelper.CreateCustomImageView(DimensionHelper.ImageAvatarSize, DimensionHelper.ImageAvatarSize, ImageHelper.DefaultAvatar, DimensionHelper.ImageAvatarSize / 2);
			_avatarIcon.SetPlaceHolder(ImageHelper.DefaultAvatar, ImageHelper.DefaultAvatar);
			ContentView.AddSubview(_avatarIcon);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_avatarIcon, NSLayoutAttribute.Top, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Top, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_avatarIcon, NSLayoutAttribute.Left, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort)
			});

			_userName = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.PostDetailNormalTextSize, FontType.Medium);
			ContentView.AddSubview(_userName);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_userName, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _avatarIcon,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_userName, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _avatarIcon,
					NSLayoutAttribute.Right, 1, DimensionHelper.MarginNormal)
			});

			_dateCreated = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.SmallTextSize);
			ContentView.AddSubview(_dateCreated);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_dateCreated, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _userName,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_dateCreated, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _avatarIcon,
					NSLayoutAttribute.Right, 1, DimensionHelper.MarginNormal)
			});


			_commentContent = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.PostDescriptionTextSize);
			_commentContent.Lines = 0;
			ContentView.AddSubview(_commentContent);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_commentContent, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _dateCreated,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_commentContent, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _userName,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_commentContent, NSLayoutAttribute.Right, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MarginObjectPostDetail),
				NSLayoutConstraint.Create(ContentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _commentContent,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal)
			});

			_showMoreOptionImg = UIHelper.CreateImageView(DimensionHelper.ButtonExtensionHeight, DimensionHelper.ButtonExtensionWidth, ImageHelper.Extension);
			ContentView.AddSubview(_showMoreOptionImg);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_showMoreOptionImg, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _userName,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_showMoreOptionImg, NSLayoutAttribute.Right, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MarginNormal)
			});

			_showMoreOptionViewTouch = UIHelper.CreateView(DimensionHelper.ButtonExtensionHeight * 4, DimensionHelper.ButtonExtensionWidth * 4);
			ContentView.AddSubview(_showMoreOptionViewTouch);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_showMoreOptionViewTouch, NSLayoutAttribute.Top, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_showMoreOptionViewTouch, NSLayoutAttribute.Right, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Right, 1, 0)
			});
		}
	}
}