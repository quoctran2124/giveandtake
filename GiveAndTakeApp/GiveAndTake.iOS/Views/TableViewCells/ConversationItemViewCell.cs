﻿using System;
using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Plugin.Color;
using UIKit;

namespace GiveAndTake.iOS.Views.TableViewCells
{
	[Register(nameof(ConversationItemViewCell))]
	public class ConversationItemViewCell : MvxTableViewCell
	{
		private CustomMvxCachedImageView _imgAvatar;
		private UILabel _lbUserName;
		private UILabel _lbCreatedTime;
		private UILabel _lbMessage;
		private UIImageView _imgExtension;

		private UIView _optionView;

		public ConversationItemViewCell(IntPtr handle) : base(handle)
		{
			InitViews();
			CreateBinding();
		}

		private void CreateBinding()
		{
			var set = this.CreateBindingSet<ConversationItemViewCell, ConversationItemViewModel>();

			set.Bind(ContentView.Tap())
				.For(v => v.Command)
				.To(vm => vm.ClickCommand);

			set.Bind(_imgAvatar)
				.For(v => v.ImageUrl)
				.To(vm => vm.AvatarUrl);

			set.Bind(_lbUserName)
				.To(vm => vm.UserName);

			set.Bind(_lbMessage)
				.To(vm => vm.MessageContent);

			set.Bind(_lbCreatedTime)
				.To(vm => vm.CreatedTime);

			set.Bind(_optionView.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowMenuPopupCommand);

			set.Bind(ContentView)
				.For(v => v.BackgroundColor)
				.To(vm => vm.BackgroundColor)
				.WithConversion(new MvxNativeColorValueConverter());

			set.Apply();
		}

		private void InitViews()
		{
			InitExtensionIcon();
			InitContent();
		}

		private void InitExtensionIcon()
		{
			_optionView = UIHelper.CreateView(0, DimensionHelper.BigButtonExtensionWidth * 1.5f);

			ContentView.AddSubview(_optionView);

			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_optionView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_optionView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_optionView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Right, 1, 0)
			});

			_imgExtension = UIHelper.CreateImageView(DimensionHelper.BigButtonExtensionHeight, DimensionHelper.BigButtonExtensionWidth, ImageHelper.Extension);

			_optionView.AddSubview(_imgExtension);

			_optionView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imgExtension, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _optionView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_imgExtension, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _optionView,
					NSLayoutAttribute.Right, 1, - DimensionHelper.MarginShort)
			});
		}

		private void InitContent()
		{
			_imgAvatar = UIHelper.CreateCustomImageView(DimensionHelper.PostDetailAvatarSize,
							DimensionHelper.PostDetailAvatarSize, ImageHelper.DefaultAvatar,
							DimensionHelper.PostDetailAvatarSize / 2);
			ContentView.AddSubview(_imgAvatar);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imgAvatar, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					ContentView,
					NSLayoutAttribute.Top, 1, DimensionHelper.MarginObjectPostDetail),
				NSLayoutConstraint.Create(_imgAvatar, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					ContentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.MarginObjectPostDetail)
			});

			_lbUserName = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.PostDetailNormalTextSize);
			_lbUserName.Lines = 1;
			_lbUserName.LineBreakMode = UILineBreakMode.TailTruncation;
			ContentView.AddSubview(_lbUserName);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbUserName, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_imgAvatar,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbUserName, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imgAvatar,
					NSLayoutAttribute.Right, 1, DimensionHelper.MarginNormal)
			});

			_lbMessage = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.PostDetailNormalTextSize);
			_lbMessage.Lines = 1;
			_lbMessage.LineBreakMode = UILineBreakMode.TailTruncation;
			ContentView.AddSubview(_lbMessage);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbMessage, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbUserName,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_lbMessage, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbUserName,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbMessage, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _optionView,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginShort)
			});

			_lbCreatedTime = UIHelper.CreateLabel(UIColor.Gray, DimensionHelper.PostDetailSmallTextSize, FontType.Light);
			ContentView.AddSubview(_lbCreatedTime);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbCreatedTime, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbUserName,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbCreatedTime, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _optionView,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginShort)
			});
		}
	}
}