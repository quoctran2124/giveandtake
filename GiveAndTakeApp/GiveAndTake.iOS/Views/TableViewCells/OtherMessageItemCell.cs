﻿using System;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;

namespace GiveAndTake.iOS.Views.TableViewCells
{
	public class OtherMessageItemCell : MvxTableViewCell
	{
		private CustomMvxCachedImageView _avatarIcon;
		private UILabel _message;
		private UIView _messageView;

		private bool _isBlocked;
		public bool IsBlocked
		{
			get => _isBlocked;
			set
			{
				_isBlocked = value;
				_messageView.BackgroundColor = (value) ? ColorHelper.Gray : ColorHelper.BlueMessageBackground;
			}
		}
		public OtherMessageItemCell(IntPtr handle) : base(handle)
		{
			InitView();
			CreateBinding();
		}

		private void CreateBinding()
		{
			var set = this.CreateBindingSet<OtherMessageItemCell, MessageItemViewModel>();

			set.Bind(_message)
				.To(vm => vm.MessageContent);

			set.Bind(_avatarIcon)
				.For(v => v.ImageUrl)
				.To(vm => vm.AvatarUrl);

			set.Bind(this)
				.For(v => v.IsBlocked)
				.To(vm => vm.IsBlocked);

			set.Bind(_avatarIcon)
				.For("Visibility")
				.To(vm => vm.IsAvatarShown)
				.WithConversion("InvertBool");

			set.Apply();
		}

		private void InitView()
		{
			_avatarIcon = UIHelper.CreateCustomImageView(DimensionHelper.PopupRequestGiverAvartarSize,
				DimensionHelper.PopupRequestGiverAvartarSize, ImageHelper.DefaultAvatar,
				DimensionHelper.PopupRequestGiverAvartarSize / 2);

			ContentView.AddSubview(_avatarIcon);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_avatarIcon, NSLayoutAttribute.Top, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Top, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_avatarIcon, NSLayoutAttribute.Left, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.MarginNormal),
			});

			_messageView = UIHelper.CreateView(0, 0, ColorHelper.BlueMessageBackground, DimensionHelper.DefaultCornerRadius);

			ContentView.AddSubview(_messageView);
			ContentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_messageView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _avatarIcon,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_messageView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _avatarIcon,
					NSLayoutAttribute.Right, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_messageView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, ContentView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_messageView, NSLayoutAttribute.Width, NSLayoutRelation.LessThanOrEqual,
					ContentView, NSLayoutAttribute.Width, 1,
					-DimensionHelper.MessageMarginLeft - DimensionHelper.MarginNormal),
			});

			_message = UIHelper.CreateLabel(UIColor.White, DimensionHelper.PostDetailNormalTextSize);

			_messageView.AddSubview(_message);
			_messageView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_message, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _messageView,
					NSLayoutAttribute.Top, 1, DimensionHelper.TextPaddingShort),
				NSLayoutConstraint.Create(_message, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _messageView,
					NSLayoutAttribute.Left, 1, DimensionHelper.TextPaddingMedium),
				NSLayoutConstraint.Create(_message, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _messageView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.TextPaddingShort),
				NSLayoutConstraint.Create(_message, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _messageView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.TextPaddingMedium),
			});
		}
	}
}