﻿using GiveAndTake.Core.ViewModels.Popup;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views.Popups
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class PopupAlertView : BaseView
	{
		private UIView _contentView;
		private UIView _overlayView;
		private UILabel _titleLabel;
		private UILabel _messageLabel;
		private UIButton _cancelButton;
		private UIButton _submitButton;
		private UIStackView _popupMessageView;
		private UIStackView _buttonsStackView;

		protected override void InitView()
		{
			HeaderBar.Hidden = true;
			View.BackgroundColor = UIColor.Clear;

			_overlayView = UIHelper.CreateView(0,0, UIColor.Black.ColorWithAlpha(0.7f));

			View.Add(_overlayView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, View, NSLayoutAttribute.Height, 1, 0),
			});

			_contentView = UIHelper.CreateView(0, DimensionHelper.PopupContentWidth, UIColor.White, DimensionHelper.PopupContentRadius);
			_contentView.AddGestureRecognizer(new UITapGestureRecognizer { CancelsTouchesInView = true });

			_overlayView.Add(_contentView);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterY, 1, 0)
			});

			_titleLabel = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.MediumTextSize);
			_titleLabel.TextAlignment = UITextAlignment.Center;

			_contentView.Add(_titleLabel);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_titleLabel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Top, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_titleLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.CenterX, 1, 0)
			});

			_popupMessageView = UIHelper.CreateStackView(0, DimensionHelper.PopupContentWidth);
			_popupMessageView.Distribution = UIStackViewDistribution.EqualSpacing;
			_popupMessageView.Spacing = DimensionHelper.DefaultMargin;
			_popupMessageView.Axis = UILayoutConstraintAxis.Horizontal;

			_contentView.AddSubview(_popupMessageView);

			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_popupMessageView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _titleLabel,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_popupMessageView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _titleLabel,
					NSLayoutAttribute.CenterX, 1, 0)
			});

			_messageLabel = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);
			_messageLabel.TextAlignment = UITextAlignment.Center;
			_popupMessageView.AddArrangedSubview(_messageLabel);

			_buttonsStackView = UIHelper.CreateStackView(0, 0);
			_buttonsStackView.Distribution = UIStackViewDistribution.EqualSpacing;
			_buttonsStackView.Spacing = DimensionHelper.DefaultMargin;
			_buttonsStackView.Axis = UILayoutConstraintAxis.Horizontal;

			_contentView.AddSubview(_buttonsStackView);

			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_buttonsStackView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _popupMessageView,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_buttonsStackView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _buttonsStackView,
					NSLayoutAttribute.Bottom, 1,  DimensionHelper.MediumMargin)
			});

			_cancelButton = UIHelper.CreateButton(DimensionHelper.PopupButtonHeight,
				DimensionHelper.PopupMessageButtonWidth,
				UIColor.White,
				ColorHelper.Blue,
				DimensionHelper.PopupMediumTextSize,
				null,
				DimensionHelper.PopupButtonHeight / 2,
				ColorHelper.Blue,
				DimensionHelper.PopupCancelButtonBorder);

			_submitButton = UIHelper.CreateButton(DimensionHelper.PopupButtonHeight,
				DimensionHelper.PopupMessageButtonWidth,
				ColorHelper.Blue,
				UIColor.White,
				DimensionHelper.PopupMediumTextSize,
				DimensionHelper.PopupButtonHeight / 2);

			_buttonsStackView.AddArrangedSubview(_cancelButton);
			_buttonsStackView.AddArrangedSubview(_submitButton);
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<PopupAlertView, PopupAlertViewModel>();

			bindingSet.Bind(_titleLabel)
				.To(vm => vm.Title);

			bindingSet.Bind(_messageLabel)
				.To(vm => vm.Message);

			bindingSet.Bind(_submitButton)
				.For("Title")
				.To(vm => vm.SubmitButtonTitle);

			bindingSet.Bind(_cancelButton)
				.For("Title")
				.To(vm => vm.CancelButtonTitle);

			bindingSet.Bind(_overlayView.Tap())
				.For(v => v.Command)
				.To(vm => vm.CancelCommand);

			bindingSet.Bind(_cancelButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.CancelCommand);

			bindingSet.Bind(_submitButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.SubmitCommand);

			bindingSet.Bind(_cancelButton)
				.For("Visibility")
				.To(vm => vm.HasCancelButton)
				.WithConversion("InvertBool");

			bindingSet.Apply();
		}
	}
}