﻿using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.Popup;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views.Popups
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class PopupReportView : BaseView
	{
		private UIView _overlayView;
		private UIView _contentView;
		private UILabel _lbPopupTitle;
		private RadioButton _radBtnInappropriateContentReason;
		private RadioButton _radBtnInappropriatePictureReason;
		private RadioButton _radBtnFakeContentReason;
		private RadioButton _radBtnOtherReason;
		private PlaceholderTextView _otherReasonContent;
		private UIButton _btnClose;
		private UIButton _btnCancel;
		private UIButton _btnSubmit;

		protected override void InitView()
		{
			View.BackgroundColor = UIColor.Clear;
			InitOverlayView();
			InitContentView();
		}

		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<PopupReportView, PopupReportViewModel>();

			bindingSet.Bind(_lbPopupTitle)
				.To(vm => vm.PopupTitle);

			bindingSet.Bind(_radBtnInappropriateContentReason)
				.For(v => v.Title)
				.To(vm => vm.InappropriateContentReason);

			bindingSet.Bind(_radBtnInappropriateContentReason)
				.For(v => v.Checked)
				.To(vm => vm.IsInappropriateContentReasonChecked);

			bindingSet.Bind(_radBtnInappropriateContentReason)
				.For(v => v.ClickCommand)
				.To(vm => vm.InappropriateContentClickCommand)
				.CommandParameter(PopupReason.InappropriateContentReason);

			bindingSet.Bind(_radBtnInappropriatePictureReason)
				.For(v => v.Title)
				.To(vm => vm.InappropriatePictureReason);

			bindingSet.Bind(_radBtnInappropriatePictureReason)
				.For(v => v.Checked)
				.To(vm => vm.IsInappropriatePictureReasonChecked);

			bindingSet.Bind(_radBtnInappropriatePictureReason)
				.For(v => v.ClickCommand)
				.To(vm => vm.InappropriatePictureClickCommand)
				.CommandParameter(PopupReason.InappropriatePictureReason);

			bindingSet.Bind(_radBtnFakeContentReason)
				.For(v => v.Title)
				.To(vm => vm.FakeContentReason);

			bindingSet.Bind(_radBtnFakeContentReason)
				.For(v => v.Checked)
				.To(vm => vm.IsFakeContentReasonChecked);

			bindingSet.Bind(_radBtnFakeContentReason)
				.For(v => v.ClickCommand)
				.To(vm => vm.FakeContentClickCommand)
				.CommandParameter(PopupReason.FakeContentReason);

			bindingSet.Bind(_radBtnOtherReason)
				.For(v => v.Title)
				.To(vm => vm.OtherReason);

			bindingSet.Bind(_radBtnOtherReason)
				.For(v => v.Checked)
				.To(vm => vm.IsOtherReasonChecked);

			bindingSet.Bind(_radBtnOtherReason)
				.For(v => v.ClickCommand)
				.To(vm => vm.OtherClickCommand)
				.CommandParameter(PopupReason.OtherReason);

			bindingSet.Bind(_otherReasonContent)
				.For(v => v.Placeholder)
				.To(vm => vm.TextHint);

			bindingSet.Bind(_otherReasonContent)
				.For(v => v.Text)
				.To(vm => vm.OtherReasonContent);

			bindingSet.Bind(_otherReasonContent)
				.For(v => v.Editable)
				.To(vm => vm.IsOtherReasonChecked);

			bindingSet.Bind(_btnClose.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Bind(_btnCancel)
				.For("Title")
				.To(vm => vm.BtnCancelTitle);

			bindingSet.Bind(_btnCancel.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Bind(_btnSubmit)
				.For("Title")
				.To(vm => vm.BtnSubmitTitle);

			bindingSet.Bind(_btnSubmit.Tap())
				.For(v => v.Command)
				.To(vm => vm.SubmitCommand);

			bindingSet.Apply();
		}

		private void InitOverlayView()
		{
			_overlayView = UIHelper.CreateView(0, 0, UIColor.Black.ColorWithAlpha(0.7f));

			View.Add(_overlayView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, View, NSLayoutAttribute.Height, 1, 0),
			});
		}

		private void InitContentView()
		{
			_contentView = UIHelper.CreateView(DimensionHelper.PopupRequestHeight, DimensionHelper.PopupContentWidth, UIColor.White, DimensionHelper.PopupContentRadius);
			_contentView.AddGestureRecognizer(new UITapGestureRecognizer { CancelsTouchesInView = true });

			_overlayView.Add(_contentView);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterY, 1, 0)
			});

			_lbPopupTitle = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.PopupRequestTitleTextSize);
			_contentView.Add(_lbPopupTitle);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbPopupTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbPopupTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Top, 1, DimensionHelper.MediumMargin)
			});
			_btnClose = UIHelper.CreateImageButton(DimensionHelper.DeletePhotoButtonWidth,
				DimensionHelper.DeletePhotoButtonWidth, ImageHelper.DeleteRequestDetailButton);
			_contentView.AddSubview(_btnClose);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnClose, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbPopupTitle,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_btnClose, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin)
			});

			_radBtnInappropriateContentReason = UIHelper.CreateRadioButton(0);

			_contentView.Add(_radBtnInappropriateContentReason);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_radBtnInappropriateContentReason, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbPopupTitle,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_radBtnInappropriateContentReason, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.BigMargin)
			});

			_radBtnInappropriatePictureReason = UIHelper.CreateRadioButton(0);
			_contentView.Add(_radBtnInappropriatePictureReason);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_radBtnInappropriatePictureReason, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _radBtnInappropriateContentReason,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_radBtnInappropriatePictureReason, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _radBtnInappropriateContentReason,
					NSLayoutAttribute.Left, 1, 0)
			});

			_radBtnFakeContentReason = UIHelper.CreateRadioButton(0);
			_contentView.Add(_radBtnFakeContentReason);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_radBtnFakeContentReason, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _radBtnInappropriatePictureReason,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_radBtnFakeContentReason, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _radBtnInappropriatePictureReason,
					NSLayoutAttribute.Left, 1, 0)
			});

			_radBtnOtherReason = UIHelper.CreateRadioButton(0);
			_contentView.Add(_radBtnOtherReason);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_radBtnOtherReason, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _radBtnFakeContentReason,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_radBtnOtherReason, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _radBtnFakeContentReason,
					NSLayoutAttribute.Left, 1, 0)
			});

			_btnCancel = UIHelper.CreateAlphaButton(0,
				DimensionHelper.PopupRequestButtonHeight,
				ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
				UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
				true, true, FontType.Light);
			_contentView.Add(_btnCancel);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.DefaultMargin/2)
			});

			_btnSubmit = UIHelper.CreateAlphaButton(0,
				DimensionHelper.PopupRequestButtonHeight,
				UIColor.White, UIColor.White, DimensionHelper.MediumTextSize,
				ColorHelper.LightBlue, ColorHelper.DarkBlue, ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);
			_contentView.Add(_btnSubmit);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _btnCancel,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.DefaultMargin/2),
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
			});

			_otherReasonContent = UIHelper.CreateTextView(0, DimensionHelper.PopupRequestGiverInformationViewWidth,
				ColorHelper.LightGray, ColorHelper.Gray, DimensionHelper.RoundCorner, ColorHelper.Gray, DimensionHelper.MediumTextSize, FontType.Light);
			_contentView.Add(_otherReasonContent);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_otherReasonContent, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _radBtnOtherReason,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_otherReasonContent, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _btnCancel,
					NSLayoutAttribute.Top, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_otherReasonContent, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_otherReasonContent, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, - DimensionHelper.DefaultMargin),

			});
		}
	}
}