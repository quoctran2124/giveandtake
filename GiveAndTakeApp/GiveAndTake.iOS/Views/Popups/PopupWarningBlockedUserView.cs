﻿using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Popup;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Interfaces;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views.Popups
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class PopupWarningBlockedUserView : BaseView, ITextViewDelegate
	{
		private UIView _contentView;
		private UIView _overlayView;
		private UITextView _messageTextView;
		private UIButton _closeButton;
		private string _htmlContent;

		public string HtmlContent
		{
			get => _htmlContent;
			set
			{
				var colorKey = UIStringAttributeKey.ForegroundColor;
				var colorValue = ColorHelper.LightBlue;
				var styleUnderlineKey = UIStringAttributeKey.UnderlineStyle;
				var styleUnderlineValue = new NSNumber(-1); // 0 without underline 1 with underline
				var dict = new NSDictionary(colorKey, colorValue, styleUnderlineKey, styleUnderlineValue);

				_htmlContent = value;
				_messageTextView.AttributedText =
					UIHelper.GetAttributedStringFromHtml(value, DimensionHelper.MediumTextSize);
				_messageTextView.TextAlignment = UITextAlignment.Center;

				_messageTextView.Delegate = new DisabledInteractionTextViewDelegate(this);
				_messageTextView.Editable = false;
				_messageTextView.ScrollEnabled = false;
				_messageTextView.TextContainerInset = UIEdgeInsets.Zero;
				_messageTextView.ContentInset = UIEdgeInsets.Zero;
				_messageTextView.ClipsToBounds = false;
				_messageTextView.ScrollIndicatorInsets = UIEdgeInsets.Zero;
				_messageTextView.WeakLinkTextAttributes = dict;
			}
		}

		public IMvxCommand MailingCommand { get; set; }

		protected override void InitView()
		{
			HeaderBar.Hidden = true;
			View.BackgroundColor = UIColor.Clear;

			_overlayView = UIHelper.CreateView(0, 0, UIColor.Black.ColorWithAlpha(0.7f));

			View.Add(_overlayView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, View, NSLayoutAttribute.Height, 1, 0),
			});

			_contentView = UIHelper.CreateView(0, DimensionHelper.PopupContentWidth, UIColor.White, DimensionHelper.PopupContentRadius);
			_contentView.AddGestureRecognizer(new UITapGestureRecognizer { CancelsTouchesInView = true });

			_overlayView.Add(_contentView);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterY, 1, 0)
			});

			_messageTextView = UIHelper.CreateDisabledInteractionTextView(ColorHelper.Black, 0);

			_contentView.Add(_messageTextView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_messageTextView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Top, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_messageTextView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.CenterX, 1, 0)
			});

			_closeButton = UIHelper.CreateButton(DimensionHelper.PopupButtonHeight,
				DimensionHelper.PopupMessageButtonWidth,
				ColorHelper.Blue,
				UIColor.White,
				DimensionHelper.SmallTextSize,
				DimensionHelper.PopupButtonHeight / 2);

			_contentView.Add(_closeButton);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_closeButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _messageTextView, NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_closeButton, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _closeButton, NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal)
			});
		}

		public bool ShouldInteractWithUrl(UITextView uiTextView, NSUrl nsUrl, NSRange arg3)
		{
			MailingCommand?.Execute();
			return true;
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<PopupWarningBlockedUserView, PopupWarningBlockedUserViewModel>();

			bindingSet.Bind(_closeButton)
				.For("Title")
				.To(vm => vm.CloseButtonTitle);

			bindingSet.Bind(_overlayView.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Bind(_closeButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Bind(this)
				.For(v => v.MailingCommand)
				.To(vm => vm.MailingCommand);

			bindingSet.Bind(this)
				.For(v => v.HtmlContent)
				.To(vm => vm.HtmlContent);

			bindingSet.Apply();
		}
	}
}