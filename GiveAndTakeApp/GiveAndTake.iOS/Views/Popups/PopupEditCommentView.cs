﻿using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.Popup;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views.Popups
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class PopupEditCommentView : BaseView
	{
		private UIView _backgroundView;
		private UIView _overlayView;
		private UILabel _lbPopupTitle;
		private PlaceholderTextView _commentTextView;
		private UIButton _btnCancel;
		private UIButton _btnSubmit;

		protected override void InitView()
		{
			HeaderBar.Hidden = true;
			View.BackgroundColor = UIColor.Clear;
			InitOverlayView();
			InitContentView();
		}
		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<PopupEditCommentView, PopupEditCommentViewModel>();

			bindingSet.Bind(_lbPopupTitle)
				.For(v => v.Text)
				.To(vm => vm.PopupTitle);

			bindingSet.Bind(_commentTextView)
				.For(v => v.Text)
				.To(vm => vm.CommentMessage);

			bindingSet.Bind(_btnCancel)
				.For("Title")
				.To(vm => vm.BtnCancelTitle);

			bindingSet.Bind(_btnSubmit)
				.For("Title")
				.To(vm => vm.BtnSubmitTitle);

			bindingSet.Bind(_btnSubmit.Tap())
				.For(v => v.Command)
				.To(vm => vm.SubmitCommand);

			bindingSet.Bind(_btnSubmit)
				.For("Enabled")
				.To(vm => vm.IsSubmitBtnEnabled);

			bindingSet.Bind(_btnCancel.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Apply();
		}

		private void InitOverlayView()
		{
			_overlayView = UIHelper.CreateView(0, 0, UIColor.Black.ColorWithAlpha(0.7f));

			View.Add(_overlayView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, View, NSLayoutAttribute.Height, 1, 0),
			});
		}

		private void InitContentView()
		{

			_commentTextView = UIHelper.CreateTextView(DimensionHelper.PopupRequestDescriptionTextViewHeight, DimensionHelper.PopupRequestGiverInformationViewWidth,
				ColorHelper.LightGray, ColorHelper.Gray, DimensionHelper.RoundCorner, ColorHelper.Gray, DimensionHelper.MediumTextSize, FontType.Light);
			_overlayView.AddSubview(_commentTextView);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_commentTextView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _overlayView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_commentTextView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _overlayView,
					NSLayoutAttribute.CenterX, 1, 0)
			});

			_lbPopupTitle = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.PopupRequestTitleTextSize);
			_overlayView.AddSubview(_lbPopupTitle);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbPopupTitle, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _commentTextView, NSLayoutAttribute.Top, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_lbPopupTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _overlayView,
					NSLayoutAttribute.CenterX, 1, 0)
			});

			_btnCancel = UIHelper.CreateAlphaButton(DimensionHelper.PopupRequestButtonWidth,
				DimensionHelper.CreatePostButtonHeight,
				ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
				UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
				true, true, FontType.Light);
			_overlayView.AddSubview(_btnCancel);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _commentTextView,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _commentTextView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin)
			});

			_btnSubmit = UIHelper.CreateAlphaButton(DimensionHelper.PopupRequestButtonWidth,
				DimensionHelper.CreatePostButtonHeight,
				UIColor.White, UIColor.White, DimensionHelper.MediumTextSize,
				ColorHelper.LightBlue, ColorHelper.DarkBlue, ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);
			_overlayView.Add(_btnSubmit);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _commentTextView,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _commentTextView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin)
			});



			_backgroundView = UIHelper.CreateView(0, DimensionHelper.PopupContentWidth, UIColor.White, DimensionHelper.PopupContentRadius);
			_backgroundView.AddGestureRecognizer(new UITapGestureRecognizer { CancelsTouchesInView = true });		
			_overlayView.Add(_backgroundView);
			_overlayView.SendSubviewToBack(_backgroundView);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_backgroundView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbPopupTitle, NSLayoutAttribute.Top, 1, -DimensionHelper.DefaultMargin),			
				NSLayoutConstraint.Create(_backgroundView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _btnSubmit, NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_backgroundView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _commentTextView, NSLayoutAttribute.Left, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_backgroundView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _commentTextView, NSLayoutAttribute.Right, 1, DimensionHelper.DefaultMargin)
			});

		}
	}
}