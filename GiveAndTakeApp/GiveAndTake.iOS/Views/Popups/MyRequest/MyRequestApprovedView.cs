﻿using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.Popup.MyRequest;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views.Popups.MyRequest
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class MyRequestApprovedView : BaseRequestApprovedView
	{
		private UIButton _btnReceived;
		private AlphaUiButton _btnRemoveRequest;
		private UIView _bottomPopupView;

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			_scrollViewHeightConstraint.Constant = _contentScrollView.Frame.Height > DimensionHelper.PopupRequestScrollApprovedViewMaxHeight ? DimensionHelper.PopupRequestScrollApprovedViewMaxHeight : _contentScrollView.Frame.Height;
			_scrollView.ContentSize = _contentScrollView.Frame.Size;
			_contentViewHeightConstraint.Constant = _headerPopupView.Frame.Height + _scrollViewHeightConstraint.Constant + _bottomPopupView.Frame.Height;
		}

		protected override void InitView()
		{
			base.InitView();
			InitActionButtonView();
		}
		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<MyRequestApprovedView, MyRequestApprovedViewModel>();

			bindingSet.Bind(_btnReceived)
				.For("Title")
				.To(vm => vm.BtnReceivedTitle);

			bindingSet.Bind(_btnRemoveRequest)
				.For("Title")
				.To(vm => vm.BtnRemoveRequestTitle);

			bindingSet.Bind(_btnReceived.Tap())
				.For(v => v.Command)
				.To(vm => vm.ReceivedCommand);

			bindingSet.Bind(_btnRemoveRequest.Tap())
				.For(v => v.Command)
				.To(vm => vm.RemoveRequestCommand);
			bindingSet.Apply();
		}

		private void InitActionButtonView()
		{
			_bottomPopupView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_contentView.AddSubview(_bottomPopupView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
			});

			_btnRemoveRequest = UIHelper.CreateAlphaButton(0,
					DimensionHelper.PopupRequestButtonHeight,
					ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
					UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
					true, true, FontType.Light);
			_bottomPopupView.Add(_btnRemoveRequest);
			_bottomPopupView.AddConstraints(new[]
				{
				NSLayoutConstraint.Create(_btnRemoveRequest, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.Top, 1, DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_btnRemoveRequest, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_btnRemoveRequest, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.DefaultMargin/2),
				});

			_btnReceived = UIHelper.CreateAlphaButton(0,
					DimensionHelper.PopupRequestButtonHeight,
					UIColor.White, UIColor.White, DimensionHelper.MediumTextSize,
					ColorHelper.LightBlue, ColorHelper.DarkBlue, ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);
			_bottomPopupView.Add(_btnReceived);
			_bottomPopupView.AddConstraints(new[]
				{
				NSLayoutConstraint.Create(_btnReceived, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _btnRemoveRequest,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_btnReceived, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.DefaultMargin/2),
				NSLayoutConstraint.Create(_btnReceived, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _btnRemoveRequest,
					NSLayoutAttribute.Bottom, 1, 0),
				});
		}
	}
}