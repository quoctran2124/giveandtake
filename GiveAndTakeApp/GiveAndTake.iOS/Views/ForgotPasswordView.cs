using CoreGraphics;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;
using ColorHelper = GiveAndTake.iOS.Helpers.ColorHelper;

namespace GiveAndTake.iOS.Views
{
    [MvxRootPresentation]
	public class ForgotPasswordView : BaseView
	{
		private UIImageView _logoImage;
		private UIImageView _backgroundImage;
        private UITextField _phoneNumberEdt;
        private PopupItemLabel _otpTitle;
        private PopupItemLabel _otpDetail;
        private AlphaUiButton _btnCancel;
        private AlphaUiButton _btnSubmit;

        protected override void InitView()
		{
			InitBackground();
			InitContent();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var set = this.CreateBindingSet<ForgotPasswordView, ForgotPasswordViewModel>();

			set.Bind(_otpTitle)
				.To(vm => vm.TxtResetPassword);

            set.Bind(_otpDetail)
                .To(vm => vm.TxtInputYourPhoneNumber);

            set.Bind(_phoneNumberEdt)
                .For("Text")
                .To(vm => vm.PhoneNumber);

            set.Bind(_phoneNumberEdt)
                .For(v => v.Placeholder)
                .To(vm => vm.EdtPhoneNumber);

            set.Bind(_btnSubmit)
                .For("Title")
                .To(vm => vm.BtnSubmit);

            set.Bind(_btnSubmit)
                .For(v => v.Enabled)
                .To(vm => vm.SubmitButtonEnabled);

            set.Bind(_btnSubmit.Tap())
                .For(v => v.Command)
                .To(vm => vm.SendOtpCommand);

            set.Bind(_btnCancel)
                .For("Title")
                .To(vm => vm.BtnCancelTitle);

            set.Bind(_btnCancel.Tap())
                .For(v => v.Command)
                .To(vm => vm.BackPressedCommand);

            set.Apply();
		}

        private void InitBackground()
		{
			_backgroundImage = UIHelper.CreateImageView(ResolutionHelper.Width, ResolutionHelper.Height, UIColor.White, ImageHelper.LoginBackground);
			_backgroundImage.UserInteractionEnabled = true;

            View.AddSubviews(_backgroundImage);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
            });
		}

		private void InitContent()
        {
			_logoImage = UIHelper.CreateImageView(DimensionHelper.LoginLogoWidth, DimensionHelper.LoginLogoHeight, UIColor.White, ImageHelper.LoginLogo);
			_otpTitle = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.LoginTitleTextSize, FontType.Bold);
            _otpDetail = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize, textAlignment: UITextAlignment.Center);
            _phoneNumberEdt = CreateEditText(UIKeyboardType.PhonePad, false);
            _btnCancel = UIHelper.CreateAlphaButton(0, DimensionHelper.PopupRequestButtonHeight,
                ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
                UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
                true, true, FontType.Light);
            _btnSubmit = UIHelper.CreateAlphaButton(0, DimensionHelper.PopupRequestButtonHeight,
                UIColor.White, UIColor.White, DimensionHelper.MediumTextSize,
                ColorHelper.LightBlue, ColorHelper.DarkBlue, ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);

            View.AddSubviews(_logoImage, _otpTitle, _otpDetail, _phoneNumberEdt, _btnCancel, _btnSubmit);

            View.AddConstraints(new[]
			{
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Top, 1, DimensionHelper.LogoMarginTop),
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_otpTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _logoImage,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_otpTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_otpDetail, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _otpTitle,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_otpDetail, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_otpDetail, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Width, 1, - DimensionHelper.BigMargin * 2),

                NSLayoutConstraint.Create(_phoneNumberEdt, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _otpDetail,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_phoneNumberEdt, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _phoneNumberEdt,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.RegisterButtonMargin),
                NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Left, 1, DimensionHelper.MarginNormal),
                NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, - DimensionHelper.MarginShort),

                NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _phoneNumberEdt,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.RegisterButtonMargin),
                NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Right, 1, - DimensionHelper.MarginShort),
            });
		}

        private UITextField CreateEditText(UIKeyboardType keyboardType, bool secureTextEntry)
        {
            var inputWidth = ResolutionHelper.Width - DimensionHelper.MarginNormal * 2;
            var input = UIHelper.CreateTextField(DimensionHelper.DropDownButtonHeight, inputWidth,
                ColorHelper.LightGray, ColorHelper.Gray, DimensionHelper.RoundCorner, DimensionHelper.MediumTextSize, FontType.Light);

            input.LeftView = new UIView(new CGRect(0, 0, 15, input.Frame.Height));
            input.LeftViewMode = UITextFieldViewMode.Always;
            input.ShouldReturn = (textField) =>
            {
                textField.ResignFirstResponder();
                return true;
            };
            input.KeyboardType = keyboardType;
            input.SecureTextEntry = secureTextEntry;

            return input;
        }
    }
}