using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using System;
using Foundation;
using UIKit;
using ColorHelper = GiveAndTake.iOS.Helpers.ColorHelper;

namespace GiveAndTake.iOS.Views
{
    [MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class OtpVerificationView : BaseView
	{
        private UIView _otpCodeView;
        private UITextField _edtOtp1;
        private UITextField _edtOtp2;
        private UITextField _edtOtp3;
        private UITextField _edtOtp4;
        private UITextField _edtOtp5;
        private UITextField _edtOtp6;
		private UIImageView _logoImage;
		private UIImageView _backgroundImage;
        private PopupItemLabel _resendOtpBtn;
        private PopupItemLabel _otpTitle;
        private PopupItemLabel _otpDetail;
        private AlphaUiButton _btnCancel;
        private AlphaUiButton _btnSubmit;

        protected override void InitView()
		{
			InitBackground();
			InitContent();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var set = this.CreateBindingSet<OtpVerificationView, OtpVerificationViewModel>();

			set.Bind(_otpTitle)
				.To(vm => vm.TxtOtpTitle);

            set.Bind(_otpDetail)
                .To(vm => vm.TxtOtpDetail);

            set.Bind(_edtOtp1)
                .For(v => v.Text)
                .To(vm => vm.OtpCode1);

            set.Bind(_edtOtp2)
                .For(v => v.Text)
                .To(vm => vm.OtpCode2);

            set.Bind(_edtOtp3)
                .For(v => v.Text)
                .To(vm => vm.OtpCode3);

            set.Bind(_edtOtp4)
                .For(v => v.Text)
                .To(vm => vm.OtpCode4);

            set.Bind(_edtOtp5)
                .For(v => v.Text)
                .To(vm => vm.OtpCode5);

            set.Bind(_edtOtp6)
                .For(v => v.Text)
                .To(vm => vm.OtpCode6);

            set.Bind(_resendOtpBtn)
                .To(vm => vm.TxtResendOtp);

            set.Bind(_resendOtpBtn.Tap())
                .For(v => v.Command)
                .To(vm => vm.ResendOtpCommand);

            set.Bind(_btnSubmit)
                .For("Title")
                .To(vm => vm.BtnSubmit);

            set.Bind(_btnSubmit)
                .For(v => v.Enabled)
                .To(vm => vm.RegisterButtonEnabled);

            set.Bind(_btnSubmit.Tap())
                .For(v => v.Command)
                .To(vm => vm.SubmitCommand);

            set.Bind(_btnCancel)
                .For("Title")
                .To(vm => vm.BtnCancelTitle);

            set.Bind(_btnCancel.Tap())
                .For(v => v.Command)
                .To(vm => vm.BackPressedCommand);

            set.Apply();
		}

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            _edtOtp1.BecomeFirstResponder();
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            _edtOtp1.EditingChanged -= OnEditingChanged;
            _edtOtp2.EditingChanged -= OnEditingChanged;
            _edtOtp3.EditingChanged -= OnEditingChanged;
            _edtOtp4.EditingChanged -= OnEditingChanged;
            _edtOtp5.EditingChanged -= OnEditingChanged;
            _edtOtp6.EditingChanged -= OnEditingChanged;
        }

        private void InitBackground()
		{
			_backgroundImage = UIHelper.CreateImageView(ResolutionHelper.Width, ResolutionHelper.Height, UIColor.White, ImageHelper.LoginBackground);
			_backgroundImage.UserInteractionEnabled = true;

            View.AddSubviews(_backgroundImage);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
            });
		}

		private void InitContent()
        {
			_logoImage = UIHelper.CreateImageView(DimensionHelper.LoginLogoWidth, DimensionHelper.LoginLogoHeight, UIColor.White, ImageHelper.LoginLogo);
			_otpTitle = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.LoginTitleTextSize, FontType.Bold);
            _otpDetail = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize, textAlignment: UITextAlignment.Center);
            _otpCodeView = CreateOtpInputView();
            _resendOtpBtn = UIHelper.CreateLabel(ColorHelper.ColorPrimary, DimensionHelper.MediumTextSize, FontType.Bold, UITextAlignment.Center);
            _btnCancel = UIHelper.CreateAlphaButton(0, DimensionHelper.PopupRequestButtonHeight,
                ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
                UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
                true, true, FontType.Light);
            _btnSubmit = UIHelper.CreateAlphaButton(0, DimensionHelper.PopupRequestButtonHeight,
                UIColor.White, UIColor.White, DimensionHelper.MediumTextSize,
                ColorHelper.LightBlue, ColorHelper.DarkBlue, ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);

            View.AddSubviews(_logoImage, _otpTitle, _otpDetail, _otpCodeView, _resendOtpBtn, _btnCancel, _btnSubmit);

            View.AddConstraints(new[]
			{
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Top, 1, DimensionHelper.LogoMarginTop),
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_otpTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _logoImage,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_otpTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_otpDetail, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _otpTitle,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_otpDetail, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_otpDetail, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Width, 1, - DimensionHelper.BigMargin * 2),

                NSLayoutConstraint.Create(_otpCodeView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _otpDetail,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_otpCodeView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_resendOtpBtn, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _otpCodeView,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_resendOtpBtn, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_resendOtpBtn, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                    NSLayoutAttribute.NoAttribute, 1, DimensionHelper.PopupButtonHeight),

                NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _resendOtpBtn,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.RegisterButtonMargin),
                NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Left, 1, DimensionHelper.MarginNormal),
                NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, - DimensionHelper.MarginShort),

                NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _resendOtpBtn,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.RegisterButtonMargin),
                NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Right, 1, - DimensionHelper.MarginShort),
            });
		}

        private UIView CreateOtpInputView()
        {
            var container = UIHelper.CreateView(0, 0);

            _edtOtp1 = CreateOtpCode(1);
            _edtOtp2 = CreateOtpCode(2);
            _edtOtp3 = CreateOtpCode(3);
            _edtOtp4 = CreateOtpCode(4);
            _edtOtp5 = CreateOtpCode(5);
            _edtOtp6 = CreateOtpCode(6);

            var underLine1 = UIHelper.CreateView(DimensionHelper.OtpUnderlineHeight, DimensionHelper.OtpCodeViewSize, ColorHelper.DarkGray);
            var underLine2 = UIHelper.CreateView(DimensionHelper.OtpUnderlineHeight, DimensionHelper.OtpCodeViewSize, ColorHelper.DarkGray);
            var underLine3 = UIHelper.CreateView(DimensionHelper.OtpUnderlineHeight, DimensionHelper.OtpCodeViewSize, ColorHelper.DarkGray);
            var underLine4 = UIHelper.CreateView(DimensionHelper.OtpUnderlineHeight, DimensionHelper.OtpCodeViewSize, ColorHelper.DarkGray);
            var underLine5 = UIHelper.CreateView(DimensionHelper.OtpUnderlineHeight, DimensionHelper.OtpCodeViewSize, ColorHelper.DarkGray);
            var underLine6 = UIHelper.CreateView(DimensionHelper.OtpUnderlineHeight, DimensionHelper.OtpCodeViewSize, ColorHelper.DarkGray);

            container.AddSubviews(_edtOtp1, _edtOtp2, _edtOtp3, _edtOtp4, _edtOtp5, _edtOtp6,
                underLine1, underLine2, underLine3, underLine4, underLine5, underLine6);

            container.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_edtOtp1, NSLayoutAttribute.Top, NSLayoutRelation.Equal, container,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_edtOtp1, NSLayoutAttribute.Left, NSLayoutRelation.Equal, container,
                    NSLayoutAttribute.Left, 1, 0),

                NSLayoutConstraint.Create(underLine1, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp1,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginText),
                NSLayoutConstraint.Create(underLine1, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp1,
                    NSLayoutAttribute.Left, 1, 0),

                NSLayoutConstraint.Create(_edtOtp2, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp1,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_edtOtp2, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp1,
                    NSLayoutAttribute.Right, 1, DimensionHelper.MarginShort),

                NSLayoutConstraint.Create(underLine2, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp1,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginText),
                NSLayoutConstraint.Create(underLine2, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp2,
                    NSLayoutAttribute.Left, 1, 0),

                NSLayoutConstraint.Create(_edtOtp3, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp2,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_edtOtp3, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp2,
                    NSLayoutAttribute.Right, 1, DimensionHelper.MarginShort),

                NSLayoutConstraint.Create(underLine3, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp1,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginText),
                NSLayoutConstraint.Create(underLine3, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp3,
                    NSLayoutAttribute.Left, 1, 0),

                NSLayoutConstraint.Create(_edtOtp4, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp3,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_edtOtp4, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp3,
                    NSLayoutAttribute.Right, 1, DimensionHelper.MarginShort),

                NSLayoutConstraint.Create(underLine4, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp1,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginText),
                NSLayoutConstraint.Create(underLine4, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp4,
                    NSLayoutAttribute.Left, 1, 0),

                NSLayoutConstraint.Create(_edtOtp5, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp4,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_edtOtp5, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp4,
                    NSLayoutAttribute.Right, 1, DimensionHelper.MarginShort),

                NSLayoutConstraint.Create(underLine5, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp1,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginText),
                NSLayoutConstraint.Create(underLine5, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp5,
                    NSLayoutAttribute.Left, 1, 0),

                NSLayoutConstraint.Create(_edtOtp6, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp5,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_edtOtp6, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp5,
                    NSLayoutAttribute.Right, 1, DimensionHelper.MarginShort),

                NSLayoutConstraint.Create(underLine6, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _edtOtp1,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginText),
                NSLayoutConstraint.Create(underLine6, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _edtOtp6,
                    NSLayoutAttribute.Left, 1, 0),

                NSLayoutConstraint.Create(container, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, underLine1,
                    NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(container, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _edtOtp6,
                    NSLayoutAttribute.Right, 1, 0),
            });

            return container;
        }

        private UITextField CreateOtpCode(int tag)
        {
            var edtOtpCode = UIHelper.CreateTextField(DimensionHelper.OtpCodeViewSize, DimensionHelper.OtpCodeViewSize, UIColor.White, DimensionHelper.BigTextSize, tag);
            edtOtpCode.ShouldChangeCharacters = ShouldChangeCharacters;
            edtOtpCode.EditingChanged += OnEditingChanged;
            return edtOtpCode;
        }

        private bool ShouldChangeCharacters(UITextField textField, NSRange range, string replacementString)
        {
            var newLength = textField.Text.Length + replacementString.Length - range.Length;
            return newLength <= 1;
        }

        private void OnEditingChanged(object sender, EventArgs e)
        {
            if (!(sender is UITextField otpTextField) || string.IsNullOrEmpty(otpTextField.Text))
            {
                return;
            }

            switch (otpTextField.Tag)
            {
                case 1:
                    _edtOtp2.BecomeFirstResponder();
                    break;

                case 2:
                    _edtOtp3.BecomeFirstResponder();
                    break;

                case 3:
                    _edtOtp4.BecomeFirstResponder();
                    break;

                case 4:
                    _edtOtp5.BecomeFirstResponder();
                    break;

                case 5:
                    _edtOtp6.BecomeFirstResponder();
                    break;

                case 6:
                    KeyboardHelper.HideKeyboard();
                    break;
            }
        }
    }
}