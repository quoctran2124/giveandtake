﻿using System;
using CoreGraphics;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Extensions;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using GiveAndTake.iOS.Views.TableViewSources;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Extensions;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.ViewModels;
using UIKit;

namespace GiveAndTake.iOS.Views
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen,
		ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class ConversationView : BaseView
	{
		private int _line;
		private const int TextBoxMaxLine = 5;
		private readonly nfloat _increaseHeight = 20;
		private CGRect _previousRect;
		private nfloat _starLineX;

		private UITableView _messageTableView;
		private CustomMvxCachedImageView _friendAvatarIcon;
		private UIButton _friendOptionButton;
		private UILabel _friendUsername;
		private MessageItemTableViewSource _messageTableViewSource;
		private UIButton _sendButton;
		private PlaceholderTextView _messageTextView;
		private UIView _friendInfoView;
		private UIView _contentView;
		private UIView _contentScrollView;
		private UIView _messageBoxView;
		private NSLayoutConstraint _textboxHeightConstraint;
		private NSLayoutConstraint _textmessageHeightConstraint;
		private NSLayoutConstraint _contentViewMarginBottomConstraint;

		public IMvxCommand LoadMoreCommand { get; set; }
		public IMvxCommand SendMessageCommand { get; set; }

		private bool _isSendCommentIconActivated;
		public bool IsSendCommentIconActivated
		{
			get => _isSendCommentIconActivated;
			set
			{
				_isSendCommentIconActivated = value;
				ResetSendIcon();
			}
		}

		private bool _isBlocked;

		public bool IsBLocked
		{
			get => _isBlocked;
			set
			{
				_isBlocked = value;
				UpdateConversationUi(value);
			}
		}

		private MvxInteraction<int> _scrollToMessageInteraction;
		public MvxInteraction<int> ScrollToMessageInteraction
		{
			get => _scrollToMessageInteraction;
			set
			{
				if (_scrollToMessageInteraction != null)
				{
					_scrollToMessageInteraction.Requested -= OnRequestedScrollToMessage;
				}
				_scrollToMessageInteraction = value;
				_scrollToMessageInteraction.Requested += OnRequestedScrollToMessage;
			}
		}

		private MvxInteraction _hideKeyboardInteraction;
		public MvxInteraction HideKeyboardInteraction
		{
			get => _hideKeyboardInteraction;
			set
			{
				if (_hideKeyboardInteraction != null)
				{
					_hideKeyboardInteraction.Requested -= OnRequestedHideKeyboardInteraction;
				}
				_hideKeyboardInteraction = value;
				_hideKeyboardInteraction.Requested += OnRequestedHideKeyboardInteraction;
			}
		}

		protected override void InitView()
		{
			InitHeader();
			InitBody();
			InitBottom();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<ConversationView, ConversationViewModel>();

			bindingSet.Bind(HeaderBar)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);

			bindingSet.Bind(this)
				.For(v => v.ScrollToMessageInteraction)
				.To(vm => vm.ScrollToMessageInteraction);

			bindingSet.Bind(this)
				.For(v => v.HideKeyboardInteraction)
				.To(vm => vm.HideKeyBoardInteraction);

			bindingSet.Bind(_messageTableViewSource)
				.To(vm => vm.MessageItemViewModels);

			bindingSet.Bind(this)
				.For(v => v.LoadMoreCommand)
				.To(vm => vm.LoadMoreCommand);

			bindingSet.Bind(this)
				.For(v => v.SendMessageCommand)
				.To(vm => vm.SendMessageCommand);

			bindingSet.Bind(_friendOptionButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowMenuPopupCommand);

			bindingSet.Bind(this)
				.For(v => v.IsSendCommentIconActivated)
				.To(vm => vm.IsSendIconActivated);

			bindingSet.Bind(_messageTextView)
				.For(v => v.Text)
				.To(vm => vm.Message);

			bindingSet.Bind(_messageTextView)
				.For(v => v.Placeholder)
				.To(vm => vm.SendMessageHint);

			bindingSet.Bind(_friendAvatarIcon)
				.For(v => v.ImageUrl)
				.To(vm => vm.FriendAvatarUrl);

			bindingSet.Bind(_friendUsername)
				.To(vm => vm.FriendUserName);

			bindingSet.Bind(this)
				.For(v => v.IsBLocked)
				.To(vm => vm.IsBLocked);

			bindingSet.Apply();
		}

		private void InitHeader()
		{
			HeaderBar.BackButtonIsShown = true;

			_contentView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_contentViewMarginBottomConstraint = NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom,
				NSLayoutRelation.Equal, View,
				NSLayoutAttribute.Bottom, 1, -UIApplication.SharedApplication.KeyWindow.SafeAreaInsets.Bottom);
			View.AddSubview(_contentView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, HeaderBar,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				_contentViewMarginBottomConstraint
			});
		}

		private void InitBody()
		{
			_friendInfoView = UIHelper.CreateView(DimensionHelper.HeaderBarHeight, 0, ColorHelper.LightBlue);
			_contentView.AddSubview(_friendInfoView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_friendInfoView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_friendInfoView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_friendInfoView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Width, 1, 0),
			});

			_friendOptionButton = UIHelper.CreateImageButton(DimensionHelper.PopupButtonHeight,
				DimensionHelper.PopupButtonHeight, ImageHelper.More);
			_friendInfoView.AddSubview(_friendOptionButton);
			_friendInfoView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_friendOptionButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_friendInfoView, NSLayoutAttribute.Right, 1, -DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_friendOptionButton, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_friendInfoView, NSLayoutAttribute.CenterY, 1, 0),
			});

			_friendAvatarIcon = UIHelper.CreateCustomImageView(DimensionHelper.PopupRequestGiverAvartarSize,
				DimensionHelper.PopupRequestGiverAvartarSize, ImageHelper.DefaultAvatar,
				DimensionHelper.PopupRequestGiverAvartarSize / 2);
			_friendInfoView.AddSubview(_friendAvatarIcon);
			_friendInfoView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_friendAvatarIcon, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_friendInfoView, NSLayoutAttribute.Left, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_friendAvatarIcon, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_friendInfoView, NSLayoutAttribute.CenterY, 1, 0),
			});

			_friendUsername = UIHelper.CreateLabel(UIColor.White, DimensionHelper.LargeTextSize, FontType.Bold);
			_friendUsername.Lines = 1;
			_friendUsername.LineBreakMode = UILineBreakMode.TailTruncation;
			_friendInfoView.AddSubview(_friendUsername);
			_friendInfoView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_friendUsername, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_friendAvatarIcon, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_friendUsername, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_friendAvatarIcon, NSLayoutAttribute.Right, 1,
					DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_friendUsername, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_friendOptionButton, NSLayoutAttribute.Left, 1,
					-DimensionHelper.MarginNormal),
			});

			_contentScrollView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_contentView.AddSubview(_contentScrollView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _friendInfoView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Bottom, 1, 0),
			});

			_messageTableView = UIHelper.CreateTableView(0, 0);
			_messageTableView.RowHeight = UITableView.AutomaticDimension;
			_messageTableView.Transform = CGAffineTransform.MakeScale(1, -1);
			_messageTableViewSource = new MessageItemTableViewSource(_messageTableView)
			{
				LoadMoreEvent = LoadMoreEvent
			};

			_messageTableView.Source = _messageTableViewSource;

			_contentScrollView.AddSubview(_messageTableView);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_messageTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_contentScrollView, NSLayoutAttribute.Top, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_messageTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_contentScrollView, NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_messageTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_contentScrollView, NSLayoutAttribute.Right, 1, -DimensionHelper.MarginShort)
			});
		}

		private void InitBottom()
		{
			_messageBoxView = UIHelper.CreateView(DimensionHelper.MessageBoxHeight, 0, UIColor.White, DimensionHelper.MessageBoxCornerRadius);
			_messageBoxView.Layer.BorderWidth = 1;
			_messageBoxView.Layer.BorderColor = ColorHelper.Blue.CGColor;
			_textboxHeightConstraint = NSLayoutConstraint.Create(_messageBoxView, NSLayoutAttribute.Height,
				NSLayoutRelation.Equal, 1, DimensionHelper.MessageBoxHeight);
			_contentScrollView.AddSubview(_messageBoxView);
			_contentScrollView.AddConstraints(new[]
			{
				_textboxHeightConstraint,
				NSLayoutConstraint.Create(_messageBoxView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_messageTableView, NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_messageBoxView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_contentScrollView, NSLayoutAttribute.Bottom, 1, -DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_messageBoxView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_contentScrollView, NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_messageBoxView, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_contentScrollView, NSLayoutAttribute.Right, 1, -DimensionHelper.MarginShort)
			});

			_sendButton = UIHelper.CreateImageButton(DimensionHelper.SendButtonHeight, DimensionHelper.SendButtonWidth, ImageHelper.SendCommentButtonDeactivated);
			_messageBoxView.AddSubview(_sendButton);
			_messageBoxView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_sendButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _messageBoxView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_sendButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _messageBoxView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MarginNormal)
			});


			_messageTextView = UIHelper.CreateTextView(DimensionHelper.MessageTextViewHeight, 0, UIColor.White, null, 0, UIColor.LightGray, DimensionHelper.MediumTextSize);
			_messageTextView.Layer.BorderWidth = 0.0f;
			IsViewMoved = true;
			_textmessageHeightConstraint = NSLayoutConstraint.Create(_messageTextView, NSLayoutAttribute.Height,
				NSLayoutRelation.Equal, 1, DimensionHelper.MessageTextViewHeight);
			_messageBoxView.AddSubview(_messageTextView);
			_messageBoxView.AddConstraints(new[]
			{
				_textmessageHeightConstraint,
				NSLayoutConstraint.Create(_messageTextView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _messageBoxView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_messageTextView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _messageBoxView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_messageTextView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _sendButton,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginShort),
			});
			KeyboardHelper.IsKeyboardShown += OnTextFieldBeginEditting;
			_messageTextView.ShouldEndEditing += OnTextFieldFinishEditting;
			_messageTextView.Changed += OnMessageBeginEditing;
			_sendButton.TouchUpInside += OnSendButtonClick;
			_previousRect = _messageTextView.GetCaretRectForPosition(_messageTextView.EndOfDocument);
			_starLineX = _previousRect.X;
		}

		private void OnSendButtonClick(object sender, EventArgs e)
		{
			SendMessageCommand.Execute(null);
			_line = 0;
			KeyboardHelper.HideKeyboard();
		}

		private void OnTextFieldBeginEditting(object sender, bool a)
		{
			var keyboardHeight = KeyboardHelper.KeyboardFrameHeight;
			_contentViewMarginBottomConstraint.Constant = -keyboardHeight;
			if (_line < 0)
			{
				//Just to ensure that in some case, if the number of line fall below 0, it will end the function.
				return;
			}
			if (_line < TextBoxMaxLine)
			{
				_textboxHeightConstraint.Constant += _increaseHeight * _line;
				_textmessageHeightConstraint.Constant += _increaseHeight * _line;
			}
			else
			{
				_textboxHeightConstraint.Constant += _increaseHeight * TextBoxMaxLine;
				_textmessageHeightConstraint.Constant += _increaseHeight * TextBoxMaxLine;
			}
		}

		private bool OnTextFieldFinishEditting(UIView textView)
		{
			_contentViewMarginBottomConstraint.Constant = -UIApplication.SharedApplication.KeyWindow.SafeAreaInsets.Bottom;
			_textboxHeightConstraint.Constant = DimensionHelper.MessageBoxHeight;
			_textmessageHeightConstraint.Constant = DimensionHelper.MessageTextViewHeight;
			return true;
		}

		private void OnMessageBeginEditing(object sender, EventArgs e)
		{
			var a = _messageTextView.EndOfDocument;
			var currentRect = _messageTextView.GetCaretRectForPosition(a);
			if (currentRect.Y > _previousRect.Y || currentRect.Height == 0 || currentRect.Y == -1 && _previousRect.Y != -1)
			{
				_line++;
				if (_line <= TextBoxMaxLine - 1)
				{
					_textboxHeightConstraint.Constant += _increaseHeight;
					_textmessageHeightConstraint.Constant += _increaseHeight;
				}
				else
				{
					_messageTextView.ScrollEnabled = true;
				}

			}
			else if ((_previousRect.X == _starLineX && currentRect.Y != _previousRect.Y) ||
					 (currentRect.Y < _previousRect.Y))
			{
				_line--;
				if (_textboxHeightConstraint.Constant > DimensionHelper.SendCommentBoxHeight && _textmessageHeightConstraint.Constant > DimensionHelper.SendCommentMessageHeight)
				{
					_textboxHeightConstraint.Constant -= _increaseHeight;
					_textmessageHeightConstraint.Constant -= _increaseHeight;
				}
			}
			_previousRect = currentRect;
		}

		private void LoadMoreEvent()
		{
			LoadMoreCommand?.Execute();
		}

		private void ResetSendIcon()
		{
			_sendButton.SetBackgroundImage(
				IsSendCommentIconActivated
					? new UIImage(ImageHelper.SendCommentButtonActivated)
					: new UIImage(ImageHelper.SendCommentButtonDeactivated), UIControlState.Normal);
		}

		private void OnRequestedScrollToMessage(object sender, MvvmCross.Base.MvxValueEventArgs<int> e)
		{
			ScrollToMessage(e.Value);
		}

		public void OnRequestedHideKeyboardInteraction(object sender, EventArgs e)
		{
			KeyboardHelper.HideKeyboard();
		}

		private void UpdateConversationUi(bool isBlockedConversation)
		{
			_messageBoxView.UserInteractionEnabled = (!isBlockedConversation);
			if (isBlockedConversation)
			{
				_friendInfoView.BackgroundColor = ColorHelper.Gray;
				_messageBoxView.Layer.BorderColor = ColorHelper.Gray.CGColor;
				_messageBoxView.BackgroundColor = ColorHelper.LightGray;
				_messageTextView.BackgroundColor = ColorHelper.LightGray;
			}
			else
			{
				_friendInfoView.BackgroundColor = ColorHelper.LightBlue;
				_messageBoxView.Layer.BorderColor = ColorHelper.Blue.CGColor;
				_messageBoxView.BackgroundColor = UIColor.White;
				_messageTextView.BackgroundColor = UIColor.White;
			}
		}

		private void ScrollToMessage(int index)
		{
			KeyboardHelper.HideKeyboard();

			if (index >= 0 && index <= _messageTableViewSource.ItemsSource.Count())
			{
				InvokeOnMainThread(() =>
				{
					_messageTableView.TryScrollToRow(index, 0, false);
				});
			}
		}
	}
}