using CoreGraphics;
using GiveAndTake.iOS.CustomControls;
using Foundation;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.iOS.Helpers;
using MvvmCross.Platforms.Ios.Views;
using UIKit;

namespace GiveAndTake.iOS.Views.Base
{
	public abstract class BaseView : MvxViewController
	{
		protected HeaderBar HeaderBar;
		private NSObject _didBecomeActiveNotificationObserver;
		private NSObject _willEnterForegroundNotificationObserver;
		protected bool IsViewMoved;

		public override void ViewDidLoad()
		{
			View = new UIView
			{
				BackgroundColor = UIColor.White,
				MultipleTouchEnabled = false
			};

			KeyboardHelper.TopView = View;

			base.ViewDidLoad();

			NavigationController?.SetNavigationBarHidden(true, true);

			CreateHeaderBar();
			InitView();
			DismissKeyboard();
			CreateBinding();
		}

		protected void RegisterViewToCenterOnKeyboardShown(UIView view)
		{
			KeyboardHelper.ViewToCenterOnKeyboardShown = view;
		}

		protected void UnRegisterViewToCenterOnKeyboardShown()
		{
			KeyboardHelper.ViewToCenterOnKeyboardShown = null;
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			KeyboardHelper.ViewToCenterOnKeyboardShown = null;
			_didBecomeActiveNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidBecomeActiveNotification, OnDidBecomeActive);
			_willEnterForegroundNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidEnterBackgroundNotification, OnDidEnterBackground);
		}
		protected virtual void OnDidBecomeActive(NSNotification obj)
		{
			if (!View.Hidden) (ViewModel as BaseViewModel)?.OnActive();
		}

		protected virtual void OnDidEnterBackground(NSNotification obj)
		{
			if (!View.Hidden) (ViewModel as BaseViewModel)?.OnDeactive();
		}
		protected void CreateHeaderBar()
		{
			HeaderBar = UIHelper.CreateHeaderBar(ResolutionHelper.Width, DimensionHelper.HeaderBarHeight,
				UIColor.White);
			View.Add(HeaderBar);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(HeaderBar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, ResolutionHelper.StatusHeight),
				NSLayoutConstraint.Create(HeaderBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
			});
		}

		protected virtual void ConfigNavigationBar()
		{
			//NavigationItem
		}

		protected abstract void InitView();

		protected virtual void CreateBinding()
		{
		}

		private void DismissKeyboard()
		{
			var g = new UITapGestureRecognizer(() => View.EndEditing(true));
			g.CancelsTouchesInView = false; //for iOS5

			View.AddGestureRecognizer(g);
		}

		protected bool OnTextFieldEditting(UIView textField, UIScrollView scrollView)
		{
			if (textField == null || scrollView == null) return true;

			float distanceMovingTextField = 0;

			var relativeFrameToScrollView = textField.Superview.ConvertRectToView(textField.Frame, scrollView);

			if (relativeFrameToScrollView.Y + textField.Frame.Height > scrollView.Bounds.Height + scrollView.ContentOffset.Y)
			{
				var newOffset = relativeFrameToScrollView.Y + textField.Frame.Height - scrollView.Bounds.Height;
				distanceMovingTextField = (float)(newOffset - scrollView.ContentOffset.Y);
				scrollView.SetContentOffset(new CGPoint(0, newOffset), true);
			}

			SetTextFieldPosition(textField, distanceMovingTextField);

			return true;
		}


		protected virtual bool OnTextFieldEditting(UIView textField)
		{
			SetTextFieldPosition(textField, 0);

			if (KeyboardHelper.IsKeyBoardVisible)
			{
				KeyboardHelper.OnKeyboardChanged(true);
			}
			return true;
		}

		protected virtual void SetTextFieldPosition(UIView textField, float distanceMovingTextField)
		{
			if (textField == null) return;

			if (IsViewMoved)
			{
				var relativeFrameToScrollView =
					View.Superview.ConvertRectToView(View.Frame, KeyboardHelper.ViewToCenterOnKeyboardShown);
				KeyboardHelper.TextFieldPosition = new Position((float) relativeFrameToScrollView.X,
					(float) (relativeFrameToScrollView.Y + relativeFrameToScrollView.Height - distanceMovingTextField));
			}
			else
			{
				var relativeFrameWithContentView = textField.Superview.ConvertRectToView(textField.Frame,
					KeyboardHelper.ViewToCenterOnKeyboardShown);
				KeyboardHelper.TextFieldPosition = new Position((float) relativeFrameWithContentView.X,
					(float) (relativeFrameWithContentView.Y + textField.Frame.Height - distanceMovingTextField));
			}
		}
	}
}