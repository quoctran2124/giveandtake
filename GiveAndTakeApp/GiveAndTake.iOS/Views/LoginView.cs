using CoreGraphics;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Interfaces;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;
using ColorHelper = GiveAndTake.iOS.Helpers.ColorHelper;

namespace GiveAndTake.iOS.Views
{
	[MvxRootPresentation]
	public class LoginView : BaseView, ITextViewDelegate
	{
		private string _acceptTermAndConditionInHtml;
        private UIView _contentView;
        private UIView _registerView;
		private UIImageView _logoImage;
		private UIImageView _backgroundImage;
        private UIImageView _facebookLogo;
        private UITextField _phoneNumberEdt;
        private UITextField _passwordEdt;
        private PopupItemLabel _loginTitle;
        private PopupItemLabel _loginWithFacebookLabel;
        private PopupItemLabel _loginWithPhoneNumberLabel;
        private PopupItemLabel _togglePassword;
        private PopupItemLabel _forgotPassword;
        private PopupItemLabel _notHavingAnAccountLabel;
        private PopupItemLabel _registerBtn;
		private UIButton _customLoginFacebookButton;
        private AlphaUiButton _loginWithPhoneNumberBtn;
		private DisabledInteractionTextView _tvTermAndCondition;
		private NSObject _profileDidChangeObserver;


        public IMvxCommand LoginCommand { get; set; }

		public IMvxCommand ShowConditionCommand { get; set; }

		public string AcceptTermAndConditionInHtml
		{
			get => _acceptTermAndConditionInHtml;
			set
			{
				_acceptTermAndConditionInHtml = value;
				_tvTermAndCondition.AttributedText =
					UIHelper.GetAttributedStringFromHtml(value, DimensionHelper.MediumTextSize);
				_tvTermAndCondition.TextAlignment = UITextAlignment.Center;
			}
		}

		public bool ShouldInteractWithUrl(UITextView uiTextView, NSUrl nsUrl, NSRange arg3)
		{
			ShowConditionCommand?.Execute();
			return true;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			ResolutionHelper.InitStaticVariable();

			_profileDidChangeObserver = Profile.Notifications.ObserveDidChange((sender, e) => {
				if (e.NewProfile == null)
				{
					return;
				}

				_profileDidChangeObserver.Dispose();
				LoginCommand?.Execute();
			});
		}

		protected override void InitView()
		{
			InitBackground();
			InitContent();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var set = this.CreateBindingSet<LoginView, LoginViewModel>();

			set.Bind(this)
				.For(v => v.LoginCommand)
				.To(vm => vm.LoginCommand);

			set.Bind(this)
				.For(v => v.ShowConditionCommand)
				.To(vm => vm.ShowConditionCommand);

			set.Bind(_loginTitle)
				.To(vm => vm.LoginTitle);

            set.Bind(_loginWithFacebookLabel)
                .To(vm => vm.BtnLoginWithFacebook);

            set.Bind(_loginWithPhoneNumberLabel)
                .To(vm => vm.TxtLoginWithPhoneNumber);

            set.Bind(_phoneNumberEdt)
                .For("Text")
                .To(vm => vm.PhoneNumber);

            set.Bind(_phoneNumberEdt)
                .For(v => v.Placeholder)
                .To(vm => vm.EdtPhoneNumber);

            set.Bind(_passwordEdt)
                .For("Text")
                .To(vm => vm.Password);

            set.Bind(_passwordEdt)
                .For(v => v.SecureTextEntry)
                .To(vm => vm.IsPasswordShown)
                .WithConversion("InvertBool");

            set.Bind(_passwordEdt)
                .For(v => v.Placeholder)
                .To(vm => vm.EdtPassword);

            set.Bind(_togglePassword)
                .To(vm => vm.BtnTogglePassword);

            set.Bind(_togglePassword.Tap())
                .For(v => v.Command)
                .To(vm => vm.TogglePasswordCommand);

            set.Bind(_forgotPassword)
                .To(vm => vm.BtnForgotPassword);

            set.Bind(_forgotPassword.Tap())
                .For(v => v.Command)
                .To(vm => vm.HandleForgotPasswordCommand);

            set.Bind(_loginWithPhoneNumberBtn)
                .For("Title")
                .To(vm => vm.BtnLogin);

            set.Bind(_loginWithPhoneNumberBtn.Tap())
                .For(v => v.Command)
                .To(vm => vm.LoginWithPhoneNumberCommand);

            set.Bind(_notHavingAnAccountLabel)
                .To(vm => vm.TxtNotHavingAnAccount);

            set.Bind(_registerBtn)
                .To(vm => vm.BtnRegister);

            set.Bind(_registerBtn.Tap())
                .For(v => v.Command)
                .To(vm => vm.NavigateToRegisterCommand);

            set.Bind(this)
				.For(v => v.AcceptTermAndConditionInHtml)
				.To(vm => vm.AcceptTermAndConditionInHtml);

			set.Apply();
		}

		private void InitBackground()
		{
			_backgroundImage = UIHelper.CreateImageView(ResolutionHelper.Width, ResolutionHelper.Height, UIColor.White,
				ImageHelper.LoginBackground);
			_backgroundImage.UserInteractionEnabled = true;

			View.Add(_backgroundImage);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0)
			});
		}

		private void InitContent()
        {
            _contentView = UIHelper.CreateView(0, ResolutionHelper.Width);
            View.Add(_contentView);
            View.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0)
            });

			_logoImage = UIHelper.CreateImageView(DimensionHelper.LoginLogoWidth, DimensionHelper.LoginLogoHeight,
				UIColor.White, ImageHelper.LoginLogo);
			_loginTitle = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.LoginTitleTextSize, FontType.Bold);
            _customLoginFacebookButton = CreateCustomLoginWithFacebookButton();
            _tvTermAndCondition = CreateTermAndConditionLabel();
            _loginWithPhoneNumberLabel = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);
            _phoneNumberEdt = CreateEditText(UIKeyboardType.PhonePad, false);
            _passwordEdt = CreateEditText(UIKeyboardType.Default, true);
            _passwordEdt.SecureTextEntry = true;
            _togglePassword = UIHelper.CreateLabel(ColorHelper.ColorPrimary, DimensionHelper.MediumTextSize, FontType.Medium);
            _forgotPassword = UIHelper.CreateLabel(ColorHelper.ColorPrimary, DimensionHelper.MediumTextSize, FontType.Medium);
            _loginWithPhoneNumberBtn = UIHelper.CreateAlphaButton(ResolutionHelper.Width / 2, DimensionHelper.PopupRequestButtonHeight, UIColor.White,
                UIColor.White, DimensionHelper.MediumTextSize, ColorHelper.LightBlue, ColorHelper.DarkBlue,
                ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);
            _registerView = CreateRegisterView();

            _contentView.AddSubviews(_logoImage, _loginTitle, _customLoginFacebookButton, _tvTermAndCondition,
                _loginWithPhoneNumberLabel, _phoneNumberEdt, _passwordEdt, _togglePassword, _forgotPassword, _loginWithPhoneNumberBtn, _registerView);

            _contentView.AddConstraints(new[]
			{
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_loginTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _logoImage,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_loginTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_tvTermAndCondition, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _loginTitle,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
                NSLayoutConstraint.Create(_tvTermAndCondition, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_customLoginFacebookButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _tvTermAndCondition,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin),
                NSLayoutConstraint.Create(_customLoginFacebookButton, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_loginWithPhoneNumberLabel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _customLoginFacebookButton,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonHeight),
                NSLayoutConstraint.Create(_loginWithPhoneNumberLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_phoneNumberEdt, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _loginWithPhoneNumberLabel,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_phoneNumberEdt, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_passwordEdt, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _phoneNumberEdt,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_passwordEdt, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_togglePassword, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _passwordEdt,
                    NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_togglePassword, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _passwordEdt,
                    NSLayoutAttribute.Right, 1, - DimensionHelper.AvatarMargin),

                NSLayoutConstraint.Create(_forgotPassword, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _passwordEdt,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
                NSLayoutConstraint.Create(_forgotPassword, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _togglePassword,
                    NSLayoutAttribute.Right, 1, 0),

                NSLayoutConstraint.Create(_loginWithPhoneNumberBtn, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _forgotPassword,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.LoginButtonMargin),
                NSLayoutConstraint.Create(_loginWithPhoneNumberBtn, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_registerView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _loginWithPhoneNumberBtn,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
                NSLayoutConstraint.Create(_registerView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _registerView,
                    NSLayoutAttribute.Bottom, 1, 0),
            });

			_customLoginFacebookButton.AddGestureRecognizer(new UITapGestureRecognizer(LoginToFacebook));
		}

        private UIView CreateRegisterView()
        {
            var registerView = UIHelper.CreateView(DimensionHelper.LoginButtonHeight, 0);
            _notHavingAnAccountLabel = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);
            _registerBtn = UIHelper.CreateLabel(ColorHelper.ColorPrimary, DimensionHelper.MediumTextSize, FontType.Bold);

            registerView.AddSubviews(_notHavingAnAccountLabel, _registerBtn);

            registerView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_notHavingAnAccountLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, registerView,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_notHavingAnAccountLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, registerView,
                    NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(_registerBtn, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _notHavingAnAccountLabel,
                    NSLayoutAttribute.Right, 1, DimensionHelper.MarginText),
                NSLayoutConstraint.Create(_registerBtn, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, registerView,
                    NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(registerView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _registerBtn,
                    NSLayoutAttribute.Right, 1, 0),
            });

            return registerView;
        }

        private UITextField CreateEditText(UIKeyboardType keyboardType, bool secureTextEntry)
        {
            var inputWidth = ResolutionHelper.Width - DimensionHelper.MarginNormal * 2;
            var input = UIHelper.CreateTextField(DimensionHelper.DropDownButtonHeight, inputWidth,
                ColorHelper.LightGray, ColorHelper.Gray, DimensionHelper.RoundCorner, DimensionHelper.MediumTextSize, FontType.Light);

            input.LeftView = new UIView(new CGRect(0, 0, 15, input.Frame.Height));
            input.LeftViewMode = UITextFieldViewMode.Always;
            input.ShouldReturn = (textField) =>
            {
                textField.ResignFirstResponder();
                return true;
            };
            input.KeyboardType = keyboardType;
            input.SecureTextEntry = secureTextEntry;

            return input;
        }

        private UIButton CreateCustomLoginWithFacebookButton()
        {
            var button = UIHelper.CreateButton(DimensionHelper.LoginButtonHeight, 0,
                ColorHelper.BlueFacebook, UIColor.White, DimensionHelper.BigTextSize,
                DimensionHelper.LoginButtonHeight / 2);

            _loginWithFacebookLabel = UIHelper.CreateLabel(UIColor.White, DimensionHelper.MediumTextSize);
            _facebookLogo = UIHelper.CreateImageView(DimensionHelper.FacebookIconHeight, DimensionHelper.FacebookIconWidth, ImageHelper.FacebookLogo);

            button.AddSubviews(_loginWithFacebookLabel, _facebookLogo);
            button.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_facebookLogo, NSLayoutAttribute.Left, NSLayoutRelation.Equal, button,
                    NSLayoutAttribute.Left, 1, DimensionHelper.LargeMargin),
                NSLayoutConstraint.Create(_facebookLogo, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, button,
                    NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(_loginWithFacebookLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, button,
                    NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_loginWithFacebookLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _facebookLogo,
                    NSLayoutAttribute.Right, 1, DimensionHelper.BigMargin),

                NSLayoutConstraint.Create(button, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _loginWithFacebookLabel,
                    NSLayoutAttribute.Right, 1, DimensionHelper.BigMargin),
            });

            return button;
        }

        private void LoginToFacebook()
		{
			new DebouncerHelper().Debouce(() =>
			{
				var manager = new LoginManager();
				manager.LogInWithReadPermissions(new[] {"public_profile", "email"}, this, (result, error) => {});
			});
		}

        private DisabledInteractionTextView CreateTermAndConditionLabel()
        {
            var tvTermAndCondition = UIHelper.CreateDisabledInteractionTextView(ColorHelper.Black, DimensionHelper.MediumTextSize, FontType.Light);
            tvTermAndCondition.Editable = false;
            tvTermAndCondition.Delegate = new DisabledInteractionTextViewDelegate(this);
            tvTermAndCondition.ScrollEnabled = false;
            tvTermAndCondition.TextContainerInset = UIEdgeInsets.Zero;
            tvTermAndCondition.ContentInset = UIEdgeInsets.Zero;
            tvTermAndCondition.ClipsToBounds = false;
            tvTermAndCondition.ScrollIndicatorInsets = UIEdgeInsets.Zero;
            tvTermAndCondition.WeakLinkTextAttributes = new NSDictionary(UIStringAttributeKey.ForegroundColor, ColorHelper.ColorPrimary);
            return tvTermAndCondition;
        }
    }
}