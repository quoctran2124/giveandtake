using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;
using ColorHelper = GiveAndTake.iOS.Helpers.ColorHelper;

namespace GiveAndTake.iOS.Views
{
    [MvxRootPresentation]
	public class RegisterSuccessView : BaseView
	{
        private UIView _contentView;
		private UIImageView _logoImage;
		private UIImageView _backgroundImage;
        private UIImageView _registerSuccessImage;
        private PopupItemLabel _title;
        private PopupItemLabel _detail;

        protected override void InitView()
		{
			InitBackground();
			InitContent();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var set = this.CreateBindingSet<RegisterSuccessView, RegisterSuccessViewModel>();

			set.Bind(_title)
				.To(vm => vm.TxtRegisterSuccess);

            set.Bind(_detail)
                .To(vm => vm.TxtRegisterSuccessDetail);

            set.Apply();
		}

        private void InitBackground()
		{
			_backgroundImage = UIHelper.CreateImageView(ResolutionHelper.Width, ResolutionHelper.Height, UIColor.White, ImageHelper.LoginBackground);
			_backgroundImage.UserInteractionEnabled = true;
            _contentView = UIHelper.CreateView(0, ResolutionHelper.Width);

            View.AddSubviews(_backgroundImage, _contentView);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0)
            });
		}

		private void InitContent()
        {
			_logoImage = UIHelper.CreateImageView(DimensionHelper.LoginLogoWidth, DimensionHelper.LoginLogoHeight, UIColor.White, ImageHelper.LoginLogo);
			_registerSuccessImage = UIHelper.CreateImageView(0, 0, UIColor.White, ImageHelper.RegisterSuccess);
			_title = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.LoginTitleTextSize, FontType.Bold, UITextAlignment.Center);
            _detail = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize, textAlignment: UITextAlignment.Center);
            
            _contentView.AddSubviews(_logoImage, _title, _detail, _registerSuccessImage);

            _contentView.AddConstraints(new[]
			{
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_registerSuccessImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _logoImage,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_registerSuccessImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_title, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _registerSuccessImage,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_title, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_detail, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _title,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_detail, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _detail,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.RegisterButtonMargin),
            });
		}
    }
}