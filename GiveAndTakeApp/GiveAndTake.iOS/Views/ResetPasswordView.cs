using CoreGraphics;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;
using ColorHelper = GiveAndTake.iOS.Helpers.ColorHelper;

namespace GiveAndTake.iOS.Views
{
    [MvxRootPresentation]
	public class ResetPasswordView : BaseView
	{
		private UIImageView _logoImage;
		private UIImageView _backgroundImage;
        private UITextField _passwordEdt;
        private PopupItemLabel _otpTitle;
        private PopupItemLabel _otpDetail;
        private PopupItemLabel _togglePasswordBtn;
        private AlphaUiButton _btnCancel;
        private AlphaUiButton _btnSubmit;
        private PopupItemLabel _validationText;
        private UIStackView _formValidationText;

        protected override void InitView()
		{
			InitBackground();
			InitContent();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var set = this.CreateBindingSet<ResetPasswordView, ResetPasswordViewModel>();

			set.Bind(_otpTitle)
				.To(vm => vm.TxtResetPassword);

            set.Bind(_otpDetail)
                .To(vm => vm.TxtInputYourPassword);

            set.Bind(_passwordEdt)
                .For("Text")
                .To(vm => vm.Password);

            set.Bind(_passwordEdt)
                .For(v => v.SecureTextEntry)
                .To(vm => vm.IsPasswordShown)
                .WithConversion("InvertBool");

            set.Bind(_passwordEdt)
                .For(v => v.Placeholder)
                .To(vm => vm.EdtPassword);

            set.Bind(_validationText)
                .To(vm => vm.ValidationText);

            set.Bind(_togglePasswordBtn)
                .To(vm => vm.BtnTogglePassword);

            set.Bind(_togglePasswordBtn.Tap())
                .For(v => v.Command)
                .To(vm => vm.TogglePasswordCommand);

            set.Bind(_btnSubmit)
                .For("Title")
                .To(vm => vm.BtnSubmit);

            set.Bind(_btnSubmit)
                .For(v => v.Enabled)
                .To(vm => vm.SubmitButtonEnabled);

            set.Bind(_btnSubmit.Tap())
                .For(v => v.Command)
                .To(vm => vm.SubmitCommand);

            set.Bind(_btnCancel)
                .For("Title")
                .To(vm => vm.BtnCancelTitle);

            set.Bind(_btnCancel.Tap())
                .For(v => v.Command)
                .To(vm => vm.BackPressedCommand);

            set.Apply();
		}

        private void InitBackground()
		{
			_backgroundImage = UIHelper.CreateImageView(ResolutionHelper.Width, ResolutionHelper.Height, UIColor.White, ImageHelper.LoginBackground);
			_backgroundImage.UserInteractionEnabled = true;

            View.AddSubviews(_backgroundImage);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
            });
		}

		private void InitContent()
        {
			_logoImage = UIHelper.CreateImageView(DimensionHelper.LoginLogoWidth, DimensionHelper.LoginLogoHeight, UIColor.White, ImageHelper.LoginLogo);
			_otpTitle = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.LoginTitleTextSize, FontType.Bold);
            _otpDetail = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize, textAlignment: UITextAlignment.Center);
            _passwordEdt = CreateEditText(UIKeyboardType.Default, true);
            _togglePasswordBtn = UIHelper.CreateLabel(ColorHelper.ColorPrimary, DimensionHelper.MediumTextSize, FontType.Medium);
            _formValidationText = CreateValidationTextView();
            _btnCancel = UIHelper.CreateAlphaButton(0, DimensionHelper.PopupRequestButtonHeight,
                ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
                UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
                true, true, FontType.Light);
            _btnSubmit = UIHelper.CreateAlphaButton(0, DimensionHelper.PopupRequestButtonHeight,
                UIColor.White, UIColor.White, DimensionHelper.MediumTextSize,
                ColorHelper.LightBlue, ColorHelper.DarkBlue, ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);

            View.AddSubviews(_logoImage, _otpTitle, _otpDetail, _passwordEdt, _togglePasswordBtn, _btnCancel, _btnSubmit, _formValidationText);

            View.AddConstraints(new[]
			{
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Top, 1, DimensionHelper.LogoMarginTop),
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_otpTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _logoImage,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_otpTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_otpDetail, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _otpTitle,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_otpDetail, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_otpDetail, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Width, 1, - DimensionHelper.BigMargin * 2),

                NSLayoutConstraint.Create(_passwordEdt, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _otpDetail,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_passwordEdt, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_togglePasswordBtn, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _passwordEdt,
                    NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_togglePasswordBtn, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _passwordEdt,
                    NSLayoutAttribute.Right, 1, - DimensionHelper.AvatarMargin),
                NSLayoutConstraint.Create(_togglePasswordBtn, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                    NSLayoutAttribute.NoAttribute, 1, DimensionHelper.PopupButtonHeight),

                NSLayoutConstraint.Create(_formValidationText, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _passwordEdt,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginShort),
                NSLayoutConstraint.Create(_formValidationText, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _formValidationText,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.RegisterButtonMargin),
                NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Left, 1, DimensionHelper.MarginNormal),
                NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, - DimensionHelper.MarginShort),

                NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _formValidationText,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.RegisterButtonMargin),
                NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.Right, 1, - DimensionHelper.MarginShort),
            });
		}

        private UITextField CreateEditText(UIKeyboardType keyboardType, bool secureTextEntry)
        {
            var inputWidth = ResolutionHelper.Width - DimensionHelper.MarginNormal * 2;
            var input = UIHelper.CreateTextField(DimensionHelper.DropDownButtonHeight, inputWidth,
                ColorHelper.LightGray, ColorHelper.Gray, DimensionHelper.RoundCorner, DimensionHelper.MediumTextSize, FontType.Light);

            input.LeftView = new UIView(new CGRect(0, 0, 15, input.Frame.Height));
            input.LeftViewMode = UITextFieldViewMode.Always;
            input.ShouldReturn = (textField) =>
            {
                textField.ResignFirstResponder();
                return true;
            };
            input.KeyboardType = keyboardType;
            input.SecureTextEntry = secureTextEntry;

            return input;
        }

        private UIStackView CreateValidationTextView()
        {
            var container = UIHelper.CreateStackView(0, DimensionHelper.PopupContentWidth);
            container.Distribution = UIStackViewDistribution.EqualSpacing;
            container.Spacing = DimensionHelper.DefaultMargin;
            container.Axis = UILayoutConstraintAxis.Horizontal;

            _validationText = UIHelper.CreateLabel(ColorHelper.Red, DimensionHelper.MediumTextSize);
            _validationText.TextAlignment = UITextAlignment.Center;
            container.AddArrangedSubview(_validationText);
            return container;
        }
    }
}