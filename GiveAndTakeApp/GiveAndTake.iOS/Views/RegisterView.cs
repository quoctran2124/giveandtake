using CoreGraphics;
using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Interfaces;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;
using ColorHelper = GiveAndTake.iOS.Helpers.ColorHelper;

namespace GiveAndTake.iOS.Views
{
    [MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class RegisterView : BaseView, ITextViewDelegate
	{
		private string _acceptTermAndConditionInHtml;
        private UIView _contentView;
        private UIView _loginTextViews;
        private UIView _termAndConditionView;
        private UIStackView _formValidationText;
		private UIImageView _logoImage;
		private UIImageView _backgroundImage;
        private UITextField _passwordEdt;
        private UITextField _phoneNumberEdt;
        private UITextField _displayNameEdt;
        private PopupItemLabel _loginBtn;
        private PopupItemLabel _registerLabel;
        private PopupItemLabel _validationText;
        private PopupItemLabel _togglePasswordBtn;
        private PopupItemLabel _alreadyHadAnAccountLabel;
        private PopupItemLabel _registerWithPhoneNumberLabel;
        private AlphaUiButton _registerWithPhoneNumberBtn;
		private DisabledInteractionTextView _tvTermAndCondition;
        private CheckboxButton _checkBox;

        public IMvxCommand ShowConditionCommand { get; set; }

		public string AcceptTermAndConditionInHtml
		{
			get => _acceptTermAndConditionInHtml;
			set
			{
				_acceptTermAndConditionInHtml = value;
				_tvTermAndCondition.AttributedText =
					UIHelper.GetAttributedStringFromHtml(value, DimensionHelper.MediumTextSize);
				_tvTermAndCondition.TextAlignment = UITextAlignment.Center;
			}
		}

		public bool ShouldInteractWithUrl(UITextView uiTextView, NSUrl nsUrl, NSRange arg3)
		{
			ShowConditionCommand?.Execute();
			return true;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			ResolutionHelper.InitStaticVariable();
		}

		protected override void InitView()
		{
			InitBackground();
			InitContent();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var set = this.CreateBindingSet<RegisterView, RegisterViewModel>();

			set.Bind(_registerLabel)
				.To(vm => vm.TxtRegister);

            set.Bind(_registerWithPhoneNumberLabel)
                .To(vm => vm.TxtRegisterWithPhoneNumber);

            set.Bind(_phoneNumberEdt)
                .For("Text")
                .To(vm => vm.PhoneNumber);

            set.Bind(_phoneNumberEdt)
                .For(v => v.Placeholder)
                .To(vm => vm.EdtPhoneNumber);

            set.Bind(_displayNameEdt)
                .For("Text")
                .To(vm => vm.DisplayName);

            set.Bind(_displayNameEdt)
                .For(v => v.Placeholder)
                .To(vm => vm.EdtDisplayName);

            set.Bind(_passwordEdt)
                .For("Text")
                .To(vm => vm.Password);

            set.Bind(_passwordEdt)
                .For(v => v.SecureTextEntry)
                .To(vm => vm.IsPasswordShown)
                .WithConversion("InvertBool");

            set.Bind(_passwordEdt)
                .For(v => v.Placeholder)
                .To(vm => vm.EdtPassword);

            set.Bind(_validationText)
                .To(vm => vm.ValidationText);

            set.Bind(_togglePasswordBtn)
                .To(vm => vm.BtnTogglePassword);

            set.Bind(_togglePasswordBtn.Tap())
                .For(v => v.Command)
                .To(vm => vm.TogglePasswordCommand);

            set.Bind(this)
                .For(v => v.AcceptTermAndConditionInHtml)
                .To(vm => vm.AcceptTermAndConditionInHtml);

            set.Bind(_checkBox.Tap())
                .For(v => v.Command)
                .To(vm => vm.ToggleCheckboxCommand);

            set.Bind(_checkBox)
                .For(v => v.Checked)
                .To(vm => vm.TermAgreement);

            set.Bind(_registerWithPhoneNumberBtn)
                .For("Title")
                .To(vm => vm.BtnRegister);

            set.Bind(_registerWithPhoneNumberBtn)
                .For(v => v.Enabled)
                .To(vm => vm.RegisterButtonEnabled);

            set.Bind(_registerWithPhoneNumberBtn.Tap())
                .For(v => v.Command)
                .To(vm => vm.RegisterCommand);

            set.Bind(_alreadyHadAnAccountLabel)
                .To(vm => vm.TxtAlreadyHadAnAccount);

            set.Bind(_loginBtn)
                .To(vm => vm.BtnLogin);

            set.Bind(_loginBtn.Tap())
                .For(v => v.Command)
                .To(vm => vm.NavigateToLoginCommand);

            set.Bind(this)
                .For(v => v.ShowConditionCommand)
                .To(vm => vm.ShowConditionCommand);

            set.Apply();
		}

		private void InitBackground()
		{
			_backgroundImage = UIHelper.CreateImageView(ResolutionHelper.Width, ResolutionHelper.Height, UIColor.White, ImageHelper.LoginBackground);
			_backgroundImage.UserInteractionEnabled = true;
            _contentView = UIHelper.CreateView(0, ResolutionHelper.Width);

            View.AddSubviews(_backgroundImage, _contentView);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_backgroundImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0)
            });
		}

		private void InitContent()
        {
			_logoImage = UIHelper.CreateImageView(DimensionHelper.LoginLogoWidth, DimensionHelper.LoginLogoHeight, UIColor.White, ImageHelper.LoginLogo);
			_registerLabel = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.LoginTitleTextSize, FontType.Bold);
            _registerWithPhoneNumberLabel = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);
            _phoneNumberEdt = CreateEditText(UIKeyboardType.PhonePad, false);
            _displayNameEdt = CreateEditText(UIKeyboardType.Default, false);
            _passwordEdt = CreateEditText(UIKeyboardType.Default, true);
            _formValidationText = CreateValidationTextView();
            _togglePasswordBtn = UIHelper.CreateLabel(ColorHelper.ColorPrimary, DimensionHelper.MediumTextSize, FontType.Medium);
            _termAndConditionView = CreateTermAndConditionView();
            _registerWithPhoneNumberBtn = UIHelper.CreateAlphaButton(ResolutionHelper.Width / 2, DimensionHelper.PopupRequestButtonHeight, UIColor.White,
                UIColor.White, DimensionHelper.MediumTextSize, ColorHelper.LightBlue, ColorHelper.DarkBlue,
                ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);
            _loginTextViews = CreateRegisterView();

            _contentView.AddSubviews(_logoImage, _registerLabel, _registerWithPhoneNumberLabel, _phoneNumberEdt,
                _displayNameEdt, _passwordEdt, _togglePasswordBtn, _termAndConditionView, _registerWithPhoneNumberBtn, _loginTextViews, _formValidationText);

            _contentView.AddConstraints(new[]
			{
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_registerLabel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _logoImage,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
                NSLayoutConstraint.Create(_registerLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_registerWithPhoneNumberLabel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _registerLabel,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_registerWithPhoneNumberLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_phoneNumberEdt, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _registerWithPhoneNumberLabel,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.AvatarMargin),
                NSLayoutConstraint.Create(_phoneNumberEdt, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_displayNameEdt, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _phoneNumberEdt,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_displayNameEdt, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),
           
                NSLayoutConstraint.Create(_passwordEdt, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _displayNameEdt,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
                NSLayoutConstraint.Create(_passwordEdt, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_formValidationText, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _passwordEdt,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginShort),
                NSLayoutConstraint.Create(_formValidationText, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_togglePasswordBtn, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _passwordEdt,
                    NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_togglePasswordBtn, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _passwordEdt,
                    NSLayoutAttribute.Right, 1, - DimensionHelper.AvatarMargin),

                NSLayoutConstraint.Create(_termAndConditionView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _formValidationText,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.AvatarMargin),
                NSLayoutConstraint.Create(_termAndConditionView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_registerWithPhoneNumberBtn, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _termAndConditionView,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.RegisterButtonMargin),
                NSLayoutConstraint.Create(_registerWithPhoneNumberBtn, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_loginTextViews, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _registerWithPhoneNumberBtn,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
                NSLayoutConstraint.Create(_loginTextViews, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _loginTextViews,
                    NSLayoutAttribute.Bottom, 1, 0),
            });
		}

        private UIStackView CreateValidationTextView()
        {
            var container = UIHelper.CreateStackView(0, DimensionHelper.PopupContentWidth);
            container.Distribution = UIStackViewDistribution.EqualSpacing;
            container.Spacing = DimensionHelper.DefaultMargin;
            container.Axis = UILayoutConstraintAxis.Horizontal;

            _validationText = UIHelper.CreateLabel(ColorHelper.Red, DimensionHelper.MediumTextSize);
            _validationText.TextAlignment = UITextAlignment.Center;
            container.AddArrangedSubview(_validationText);
            return container;
        }

        private UIView CreateRegisterView()
        {
            var registerView = UIHelper.CreateView(DimensionHelper.LoginButtonHeight, 0);
            _alreadyHadAnAccountLabel = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);
            _loginBtn = UIHelper.CreateLabel(ColorHelper.ColorPrimary, DimensionHelper.MediumTextSize, FontType.Bold);

            registerView.AddSubviews(_alreadyHadAnAccountLabel, _loginBtn);

            registerView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_alreadyHadAnAccountLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, registerView,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_alreadyHadAnAccountLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, registerView,
                    NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(_loginBtn, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _alreadyHadAnAccountLabel,
                    NSLayoutAttribute.Right, 1, DimensionHelper.MarginText),
                NSLayoutConstraint.Create(_loginBtn, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, registerView,
                    NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(registerView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _loginBtn,
                    NSLayoutAttribute.Right, 1, 0),
            });

            return registerView;
        }

        private UITextField CreateEditText(UIKeyboardType keyboardType, bool secureTextEntry)
        {
            var inputWidth = ResolutionHelper.Width - DimensionHelper.MarginNormal * 2;
            var input = UIHelper.CreateTextField(DimensionHelper.DropDownButtonHeight, inputWidth,
                ColorHelper.LightGray, ColorHelper.Gray, DimensionHelper.RoundCorner, DimensionHelper.MediumTextSize, FontType.Light);

            input.LeftView = new UIView(new CGRect(0, 0, 15, input.Frame.Height));
            input.LeftViewMode = UITextFieldViewMode.Always;
            input.ShouldReturn = (textField) =>
            {
                textField.ResignFirstResponder();
                return true;
            };
            input.KeyboardType = keyboardType;
            input.SecureTextEntry = secureTextEntry;

            return input;
        }

        private UIView CreateTermAndConditionView()
        {
            var container = UIHelper.CreateView(0, 0);
            _checkBox = UIHelper.CreateCheckBox(DimensionHelper.PopupButtonHeight, DimensionHelper.PopupButtonHeight);
            _tvTermAndCondition = CreateTermAndConditionLabel();

            container.AddSubviews(_checkBox, _tvTermAndCondition);

            container.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_checkBox, NSLayoutAttribute.Left, NSLayoutRelation.Equal, container,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_checkBox, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, container,
                    NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(_tvTermAndCondition, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _checkBox,
                    NSLayoutAttribute.Right, 1, DimensionHelper.MarginShort),
                NSLayoutConstraint.Create(_tvTermAndCondition, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, container,
                    NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(container, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _tvTermAndCondition,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(container, NSLayoutAttribute.Height, NSLayoutRelation.Equal, _tvTermAndCondition,
                    NSLayoutAttribute.Height, 1, 0),
            });
            
            return container;
        }

        private DisabledInteractionTextView CreateTermAndConditionLabel()
        {
            var tvTermAndCondition = UIHelper.CreateDisabledInteractionTextView(ColorHelper.Black, DimensionHelper.MediumTextSize, FontType.Light);
            tvTermAndCondition.Editable = false;
            tvTermAndCondition.Delegate = new DisabledInteractionTextViewDelegate(this);
            tvTermAndCondition.ScrollEnabled = false;
            tvTermAndCondition.TextContainerInset = UIEdgeInsets.Zero;
            tvTermAndCondition.ContentInset = UIEdgeInsets.Zero;
            tvTermAndCondition.ClipsToBounds = false;
            tvTermAndCondition.ScrollIndicatorInsets = UIEdgeInsets.Zero;
            tvTermAndCondition.WeakLinkTextAttributes = new NSDictionary(UIStringAttributeKey.ForegroundColor, ColorHelper.ColorPrimary);
            return tvTermAndCondition;
        }
    }
}