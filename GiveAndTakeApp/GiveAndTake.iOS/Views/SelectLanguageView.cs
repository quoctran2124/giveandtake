﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views
{
	[MvxRootPresentation]
	public class SelectLanguageView : BaseView
	{
		private UIImageView _backgroundView;
		private UIImageView _logo;
		private UILabel _selectLanguageVnText;
		private UILabel _selectLanguageEnText;
		private CustomUILabel _vnTextTitle;
		private CustomUILabel _enTextTitle;
		private UIView _separatorLine;
		private UIButton _goBtn;

		protected override void InitView()
		{
			InitBackground();
			InitContent();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var set = this.CreateBindingSet<SelectLanguageView, SelectLanguageViewModel>();

			set.Bind(_selectLanguageVnText)
				.For(v => v.Text)
				.To(vm => vm.SelectLanguageVnText);
			set.Bind(_selectLanguageEnText)
				.For(v => v.Text)
				.To(vm => vm.SelectLanguageEnText);
			set.Bind(_vnTextTitle)
				.For(v => v.Text)
				.To(vm => vm.VnTextTitle);
			set.Bind(_vnTextTitle.Tap())
				.For(v => v.Command)
				.To(vm => vm.VietnameseCommand);
			set.Bind(_vnTextTitle)
				.For(v => v.Activated)
				.To(vm => vm.IsVietnamese);
			set.Bind(_enTextTitle)
				.For(v => v.Text)
				.To(vm => vm.EnTextTitle);
			set.Bind(_enTextTitle.Tap())
				.For(v => v.Command)
				.To(vm => vm.EnglishCommand);
			set.Bind(_enTextTitle)
				.For(v => v.Activated)
				.To(vm => vm.IsVietnamese)
				.WithConversion("InvertBool");
			set.Bind(_goBtn.Tap())
				.For(v => v.Command)
				.To(vm => vm.SubmitCommand);

			set.Apply();
		}

		private void InitBackground()
		{
			_backgroundView = UIHelper.CreateImageView(ResolutionHelper.Width, ResolutionHelper.Height, UIColor.White, ImageHelper.SelectLanguageBackground);
			_backgroundView.UserInteractionEnabled = true;

			View.Add(_backgroundView);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_backgroundView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_backgroundView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_backgroundView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_backgroundView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
			});
		}

		private void InitContent()
		{
			_logo = UIHelper.CreateImageView(DimensionHelper.LogoLanguageWidth, DimensionHelper.LogoLanguageHeight, UIColor.White, ImageHelper.ChooseLanguageLogo);
			_logo.BackgroundColor = null;
			_backgroundView.AddSubview(_logo);
			_backgroundView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_logo, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _backgroundView,
					NSLayoutAttribute.Top, 1, DimensionHelper.LogoLanguageMarginTop),
				NSLayoutConstraint.Create(_logo, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _backgroundView,
					NSLayoutAttribute.CenterX, 1, 0)
			});

			_selectLanguageVnText = UIHelper.CreateLabel(UIColor.White, DimensionHelper.SelectLanguageVnTextSize);
			_backgroundView.AddSubview(_selectLanguageVnText);
			_backgroundView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_selectLanguageVnText, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _logo,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.SelectLanguageViewMargin30),
				NSLayoutConstraint.Create(_selectLanguageVnText, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _backgroundView,
					NSLayoutAttribute.CenterX, 1, 0)
			});

			_selectLanguageEnText = UIHelper.CreateLabel(UIColor.White, DimensionHelper.SelectLanguageEnTextSize);
			_backgroundView.AddSubview(_selectLanguageEnText);
			_backgroundView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_selectLanguageEnText, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _selectLanguageVnText,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_selectLanguageEnText, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _backgroundView,
					NSLayoutAttribute.CenterX, 1, 0)
			});

			_vnTextTitle = UIHelper.CreateCustomLabel(UIColor.White, DimensionHelper.LanguageTextSize);
			_backgroundView.AddSubview(_vnTextTitle);
			_backgroundView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_vnTextTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _selectLanguageEnText,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.SelectLanguageViewMargin50),
				NSLayoutConstraint.Create(_vnTextTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _backgroundView,
					NSLayoutAttribute.CenterX, 1, 0)
			});

			_separatorLine = UIHelper.CreateView(DimensionHelper.SeperatorHeight, DimensionHelper.SelectLanguageSeparatorLineWidth, ColorHelper.GreyLineColor);
			_backgroundView.Add(_separatorLine);
			_backgroundView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_separatorLine, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _vnTextTitle,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_separatorLine, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _backgroundView,
					NSLayoutAttribute.CenterX, 1, 0)
				
			});

			_enTextTitle = UIHelper.CreateCustomLabel(UIColor.White, DimensionHelper.LanguageTextSize);
			_backgroundView.AddSubview(_enTextTitle);
			_backgroundView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_enTextTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _separatorLine,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_enTextTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _backgroundView,
					NSLayoutAttribute.CenterX, 1, 0)
			});

			_goBtn = UIHelper.CreateImageButton(DimensionHelper.SelectLanguageButtonSize,
				DimensionHelper.SelectLanguageButtonSize, ImageHelper.ChooseLanguageBtnNormal);
			_goBtn.SetBackgroundImage(new UIImage(ImageHelper.ChooseLanguageBtnPressed), UIControlState.Highlighted);
			_backgroundView.AddSubview(_goBtn);
			_backgroundView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_goBtn, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _enTextTitle,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.SelectLanguageButtonMarginTop),
				NSLayoutConstraint.Create(_goBtn, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _backgroundView,
					NSLayoutAttribute.CenterX, 1, 0)
			});
		}
	}
}