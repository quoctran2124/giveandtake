﻿using System;
using CoreGraphics;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Extensions;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using GiveAndTake.iOS.Views.TableViewSources;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Extensions;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using MvvmCross.ViewModels;
using UIKit;

namespace GiveAndTake.iOS.Views
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen,
		ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class CommentView : BaseView
	{
		private const int TextBoxMaxLine = 5;
		private UIButton _shareFbButton;
		private UIImageView _fbIcon;
		private UILabel _shareFbTitle;
		private UILabel _requestCount;
		private CustomUIImageView _requestIcon;
		private CustomUIImageView _imgAppreciation;
		private UIView _requestTouchView;
		private UIView _appreciationTouchView;
		private UILabel _commentCount;
		private UILabel _lbAppreciationCount;
		private UIImageView _imgComment;
		private UIView _separatorLine;
		private UITableView _commentTableView;
		private CommentItemTableViewSource _commentTableViewSource;
		private MvxUIRefreshControl _refreshControl;
		private UIView _sendCommentBox;
		private CustomMvxCachedImageView _avatarIcon;
		private UIButton _sendCommentButton;
		private PlaceholderTextView _sendCommentMessage;
		private UIView _contentView;
		private UIView _contentScrollView;
		private CGRect _previousRect;
		private readonly nfloat _increaseHeight = 20;
		private NSLayoutConstraint _textboxHeightConstraint;
		private NSLayoutConstraint _textmessageHeightConstraint;
		private NSLayoutConstraint _contentViewMarginBottomConstraint;
		private int _line;
		private nfloat _starLineX;

		public IMvxCommand LoadMoreCommand { get; set; }
		public IMvxCommand SendCommentCommand { get; set; }

		private bool _isSendCommentIconActivated;
		public bool IsSendCommentIconActivated
		{
			get => _isSendCommentIconActivated;
			set
			{
				_isSendCommentIconActivated = value;
				ResetSendCommentIcon();
			}
		}

		private MvxInteraction<int> _scrollToCommentInteraction;
		public MvxInteraction<int> ScrollToCommentInteraction
		{
			get => _scrollToCommentInteraction;
			set
			{
				if (_scrollToCommentInteraction != null)
				{
					_scrollToCommentInteraction.Requested -= OnRequestedScrollToComment;
				}
				_scrollToCommentInteraction = value;
				_scrollToCommentInteraction.Requested += OnRequestedScrollToComment;
			}
		}

		protected override void InitView()
		{
			InitHeader();
			InitBody();
			InitBottom();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<CommentView, CommentViewModel>();

			bindingSet.Bind(HeaderBar)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);

			bindingSet.Bind(this)
				.For(v => v.ScrollToCommentInteraction)
				.To(vm => vm.ScrollToCommentInteraction);

			bindingSet.Bind(_shareFbTitle)
				.For(v => v.Text)
				.To(vm => vm.ShareTitle);

			bindingSet.Bind(_requestCount)
				.For(v => v.Text)
				.To(vm => vm.RequestCount);

			bindingSet.Bind(_commentCount)
				.For(v => v.Text)
				.To(vm => vm.CommentCount);

			bindingSet.Bind(_commentTableViewSource)
				.To(vm => vm.CommentItemViewModels);

			bindingSet.Bind(this)
				.For(v => v.LoadMoreCommand)
				.To(vm => vm.LoadMoreCommand);

			bindingSet.Bind(_refreshControl)
				.For(v => v.IsRefreshing)
				.To(vm => vm.IsRefreshing);

			bindingSet.Bind(_refreshControl)
				.For(v => v.RefreshCommand)
				.To(vm => vm.RefreshCommand);

			bindingSet.Bind(_requestTouchView.Tap())
				.For(v => v.Command)
				.To(vm => vm.OnRequestIconClickCommand);

			bindingSet.Bind(this)
				.For(v => v.SendCommentCommand)
				.To(vm => vm.SendCommentCommand);

			bindingSet.Bind(_requestIcon)
				.For(v => v.IsActivated)
				.To(vm => vm.IsRequestIconActivated);

			bindingSet.Bind(this)
				.For(v => v.IsSendCommentIconActivated)
				.To(vm => vm.IsSendIconActivated);

			bindingSet.Bind(_avatarIcon)
				.For(v => v.ImageUrl)
				.To(vm => vm.AvatarUrl);

			bindingSet.Bind(_sendCommentMessage)
				.For(v => v.Placeholder)
				.To(vm => vm.SendCommentHint);

			bindingSet.Bind(_sendCommentMessage)
				.For(v => v.Text)
				.To(vm => vm.SendCommentContent);

			bindingSet.Bind(_shareFbTitle)
				.For(v => v.Text)
				.To(vm => vm.ShareFacebookTitle);

			bindingSet.Bind(_shareFbButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShareCommand);

			bindingSet.Bind(_imgAppreciation)
				.For(v => v.IsActivated)
				.To(vm => vm.IsAppreciationIconActivated);

			bindingSet.Bind(_lbAppreciationCount)
				.To(vm => vm.AppreciationCount);

			bindingSet.Bind(_appreciationTouchView.Tap())
				.For(v => v.Command)
				.To(vm => vm.OnAppreciationIconClickCommand);

			bindingSet.Apply();
		}

		private void InitHeader()
		{
			HeaderBar.BackButtonIsShown = true;

			_contentView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_contentViewMarginBottomConstraint = NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom,
				NSLayoutRelation.Equal, View,
				NSLayoutAttribute.Bottom, 1, -UIApplication.SharedApplication.KeyWindow.SafeAreaInsets.Bottom);
			View.AddSubview(_contentView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, HeaderBar,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				_contentViewMarginBottomConstraint
			});

			_shareFbButton = UIHelper.CreateButton(0, 0, ColorHelper.BlueFacebook, UIColor.White,
				DimensionHelper.ButtonTextSize, DimensionHelper.DefaultCornerRadius);
			_contentView.AddSubview(_shareFbButton);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_shareFbButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Top, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_shareFbButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.MarginObjectPostDetail)
			});

			_fbIcon = UIHelper.CreateImageView(DimensionHelper.FacebookIconHeight,
				DimensionHelper.FacebookIconWidth, ImageHelper.FacebookIcon);
			_shareFbButton.AddSubview(_fbIcon);
			_shareFbButton.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_fbIcon, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.Top, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_fbIcon, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_fbIcon, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _shareFbButton,
				NSLayoutAttribute.Bottom, 1, -DimensionHelper.MarginShort)
			});

			_shareFbTitle = UIHelper.CreateLabel(UIColor.White, DimensionHelper.MediumTextSize);
			_shareFbButton.AddSubview(_shareFbTitle);
			_shareFbButton.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_shareFbTitle, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _fbIcon,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_shareFbTitle, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_shareFbTitle, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _fbIcon,
				NSLayoutAttribute.Right, 1, DimensionHelper.ShareFbTitleMarginLeft)
			});

			_requestCount =
				UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.RequestCountNumberSize, FontType.Light);
			_contentView.AddSubview(_requestCount);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_requestCount, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_requestCount, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MarginObjectPostDetail)
			});

			_requestIcon = UIHelper.CreateImageView(DimensionHelper.PostDetailRequestListLogoHeight,
				DimensionHelper.PostDetailRequestListLogoWidth, ImageHelper.RequestOff, ImageHelper.RequestOn);
			_contentView.AddSubview(_requestIcon);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_requestIcon, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_requestIcon, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _requestCount,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginNormal)
			});

			_requestTouchView = UIHelper.CreateView(DimensionHelper.PostDetailRequestTouchFieldHeight,
				DimensionHelper.PostDetailRequestTouchFieldWidth, null);
			_contentView.AddSubview(_requestTouchView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_requestTouchView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_requestTouchView, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_contentView,
					NSLayoutAttribute.Right, 1, 0)
			});

			_commentCount = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.RequestCountNumberSize, FontType.Light);
			_contentView.AddSubview(_commentCount);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_commentCount, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_commentCount, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _requestIcon,
					NSLayoutAttribute.Left, 1, -DimensionHelper.LargeMargin)
			});

			_imgComment = UIHelper.CreateImageView(DimensionHelper.AppreciationIconHeight,
				DimensionHelper.PostDetailCommentLogoSize, ImageHelper.CommentIcon);
			_contentView.AddSubview(_imgComment);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imgComment, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_imgComment, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _commentCount,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginNormal)
			});

			_lbAppreciationCount = UIHelper.CreateLabel(UIColor.DarkGray, DimensionHelper.RequestCountNumberSize, FontType.Light);

			_contentView.AddSubview(_lbAppreciationCount);

			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbAppreciationCount, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbAppreciationCount, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_imgComment,
					NSLayoutAttribute.Left, 1, -DimensionHelper.LargeMargin)
			});

			_imgAppreciation = UIHelper.CreateImageView(DimensionHelper.AppreciationIconHeight, DimensionHelper.AppreciationIconWidth, ImageHelper.AppreciationOff, ImageHelper.AppreciationOn);

			_contentView.AddSubview(_imgAppreciation);

			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imgAppreciation, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_imgAppreciation, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _lbAppreciationCount,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginNormal)
			});

			_appreciationTouchView = UIHelper.CreateView(DimensionHelper.PostDetailRequestTouchFieldHeight,
				DimensionHelper.PostDetailRequestTouchFieldWidth, null);
			_contentView.AddSubview(_appreciationTouchView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_appreciationTouchView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_appreciationTouchView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _imgComment,
					NSLayoutAttribute.Left, 1, 0)
			});

			_separatorLine = UIHelper.CreateView(DimensionHelper.SeperatorHeight, 0, ColorHelper.Blue);

			_contentView.AddSubview(_separatorLine);

			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_separatorLine, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _shareFbButton,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_separatorLine, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1,  DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_separatorLine, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, - DimensionHelper.MarginShort),
			});

		}

		private void InitBody()
		{
			_contentScrollView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_contentView.AddSubview(_contentScrollView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _separatorLine,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Bottom, 1, 0),
			});

			_commentTableView = UIHelper.CreateTableView(0, 0);
			_commentTableView.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
			_commentTableView.SeparatorColor = ColorHelper.Line;
			_commentTableView.TableFooterView = new UIView();
			_commentTableView.RowHeight = UITableView.AutomaticDimension;
			_commentTableViewSource = new CommentItemTableViewSource(_commentTableView)
			{
				LoadMoreEvent = LoadMoreEvent
			};

			_commentTableView.Source = _commentTableViewSource;
			_refreshControl = new MvxUIRefreshControl();
			_commentTableView.RefreshControl = _refreshControl;

			_contentScrollView.AddSubview(_commentTableView);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_commentTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentScrollView, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_commentTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentScrollView, NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_commentTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentScrollView, NSLayoutAttribute.Right, 1, -DimensionHelper.MarginShort)
			});
		}

		private void InitBottom()
		{
			_sendCommentBox = UIHelper.CreateView(DimensionHelper.SendCommentBoxHeight, 0, UIColor.White, DimensionHelper.SendCommentBoxCornerRadius);
			_sendCommentBox.Layer.BorderWidth = 1;
			_sendCommentBox.Layer.BorderColor = ColorHelper.Blue.CGColor;
			_textboxHeightConstraint = NSLayoutConstraint.Create(_sendCommentBox, NSLayoutAttribute.Height,
				NSLayoutRelation.Equal, 1, DimensionHelper.SendCommentBoxHeight);
			_contentScrollView.AddSubview(_sendCommentBox);
			_contentScrollView.AddConstraints(new[]
			{
				_textboxHeightConstraint,
				NSLayoutConstraint.Create(_sendCommentBox, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _commentTableView, NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_sendCommentBox, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentScrollView, NSLayoutAttribute.Bottom, 1, -DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_sendCommentBox, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentScrollView, NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_sendCommentBox, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentScrollView, NSLayoutAttribute.Right, 1, -DimensionHelper.MarginShort)
			});

			_avatarIcon = UIHelper.CreateCustomImageView(DimensionHelper.ImageAvatarSize, DimensionHelper.ImageAvatarSize, ImageHelper.DefaultAvatar, DimensionHelper.ImageAvatarSize / 2);
			_avatarIcon.SetPlaceHolder(ImageHelper.DefaultAvatar, ImageHelper.DefaultAvatar);
			_sendCommentBox.AddSubview(_avatarIcon);
			_sendCommentBox.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_avatarIcon, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _sendCommentBox,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_avatarIcon, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _sendCommentBox,
					NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort)
			});

			_sendCommentButton = UIHelper.CreateImageButton(DimensionHelper.SendCommentButtonHeight, DimensionHelper.SendCommentButtonWidth, ImageHelper.SendCommentButtonDeactivated);
			_sendCommentBox.AddSubview(_sendCommentButton);
			_sendCommentBox.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_sendCommentButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _sendCommentBox,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_sendCommentButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _sendCommentBox,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MarginNormal)
			});


			_sendCommentMessage = UIHelper.CreateTextView(DimensionHelper.SendCommentMessageHeight, 0, UIColor.White, null, 0, UIColor.LightGray, 15);
			_sendCommentMessage.Layer.BorderWidth = 0.0f;
			IsViewMoved = true;
			_textmessageHeightConstraint = NSLayoutConstraint.Create(_sendCommentMessage, NSLayoutAttribute.Height,
				NSLayoutRelation.Equal, 1, DimensionHelper.SendCommentMessageHeight);
			_sendCommentBox.AddSubview(_sendCommentMessage);
			_sendCommentBox.AddConstraints(new[]
			{
				_textmessageHeightConstraint,
				NSLayoutConstraint.Create(_sendCommentMessage, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _sendCommentBox,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_sendCommentMessage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _avatarIcon,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_sendCommentMessage, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _sendCommentButton,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginShort),
			});
			KeyboardHelper.IsKeyboardShown += OnTextFieldBeginEditting;
			_sendCommentMessage.ShouldEndEditing += OnTextFieldFinishEditting;
			_sendCommentMessage.Changed += OnCommentMessageBeginEditing;
			_sendCommentButton.TouchUpInside += OnCommentButtonClick;
			_previousRect = _sendCommentMessage.GetCaretRectForPosition(_sendCommentMessage.EndOfDocument);
			_starLineX = _previousRect.X;
		}

		private void OnCommentButtonClick(object sender, EventArgs e)
		{
			SendCommentCommand.Execute(null);
			_line = 0;
			KeyboardHelper.HideKeyboard();
		}

		private void OnTextFieldBeginEditting(object sender, bool a)
		{
			var keyboardHeight = KeyboardHelper.KeyboardFrameHeight;
			_contentViewMarginBottomConstraint.Constant = -keyboardHeight;
			if (_line < 0)
			{
				//Just to ensure that in some case, if the number of line fall below 0, it will end the function.
				return;
			}
			if (_line < TextBoxMaxLine)
			{
				_textboxHeightConstraint.Constant += _increaseHeight * _line;
				_textmessageHeightConstraint.Constant += _increaseHeight * _line;
			}
			else
			{
				_textboxHeightConstraint.Constant += _increaseHeight * TextBoxMaxLine;
				_textmessageHeightConstraint.Constant += _increaseHeight * TextBoxMaxLine;
			}
		}

		private bool OnTextFieldFinishEditting(UIView textView)
		{
			_contentViewMarginBottomConstraint.Constant = -UIApplication.SharedApplication.KeyWindow.SafeAreaInsets.Bottom;
			_textboxHeightConstraint.Constant = DimensionHelper.SendCommentBoxHeight;
			_textmessageHeightConstraint.Constant = DimensionHelper.SendCommentMessageHeight;
			return true;
		}

		private void OnCommentMessageBeginEditing(object sender, EventArgs e)
		{
			var a = _sendCommentMessage.EndOfDocument;
			var currentRect = _sendCommentMessage.GetCaretRectForPosition(a);
			if (currentRect.Y > _previousRect.Y || currentRect.Height == 0 || currentRect.Y == -1 && _previousRect.Y != -1)
			{
				_line++;
				if (_line <= TextBoxMaxLine - 1)
				{
					_textboxHeightConstraint.Constant += _increaseHeight;
					_textmessageHeightConstraint.Constant += _increaseHeight;
				}
				else
				{
					_sendCommentMessage.ScrollEnabled = true;
				}

			}
			else if ((_previousRect.X == _starLineX && currentRect.Y != _previousRect.Y) ||
			         (currentRect.Y < _previousRect.Y))
			{
				_line--;
				if (_textboxHeightConstraint.Constant > DimensionHelper.SendCommentBoxHeight && _textmessageHeightConstraint.Constant > DimensionHelper.SendCommentMessageHeight)
				{
					_textboxHeightConstraint.Constant -= _increaseHeight;
					_textmessageHeightConstraint.Constant -= _increaseHeight;
				}
			}
			_previousRect = currentRect;
		}

		private void LoadMoreEvent()
		{
			LoadMoreCommand?.Execute();
		}

		private void ResetSendCommentIcon()
		{
			_sendCommentButton.SetBackgroundImage(
				IsSendCommentIconActivated
					? new UIImage(ImageHelper.SendCommentButtonActivated)
					: new UIImage(ImageHelper.SendCommentButtonDeactivated), UIControlState.Normal);
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
			_sendCommentMessage.Changed -= OnCommentMessageBeginEditing;
		}

		private void OnRequestedScrollToComment(object sender, MvvmCross.Base.MvxValueEventArgs<int> e)
		{
			ScrollToComment(e.Value);
		}

		private void ScrollToComment(int index)
		{
			KeyboardHelper.HideKeyboard();

			if (index >= 0 && index < _commentTableViewSource.ItemsSource.Count())
			{
				_commentTableView.TryScrollToRow(index, 0, false);
			}
		}
	}
}