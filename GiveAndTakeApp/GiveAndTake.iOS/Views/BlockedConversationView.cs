﻿using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using GiveAndTake.iOS.Views.TableViewSources;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using UIKit;

namespace GiveAndTake.iOS.Views
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen,
		ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class BlockedConversationView : BaseView
	{
		private UILabel _headerLabel;
		private UIView _headerBarView;
		private UIView _separateLine;

		private UIScrollView _scrollView;
		private UIView _contentView;
		private UITableView _conversationsTableView;
		private ConversationItemTableViewSource _conversationTableViewSource;
		private MvxUIRefreshControl _refreshControl;

		public IMvxCommand LoadMoreCommand { get; set; }

		public IMvxCommand BackPressedCommand { get; set; }

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var set = this.CreateBindingSet<BlockedConversationView, BlockedConversationViewModel>();

			set.Bind(_conversationTableViewSource)
				.To(vm => vm.ConversationItemViewModels);

			set.Bind(this)
				.For(v => v.LoadMoreCommand)
				.To(vm => vm.LoadMoreCommand);

			set.Bind(HeaderBar)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);

			set.Bind(_refreshControl)
				.For(v => v.IsRefreshing)
				.To(vm => vm.IsRefreshing);

			set.Bind(_refreshControl)
				.For(v => v.RefreshCommand)
				.To(vm => vm.RefreshCommand);

			set.Bind(_headerLabel)
				.To(vm => vm.BlockedConverstationTitle);

			set.Apply();
		}

		protected override void InitView()
		{
			HeaderBar.BackButtonIsShown = true;
			InitHeaderBar();
			InitScrollAndContentView();
			InitConversationsTableView();
		}

		private void InitHeaderBar()
		{
			//headerBar
			_headerBarView = UIHelper.CreateView(DimensionHelper.HeaderBarHeight, ResolutionHelper.Width, UIColor.White);
			View.Add(_headerBarView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_headerBarView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, HeaderBar,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_headerBarView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
			});
			//text
			_headerLabel = UIHelper.CreateLabel(ColorHelper.LightBlue, DimensionHelper.BigTextSize, FontType.Medium);

			_headerBarView.AddSubview(_headerLabel);
			_headerBarView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_headerLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _headerBarView,
					NSLayoutAttribute.Left, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_headerLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _headerBarView,
					NSLayoutAttribute.CenterY, 1, 0),
			});

			_separateLine = UIHelper.CreateView(DimensionHelper.HeaderBarLogoWidth, DimensionHelper.SeperatorHeight, ColorHelper.Gray);

			_headerBarView.AddSubview(_separateLine);
			_headerBarView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_separateLine, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _headerBarView,
					NSLayoutAttribute.Width,1 , 0),
				NSLayoutConstraint.Create(_separateLine, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1 , DimensionHelper.SeparateLineHeaderHeight),
				NSLayoutConstraint.Create(_separateLine, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _headerBarView,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin)
			});
		}

		private void InitScrollAndContentView()
		{
			_scrollView = UIHelper.CreateScrollView(0, 0, UIColor.Clear);
			View.AddSubview(_scrollView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _headerBarView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),
			});

			_contentView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_scrollView.AddSubview(_contentView);
			_scrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Height, 1, 0),
			});
		}

		private void InitConversationsTableView()
		{
			_conversationsTableView = UIHelper.CreateTableView(0, 0);
			_conversationTableViewSource = new ConversationItemTableViewSource(_conversationsTableView)
			{
				LoadMoreEvent = () => LoadMoreCommand?.Execute()
			};

			_conversationsTableView.Source = _conversationTableViewSource;
			_refreshControl = new MvxUIRefreshControl();
			_conversationsTableView.RefreshControl = _refreshControl;

			_contentView.Add(_conversationsTableView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_conversationsTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Top, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_conversationsTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_conversationsTableView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_conversationsTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, - DimensionHelper.MarginShort)
			});
		}
	}
}