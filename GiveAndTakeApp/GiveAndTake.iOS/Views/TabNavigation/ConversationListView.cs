﻿using System;
using GiveAndTake.Core.ViewModels.TabNavigation;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using GiveAndTake.iOS.Views.TableViewSources;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using UIKit;

namespace GiveAndTake.iOS.Views.TabNavigation
{
	[MvxTabPresentation(TabIconName = "Images/conversation_off",
		TabSelectedIconName = "Images/conversation_on",
		WrapInNavigationController = true)]
	public class ConversationListView : BaseView
	{
		public IMvxCommand LoadMoreCommand { get; set; }

		public IMvxCommand SearchCommand { get; set; }

		private UIScrollView _scrollView;
		private UIView _contentView;
		private UIButton _blockedConversationButton;
		private UISearchBar _searchBar;
		private UITableView _conversationsTableView;
		private ConversationItemTableViewSource _conversationTableViewSource;
		private MvxUIRefreshControl _refreshControl;
		private PopupItemLabel _searchResult;

		protected override void InitView()
		{
			View.AddGestureRecognizer(new UITapGestureRecognizer(HideKeyboard)
			{
				CancelsTouchesInView = false
			});

			InitScrollAndContentView();
			InitFilterBar();
			InitConversationsTableView();

			RegisterViewToCenterOnKeyboardShown(_contentView);
			IsViewMoved = false;

			_searchBar.ShouldBeginEditing += OnTextFieldEditting;
		}

		private void InitScrollAndContentView()
		{
			_scrollView = UIHelper.CreateScrollView(0, 0, UIColor.Clear);
			View.AddSubview(_scrollView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, ResolutionHelper.StatusHeight + DimensionHelper.HeaderBarHeight),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),
			});

			_contentView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_scrollView.AddSubview(_contentView);
			_scrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Height, 1, 0),
			});
		}

		private void InitFilterBar()
		{
			_blockedConversationButton = UIHelper.CreateImageButton(DimensionHelper.FilterSize, DimensionHelper.FilterSize, ImageHelper.BlockedConversation, ImageHelper.BlockedConversation);

			_contentView.Add(_blockedConversationButton);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_blockedConversationButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Top, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_blockedConversationButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MarginNormal)
			});

			_searchBar = UIHelper.CreateSearchBar(DimensionHelper.FilterSize, DimensionHelper.FilterSize);
			_searchBar.SearchButtonClicked += OnSearchSubmit;
			_searchBar.TextChanged += OnCancelClicked;
			_contentView.Add(_searchBar);

			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_searchBar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _blockedConversationButton,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_searchBar, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _blockedConversationButton,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_searchBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort)
			});
		}

		private void OnCancelClicked(object sender, UISearchBarTextChangedEventArgs e)
		{
			if (_searchBar.Text == "")
			{
				HeaderBar.BackPressedCommand.Execute();
				_searchBar.EndEditing(true);
			}
		}

		protected override void Dispose(bool disposing)
		{
			_searchBar.SearchButtonClicked -= OnSearchSubmit;
			_searchBar.TextChanged -= OnCancelClicked;
			base.Dispose(disposing);
		}

		private void InitConversationsTableView()
		{
			_conversationsTableView = UIHelper.CreateTableView(0, 0);
			_conversationTableViewSource = new ConversationItemTableViewSource(_conversationsTableView)
			{
				LoadMoreEvent = () => LoadMoreCommand?.Execute()
			};

			_conversationsTableView.Source = _conversationTableViewSource;
			_refreshControl = new MvxUIRefreshControl();
			_conversationsTableView.RefreshControl = _refreshControl;

			_contentView.Add(_conversationsTableView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_conversationsTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _searchBar, NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_conversationsTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_conversationsTableView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_conversationsTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, - DimensionHelper.MarginShort)
			});

			_searchResult = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.BigTextSize);
			_searchResult.Text = "Không tìm thấy kết quả nào";
			_contentView.Add(_searchResult);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_searchResult, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_searchResult, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterY, 1, 0)
			});
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var set = this.CreateBindingSet<ConversationListView, ConversationListViewModel>();

			set.Bind(_conversationTableViewSource)
				.To(vm => vm.ConversationItemViewModels);

			set.Bind(_searchBar)
				.For(v => v.Text)
				.To(vm => vm.CurrentQueryString);

			//			set.Bind(this)
			//				.For(v => v.LoadMoreCommand)
			//				.To(vm => vm.LoadMoreCommand);

			set.Bind(this)
				.For(v => v.SearchCommand)
				.To(vm => vm.SearchCommand);

			set.Bind(_refreshControl)
				.For(v => v.IsRefreshing)
				.To(vm => vm.IsRefreshing);

			set.Bind(_refreshControl)
				.For(v => v.RefreshCommand)
				.To(vm => vm.RefreshCommand);

			set.Bind(_searchResult)
				.For(v => v.Text)
				.To(vm => vm.SearchResultTitle);

			set.Bind(_searchResult)
				.For("Visibility")
				.To(vm => vm.IsSearchResultNull);

			set.Bind(HeaderBar)
				.For(v => v.BackButtonIsShown)
				.To(vm => vm.IsSearched);

			set.Bind(HeaderBar)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);

			set.Bind(_blockedConversationButton)
				.To(vm => vm.ShowBlockedConversationCommand);

			set.Apply();
		}

		private void HideKeyboard() => View.EndEditing(true);

		private void OnSearchSubmit(object sender, EventArgs e)
		{
			_searchBar.ResignFirstResponder();
			SearchCommand.Execute();
		}
	}
}