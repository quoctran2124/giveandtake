using FFImageLoading;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.TabNavigation;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using MvvmCross;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Views;
using System.Collections.Generic;
using System.Threading.Tasks;
using Foundation;
using UIKit;

namespace GiveAndTake.iOS.Views.TabNavigation
{
	public class TabNavigationView : MvxTabBarViewController<TabNavigationViewModel>
	{
		public IMvxCommand ClearNotificationBadgeCommand { get; set; }

		public IMvxCommand ClearConversationBadgeCommand { get; set; }

		public int AppBadgeCount
		{
			get => _appBadgeCount;
			set
			{
				_appBadgeCount = value;
				UpdateBadgeIcon(value);
			}
		}

		public int NotificationCount
		{
			get => _notificationCount;
			set
			{
				_notificationCount = value;
				TabBar.Items[(int)BottomTabBar.Notification].BadgeValue = value == 0 ? null : value.ToString();
			}
		}

		public int ConversationCount
		{
			get => _conversationCount;
			set
			{
				_conversationCount = value;
				TabBar.Items[(int)BottomTabBar.Conversation].BadgeValue = value == 0 ? null : value.ToString();
			}
		}

		private int _appBadgeCount;
		private int _notificationCount;
		private int _conversationCount;
		private bool _isOnNotificationTab;
		private bool _isOnConversationTab;
		private CustomMvxCachedImageView _imgAvatar;
		private CustomMvxCachedImageView _imgAvatarSelected;

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			ViewModel.ShowInitialViewModelsCommand.Execute();
			ConfigTabBar(animated);
			CreateBinding();
		}

		private void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<TabNavigationView, TabNavigationViewModel>();

			bindingSet.Bind(this)
				.For(v => v.AppBadgeCount)
				.To(vm => vm.AppBadgeCount);

			bindingSet.Bind(this)
				.For(v => v.NotificationCount)
				.To(vm => vm.NotificationCount);

			bindingSet.Bind(this)
				.For(v => v.ConversationCount)
				.To(vm => vm.ConversationBadgeCount);

			bindingSet.Bind(this)
				.For(v => v.ClearNotificationBadgeCommand)
				.To(vm => vm.ClearNotificationBadgeCommand);

			bindingSet.Bind(this)
				.For(v => v.ClearConversationBadgeCommand)
				.To(vm => vm.ClearConversationBadgeCommand);

			bindingSet.Apply();
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidBecomeActiveNotification, OnDidBecomeActive);
		}

		private void OnDidBecomeActive(NSNotification obj)
		{
			AppBadgeCount = Mvx.Resolve<IDataModel>().Badge;
		}

		private async void ConfigTabBar(bool animated)
		{
			TabBar.BackgroundColor = UIColor.White;
			if (TabBar.Items.Length != ViewModel.NumberOfTab)
			{
				ViewModel.ShowErrorCommand.Execute(null);
				return;
			}

			await ConfigProfileTab();

            foreach (var tabBarItem in TabBar.Items)
            {
                tabBarItem.Image = tabBarItem.Image
                    .ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);
                tabBarItem.SelectedImage = tabBarItem.SelectedImage
                    .ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);
                tabBarItem.ImageInsets = new UIEdgeInsets(5.5f, 0, -5.5f, 0);
                tabBarItem.Title = null;
            }

            if (Mvx.Resolve<IDataModel>().SelectedNotification != null)
			{
				SelectedIndex = (int)BottomTabBar.Notification;
			}

			ViewControllerSelected += TabBarControllerOnViewControllerSelected;
			NavigationController?.SetNavigationBarHidden(true, animated);
		}

		private async Task ConfigProfileTab()
		{
            if (string.IsNullOrEmpty(ViewModel.AvatarUrl))
            {
                return;
            }

			_imgAvatar = UIHelper.CreateCustomImageView(DimensionHelper.ImageAvatarSize,
				DimensionHelper.ImageAvatarSize, ImageHelper.AvtOff, DimensionHelper.ImageAvatarSize / 2);
			_imgAvatarSelected = UIHelper.CreateCustomImageView(DimensionHelper.ImageAvatarSize,
				DimensionHelper.ImageAvatarSize, ImageHelper.AvtOff, DimensionHelper.ImageAvatarSize / 2);

            await ImageService.Instance.LoadUrl(ViewModel.AvatarUrl)
                .DownSample((int)DimensionHelper.ImageAvatarSize, (int)DimensionHelper.ImageAvatarSize)
                .Transform(new List<ITransformation> { new CircleTransformation() })
                .IntoAsync(_imgAvatar);
            await ImageService.Instance.LoadUrl(ViewModel.AvatarUrl)
                .DownSample((int)DimensionHelper.ImageAvatarSize, (int)DimensionHelper.ImageAvatarSize)
                .Transform(new List<ITransformation>
                    {new CircleTransformation(DimensionHelper.AvatarBorderWidth, "3fb8ea")})
                .IntoAsync(_imgAvatarSelected);

            TabBar.Items[(int)BottomTabBar.Profile].Image = _imgAvatar.Image;
			TabBar.Items[(int)BottomTabBar.Profile].SelectedImage = _imgAvatarSelected.Image;
		}


		private void TabBarControllerOnViewControllerSelected(object sender, UITabBarSelectionEventArgs e)
		{
			if (!(((UINavigationController)e.ViewController).TopViewController is MvxViewController)) return;

			if (_isOnNotificationTab)
			{
				ClearNotificationBadgeCommand.Execute();
				_isOnNotificationTab = false;
			}

			if (_isOnConversationTab)
			{
				ClearConversationBadgeCommand.Execute();
				_isOnConversationTab = false;
			}

			if (SelectedIndex == (int)BottomTabBar.Notification)
			{
				ClearNotificationBadgeCommand.Execute();
				_isOnNotificationTab = true;
			}

			if (SelectedIndex == (int)BottomTabBar.Conversation)
			{
				ClearConversationBadgeCommand.Execute();
				_isOnConversationTab = true;
			}
		}

		private void UpdateBadgeIcon(int badgeValue)
		{
			var settings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Badge, null);
			UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = badgeValue;
		}
	}
}