﻿using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.CustomControls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Interfaces;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen,
		ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class TermAndConditionView : BaseView, ITextViewDelegate
	{
		private UIScrollView _scrollView;

		private UIView _headerBar;
		private UIView _touchFieldBackButton;
		private UIView _separateLine;
		private UILabel _headerLabel;
		private UIButton _backButton;
		private UITextView _tvTermAndConditionContent;
		private string _termAndConditionContentInHtml;

		public string TermAndConditionContentInHtml
		{
			get => _termAndConditionContentInHtml;
			set
			{
				_termAndConditionContentInHtml = value;
				var attributedString = UIHelper.GetAttributedStringFromHtml(value, DimensionHelper.MediumTextSize);
				_tvTermAndConditionContent.AttributedText = attributedString;
				_tvTermAndConditionContent.TextAlignment = UITextAlignment.Justified;
			}
		}

		public IMvxCommand BackPressedCommand { get; set; }

		public bool ShouldInteractWithUrl(UITextView textView, NSUrl url, NSRange characterRange)
		{
			throw new System.NotImplementedException();
		}

		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<TermAndConditionView, TermAndConditionViewModel>();

			bindingSet.Bind(_headerLabel)
				.To(vm => vm.TermAndCondition);

			bindingSet.Bind(this)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);

			bindingSet.Bind(this)
				.For(v => v.TermAndConditionContentInHtml)
				.To(vm => vm.TermAndConditionContentInHtml);

			bindingSet.Apply();
		}
		protected override void InitView()
		{
			HeaderBar.Hidden = true;
			InitHeaderBar();
			InitTermAndConditionContent();
		}
		private void InitHeaderBar()
		{
			//headerBar About
			_headerBar = UIHelper.CreateView(DimensionHelper.HeaderBarHeight, ResolutionHelper.Width, UIColor.White);
			View.Add(_headerBar);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_headerBar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, ResolutionHelper.StatusHeight),
				NSLayoutConstraint.Create(_headerBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
			});
			//backButton
			_touchFieldBackButton = UIHelper.CreateView(50, 60);
			_backButton = UIHelper.CreateImageButton(DimensionHelper.BackButtonHeight, DimensionHelper.BackButtonWidth,
				ImageHelper.BackButton);

			_touchFieldBackButton.AddGestureRecognizer(new UITapGestureRecognizer(() =>
			{
				BackPressedCommand?.Execute();
			}));

			_headerBar.AddSubview(_backButton);
			_headerBar.AddSubview(_touchFieldBackButton);

			_headerBar.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_touchFieldBackButton, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _headerBar,
					NSLayoutAttribute.CenterY,1 , 0),
				NSLayoutConstraint.Create(_touchFieldBackButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _headerBar,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_backButton, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _headerBar,
					NSLayoutAttribute.CenterY,1 , 0),
				NSLayoutConstraint.Create(_backButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _headerBar,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin)
			});

			_headerLabel = UIHelper.CreateLabel(ColorHelper.LightBlue, DimensionHelper.BigTextSize, FontType.Medium);

			_headerBar.AddSubview(_headerLabel);
			_headerBar.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_headerLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _headerBar,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_headerLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _headerBar,
					NSLayoutAttribute.CenterY, 1, 0),
			});

			_separateLine = UIHelper.CreateView(DimensionHelper.HeaderBarLogoWidth, DimensionHelper.SeperatorHeight, ColorHelper.Gray);

			_headerBar.AddSubview(_separateLine);
			_headerBar.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_separateLine, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _headerBar,
					NSLayoutAttribute.Width,1 , 0),
				NSLayoutConstraint.Create(_separateLine, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1 , DimensionHelper.SeparateLineHeaderHeight),
				NSLayoutConstraint.Create(_separateLine, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _headerBar,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin)
			});

		}
		private void InitTermAndConditionContent()
		{
			_scrollView = UIHelper.CreateScrollView(0, 0);

			View.Add(_scrollView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _separateLine,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),
			});

			_tvTermAndConditionContent = UIHelper.CreateDisabledInteractionTextView(UIColor.Black, DimensionHelper.MediumTextSize);
			_tvTermAndConditionContent.Editable = false;
			_tvTermAndConditionContent.ScrollEnabled = false;
			_tvTermAndConditionContent.TextContainerInset = UIEdgeInsets.Zero;
			_tvTermAndConditionContent.ContentInset = UIEdgeInsets.Zero;
			_tvTermAndConditionContent.ClipsToBounds = false;
			_tvTermAndConditionContent.ScrollIndicatorInsets = UIEdgeInsets.Zero;
			_tvTermAndConditionContent.WeakLinkTextAttributes = new NSDictionary(UIStringAttributeKey.ForegroundColor, ColorHelper.ColorPrimary);

			_scrollView.AddSubview(_tvTermAndConditionContent);
			_scrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_tvTermAndConditionContent, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Top, 1, DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_tvTermAndConditionContent, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_tvTermAndConditionContent, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_tvTermAndConditionContent, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.MediumMargin),
			});
		}
	}
}