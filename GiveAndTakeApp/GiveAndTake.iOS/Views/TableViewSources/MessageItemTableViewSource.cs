﻿using Foundation;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Views.TableViewCells;
using MvvmCross.Platforms.Ios.Binding.Views;
using System;
using CoreGraphics;
using UIKit;

namespace GiveAndTake.iOS.Views.TableViewSources
{
	public class MessageItemTableViewSource : MvxStandardTableViewSource
	{
		public Action LoadMoreEvent { get; set; }

		private const string MyMessageItemCellId = "MyMessageItemCell";

		private const string OtherMessageItemCellId = "OtherMessageItemCell";

		private bool _isLoading;

		public MessageItemTableViewSource(UITableView tableView) : base(tableView)
		{
			tableView.RegisterClassForCellReuse(typeof(MyMessageItemCell), new NSString(MyMessageItemCellId));
			tableView.RegisterClassForCellReuse(typeof(OtherMessageItemCell), new NSString(OtherMessageItemCellId));
			_isLoading = false;
		}

		protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
		{
			var cell = new UITableViewCell();
			if (!(item is MessageItemViewModel messageItemViewModel))
			{
				return cell;
			}

			if (messageItemViewModel.IsOthersMessages)
			{
				cell = (OtherMessageItemCell)tableView.DequeueReusableCell(OtherMessageItemCellId, indexPath);
			}
			else
			{
				cell = (MyMessageItemCell)tableView.DequeueReusableCell(MyMessageItemCellId, indexPath);
			}

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			cell.Transform = CGAffineTransform.MakeScale(1, -1);

			return cell;
		}

		public override void Scrolled(UIScrollView scrollView)
		{
			if (!(scrollView is UITableView))
			{
				return;
			}

			var height = scrollView.Frame.Size.Height;
			var distanceFromBottom = scrollView.ContentSize.Height - scrollView.ContentOffset.Y;
			if (distanceFromBottom >= height || _isLoading) return;
			_isLoading = true;
			LoadMoreEvent?.BeginInvoke(result => _isLoading = false, null);
		}
	}
}