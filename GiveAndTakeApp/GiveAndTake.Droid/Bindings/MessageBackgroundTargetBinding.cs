﻿using Android.Widget;
using MvvmCross.Platforms.Android.Binding.Target;

namespace GiveAndTake.Droid.Bindings
{
	public class MessageBackgroundTargetBinding : MvxAndroidTargetBinding<TextView, bool>
	{
		public const string BindingPropertyName = "MessageBackground";

		private readonly TextView _textView;

		public MessageBackgroundTargetBinding(TextView target) : base(target)
		{
			_textView = target;
		}

		protected override void SetValueImpl(TextView target, bool isBlocked)
		{
			_textView.SetBackgroundResource(isBlocked
				? Resource.Drawable.other_blocked_message_background
				: Resource.Drawable.other_active_message_background);
		}
	}
}