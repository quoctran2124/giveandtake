﻿using Android.Preferences;
using GiveAndTake.Core;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.Models;

namespace GiveAndTake.Droid.Helpers
{
	public class StorageHelper : IStorageHelper
	{
		public void SaveLanguage(LanguageType language)
		{
			var prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
			var editor = prefs.Edit();
			editor.PutString(AppConstants.DeviceLanguage, language.ToString());
			editor.Apply();
		}

        public void SaveCredentials(LoginResponse loginResponse)
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            var editor = prefs.Edit();
            editor.PutBoolean(AppConstants.Authenticated, true);
            editor.PutString(AppConstants.Credentials, JsonHelper.Serialize(loginResponse));
            editor.Apply();
        }

        public LoginResponse GetCredentials()
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            var authenticated = prefs.GetBoolean(AppConstants.Authenticated, false);
            if (!authenticated)
            {
                return null;
            }

            return JsonHelper.Deserialize<LoginResponse>(prefs.GetString(AppConstants.Credentials, null));
        }

        public void DeleteCredentials()
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            var editor = prefs.Edit();
            editor.PutBoolean(AppConstants.Authenticated, false);
            editor.Remove(AppConstants.Credentials);
            editor.Apply();
        }
    }
}