using Android.Content;
using Android.Runtime;
using GiveAndTake.Core;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.Models;
using GiveAndTake.Droid.Views;
using MvvmCross;
using MvvmCross.Platforms.Android;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Share.Model;
using Xamarin.Facebook.Share.Widget;

namespace GiveAndTake.Droid.Helpers
{
	public class FacebookHelper : Java.Lang.Object, IFacebookHelper
	{
		private readonly ShareDialog _shareDialog;

		public FacebookHelper()
		{
			_shareDialog = new ShareDialog(Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity);
		}

		public void ShareFacebookContent(string content, string contentUrl)
		{
			var hashTag = new ShareHashtag.Builder();
			hashTag.SetHashtag("\n\n" + AppConstants.GiveAndTakeHashtag);

			if (ShareDialog.CanShow(Java.Lang.Class.FromType(typeof(ShareLinkContent))))
			{
				var linkContent = new ShareLinkContent.Builder();
				linkContent.SetQuote(content);
				linkContent.SetShareHashtag((ShareHashtag)hashTag.Build());
				linkContent.SetContentUrl(Android.Net.Uri.Parse(contentUrl));
				linkContent.JavaCast<ShareLinkContent.Builder>();
				_shareDialog.Show(linkContent.Build(), ShareDialog.Mode.Web);
			}
		}

		public bool IsLoggedIn() => AccessToken.CurrentAccessToken != null && !AccessToken.CurrentAccessToken.IsExpired;

		public User GetCurrentUser() => new User
		{
			FirstName = Profile.CurrentProfile.FirstName,
			LastName = Profile.CurrentProfile.LastName,
			DisplayName = Profile.CurrentProfile.Name,
			UserName = Profile.CurrentProfile.Id,
			AvatarUrl = GetProfilePicture(Profile.CurrentProfile.Id),
			SocialAccountId = Profile.CurrentProfile.Id
		};

		public void LogOut()
		{
			LoginManager.Instance.LogOut();
			// clear back stacks and navigate to login view
			var intent = new Intent(Android.App.Application.Context, typeof(LoginView));
			intent.SetFlags(ActivityFlags.ClearTask | ActivityFlags.NewTask);
			Android.App.Application.Context.StartActivity(intent);
		}

		private static string GetProfilePicture(string profileId) =>
			string.Format(AppConstants.FaceBookAvatarUrl, profileId);
	}
}