using System;
using Xamarin.Facebook;

namespace GiveAndTake.Droid.Helpers
{
	public class MyProfileTracker : ProfileTracker
	{
		public Action ExecuteLogin { private get; set; }

		protected override void OnCurrentProfileChanged(Profile oldProfile, Profile currentProfile)
		{
			if (currentProfile != null)
			{
				StopTracking();
				ExecuteLogin?.Invoke();
			}
		}
	}
}