﻿using System;
using System.Globalization;
using Android.Views;
using MvvmCross.Converters;

namespace GiveAndTake.Droid.Converters
{
    public class OtpCodeDisplayValueConverter : MvxValueConverter<string, string>
    {
        protected override string Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Join("   ", value.ToCharArray());
        }
    }
}