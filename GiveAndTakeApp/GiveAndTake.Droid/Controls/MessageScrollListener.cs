﻿using System;
using Android.Support.V7.Widget;
using FFImageLoading;

namespace GiveAndTake.Droid.Controls
{
	public class MessageScrollListener : RecyclerView.OnScrollListener
	{
		public Action LoadMoreEvent { get; set; }

		private readonly LinearLayoutManager _layoutManager;
		private int _visibleThreshold = 1;
		private int _currentPage = 0;
		private int _previousTotalItemCount = 0;
		private bool _isLoading;
		private int _startingPageIndex = 0;

		public MessageScrollListener(LinearLayoutManager layoutManager)
		{
			_layoutManager = layoutManager;
			_isLoading = false;
		}

		public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
		{
			base.OnScrolled(recyclerView, dx, dy);
			var firstVisibleItemPosition = 0;
			var totalItemCount = _layoutManager.ItemCount;

			firstVisibleItemPosition = ((LinearLayoutManager)_layoutManager).FindFirstVisibleItemPosition();

			if (totalItemCount < _previousTotalItemCount) 
			{
				this._currentPage = this._startingPageIndex;
				this._previousTotalItemCount = totalItemCount;
				if (totalItemCount == 0)
				{
					this._isLoading = true;
				}
			}

			if (_isLoading && (totalItemCount > _previousTotalItemCount))
			{
				_isLoading = false;
				_previousTotalItemCount = totalItemCount;
			}

			if (!_isLoading && (firstVisibleItemPosition - _visibleThreshold) < 0 && (dy < 0 || firstVisibleItemPosition == 0))
			{
				_currentPage++;
				LoadMoreEvent?.BeginInvoke(result => _isLoading = false, null);
				_isLoading = true;
			}
		}

		

		public override void OnScrollStateChanged(RecyclerView recyclerView, int newState)
		{
			base.OnScrollStateChanged(recyclerView, newState);

			switch (newState)
			{
				case RecyclerView.ScrollStateDragging:
					ImageService.Instance.SetPauseWork(true);
					break;

				case RecyclerView.ScrollStateIdle:
					ImageService.Instance.SetPauseWork(false);
					break;
			}
		}
	}
}