﻿using Android.Content;
using Android.Util;
using Android.Webkit;
using GiveAndTake.Droid.Helpers;

namespace GiveAndTake.Droid.Controls
{
	public class BindableWebView : WebView
	{
		private string _core = "<html><body style='text-align:{0};color:{1};font-size:{2}px;font-family:{3};margin: 0px 0px 0px 0px;'>{4}</body></html>";
		private string _textAlign = "left";
		private string _textColor = "black";
		private string _font = "@font/sanfranciscodisplay_medium";
		private string _textSize = "12dp";
		private string _text;

		public BindableWebView(Context context, IAttributeSet attrs)
			: base(context, attrs)
		{
			var typedArray = context.ObtainStyledAttributes(attrs, Resource.Styleable.BindableWebView, 0, 0);

			TextAlign = typedArray.GetString(Resource.Styleable.BindableWebView_TextAlign);
			TextColor = typedArray.GetString(Resource.Styleable.BindableWebView_TextColor);
			TextSize = typedArray.GetString(Resource.Styleable.BindableWebView_TextSize);
			Font = typedArray.GetString(Resource.Styleable.BindableWebView_Font);

		}

		private void ReloadData()
		{
			LoadData(string.Format(_core, _textAlign, _textColor, _textSize, _font, _text), "text/html", "utf-8");
		}
		public string TextAlign
		{
			get { return _textAlign; }
			set
			{
				_textAlign = value;
				ReloadData();
			}
		}
		public string TextColor
		{
			get { return _textColor; }
			set
			{
				string colorValue = "#" + value.Substring(3, value.Length - 3);
				_textColor = colorValue;
				ReloadData();
			}
		}

		public string TextSize
		{
			get { return _textSize; }
			set
			{
				var dpValue = value.Substring(0, value.Length - 2);
				var pxValue = ConvertHelper.SpToPx(dpValue, Context);
				_textSize = value;
				ReloadData();
			}
		}

		public string Font
		{
			get { return _font; }
			set
			{
				_font = value;
				ReloadData();
			}
		}
		public string Text
		{
			get { return _text; }
			set
			{
				if (string.IsNullOrEmpty(value)) return;

				_text = value;
				ReloadData();
			}
		}
	}
}