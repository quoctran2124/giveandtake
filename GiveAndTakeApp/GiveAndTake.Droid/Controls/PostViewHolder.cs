﻿using System;
using Android.OS;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using Com.Bumptech.Glide.Load;
using Com.Bumptech.Glide.Load.Resource.Bitmap;
using Com.Bumptech.Glide.Request;
using GiveAndTake.Core.ViewModels;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Binding.BindingContext;

namespace GiveAndTake.Droid.Controls
{
	public class PostViewHolder : MvxRecyclerViewHolder
	{
		private View _separator;
		private TextView _tvPostDate;
		private TextView _tvLocation;
		private TextView _tvPostTitle;
		private TextView _tvRequestCount;
		private TextView _tvUserFullName;
		private TextView _tvCommentCount;
		private TextView _tvAppreciationCount;
		private ImageView _imgLike;
		private ImageView _imgPost;
		private ImageView _imgAvatar;
		private ImageView _imgRequest;
		private ImageView _imgMultiImages;
		private ImageButton _imgOptions;
		private CustomTextView _tvCategory;
		private PostItemViewModel _post;

		public PostViewHolder(View itemView, IMvxAndroidBindingContext context) : base(itemView, context)
		{
			InitView(itemView);
			AddListeners();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && ItemView != null)
			{
				_imgOptions.Click += ShowMenuPopup;
				_imgAvatar.Click += ShowGiverProfile;
				_tvUserFullName.Click += ShowGiverProfile;
			}

			base.Dispose(disposing);
		}

		private void InitView(View view)
		{
			// All FindViewById here
			_tvCategory = view.FindViewById<CustomTextView>(Resource.Id.tvCategory);
			_tvPostDate = view.FindViewById<TextView>(Resource.Id.tvPostDate);
			_tvUserFullName = view.FindViewById<TextView>(Resource.Id.tvUserFullName);
			_tvLocation = view.FindViewById<TextView>(Resource.Id.tvLocation);
			_tvPostTitle = view.FindViewById<TextView>(Resource.Id.tvPostTitle);
			_tvRequestCount = view.FindViewById<TextView>(Resource.Id.tvRequestCount);
			_tvAppreciationCount = view.FindViewById<TextView>(Resource.Id.tvAppreciationCount);
			_tvCommentCount = view.FindViewById<TextView>(Resource.Id.tvCommentCount);
			_imgAvatar = view.FindViewById<ImageView>(Resource.Id.imgAvatar);
			_imgPost = view.FindViewById<ImageView>(Resource.Id.imgPost);
			_separator = view.FindViewById<View>(Resource.Id.separator);
			_imgOptions = view.FindViewById<ImageButton>(Resource.Id.imgOptions);
			_imgLike = view.FindViewById<ImageView>(Resource.Id.imgLike);
			_imgRequest = view.FindViewById<ImageView>(Resource.Id.imgRequest);
			_imgMultiImages = view.FindViewById<ImageView>(Resource.Id.imgMultiImages);
		}

		private void AddListeners()
		{
			_imgOptions.Click += ShowMenuPopup;
			_imgAvatar.Click += ShowGiverProfile;
			_tvUserFullName.Click += ShowGiverProfile;
		}

		private void ShowGiverProfile(object sender, EventArgs args)
		{
			_post.ShowGiverProfileCommand?.Execute();
		}

		private void ShowMenuPopup(object sender, EventArgs args)
		{
			_post.ShowMenuPopupCommand?.Execute();
		}

		public PostItemViewModel Post
		{
			set
			{
				_post = value;
				_tvCategory.Text = _post.CategoryName;
				_tvUserFullName.Text = _post.UserName;
				_tvPostTitle.Text = _post.PostTitle;
				_tvPostDate.Text = _post.CreatedTime;
				_tvLocation.Text = _post.Address;
				_tvRequestCount.Text = _post.RequestCount.ToString();
				_tvAppreciationCount.Text = _post.AppreciationCount.ToString();
				_tvCommentCount.Text = _post.CommentCount.ToString();
				_tvCategory.CategoryBackgroundColor = _post.BackgroundColor;
				_imgLike.Activated = _post.IsAppreciationIconActivated;
				_imgRequest.Activated = _post.IsRequestIconActivated;
				_separator.Visibility = _post.IsSeparatorLineShown ? ViewStates.Visible : ViewStates.Gone;
				_imgMultiImages.Visibility = _post.HasManyPostPhotos ? ViewStates.Visible : ViewStates.Gone;
				BindImageUrl(_imgPost, _post.PostImage, Resource.Drawable.default_post, new RoundedCorners(10));
				BindImageUrl(_imgAvatar, _post.AvatarUrl, Resource.Drawable.default_avatar, new CircleCrop());
				Click = _post.ShowPostDetailCommand;
			}
		}

		private static void BindImageUrl(ImageView imageView, string imageUrl, int defaultImageDrawable,
			params ITransformation[] transformations)
		{
			if (string.IsNullOrEmpty(imageUrl))
			{
				imageView.SetImageDrawable(
					Build.VERSION.SdkInt >= BuildVersionCodes.LollipopMr1
						? Android.App.Application.Context.Resources.GetDrawable(defaultImageDrawable, null)
						: ContextCompat.GetDrawable(Android.App.Application.Context, defaultImageDrawable)
				);
				return;
			}

			var requestOptions = new RequestOptions()
				.CenterCrop()
				.Placeholder(defaultImageDrawable)
				.Transforms(transformations);

			Glide.With(Android.App.Application.Context)
				.Load(imageUrl)
				.Apply(requestOptions)
				.Into(imageView);
		}
	}
}