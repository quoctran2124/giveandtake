﻿using System;
using Android.Text.Style;
using Android.Views;


namespace GiveAndTake.Droid.Controls.HtmlTextViewSupport
{
	public class InternalUrlSpan : ClickableSpan
	{
		public Action<View> LinkClicked;

		public override void OnClick(View widget)
		{
			LinkClicked?.Invoke(widget);
		}
	}
}