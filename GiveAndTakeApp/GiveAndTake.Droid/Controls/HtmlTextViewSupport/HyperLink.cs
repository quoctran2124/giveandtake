﻿using Android.Text.Style;

namespace GiveAndTake.Droid.Controls.HtmlTextViewSupport

{
	public class HyperLink
	{
		public URLSpan UrlSpan { get; set; }
		public InternalUrlSpan InternalUrlSpan { get; set; }
		public int Start { get; set; }
		public int End { get; set; }
	}
}
