﻿using Android.Support.V7.Widget;
using Android.Views;
using GiveAndTake.Core.ViewModels;
using MvvmCross.Binding.Extensions;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Binding.BindingContext;

namespace GiveAndTake.Droid.Controls
{
	public class PostAdapter : MvxRecyclerAdapter
	{
		public PostAdapter(IMvxAndroidBindingContext bindingContext) : base(bindingContext)
		{
		}

		public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
		{
			var itemBindingContext = new MvxAndroidBindingContext(parent.Context, BindingContext.LayoutInflaterHolder);
			var view = InflateViewForHolder(parent, viewType, itemBindingContext);

			return new PostViewHolder(view, itemBindingContext);
		}

		public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
		{
			base.OnBindViewHolder(holder, position);

			if (!(holder is PostViewHolder viewHolder) ||
			    !(ItemsSource.ElementAt(position) is PostItemViewModel viewModel)) return;

			viewHolder.Post = viewModel;
		}
	}
}