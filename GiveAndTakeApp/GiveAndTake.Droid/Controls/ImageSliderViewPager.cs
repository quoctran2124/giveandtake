﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Java.Lang;

namespace GiveAndTake.Droid.Controls
{
	public class ImageSliderViewPager : Android.Support.V4.View.ViewPager
	{
		public override bool OnInterceptTouchEvent(MotionEvent ev)
		{
			try
			{
				return base.OnInterceptTouchEvent(ev);
			}
			catch (IllegalArgumentException e)
			{
				e.PrintStackTrace();
				return false;
			}
		}

		protected ImageSliderViewPager(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{
		}

		public ImageSliderViewPager(Context context) : base(context)
		{
		}

		public ImageSliderViewPager(Context context, IAttributeSet attrs) : base(context, attrs)
		{
		}
	}
}