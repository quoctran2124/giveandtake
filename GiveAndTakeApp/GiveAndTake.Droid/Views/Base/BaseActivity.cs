using Android.App;
using Android.Content.PM;
using Android.OS;
using HockeyApp.Android;
using MvvmCross.Droid.Support.V7.AppCompat;
using Plugin.CurrentActivity;

namespace GiveAndTake.Droid.Views.Base
{
	[Activity(Label = "BaseActivity", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public abstract class BaseActivity : MvxAppCompatActivity
    {
#if DEVELOPMENT
		const string HockeyAppid = "d7e22c7ae32d4012990c098e0b6791e9";
#else
		const string HockeyAppid = "9f427f51ef0e48b4b86952c0f02fa65a";
#endif
		protected abstract int LayoutId { get; }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();

            if (ViewModel != null)
            {
                SetContentView(LayoutId);
                InitView();
                CreateBinding();
            }
        }

        protected abstract void InitView();

        protected virtual void CreateBinding()
        {
        }

	    protected override void OnCreate(Bundle bundle)
	    {
		    base.OnCreate(bundle);

		    CrashManager.Register(this, HockeyAppid, new CrashListener());
		    CrossCurrentActivity.Current.Activity = this;
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
		{
			Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
			base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}

	    protected override void OnResume()
	    {
		    base.OnResume();

		    Tracking.StartUsage(this);
		}

	    protected override void OnPause()
	    {
		    Tracking.StopUsage(this);

		    base.OnPause();
	    }
	}

	public class CrashListener : CrashManagerListener
	{
		public override bool ShouldAutoUploadCrashes()
		{
			return true;
		}
	}
}