﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.Base;
using ME.Leolin.Shortcutbadger;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace GiveAndTake.Droid.Views.Base
{
	[MvxActivityPresentation]
	[Activity(Label = "View for HomeViewModel", WindowSoftInputMode = SoftInput.AdjustPan, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MasterView : BaseActivity
	{
		protected override int LayoutId => Resource.Layout.MasterView;
		public IMvxAsyncCommand ShowInitialViewModelsCommand { get; set; }
		//TODO: Refactor: Investigate if we create a generic backCommand or not?
		public IMvxCommand BackPressedFromCreatePostCommand
		{
			get => _backPressedFromCreatePostCommand;
			set
			{
				SetNullForAllRequestCommand();
				_backPressedFromCreatePostCommand = value;
			}
		}
		public IMvxCommand BackPressedFromHomeViewSearchedCommand
		{
			get => _backPressedFromHomeViewSearchedCommand;
			set
			{
				SetNullForAllRequestCommand();
				_backPressedFromHomeViewSearchedCommand = value;
			}
		}
		public IMvxCommand BackPressedFromPostDetailCommand
		{
			get => _backPressedFromPostDetailCommand;
			set
			{
				SetNullForAllRequestCommand();
				_backPressedFromPostDetailCommand = value;
			}
		}
		public IMvxCommand BackPressedFromRequestCommand
		{
			get => _backPressedFromRequestCommand;
			set
			{
				SetNullForAllRequestCommand();
				_backPressedFromRequestCommand = value;
			}
		}
		public IMvxCommand BackPressedFromCommentCommand
		{
			get => _backPressedFromCommentCommand;
			set
			{
				SetNullForAllRequestCommand();
				_backPressedFromCommentCommand = value;
			}
		}

		public event EventHandler<int> BackToHomeViewTabFromAnotherTab;

		public static bool IsForeground;
		public bool IsHomeScreen = true;
		public int CurrentTabPosition;
		private IMvxCommand _backPressedFromCreatePostCommand;
		private IMvxCommand _backPressedFromHomeViewSearchedCommand;
		private IMvxCommand _backPressedFromPostDetailCommand;
		private IMvxCommand _backPressedFromRequestCommand;
		private IMvxCommand _backPressedFromCommentCommand;

		protected override void InitView()
		{
		}

		protected override void OnResume()
		{
			base.OnResume();
			IsForeground = true;
			//(ViewModel as BaseViewModel)?.OnActive();
			ShortcutBadger.RemoveCount(Android.App.Application.Context);
		}

		protected override void OnPause()
		{
			base.OnPause();
			IsForeground = false;
			(ViewModel as BaseViewModel)?.OnDeactive();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();

			bindingSet.Bind(this)
				.For(v => v.ShowInitialViewModelsCommand)
				.To(vm => vm.ShowInitialViewModelsCommand);

			bindingSet.Apply();
		}

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			if (bundle == null)
			{
				ShowInitialViewModelsCommand.Execute();
			}
		}
		public override void OnBackPressed()
		{
			if (BackPressedFromCreatePostCommand != null)
			{
				BackPressedFromCreatePostCommand.Execute();
				BackPressedFromCreatePostCommand = null;
			}
			else if (BackPressedFromHomeViewSearchedCommand != null)
			{
				BackPressedFromHomeViewSearchedCommand.Execute();
				BackPressedFromHomeViewSearchedCommand = null;
			}
			else if (BackPressedFromPostDetailCommand != null)
			{
				BackPressedFromPostDetailCommand.Execute();
				BackPressedFromPostDetailCommand = null;
			}
			else if (BackPressedFromRequestCommand != null)
			{
				BackPressedFromRequestCommand.Execute();
				BackPressedFromRequestCommand = null;
			}
			else if (BackPressedFromCommentCommand != null)
			{
				BackPressedFromCommentCommand.Execute();
				BackPressedFromCommentCommand = null;
			}
			else
			{
				if (IsHomeScreen)
				{
					switch (CurrentTabPosition)
					{
						case AppConstants.NotificationTabIndex:
							BackToHomeViewTabFromAnotherTab?.Invoke(this, AppConstants.NotificationTabIndex);
							break;
						case AppConstants.ConversationTabIndex:
							BackToHomeViewTabFromAnotherTab?.Invoke(this, AppConstants.ConversationTabIndex);
							break;
						case AppConstants.ProfileTabIndex:
							BackToHomeViewTabFromAnotherTab?.Invoke(this, AppConstants.ProfileTabIndex);
							break;
						default:
							MoveTaskToBack(true);
							break;
					}
				}
				else
				{
					base.OnBackPressed();
				}
			}
		}

		public void SetNullForAllRequestCommand()
		{
			_backPressedFromCreatePostCommand = null;
			_backPressedFromHomeViewSearchedCommand = null;
			_backPressedFromPostDetailCommand = null;
			_backPressedFromRequestCommand = null;
			_backPressedFromCommentCommand = null;
		}
	}
}