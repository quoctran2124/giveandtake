﻿using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.TabNavigation;
using GiveAndTake.Droid.Controls;
using GiveAndTake.Droid.Helpers;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using System.Collections.Generic;

namespace GiveAndTake.Droid.Views.TabNavigation
{
	[MvxFragmentPresentation(typeof(MasterViewModel), Resource.Id.content_frame, true)]
	public class TabNavigationView : BaseFragment
	{
		#region Fields

		private CustomCircleImageView _ccimProfile;
		private TabLayout _tabLayout;
		private bool _isOnNotificationTab;
		private bool _isOnConversationTab;
		private int _notificationCount;
		private int _conversationBadgeCount;
		private TextView _badgeText;
		private TextView _conversationBadgeText;

		private static readonly Dictionary<string, int> TabTitleIconsDictionary = new Dictionary<string, int>(){
			{AppConstants.HomeTab,Resource.Drawable.tab_navigation_icon_home},
			{AppConstants.NotificationTab,Resource.Drawable.tab_navigation_icon_notification},
			{AppConstants.ConversationTab,Resource.Drawable.tab_navigation_icon_conversation},
			{AppConstants.ProfileTab,Resource.Drawable.tab_navigation_icon_profile},
		};

		#endregion

		#region Properties

		public string AvatarUrl { get; set; }

		protected override int LayoutId => Resource.Layout.TabNavigation;

		public IMvxAsyncCommand ShowInitialViewModelsCommand { get; set; }

		public IMvxCommand ShowErrorCommand { get; set; }

		public IMvxAsyncCommand ShowNotificationsCommand { get; set; }

		public IMvxCommand ClearNotificationBadgeCommand { get; set; }

		public IMvxCommand ClearConversationBadgeCommand { get; set; }

		public int NotificationCount
		{
			get => _notificationCount;
			set
			{
				_notificationCount = value;
				if (value > 99)
				{
					_badgeText.Text = "99+";
				}
				else
				{
					_badgeText.Text = value + "";
				}
				_badgeText.Visibility = value == 0 ? ViewStates.Invisible : ViewStates.Visible;
			}
		}

		public int ConversationBadgeCount
		{
			get => _conversationBadgeCount;
			set
			{
				_conversationBadgeCount = value;
				if (value > 99)
				{
					_conversationBadgeText.Text = "99+";
				}
				else
				{
					_conversationBadgeText.Text = value + "";
				}
				_conversationBadgeText.Visibility = value == 0 ? ViewStates.Invisible : ViewStates.Visible;
			}
		}

		#endregion

		#region Overides

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<TabNavigationView, TabNavigationViewModel>();

			bindingSet.Bind(this)
				.For(v => v.ShowInitialViewModelsCommand)
				.To(vm => vm.ShowInitialViewModelsCommand);

			bindingSet.Bind(this)
				.For(v => v.AvatarUrl)
				.To(vm => vm.AvatarUrl);

			bindingSet.Bind(this)
				.For(v => v.ShowErrorCommand)
				.To(vm => vm.ShowErrorCommand);

			bindingSet.Bind(this)
				.For(v => v.ShowNotificationsCommand)
				.To(vm => vm.ShowNotificationsCommand);

			bindingSet.Bind(this)
				.For(v => v.ClearNotificationBadgeCommand)
				.To(vm => vm.ClearNotificationBadgeCommand);

			bindingSet.Bind(this)
				.For(v => v.ClearConversationBadgeCommand)
				.To(vm => vm.ClearConversationBadgeCommand);

			bindingSet.Bind(this)
				.For(v => v.NotificationCount)
				.To(vm => vm.NotificationCount);

			bindingSet.Bind(this)
				.For(v => v.ConversationBadgeCount)
				.To(vm => vm.ConversationBadgeCount);

			bindingSet.Apply();
		}

		protected override void InitView(View view)
		{
			base.InitView(view);
			_tabLayout = view.FindViewById<TabLayout>(Resource.Id.tabLayout);
			var viewPager = view.FindViewById<ViewPager>(Resource.Id.viewPager);
			viewPager.OffscreenPageLimit = AppConstants.NumOfFragmentViewPager;
			
		}

		private void OnBackToHomeView(object sender, int tabPosition)
		{
			if (tabPosition != 0)
			{
				_tabLayout.GetTabAt(0).Select();
				((MasterView)Activity).CurrentTabPosition = 0;
			}			
		}

		public override void OnDestroy()
		{
			base.OnDestroy();
			Activity.Finish();
			((MasterView)Activity).BackToHomeViewTabFromAnotherTab -= OnBackToHomeView;
		}

		public override void OnViewCreated(View view, Bundle bundle)
		{
			if (bundle == null)
			{
				ShowInitialViewModelsCommand.Execute();
			}

			_ccimProfile = new CustomCircleImageView(Context)
			{
				ImageUrl = AvatarUrl,
				LayoutParameters =
					new ActionBar.LayoutParams(
						(int)DimensionHelper.FromDimensionId(Resource.Dimension.tab_navigation_icon_size),
						(int)DimensionHelper.FromDimensionId(Resource.Dimension.image_avatar_size)),
			};

			if (_tabLayout.TabCount != TabTitleIconsDictionary.Count)
			{
				ShowErrorCommand.Execute(null);
				return;
			}

			for (var index = 0; index < _tabLayout.TabCount; index++)
			{
				var tab = _tabLayout.GetTabAt(index);
				
				if (index == 1)
				{
					tab.SetCustomView(Resource.Layout.NotificationIconView);
				}
				else if (index == 2)
				{
					tab.SetCustomView(Resource.Layout.ConversationIconView);
				}
				else
				{
					tab.SetIcon(TabTitleIconsDictionary[tab.Text]);
					tab.SetText("");
				}
			}

			_tabLayout.GetTabAt(_tabLayout.TabCount - 1).SetCustomView(_ccimProfile);

			// set notification tab badge values
			_badgeText = _tabLayout.GetTabAt(1).CustomView.FindViewById<TextView>(Resource.Id.badge_notification);
			_badgeText.Text = NotificationCount + "";
			_badgeText.Visibility = _badgeText.Text == "0" ? ViewStates.Invisible : ViewStates.Visible;

			// set conversation tab badge values
			_conversationBadgeText = _tabLayout.GetTabAt(2).CustomView.FindViewById<TextView>(Resource.Id.badge_conversation);
			_conversationBadgeText.Text = ConversationBadgeCount + "";
			_conversationBadgeText.Visibility = _conversationBadgeText.Text == "0" ? ViewStates.Invisible : ViewStates.Visible;

			_tabLayout.TabSelected += OnTabSelected;
			((MasterView)Activity).BackToHomeViewTabFromAnotherTab += OnBackToHomeView;
		}

		public override void OnResume()
		{
			base.OnResume();
			((MasterView)Activity).IsHomeScreen = true;
		}

		public override void OnPause()
		{
			base.OnPause();
			((MasterView)Activity).IsHomeScreen = false;
		}

		#endregion

		#region Event Handlers

		private void OnTabSelected(object sender, TabLayout.TabSelectedEventArgs e)
		{
			if (e.Tab.Position == _tabLayout.TabCount - 1)
			{
				_ccimProfile.Transformations = new List<ITransformation>
				{
					new CircleTransformation(
						DimensionHelper.FromDimensionId(Resource.Dimension.tab_navigation_icon_border_width),
						Resources.GetString(Resource.Color.colorPrimary))
				};
			}
			else
			{
				_ccimProfile.Transformations = new List<ITransformation>
				{
					new CircleTransformation()
				};
			}
			_tabLayout.GetTabAt(_tabLayout.TabCount - 1).SetCustomView(_ccimProfile);

			if (_isOnNotificationTab)
			{
				ClearNotificationBadgeCommand.Execute();
				_isOnNotificationTab = false;
			}

			if (_isOnConversationTab)
			{
				ClearConversationBadgeCommand?.Execute();
				_isOnConversationTab = false;
			}

			switch (e.Tab.Position)
			{
				case 1:
					ClearNotificationBadgeCommand.Execute();
					_isOnNotificationTab = true;
					((MasterView)Activity).CurrentTabPosition = 1;
					break;
				case 2:
					ClearConversationBadgeCommand?.Execute();
					_isOnConversationTab = true;
					((MasterView)Activity).CurrentTabPosition = 2;
					break;
				case 3:
					((MasterView)Activity).CurrentTabPosition = 3;
					break;
			}
		}		
		#endregion
	}
}

