﻿using System;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.TabNavigation;
using GiveAndTake.Droid.Helpers;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.ViewModels;
using SearchView = Android.Support.V7.Widget.SearchView;

namespace GiveAndTake.Droid.Views.TabNavigation
{
	[MvxTabLayoutPresentation(TabLayoutResourceId = Resource.Id.tabLayout,
		Title = AppConstants.ConversationTab,
		ViewPagerResourceId = Resource.Id.viewPager,
		FragmentHostViewType = typeof(TabNavigationView))]
	[Register(nameof(ConversationListView))]
	public class ConversationListView : BaseFragment
	{
		public IMvxCommand SearchCommand { get; set; }
		public IMvxCommand LoadMoreCommand { get; set; }
		public IMvxCommand CloseSearchBarCommand { get; set; }
		public IMvxCommand BackPressedCommand { get; set; }
		public IMvxInteraction ShowProfileTab
		{
			get => _showProfileTab;
			set
			{
				if (_showProfileTab != null)
					_showProfileTab.Requested -= OnShowProfileTabRequested;

				_showProfileTab = value;
				_showProfileTab.Requested += OnShowProfileTabRequested;
			}
		}

		protected override int LayoutId => Resource.Layout.ConversationListView;
		private SearchView _searchView;
		private ImageView _clearButton;
		private IMvxInteraction _showProfileTab;

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<ConversationListView, ConversationListViewModel>();

			bindingSet.Bind(this)
				.For(v => v.SearchCommand)
				.To(vm => vm.SearchCommand);

			bindingSet.Bind(this)
				.For(v => v.LoadMoreCommand)
				.To(vm => vm.LoadMoreCommand);

			bindingSet.Bind(this)
				.For(v => v.CloseSearchBarCommand)
				.To(vm => vm.CloseSearchBarCommand);

			bindingSet.Bind(this)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);

			bindingSet.Bind(_clearButton)
				.For(v => v.Visibility)
				.To(vm => vm.IsClearButtonShown)
				.WithConversion("BoolToViewStates");

			bindingSet.Apply();
		}

		protected override void InitView(View view)
		{
			base.InitView(view);

			_searchView = view.FindViewById<SearchView>(Resource.Id.searchView);
			_searchView.QueryTextSubmit += OnQueryTextSubmit;
			_searchView.Click += OnSearchViewClicked;
			_searchView.QueryTextChange += OnQueryTextChanged;

			_clearButton = (ImageView)_searchView.FindViewById(MvvmCross.Droid.Support.V7.AppCompat.Resource.Id.search_close_btn);
			_clearButton.Click += OnClearButtonClicked;

			AddScrollEvent(view.FindViewById<MvxRecyclerView>(Resource.Id.rvConversation), () => LoadMoreCommand?.Execute());
		}

		private void OnClearButtonClicked(object sender, EventArgs e)
		{
			_searchView.SetQuery("", false);
			_clearButton.Visibility = ViewStates.Gone;
			_clearButton.Focusable = false;
			CloseSearchBarCommand.Execute();
			_searchView.ClearFocus();
			_searchView.Focusable = false;
		}

		private void OnQueryTextChanged(object sender, SearchView.QueryTextChangeEventArgs e)
		{
			if (_searchView.Query == "")
			{
				_clearButton.Visibility = ViewStates.Gone;
			}
		}

		private void OnSearchViewClicked(object sender, EventArgs args)
		{
			_searchView.Iconified = false;
		}

		private void OnQueryTextSubmit(object sender, SearchView.QueryTextSubmitEventArgs e)
		{
			KeyboardHelper.HideKeyboard(sender as View);
			SearchCommand.Execute();
			((MasterView)Activity).BackPressedFromHomeViewSearchedCommand = BackPressedCommand;
			_searchView.ClearFocus();
		}
		private void OnShowProfileTabRequested(object sender, EventArgs e)
		{
			var tabhost = Activity.FindViewById<TabLayout>(Resource.Id.tabLayout);
			tabhost.GetTabAt(AppConstants.ProfileTabIndex).Select();
		}

		private void AddScrollEvent(MvxRecyclerView recyclerView, Action action)
		{
			var layoutManager = new LinearLayoutManager(Context);
			recyclerView.AddOnScrollListener(new ScrollListener(layoutManager)
			{
				LoadMoreEvent = action
			});
			recyclerView.SetLayoutManager(layoutManager);
		}

		protected override void Dispose(bool disposing)
		{
			_searchView.QueryTextSubmit -= OnQueryTextSubmit;
			_searchView.Click -= OnSearchViewClicked;
			_clearButton.Click -= OnClearButtonClicked;
			_searchView.QueryTextChange -= OnQueryTextChanged;
			base.Dispose(disposing);
		}
	}
}