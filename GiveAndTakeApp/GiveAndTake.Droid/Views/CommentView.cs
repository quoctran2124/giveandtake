﻿using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Droid.Helpers;
using GiveAndTake.Droid.Views.Base;
using GiveAndTake.Droid.Views.TabNavigation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Extensions;
using MvvmCross.Commands;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.ViewModels;

namespace GiveAndTake.Droid.Views
{
	[MvxFragmentPresentation(typeof(MasterViewModel), Resource.Id.content_frame, true)]
	[Register(nameof(CommentView))]
	public class CommentView : BaseFragment
	{
		private MvxRecyclerView _rvComment;
		private EditText _commentText;

		public IMvxCommand LoadMoreCommand { get; set; }
		public IMvxCommand BackPressedCommand { get; set; }

		private MvxInteraction<int> _scrollToCommentInteraction;
		public MvxInteraction<int> ScrollToCommentInteraction
		{
			get => _scrollToCommentInteraction;
			set
			{
				if (_scrollToCommentInteraction != null)
				{
					_scrollToCommentInteraction.Requested -= OnRequestedScrollToComment;
				}
				_scrollToCommentInteraction = value;
				_scrollToCommentInteraction.Requested += OnRequestedScrollToComment;
			}
		}

		public override void OnDestroy()
		{
			base.OnDestroy();
			_commentText.FocusChange -= OnCommentTextFocusChangeEvent;
		}

		protected override int LayoutId => Resource.Layout.CommentView;

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<CommentView, CommentViewModel>();

			bindingSet.Bind(this)
				.For(v => v.LoadMoreCommand)
				.To(vm => vm.LoadMoreCommand);

			bindingSet.Bind(this)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);

			bindingSet.Bind(this)
				.For(v => v.ScrollToCommentInteraction)
				.To(vm => vm.ScrollToCommentInteraction);

			bindingSet.Bind(_commentText)
				.To(vm => vm.SendCommentContent);

			bindingSet.Apply();
		}

		protected override void InitView(View view)
		{
			base.InitView(view);

			var layoutManager = new LinearLayoutManager(view.Context);

			_rvComment = view.FindViewById<MvxRecyclerView>(Resource.Id.rvComment);

			_rvComment.AddOnScrollListener(new ScrollListener(layoutManager)
			{
				LoadMoreEvent = LoadMoreEvent
			});

			_rvComment.SetLayoutManager(layoutManager);

			var dividerItemDecoration = new DividerItemDecoration(_rvComment.Context, layoutManager.Orientation);
			dividerItemDecoration.SetDrawable(Resources.GetDrawable(Resource.Drawable.line_divider, null));
			_rvComment.AddItemDecoration(dividerItemDecoration);

			Activity.Window.SetSoftInputMode(SoftInput.AdjustResize);

			_commentText = view.FindViewById<EditText>(Resource.Id.commentText);
			_commentText.FocusChange += OnCommentTextFocusChangeEvent;

			view.FindViewById<View>(Resource.Id.commentSeparator);

		}

		protected override void HandleActivityCommandFromFragment()
		{
			base.HandleActivityCommandFromFragment();
			((MasterView)Activity).BackPressedFromCommentCommand = BackPressedCommand;
		}

		private void OnCommentTextFocusChangeEvent(object sender, View.FocusChangeEventArgs e)
		{
			if (!e.HasFocus)
			{
				KeyboardHelper.HideKeyboard(View);
			}
		}

		private void LoadMoreEvent()
		{
			LoadMoreCommand?.Execute();
		}

		private void OnRequestedScrollToComment(object sender, MvvmCross.Base.MvxValueEventArgs<int> e)
		{
			ScrollToComment(e.Value);
		}

		private void ScrollToComment(int index)
		{
			KeyboardHelper.HideKeyboard(View);

			if (index >= 0 && index < _rvComment.ItemsSource.Count())
			{
				_rvComment.ScrollToPosition(index);
			}
		}
	}
}