using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Widget;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using System;
using Android.Text.Method;
using Android.Views;
using GiveAndTake.Droid.Helpers;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Result = Android.App.Result;

namespace GiveAndTake.Droid.Views
{
	[MvxActivityPresentation]
	[Activity(Label = "GiveAndTake.Droid.LoginView", ScreenOrientation = ScreenOrientation.Portrait)]
	public class LoginView : BaseActivity
	{
        #region Fields

        private bool _isPasswordShown;
        private Button _btnFacebookLogin;
        private EditText _edtPassword;
        private ICallbackManager _callbackManager;
        private MyProfileTracker _profileTracker;

        #endregion

        #region Properties

        public bool IsPasswordShown
        {
            get => _isPasswordShown;
            set
            {
                _isPasswordShown = value;
                _edtPassword.TransformationMethod = value ? null : new PasswordTransformationMethod();
            }
        }

        public IMvxCommand LoginCommand { get; set; }

        protected override int LayoutId => Resource.Layout.LoginView;

        #endregion

        protected override void InitView()
		{
			_callbackManager = CallbackManagerFactory.Create();
			LoginManager.Instance.RegisterCallback(_callbackManager, null);
			_profileTracker = new MyProfileTracker { ExecuteLogin = () => LoginCommand.Execute() };
			_profileTracker.StartTracking();

			_btnFacebookLogin = FindViewById<Button>(Resource.Id.btnFb);
			_btnFacebookLogin.Click += OnLoginButtonClicked;

            _edtPassword = FindViewById<EditText>(Resource.Id.edtPassword);
            _edtPassword.FocusChange += OnEditTextFocusChange;
        }

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<LoginView, LoginViewModel>();

			bindingSet.Bind(this)
				.For(v => v.LoginCommand)
				.To(vm => vm.LoginCommand);

            bindingSet.Bind(this)
                .For(v => v.IsPasswordShown)
                .To(vm => vm.IsPasswordShown);

            bindingSet.Apply();
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			_callbackManager.OnActivityResult(requestCode, (int) resultCode, data);
			base.OnActivityResult(requestCode, resultCode, data);
		}

        protected override void OnDestroy()
        {
			_btnFacebookLogin.Click -= OnLoginButtonClicked;
            _edtPassword.FocusChange -= OnEditTextFocusChange;
            base.OnDestroy();
        }

        private void OnLoginButtonClicked(object sender, EventArgs e)
		{
			LoginManager.Instance.LogInWithReadPermissions(this, new[] {"public_profile", "email"});
		}

        private void OnEditTextFocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (sender is EditText editText && !editText.HasFocus)
            {
                KeyboardHelper.HideKeyboard(editText);
            }
        }
    }
}