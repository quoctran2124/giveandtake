﻿using Android.App;
using Android.Content.PM;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace GiveAndTake.Droid.Views
{
    [MvxActivityPresentation]
    [Activity(ScreenOrientation = ScreenOrientation.Portrait)]
    public class ForgotPasswordView : BaseActivity
    {
        protected override int LayoutId => Resource.Layout.ForgotPasswordView;

        protected override void InitView()
        {
        }
    }
}
