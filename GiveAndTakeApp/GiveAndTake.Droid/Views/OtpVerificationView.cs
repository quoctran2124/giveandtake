﻿using Android.App;
using Android.Content.PM;
using Android.Text;
using Android.Widget;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.Droid.Helpers;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace GiveAndTake.Droid.Views
{
    [MvxActivityPresentation]
    [Activity(ScreenOrientation = ScreenOrientation.Portrait)]
    public class OtpVerificationView : BaseActivity
    {
        private EditText _edtOtp1;
        private EditText _edtOtp2;
        private EditText _edtOtp3;
        private EditText _edtOtp4;
        private EditText _edtOtp5;
        private EditText _edtOtp6;

        protected override int LayoutId => Resource.Layout.OtpVerificationView;

        protected override void InitView()
        {
            _edtOtp1 = FindViewById<EditText>(Resource.Id.edtOtp1);
            _edtOtp2 = FindViewById<EditText>(Resource.Id.edtOtp2);
            _edtOtp3 = FindViewById<EditText>(Resource.Id.edtOtp3);
            _edtOtp4 = FindViewById<EditText>(Resource.Id.edtOtp4);
            _edtOtp5 = FindViewById<EditText>(Resource.Id.edtOtp5);
            _edtOtp6 = FindViewById<EditText>(Resource.Id.edtOtp6);

            _edtOtp1.TextChanged += EdtOtpOnTextChanged;
            _edtOtp2.TextChanged += EdtOtpOnTextChanged;
            _edtOtp3.TextChanged += EdtOtpOnTextChanged;
            _edtOtp4.TextChanged += EdtOtpOnTextChanged;
            _edtOtp5.TextChanged += EdtOtpOnTextChanged;
            _edtOtp6.TextChanged += EdtOtpOnTextChanged;
        }

        protected override void CreateBinding()
        {
            base.CreateBinding();

            var bindingSet = this.CreateBindingSet<OtpVerificationView, OtpVerificationViewModel>();

            bindingSet.Apply();
        }

        protected override void OnDestroy()
        {
            _edtOtp1.TextChanged -= EdtOtpOnTextChanged;
            _edtOtp2.TextChanged -= EdtOtpOnTextChanged;
            _edtOtp3.TextChanged -= EdtOtpOnTextChanged;
            _edtOtp4.TextChanged -= EdtOtpOnTextChanged;
            _edtOtp5.TextChanged -= EdtOtpOnTextChanged;
            _edtOtp6.TextChanged -= EdtOtpOnTextChanged;
            base.OnDestroy();
        }

        private void EdtOtpOnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!(sender is EditText edt) || e.AfterCount == 0)
            {
                return;
            }

            switch (edt.Id)
            {
                case Resource.Id.edtOtp1:
                    _edtOtp2.RequestFocus();
                    break;
                case Resource.Id.edtOtp2:
                    _edtOtp3.RequestFocus();
                    break;
                case Resource.Id.edtOtp3:
                    _edtOtp4.RequestFocus();
                    break;
                case Resource.Id.edtOtp4:
                    _edtOtp5.RequestFocus();
                    break;
                case Resource.Id.edtOtp5:
                    _edtOtp6.RequestFocus();
                    break;
                case Resource.Id.edtOtp6:
                    KeyboardHelper.HideKeyboard(_edtOtp6);
                    break;
                default:
                    return;
            }
        }
    }
}
