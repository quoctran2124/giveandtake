﻿using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.ViewModels;
using System;
using Android.Content;
using Android.Widget;
using GiveAndTake.Droid.Helpers;
using GiveAndTake.Droid.Views.TabNavigation;
using Java.Lang;
using MvvmCross.Binding.Extensions;
using MvvmCross.Commands;

namespace GiveAndTake.Droid.Views
{
	[MvxFragmentPresentation(typeof(MasterViewModel), Resource.Id.content_frame, true)]
	[Register(nameof(ConversationView))]
	public class ConversationView : BaseFragment
	{
		#region Properties

		private bool _isBlocked;
		public bool IsBlocked
		{
			get => _isBlocked;
			set
			{
				_isBlocked = value;
				UpdateConversationUi(value);
			}
		}

		private IMvxInteraction _scrollToBottomInteraction;
		public IMvxInteraction ScrollToBottomInteraction
		{
			get => _scrollToBottomInteraction;
			set
			{
				if (_scrollToBottomInteraction != null)
				{
					_scrollToBottomInteraction.Requested -= OnInteractionRequested;
				}

				_scrollToBottomInteraction = value;
				_scrollToBottomInteraction.Requested += OnInteractionRequested;
			}
		}

		public IMvxCommand LoadMoreCommand { get; set; }

		public IMvxCommand BackPressedCommand { get; set; }

		private MvxInteraction<int> _scrollToMessageInteraction;
		public MvxInteraction<int> ScrollToMessageInteraction
		{
			get => _scrollToMessageInteraction;
			set
			{
				if (_scrollToMessageInteraction != null)
				{
					_scrollToMessageInteraction.Requested -= OnRequestedScrollToMessage;
				}
				_scrollToMessageInteraction = value;
				_scrollToMessageInteraction.Requested += OnRequestedScrollToMessage;
			}
		}

		protected override int LayoutId => Resource.Layout.ConversationView;

		private MvxRecyclerView _rvMessages;
		private LinearLayoutManager _layoutManager;
		private RelativeLayout _friendInfoLayout;
		private LinearLayout _messageBoxLayout;
		private EditText _messageEditText;

		#endregion

		protected override void InitView(View view)
		{
			_rvMessages = view.FindViewById<MvxRecyclerView>(Resource.Id.rvMessages);

			_layoutManager = new WrapContentLinearLayoutManager(Activity, LinearLayoutManager.Vertical, true);
			_rvMessages.AddOnScrollListener(new ScrollListener(_layoutManager)
			{
				LoadMoreEvent = LoadMoreEvent,
			});
			_rvMessages.SetLayoutManager(_layoutManager);

			_friendInfoLayout = view.FindViewById<RelativeLayout>(Resource.Id.topBar);
			_messageBoxLayout = view.FindViewById<LinearLayout>(Resource.Id.comment_box);
			_messageEditText = view.FindViewById<EditText>(Resource.Id.message_edit_text);

			Activity.Window.SetSoftInputMode(SoftInput.AdjustResize);
			_messageEditText.FocusChange += OnTextFocus;
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var set = this.CreateBindingSet<ConversationView, ConversationViewModel>();

			set.Bind(this)
				.For(v => v.ScrollToMessageInteraction)
				.To(vm => vm.ScrollToMessageInteraction);

			set.Bind(this)
				.For(v => v.IsBlocked)
				.To(vm => vm.IsBLocked);

			set.Bind(this)
				.For(v => v.LoadMoreCommand)
				.To(vm => vm.LoadMoreCommand);

			set.Bind(this)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);

			set.Apply();
		}

		public override void OnDestroy()
		{
			base.OnDestroy();
			_messageEditText.FocusChange -= OnTextFocus;
		}

		private void OnTextFocus(object sender, View.FocusChangeEventArgs e)
		{
			if (!e.HasFocus)
			{
				KeyboardHelper.HideKeyboard(View);
			}
		}

		private void LoadMoreEvent()
		{
			LoadMoreCommand?.Execute();
		}

		private void OnInteractionRequested(object sender, EventArgs e)
		{
			_layoutManager.SmoothScrollToPosition(_rvMessages, new RecyclerView.State(), 0);
		}

		private void OnRequestedScrollToMessage(object sender, MvvmCross.Base.MvxValueEventArgs<int> e)
		{
			ScrollToMessage(e.Value);
		}

		private void UpdateConversationUi(bool isBlockedConversation)
		{
			_messageBoxLayout.Enabled = !isBlockedConversation;
			_messageEditText.Enabled = !isBlockedConversation;
			_friendInfoLayout.SetBackgroundColor(isBlockedConversation ? ColorHelper.Gray : ColorHelper.LightBlue);
		}

		private void ScrollToMessage(int index)
		{
			KeyboardHelper.HideKeyboard(View);

			if (index >= 0 && index <= _rvMessages.ItemsSource.Count())
			{
				_layoutManager.SmoothScrollToPosition(_rvMessages, new RecyclerView.State(), index);
			}
		}
	}

	public class WrapContentLinearLayoutManager : LinearLayoutManager
	{
		public override void OnLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state)
		{
			try
			{
				base.OnLayoutChildren(recycler, state);
			}
			catch (IndexOutOfBoundsException)
			{

			}
		}

		public WrapContentLinearLayoutManager(Context context, int orientation, bool reverseLayout) : base(context, orientation, reverseLayout)
		{
		}
	}
}