﻿using Android.App;
using Android.Content.PM;
using Android.Views;
using Android.Widget;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.Droid.Helpers;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace GiveAndTake.Droid.Views
{
    [MvxActivityPresentation]
    [Activity(ScreenOrientation = ScreenOrientation.Portrait)]
    public class ResetPasswordView : BaseActivity
    {
        private bool _isPasswordShown;
        private EditText _edtPassword;

        protected override int LayoutId => Resource.Layout.ResetPasswordView;

        public bool IsPasswordShown
        {
            get => _isPasswordShown;
            set
            {
                _isPasswordShown = value;
                _edtPassword.InputType = value
                    ? Android.Text.InputTypes.TextVariationVisiblePassword | Android.Text.InputTypes.ClassText
                    : Android.Text.InputTypes.TextVariationPassword | Android.Text.InputTypes.ClassText;
            }
        }

        protected override void InitView()
        {
            _edtPassword = FindViewById<EditText>(Resource.Id.edtPassword);
            _edtPassword.FocusChange += OnEditTextFocusChange;
        }

        protected override void CreateBinding()
        {
            base.CreateBinding();

            var bindingSet = this.CreateBindingSet<ResetPasswordView, ResetPasswordViewModel>();

            bindingSet.Bind(this)
                .For(v => v.IsPasswordShown)
                .To(vm => vm.IsPasswordShown);

            bindingSet.Apply();
        }

        protected override void OnDestroy()
        {
            _edtPassword.FocusChange -= OnEditTextFocusChange;
            base.OnDestroy();
        }

        private void OnEditTextFocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (sender is EditText editText && !editText.HasFocus)
            {
                KeyboardHelper.HideKeyboard(editText);
            }
        }
    }
}
