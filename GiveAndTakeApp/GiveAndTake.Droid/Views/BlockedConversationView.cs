﻿using System;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Droid.Views.Base;
using GiveAndTake.Droid.Views.TabNavigation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace GiveAndTake.Droid.Views
{
	[MvxFragmentPresentation(typeof(MasterViewModel), Resource.Id.content_frame, true)]
	[Register(nameof(BlockedConversationView))]
	public class BlockedConversationView : BaseFragment
	{
		public IMvxCommand LoadMoreCommand { get; set; }
		protected override int LayoutId => Resource.Layout.BlockedConversationView;

		protected override void InitView(View view)
		{
			AddScrollEvent(view.FindViewById<MvxRecyclerView>(Resource.Id.rvConversation), () => LoadMoreCommand?.Execute());
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<BlockedConversationView, BlockedConversationViewModel>();

			bindingSet.Bind(this)
				.For(v => v.LoadMoreCommand)
				.To(vm => vm.LoadMoreCommand);

			bindingSet.Apply();
		}

		private void AddScrollEvent(MvxRecyclerView recyclerView, Action action)
		{
			var layoutManager = new LinearLayoutManager(Context);
			recyclerView.AddOnScrollListener(new ScrollListener(layoutManager)
			{
				LoadMoreEvent = action
			});
			recyclerView.SetLayoutManager(layoutManager);
		}
	}
}