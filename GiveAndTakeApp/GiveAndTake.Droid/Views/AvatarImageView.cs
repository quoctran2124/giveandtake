﻿using Android.App;
using Android.Graphics;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.Droid.Views.Base;
using ImageViews.Photo;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Square.Picasso;

namespace GiveAndTake.Droid.Views
{
	[MvxActivityPresentation]
	[Activity]
	public class AvatarImageView : BaseActivity
	{
		private PhotoView _photoView;
		private string _imageUrl;
		private string _base64String;

		public string ImageUrl
		{
			get => _imageUrl;
			set
			{
				_imageUrl = value;
				Picasso.With(this).Load(_imageUrl).Into(_photoView);
			}
		}

		public string Base64String
		{
			get => _base64String;
			set
			{
				_base64String = value;
				var bytes = System.Convert.FromBase64String(_base64String);
				var bitmap = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
				_photoView.SetImageBitmap(bitmap);
			}
		}

		protected override int LayoutId => Resource.Layout.AvatarImageView;

		protected override void InitView()
		{
			_photoView = FindViewById<PhotoView>(Resource.Id.avatarView);
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<AvatarImageView, AvatarImageViewModel>();

			bindingSet.Bind(this)
				.For(v => v.ImageUrl)
				.To(vm => vm.ImageUrl);

			bindingSet.Bind(this)
				.For(v => v.Base64String)
				.To(vm => vm.Base64String);

			bindingSet.Apply();
		}
	}
}