﻿using Android.App;
using Android.Content.PM;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace GiveAndTake.Droid.Views
{
    [MvxActivityPresentation]
    [Activity(ScreenOrientation = ScreenOrientation.Portrait)]
    public class RegisterSuccessView : BaseActivity
    {
        protected override int LayoutId => Resource.Layout.RegisterSuccessView;

        protected override void InitView()
        {
        }
    }
}