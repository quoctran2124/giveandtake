﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace GiveAndTake.Core.ViewModels
{
	public class BlockedConversationViewModel : BaseViewModelResult<bool>
	{
		public string BlockedConverstationTitle => Strings["BlockedConverstationTitle"];
		public IMvxCommand LoadMoreCommand => _loadMoreCommand = _loadMoreCommand ?? new MvxAsyncCommand(OnLoadMore);

		public IMvxCommand BackPressedCommand => _backPressedCommand = _backPressedCommand ?? new MvxAsyncCommand(OnBackPressed);

		public IMvxCommand RefreshCommand => _refreshCommand = _refreshCommand ?? new MvxCommand(OnRefresh);

		public bool IsRefreshing
		{
			get => _isRefresh;
			set => SetProperty(ref _isRefresh, value);
		}

		public MvxObservableCollection<ConversationItemViewModel> ConversationItemViewModels
		{
			get => _conversationItemViewModels;
			set => SetProperty(ref _conversationItemViewModels, value);
		}

		private bool _isRefresh;
		private readonly ILoadingOverlayService _overlay;
		private IMvxCommand _loadMoreCommand;
		private IMvxCommand _backPressedCommand;
		private IMvxCommand _refreshCommand;
		private MvxObservableCollection<ConversationItemViewModel> _conversationItemViewModels;
		private bool _isConversationStatusChanged;
		private int _currentPage;
		private List<Conversation> _conversations;

		public BlockedConversationViewModel(ILoadingOverlayService loadingOverlayService)
		{
			_overlay = loadingOverlayService;
			_isConversationStatusChanged = true;
		}

		public override Task Initialize() => UpdateConversationItemViewModelCollection();

		private async void OnRefresh()
		{
			IsRefreshing = true;
			await UpdateConversationItemViewModelCollection();
			IsRefreshing = false;
		}

		private async Task OnBackPressed()
		{
			await Task.Delay(AppConstants.DefaultDelayLoadingTime); //for iOS					
			await NavigationService.Close(this, _isConversationStatusChanged);
		}

		private async void OnItemClicked(Conversation conversation)
		{
			var needToReload = await NavigationService.Navigate<ConversationViewModel, Conversation, bool>(conversation);
			if (needToReload)
			{
				await UpdateConversationItemViewModelCollection();
			}
		}

		private async Task OnLoadMore()
		{
			try
			{
				_currentPage = DataModel.ApiMessagesResponse?.Pagination.Page ?? AppConstants.DefaultApiPageNumber;
				DataModel.ApiConversationResponse = await ManagementService.GetConversations($"page={ _currentPage + AppConstants.DefaultApiPageStep}", DataModel.LoginResponse.Token);

				if (DataModel.ApiConversationResponse.Conversations.Any())
				{
					var conversations = DataModel.ApiConversationResponse.Conversations;
					var conversationItemViewModels = new MvxObservableCollection<ConversationItemViewModel>();

					foreach (var conversation in conversations)
					{
						if (_conversations.Exists(c => c.Id.Equals(conversation.Id)))
						{
							continue;
						}
						_conversations.Add(conversation);
						conversationItemViewModels.Add(GenerateConversationItem(conversation));
					}
					ConversationItemViewModels.AddRange(conversationItemViewModels);
				}
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private async Task LoadConversationDataWithOverlay()
		{
			var isSucess = true;
			try
			{
				await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);

				await LoadConversationData();
			}
			catch (AppException.ApiException)
			{
				isSucess = false;
			}
			finally
			{
				await _overlay.CloseOverlay(isSucess ? 0 : AppConstants.DefaultDelayLoadingTime);

				if (!isSucess)
				{
					await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
				}
			}
		}

		private async void ReloadData()
		{
			await LoadConversationDataWithOverlay();
		}

		private ConversationItemViewModel GenerateConversationItem(Conversation conversation)
		{
			var conversationItem = new ConversationItemViewModel(conversation, ReloadData)
			{
				ClickAction = OnItemClicked,
			};
			return conversationItem;
		}

		public async Task UpdateConversationItemViewModelCollection()
		{
			try
			{
				await LoadConversationData();
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private async Task LoadConversationData()
		{
			DataModel.ApiConversationResponse = await ManagementService.GetConversations($"status={AppConstants.ConversationStatusBlocked}", DataModel.LoginResponse.Token);

			_conversations = DataModel.ApiConversationResponse.Conversations;

			ConversationItemViewModels = new MvxObservableCollection<ConversationItemViewModel>(DataModel.ApiConversationResponse.Conversations.Select(GenerateConversationItem));
			if (ConversationItemViewModels.Any())
			{
				ConversationItemViewModels.Last().IsLastConversationItemViewModel = false;
			}
		}
	}
}
