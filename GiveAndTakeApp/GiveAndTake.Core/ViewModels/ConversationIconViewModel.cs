﻿using GiveAndTake.Core.ViewModels.Base;

namespace GiveAndTake.Core.ViewModels
{
	public class ConversationIconViewModel : BaseViewModel<int>
	{
		public int ConversationBadge
		{
			get => _conversationBadge;
			set => SetProperty(ref _conversationBadge, value);
		}

		private int _conversationBadge;

		public ConversationIconViewModel()
		{
			ConversationBadge = 3;
		}

		public override void Prepare(int number)
		{
			ConversationBadge = number;
		}
	}
}