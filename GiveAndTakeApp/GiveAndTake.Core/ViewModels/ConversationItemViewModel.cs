﻿using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.UI;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels
{
	public class ConversationItemViewModel : BaseViewModel
	{
		private readonly Conversation _conversation;
		private string _userName;
		private string _avatarUrl;
		private string _createdTime;
		private string _messageContent;
		private bool _isLastConversationItemViewModel = true;
		private readonly Action _reloadMessageList;
		private IMvxCommand _showMenuPopupCommand;
		private IMvxCommand _clickCommand;
		private MvxColor _backgroundColor;
		private readonly ILoadingOverlayService _overlay = Mvx.Resolve<ILoadingOverlayService>();

		public Action<Conversation> ClickAction { get; set; }
		public IMvxCommand ClickCommand => _clickCommand ?? (_clickCommand = new MvxCommand(HandleOnClicked));
		public IMvxCommand ShowMenuPopupCommand =>
			_showMenuPopupCommand ?? (_showMenuPopupCommand = new MvxAsyncCommand(OnShowMenuPopup));

		public string UserName
		{
			get => _userName;
			set => SetProperty(ref _userName, value);
		}

		public string AvatarUrl
		{
			get => _avatarUrl;
			set => SetProperty(ref _avatarUrl, value);
		}

		public string MessageContent
		{
			get => _messageContent;
			set => SetProperty(ref _messageContent, value);
		}

		public string CreatedTime
		{
			get => _createdTime;
			set => SetProperty(ref _createdTime, value);
		}

		public bool IsLastConversationItemViewModel
		{
			get => _isLastConversationItemViewModel;
			set => SetProperty(ref _isLastConversationItemViewModel, value);
		}

		public MvxColor BackgroundColor
		{
			get => _backgroundColor;
			set => SetProperty(ref _backgroundColor, value);
		}

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };

		public ConversationItemViewModel(Conversation conversation, Action reloadMessageList = null)
		{
			_conversation = conversation;
			AvatarUrl = conversation.User.AvatarUrl;
			UserName = conversation.User.DisplayName ?? AppConstants.DefaultUserName;
			CreatedTime = TimeHelper.ToTimeAgo(conversation.LastMessage.CreatedTime);
			MessageContent = conversation.LastMessage.MessageContent;
			BackgroundColor = conversation.IsRead ? ColorHelper.White : ColorHelper.Cyan;
			_reloadMessageList = reloadMessageList;
		}

		private async Task OnShowMenuPopup()
		{
			try
			{
				var conversationItemType = StringHelper.GetEnumFromDescription(_conversation.ConversationStatus,
					ConversationItemType.NormalConversation);
				var postOptions = MenuOptionHelper.GetMenuPopupList(conversationItemType);
				var result =
					await NavigationService.Navigate<PopupExtensionOptionViewModel, List<string>, string>(postOptions);
				if (string.IsNullOrEmpty(result))
				{
					return;
				}

				if (result == Strings["BlockThisConversation"])
				{
					var confirmResult =
						await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(
							Strings["BlockConversationConfirmationMessage"]);
					if (confirmResult == RequestStatus.Submitted)
					{
						await ManagementService.ChangeConversationStatus(_conversation.Id,
							DataModel.LoginResponse.Token, AppConstants.ConversationStatusBlocked);
					}
				}
				else if (result == Strings["DeleteThisConversation"])
				{
					var confirmResult =
						await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(
							Strings["DeleteConversationConfirmationMessage"]);
					if (confirmResult == RequestStatus.Submitted)
					{
						await ManagementService.DeleteConversation(_conversation.Id, DataModel.LoginResponse.Token);
					}
				}
				else if (result == Strings["UnBlockThisConversation"])
				{
					await ManagementService.ChangeConversationStatus(_conversation.Id, DataModel.LoginResponse.Token,
						AppConstants.ConversationStatusNormal);
				}
				_reloadMessageList.Invoke();
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private void HandleOnClicked()
		{
			ClickAction?.Invoke(_conversation);
			BackgroundColor = ColorHelper.White;
		}
	}
}
