﻿using System;
using System.Collections.Generic;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System.Threading.Tasks;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using System.Linq;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.ViewModels.Popup.MyRequest;
using GiveAndTake.Core.ViewModels.Popup.OtherRequest;
using I18NPortable;
using MvvmCross;

namespace GiveAndTake.Core.ViewModels
{
	public class CommentViewModel : BaseViewModel<Post, ViewResult>
	{
		private readonly IDataModel _dataModel;
		private Post _post;
		private readonly ILoadingOverlayService _overlay;
		public string ShareTitle => Strings["ShareTitle"];

		private IMvxCommand _refreshCommand;
		public IMvxCommand RefreshCommand => _refreshCommand = _refreshCommand ?? new MvxCommand(OnRefresh);

		private IMvxCommand _loadMoreCommand;
		public IMvxCommand LoadMoreCommand => _loadMoreCommand = _loadMoreCommand ?? new MvxAsyncCommand(OnLoadMore);

		private IMvxCommand _onRequestIconClickCommand;
		public IMvxCommand OnRequestIconClickCommand =>
			_onRequestIconClickCommand ?? (_onRequestIconClickCommand = new MvxAsyncCommand(OnRequestIconClicked));

		private IMvxCommand _onAppreciationIconClickCommand;
		public IMvxCommand OnAppreciationIconClickCommand =>
			_onAppreciationIconClickCommand ?? (_onAppreciationIconClickCommand = new MvxAsyncCommand(OnAppreciationIconClicked));

		private IMvxCommand _backPressedCommand;
		public IMvxCommand BackPressedCommand =>
			_backPressedCommand = _backPressedCommand ?? new MvxCommand(OnBackPressed);

		private MvxCommand _sendCommentCommand;
		public IMvxCommand SendCommentCommand => _sendCommentCommand = _sendCommentCommand ?? new MvxCommand(OnSendCommentButtonClicked);

		private IMvxCommand _shareCommand;
		public IMvxCommand ShareCommand => _shareCommand = _shareCommand ?? new MvxCommand(OnShareFacebook);

		public MvxInteraction<int> ScrollToCommentInteraction { get; } = new MvxInteraction<int>();

		public string SendCommentHint => Strings["SendCommentHint"];
		public string ShareFacebookTitle => Strings["ShareTitle"];

		private bool _isRefresh;
		public bool IsRefreshing
		{
			get => _isRefresh;
			set => SetProperty(ref _isRefresh, value);
		}

		private int _commentCount;
		public int CommentCount
		{
			get => _commentCount;
			set => SetProperty(ref _commentCount, value);
		}

		private int _appreciationCount;
		public int AppreciationCount
		{
			get => _appreciationCount;
			set => SetProperty(ref _appreciationCount, value);
		}

		private int _requestCount;
		public int RequestCount
		{
			get => _requestCount;
			set => SetProperty(ref _requestCount, value);
		}

		private bool _isRequestIconActivated;
		public bool IsRequestIconActivated
		{
			get => _isRequestIconActivated;
			set => SetProperty(ref _isRequestIconActivated, value);
		}

		private bool _isAppreciationIconActivated;
		public bool IsAppreciationIconActivated
		{
			get => _isAppreciationIconActivated;
			set => SetProperty(ref _isAppreciationIconActivated, value);
		}

		private bool _isSendIconActivated;
		public bool IsSendIconActivated
		{
			get => _isSendIconActivated;
			set => SetProperty(ref _isSendIconActivated, value);
		}

		private string _avatarUrl;
		public string AvatarUrl
		{
			get => _avatarUrl;
			set => SetProperty(ref _avatarUrl, value);
		}

		private string _sendCommentContent;
		public string SendCommentContent
		{
			get => _sendCommentContent;
			set
			{
				SetProperty(ref _sendCommentContent, value);
				IsSendIconActivated = !string.IsNullOrEmpty(SendCommentContent);

			}
		}

		private MvxObservableCollection<CommentItemViewModel> _commentItemViewModels;
		public MvxObservableCollection<CommentItemViewModel> CommentItemViewModels
		{
			get => _commentItemViewModels;
			set => SetProperty(ref _commentItemViewModels, value);
		}

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };

		public CommentViewModel(IDataModel dataModel, ILoadingOverlayService loadingOverlayService)
		{
			_dataModel = dataModel;
			_overlay = loadingOverlayService;
		}

		public override void Prepare(Post post)
		{
			_post = post;
			_post.IsMyPost = DataModel.CurrentPost.IsMyPost = post.User.Id.Equals(_dataModel.LoginResponse.Profile.Id);
			AvatarUrl = _dataModel.LoginResponse.Profile.AvatarUrl;
		}

		public override async void ViewAppeared()
		{
			base.ViewAppeared();
			await LoadCommentDataWithOverlay();
			RequestCount = _post.RequestCount;
			IsRequestIconActivated = _post.IsMyPost ? RequestCount > 0 : _post.IsRequested;
			AppreciationCount = _post.AppreciationCount;
			IsAppreciationIconActivated = _post.IsAppreciated;
		}

		private async void OnRefresh()
		{
			IsRefreshing = true;
			await UpdateCommentItemViewModelCollection();
			IsRefreshing = false;
		}

		private async void OnBackPressed()
		{
			await NavigationService.Close(this, ViewResult.ReloadData);
		}

		private async void OnSendCommentButtonClicked()
		{
			await _overlay.ShowOverlay(Strings["ProcessingDataOverLayTitle"]);

			var isSucess = true;
			try
			{
				var comment = new Comment()
				{
					CommentMessage = SendCommentContent,
					PostId = Guid.Parse(_post.PostId),
					UserId = Guid.Parse(_dataModel.LoginResponse.Profile.Id)
				};
				await ManagementService.CreateComment(comment, DataModel.LoginResponse.Token);
				await LoadCommentData();

				SendCommentContent = "";
			}
			catch (AppException.ApiException)
			{
				isSucess = false;
			}
			finally
			{
				await _overlay.CloseOverlay(isSucess ? 0 : AppConstants.DefaultDelayLoadingTime);

				if (!isSucess)
				{
					await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
				}
			}

		}

		private async Task OnLoadMore()
		{
			try
			{
				_dataModel.ApiCommentResponse = await ManagementService.GetListOfComments(_post.PostId,
					$"limit={AppConstants.NumberOfRequestPerPage}&page={_dataModel.ApiCommentResponse.Pagination.Page + AppConstants.DefaultApiPageStep}", _dataModel.LoginResponse.Token);
				if (_dataModel.ApiCommentResponse.Comments.Any())
				{
					foreach (var comment in _dataModel.ApiCommentResponse.Comments)
					{
						var commentViewModel = GenerateCommentItem(comment);
						CommentItemViewModels.Insert(0, commentViewModel);
					}
				}
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private void OnShareFacebook()
		{
			var content = string.Format(Strings["ShareContent"],
				_post.Category.CategoryName.Translate(),
				_post.Title,
				_post.Description,
				_post.CreatedTime.ToString(AppConstants.DateStringFormat),
				_post.ProvinceCity.ProvinceCityName.Translate()
			);
			Mvx.Resolve<IFacebookHelper>().ShareFacebookContent(content, AppConstants.ShareFacebookLink + _post.PostId);
		}

		private async Task OnAppreciationIconClicked()
		{
			try
			{
				var result = await ManagementService.AppreciateAPost(_post.PostId, _dataModel.LoginResponse.Token);
				if (result.StatusCode == 200)
				{
					_post.IsAppreciated = !_post.IsAppreciated;
					_post.AppreciationCount = (_post.IsAppreciated) ? (_post.AppreciationCount + 1) : (_post.AppreciationCount - 1);

					IsAppreciationIconActivated = _post.IsAppreciated;
					AppreciationCount = _post.AppreciationCount;
				}
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private async Task OnRequestIconClicked()
		{
			if (_post.IsMyPost)
			{
				var result = await NavigationService.Navigate<RequestsViewModel, Post, ViewResult>(_post);
				if (result == ViewResult.ShowProfileView)
				{
					await Task.Delay(AppConstants.DefaultDelayLoadingTime);
					await NavigationService.Close(this, ViewResult.ShowProfileView);
				}
				else
				{
					RequestCount = _dataModel.ApiRequestsResponse.Pagination.Totals;
					IsRequestIconActivated = RequestCount > 0;
				}
			}
			else
			{
				if (IsRequestIconActivated)
				{
					await ReviewMyRequest();
				}
				else
				{
					await CreateNewRequest();
				}
			}
		}

		private async Task LoadCommentDataWithOverlay()
		{
			var isSucess = true;
			try
			{
				await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);

				await LoadCommentData();
			}
			catch (AppException.ApiException)
			{
				isSucess = false;
			}
			finally
			{
				await _overlay.CloseOverlay(isSucess ? 0 : AppConstants.DefaultDelayLoadingTime);

				if (!isSucess)
				{
					await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
				}
			}
		}

		private async Task ReviewMyRequest()
		{
			try
			{
				await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);

				var request = await ManagementService.GetRequestOfCurrentUserByPostId(_post.PostId, _dataModel.LoginResponse.Token);

				await _overlay.CloseOverlay();

				var requestStatus = request.RequestStatus;
				var popupResult = PopupRequestStatusResult.Cancelled;
				switch (requestStatus)
				{
					case RequestStatus.Pending:
						popupResult = await NavigationService.Navigate<MyRequestPendingViewModel, Request, PopupRequestStatusResult>(request);
						break;
					case RequestStatus.Received:
						if (DataModel.LoginResponse.Profile.Id == request.Post.User.Id)
						{
							popupResult = await NavigationService.Navigate<MyRequestReceivedViewModel, Request, PopupRequestStatusResult>(request);
						}
						else
						{
							popupResult = await NavigationService.Navigate<OtherRequestReceivedViewModel, Request, PopupRequestStatusResult>(request);
						}
						break;
					case RequestStatus.Approved:
						popupResult = await NavigationService.Navigate<MyRequestApprovedViewModel, Request, PopupRequestStatusResult>(request);
						break;
					case RequestStatus.Rejected:
						await CreateNewRequest();
						break;
				}

				switch (popupResult)
				{
					case PopupRequestStatusResult.Received:
						await ReceiveItem(request.Id);
						break;
					case PopupRequestStatusResult.Removed:
						await CancelOldRequest();
						break;
					case PopupRequestStatusResult.ShowPostDetail:
						await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(_post);
						break;
					case PopupRequestStatusResult.ShowMyProfile:
						await Task.Delay(AppConstants.DefaultDelayLoadingTime);
						await NavigationService.Close(this, ViewResult.ShowProfileView);
						break;
					case PopupRequestStatusResult.ShowOtherProfile:
						await NavigationService.Navigate<UserProfileViewModel, User>(request.Post.User);
						break;
					case PopupRequestStatusResult.Cancelled:
						return;
				}
			}
			catch (Exception e)
			{
				await _overlay.CloseOverlay();
				if (e.Message != AppConstants.NotFound)
				{
					await PopupHelper.ShowWarningPopup(Strings["PostIsDeleted"]);
				}
				else await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}

		private async Task CreateNewRequest()
		{
			var result = await NavigationService.Navigate<PopupCreateRequestViewModel, Post, RequestStatus>(_post);
			if (result == RequestStatus.Submitted)
			{
				await LoadCommentDataWithOverlay();
			}
		}

		private async Task ReceiveItem(string requestId)
		{
			await ManagementService.ChangeStatusOfRequest(requestId, RequestStatus.Received.ToString(), _dataModel.LoginResponse.Token);
		}

		private async Task CancelOldRequest()
		{
			var popupResult = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["CancelRequestConfirm"]);
			if (popupResult != RequestStatus.Submitted)
			{
				return;
			}

			await ManagementService.CancelUserRequest(_post.PostId, _dataModel.LoginResponse.Token);
			await LoadCommentDataWithOverlay();
		}

		private async Task LoadCommentData()
		{
			_dataModel.ApiCommentResponse = await ManagementService.GetListOfComments(_post.PostId,
				$"limit={AppConstants.NumberOfRequestPerPage}", _dataModel.LoginResponse.Token);
			_dataModel.CurrentPost =
				await ManagementService.GetPostDetail(_post.PostId, _dataModel.LoginResponse.Token);
			_dataModel.CurrentPost.IsMyPost = _post.IsMyPost;
			_post = _dataModel.CurrentPost;
			RequestCount = _post.RequestCount;
			AppreciationCount = _post.AppreciationCount;
			IsRequestIconActivated = _post.IsMyPost ? RequestCount > 0 : _post.IsRequested;
			IsAppreciationIconActivated = _post.IsAppreciated;
			CommentCount = _dataModel.ApiCommentResponse.Pagination.Totals;
			CommentItemViewModels = new MvxObservableCollection<CommentItemViewModel>(_dataModel.ApiCommentResponse.Comments.Select(GenerateCommentItem).Reverse());

			ScrollToCommentInteraction.Raise(CommentItemViewModels.Count - 1);
		}

		private CommentItemViewModel GenerateCommentItem(Comment comment)
		{
			var commentItem = new CommentItemViewModel(comment, _overlay, ReloadData)
			{
				ShowProfileTab = () => { NavigationService.Close(this, ViewResult.ShowProfileView); }
			};
			return commentItem;
		}

		private async Task UpdateCommentItemViewModelCollection()
		{
			try
			{
				await LoadCommentData();
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private async void ReloadData()
		{
			await LoadCommentDataWithOverlay();
		}
	}
}
