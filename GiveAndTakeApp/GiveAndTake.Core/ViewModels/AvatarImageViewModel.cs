﻿using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels
{
	public class AvatarImageViewModel : BaseViewModel<AvatarData>
	{
		public IMvxCommand CloseCommand =>
			_closeCommand ?? (_closeCommand = new MvxCommand(HandleOnClose));

		public string ImageUrl
		{
			get => _imageUrl;
			set => SetProperty(ref _imageUrl, value);
		}

		public string Base64String
		{
			get => _base64String;
			set => SetProperty(ref _base64String, value);
		}

		private IMvxCommand _closeCommand;
		private string _imageUrl;
		private string _base64String;

		private void HandleOnClose()
		{
			NavigationService.Close(this);
		}

		public override void Prepare(AvatarData avaData)
		{
			ImageUrl = avaData.ImageUrl;
			Base64String = avaData.Base64String;
		}
	}
}
