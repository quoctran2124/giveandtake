﻿using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross.Commands;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels
{
	public class CommentItemViewModel : BaseViewModel
	{
		#region Properties
		private bool _isOptionsMenuShown;
		private readonly Comment _comment;
		private string _userName;
		private string _avatarUrl;
		private string _createdTime;
		private string _commentMessage;
		private bool _isMyPost;
		private bool _isMyComment;
		private readonly Action _reloadCommentList;
		private IMvxCommand _showMenuPopupCommand;
		private IMvxCommand _showProfileCommand;
		private readonly ILoadingOverlayService _overlayService;
		public Action ShowProfileTab { get; set; }

		public IMvxCommand ShowUserProfileCommamd =>
			_showProfileCommand ?? (_showProfileCommand = new MvxAsyncCommand(OnShowUserProfile));
		public IMvxCommand ShowMenuPopupCommand =>
			_showMenuPopupCommand ?? (_showMenuPopupCommand = new MvxAsyncCommand(OnShowMenuPopup));

		public string UserName
		{
			get => _userName;
			set => SetProperty(ref _userName, value);
		}

		public string AvatarUrl
		{
			get => _avatarUrl;
			set => SetProperty(ref _avatarUrl, value);
		}

		public string CommentMessage
		{
			get => _commentMessage;
			set => SetProperty(ref _commentMessage, value);
		}

		public string CreatedTime
		{
			get => _createdTime;
			set => SetProperty(ref _createdTime, value);
		}

		public bool IsOptionsMenuShown
		{
			get => _isOptionsMenuShown;
			set => SetProperty(ref _isOptionsMenuShown, value);
		}

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };

		#endregion

		#region Constructor

		public CommentItemViewModel(Comment comment, ILoadingOverlayService overlayService, Action reloadCommentList = null)
		{
			_comment = comment;
			_overlayService = overlayService;
			AvatarUrl = comment.User.AvatarUrl;
			UserName = comment.User.DisplayName ?? AppConstants.DefaultUserName;
			CreatedTime = TimeHelper.ToTimeAgo(comment.CreatedTime);
			CommentMessage = comment.CommentMessage;
			_isMyPost = DataModel.CurrentPost.IsMyPost;
			_isMyComment = comment.User.Id == DataModel.LoginResponse.Profile.Id;
			_reloadCommentList = reloadCommentList;
			if (!_isMyPost && !_isMyComment)
			{
				IsOptionsMenuShown = false;
			}
			else
			{
				IsOptionsMenuShown = true;
			}
		}

		#endregion
		#region Methods
		private async Task OnShowMenuPopup()
		{
			var postOptions = MenuOptionHelper.GetActionsForComment(_isMyPost, _isMyComment);
			var result = await NavigationService.Navigate<PopupExtensionOptionViewModel, List<string>, string>(postOptions);
			try
			{
				if (string.IsNullOrEmpty(result))
				{
					return;
				}

				if (result == Strings["Modify"])
				{
					var commentResult = await NavigationService.Navigate<PopupEditCommentViewModel, Comment, RequestStatus>(_comment);
					if (commentResult == RequestStatus.Submitted)
					{
						_reloadCommentList?.Invoke();
					}
				}
				else if (result == Strings["Delete"])
				{
					var confirmResult = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["DeleteCommentConfirmation"]);
					if (confirmResult == RequestStatus.Submitted)
					{
						await ManagementService.DeleteComment(_comment.Id.ToString(), DataModel.LoginResponse.Token);
						_reloadCommentList?.Invoke();
					}
				}
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private async Task OnShowUserProfile()
		{
			if (_isMyComment)
			{
				ShowProfileTab?.Invoke();
			}
			else
			{
				await NavigationService.Navigate<UserProfileViewModel, User>(_comment.User);
			}
		}
		#endregion
	}
}
