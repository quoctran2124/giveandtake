﻿using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using System;
using System.Threading.Tasks;
using GiveAndTake.Core.Exceptions;

namespace GiveAndTake.Core.ViewModels
{
    public class ForgotPasswordViewModel : BaseViewModel
    {
        #region Fields

        private readonly ILoadingOverlayService _overlay;
        private bool _submitButtonEnabled;
        private string _phoneNumber;
        private IMvxCommand _sendOtpCommand;
        private IMvxCommand _backPressedCommand;

        #endregion

        #region Constructors

        public ForgotPasswordViewModel(ILoadingOverlayService loadingOverlayService)
        {
            _overlay = loadingOverlayService;
        }

        #endregion

        #region Properties

        public string TxtResetPassword => Strings["TxtResetPassword"];

        public string TxtInputYourPhoneNumber => Strings["TxtInputYourPhoneNumber"];

        public string EdtPhoneNumber => Strings["EdtPhoneNumber"];

        public string BtnCancelTitle => Strings["CancelTitle"];

        public string BtnSubmit => Strings["SubmitTitle"];

        public string PhoneNumber
        {
            get => _phoneNumber;
            set
            {
                SetProperty(ref _phoneNumber, value);
                SubmitButtonEnabled = !string.IsNullOrEmpty(value);
            }
        }

        public bool SubmitButtonEnabled
        {
            get => _submitButtonEnabled;
            set => SetProperty(ref _submitButtonEnabled, value);
        }

        public IMvxCommand SendOtpCommand =>
            _sendOtpCommand ?? (_sendOtpCommand = new MvxAsyncCommand(SendOtpAsync));

        public IMvxCommand BackPressedCommand =>
            _backPressedCommand ?? (_backPressedCommand = new MvxCommand(NavigateToLogin));

        #endregion

        #region Methods

        private async Task SendOtpAsync()
        {
            try
            {
                await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
                var isPhoneExisted = await ManagementService.CheckPhoneNumberExisted(PhoneNumber);
                if (!isPhoneExisted)
                {
                    await _overlay.CloseOverlay();
                    await PopupHelper.ShowWarningPopup(Strings["ErrorAccountNotExisted"]);
                    return;
                }

                var user = new UserRegisterRequest
                {
                    PhoneNumber = PhoneNumber,
                    OtpCode = new Random().Next(100000, 999999).ToString(),
                    OtpVerifyType = OtpVerifyType.ForgotPassword
                };
                await ManagementService.SendOtpCode(user.PhoneNumber, user.OtpCode);
                await _overlay.CloseOverlay();
                await NavigationService.Navigate<OtpVerificationViewModel, UserRegisterRequest>(user);
            }
            catch (AppException.ApiSendOtpErrorException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["OtpServiceError"]);
            }
            catch (AppException.ApiException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
            }
        }

        private void NavigateToLogin()
        {
            NavigationService.Navigate<LoginViewModel>();
        }

        #endregion
    }
}