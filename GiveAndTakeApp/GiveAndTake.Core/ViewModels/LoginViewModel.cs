using System;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross;
using MvvmCross.Commands;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels
{
	public class LoginViewModel : BaseViewModel
	{
        #region Fields

        private readonly IDataModel _dataModel;
        private readonly IStorageHelper _storageHelper;
        private readonly IFacebookHelper _facebookHelper;
        private readonly ILoadingOverlayService _overlay;
        private bool _isPasswordShown;
        private bool _isLoginButtonEnabled;
        private string _password;
        private string _phoneNumber;
        private string _btnTogglePassword;
        private IMvxCommand _loginCommand;
        private IMvxCommand _showConditionCommand;
        private IMvxCommand _togglePasswordCommand;
        private IMvxCommand _loginWithPhoneNumberCommand;
        private IMvxCommand _navigateToRegisterCommand;
        private IMvxCommand _handleForgotPasswordCommand;

        #endregion

        #region Constructors

        public LoginViewModel(IDataModel dataModel, ILoadingOverlayService loadingOverlayService, IFacebookHelper facebookHelper, IStorageHelper storageHelper)
        {
            _dataModel = dataModel;
            _overlay = loadingOverlayService;
            _facebookHelper = facebookHelper;
            _storageHelper = storageHelper;
            _isPasswordShown = false;
            _btnTogglePassword = Strings["BtnShowPassword"];
        }

        #endregion

        #region Properties

        public string LoginTitle => Strings["BtnLogin"];

        public string AcceptTermAndConditionInHtml => Strings["AcceptTermAndConditionInHtml"];

        public string BtnLoginWithFacebook => Strings["LoginScreenBtnLoginWithFacebook"];

        public string TxtLoginWithPhoneNumber => Strings["LoginScreenTxtLoginWithPhoneNumber"];

        public string EdtPhoneNumber => Strings["EdtPhoneNumber"];

        public string EdtPassword => Strings["EdtPassword"];

        public string BtnForgotPassword => Strings["LoginScreenBtnForgotPassword"];

        public string BtnLogin => Strings["BtnLogin"];

        public string TxtNotHavingAnAccount => Strings["LoginScreenTxtNotHavingAnAccount"];

        public string BtnRegister => Strings["BtnRegister"];


        public string BtnTogglePassword
        {
            get => _btnTogglePassword;
            set => SetProperty(ref _btnTogglePassword, value);
        }

        public bool IsPasswordShown
        {
            get => _isPasswordShown;
            set
            {
                SetProperty(ref _isPasswordShown, value);
                BtnTogglePassword = value ? Strings["BtnHidePassword"] : Strings["BtnShowPassword"];
            }
        }

        public bool IsLoginButtonEnabled
        {
            get => _isLoginButtonEnabled;
            set => SetProperty(ref _isLoginButtonEnabled, value);
        }

        public string PhoneNumber
        {
            get => _phoneNumber;
            set
            {
                SetProperty(ref _phoneNumber, value); 
                ValidateLoginForm();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                SetProperty(ref _password, value);
                ValidateLoginForm();
            }
        }

        public IMvxCommand TogglePasswordCommand =>
            _togglePasswordCommand ?? (_togglePasswordCommand = new MvxCommand(TogglePassword));

        public IMvxCommand LoginCommand =>
            _loginCommand ?? (_loginCommand = new MvxAsyncCommand(LoginWithFacebookAsync));

        public IMvxCommand ShowConditionCommand =>
            _showConditionCommand ?? (_showConditionCommand = new MvxCommand(NavigateToConditionView));

        public IMvxCommand LoginWithPhoneNumberCommand => 
            _loginWithPhoneNumberCommand ??(_loginWithPhoneNumberCommand = new MvxAsyncCommand(LoginWithPhoneNumberAsync));

        public IMvxCommand NavigateToRegisterCommand =>
            _navigateToRegisterCommand ?? (_navigateToRegisterCommand = new MvxCommand(NavigateToRegister));

        public IMvxCommand HandleForgotPasswordCommand => 
            _handleForgotPasswordCommand ?? (_handleForgotPasswordCommand = new MvxCommand(HandleForgotPassword));

        #endregion

        #region Methods

        public override async void ViewCreated()
        {
            base.ViewCreated();
            try
            {
                await _overlay.ShowOverlay(Strings["LoginProcessOverLayTitle"]);
                await ManagementService.CheckAppVersion();
                await NavigateToHomeIfAuthenticated();
            }
            catch (AppException.ApiServiceUnavailableException)
            {
                await _overlay.CloseOverlay();
                await NavigationService.Navigate<PopupAppUpdateViewModel>();
            }
            catch (AppException.ApiException e)
            {
                Console.WriteLine(e);
                await _overlay.CloseOverlay();
            }
        }

        private async Task NavigateToHomeIfAuthenticated()
        {
            var credentials = _storageHelper.GetCredentials();
            if (credentials?.Token != null)
            {
                _dataModel.LoginResponse = credentials;
                await SendDeviceInfoAndNavigateToHome();
            }
            else
            {
                await _overlay.CloseOverlay();
            }
        }

        private async Task LoginWithFacebookAsync()
        {
            await LoginAsync(LoginType.Facebook);
        }

        private async Task LoginWithPhoneNumberAsync()
        {
            await LoginAsync(LoginType.PhoneNumber);
        }

        private async Task LoginAsync(LoginType loginType)
        {
            try
            {
                await _overlay.ShowOverlay(Strings["LoginProcessOverLayTitle"]);
                _dataModel.LoginResponse = await CallLoginApiAsync(loginType);
                _storageHelper.SaveCredentials(_dataModel.LoginResponse);
                if (_dataModel.LoginResponse.Profile.Status == AppConstants.UserStatusBlocked)
                {
                    await _overlay.CloseOverlay();
                    await NavigationService.Navigate<PopupWarningBlockedUserViewModel>();
                }
                else
                {
                    await SendDeviceInfoAndNavigateToHome();
                }
            }
            catch (AppException.ApiLoginBadRequestException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["ErrorLoginBadRequestMessage"]);
            }
            catch (AppException.ApiException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
            }
        }

        private async Task SendDeviceInfoAndNavigateToHome()
        {
            await ManagementService.SendPushNotificationUserInformation(new PushNotificationUserInformation
            {
                DeviceToken = Mvx.Resolve<IDeviceInfo>().DeviceToken,
                MobilePlatform = Mvx.Resolve<IDeviceInfo>().MobilePlatform,
                Language = DataModel.Language.ToString()
            }, _dataModel.LoginResponse.Token);
            await _overlay.CloseOverlay();
            await NavigationService.Navigate<MasterViewModel>();
        }

        private Task<LoginResponse> CallLoginApiAsync(LoginType loginType)
        {
            if (loginType == LoginType.PhoneNumber)
            {
                var user = new LoginWithPhoneNumberDto {PhoneNumber = PhoneNumber, Password = Password};
                return ManagementService.LoginWithPhoneNumber(user);
            }
            else
            {
                var user = _facebookHelper.GetCurrentUser();
                return ManagementService.LoginFacebook(user);
            }
        }

        private void NavigateToConditionView()
        {
            NavigationService.Navigate<TermAndConditionViewModel>();
        }

        private void TogglePassword()
        {
            IsPasswordShown = !IsPasswordShown;
        }

        private void ValidateLoginForm()
        {
            IsLoginButtonEnabled = !string.IsNullOrEmpty(PhoneNumber) && !string.IsNullOrEmpty(Password);
        }

        private void NavigateToRegister()
        {
            NavigationService.Navigate<RegisterViewModel>();
        }

        private void HandleForgotPassword()
        {
            NavigationService.Navigate<ForgotPasswordViewModel>();
        }

        #endregion
    }
}