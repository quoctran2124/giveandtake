﻿using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GiveAndTake.Core.ViewModels.Popup
{
	public class PopupEditCommentViewModel : BaseViewModel<Comment, RequestStatus>
	{
		#region Properties
		private readonly IDataModel _dataModel;
		private string _commentMessage;
		private bool _isSubmitBtnEnabled;
		private readonly ILoadingOverlayService _overlay;
		private Comment _comment;
		private IMvxCommand _closeCommand;
		private ICommand _submitCommand;
		public IMvxCommand CloseCommand =>
			_closeCommand ?? (_closeCommand = new MvxAsyncCommand(() => NavigationService.Close(this, RequestStatus.Cancelled)));
		public ICommand SubmitCommand => _submitCommand ?? (_submitCommand = new MvxAsyncCommand(InitCreateNewRequest));


		public string PopupTitle { get; set; } = Strings["EditCommentTitle"];
		public string BtnSubmitTitle { get; set; } = Strings["SubmitTitle"];
		public string BtnCancelTitle { get; set; } = Strings["CancelTitle"];


		public string CommentMessage
		{
			get => _commentMessage;
			set
			{
				SetProperty(ref _commentMessage, value);
				UpdateSubmitBtn();
			}
		}

		public bool IsSubmitBtnEnabled
		{
			get => _isSubmitBtnEnabled;
			set => SetProperty(ref _isSubmitBtnEnabled, value);
		}

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };

		#endregion

		#region Constructor

		public PopupEditCommentViewModel(IDataModel dataModel, ILoadingOverlayService loadingOverlayService)
		{
			_dataModel = dataModel;
			_overlay = loadingOverlayService;
		}

		public override void Prepare(Comment comment)
		{
			_comment = comment;
			CommentMessage = comment.CommentMessage;
		}

		public async Task InitCreateNewRequest()
		{
			var isSuccess = true;
			await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
			try
			{
				_comment.CommentMessage = CommentMessage;
				await ManagementService.EditComment(_comment, _dataModel.LoginResponse.Token);
			}
			catch (AppException.ApiException)
			{
				isSuccess = false;
			}
			finally
			{
				await _overlay.CloseOverlay(isSuccess ? AppConstants.DefaultDelayLoadingTime : 0);
				if (isSuccess)
				{
					await NavigationService.Close(this, RequestStatus.Submitted);
				}
				else
				{
					await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
				}
			}
		}

		public void UpdateSubmitBtn() => IsSubmitBtnEnabled = !string.IsNullOrEmpty(_commentMessage);

		#endregion
	}
}
