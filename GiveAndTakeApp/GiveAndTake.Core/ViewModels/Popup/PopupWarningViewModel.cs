﻿using System.Threading.Tasks;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup
{
	public class PopupWarningViewModel : BaseViewModel<string>
	{
		public string CloseButtonTitle => Strings["SubmitTitle"];

		public IMvxAsyncCommand CloseCommand =>
			_closeCommand ?? (_closeCommand = new MvxAsyncCommand(ClosePopup));
	
		public string Message
		{
			get => _message;
			set => SetProperty(ref _message, value);
		}

		private string _message;
		private IMvxAsyncCommand _closeCommand;

		public PopupWarningViewModel()
		{
			PopupHelper.PopupWarningViewModel = this;
		}

		public override void Prepare(string message)
		{
			_message = message;
		}

		public override void ViewDestroy(bool viewFinishing = true)
		{
			PopupHelper.PopupWarningViewModel = null;
			base.ViewDestroy(viewFinishing);
		}

		private async Task ClosePopup()
		{
			PopupHelper.PopupWarningViewModel = null;
			await NavigationService.Close(this);
		}
	}
}