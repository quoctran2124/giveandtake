﻿using System.Threading.Tasks;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup
{
	public class PopupAlertViewModel : BaseViewModel<PopupMessageParam, RequestStatus>
	{
		public IMvxAsyncCommand SubmitCommand =>
			_submitCommand ?? (_submitCommand = new MvxAsyncCommand(OnSubmit));
		public IMvxAsyncCommand CancelCommand =>
			_cancelCommand ?? (_cancelCommand = new MvxAsyncCommand(OnCancel));

		public string SubmitButtonTitle
		{
			get => _submitButtonTitle;
			set => SetProperty(ref _submitButtonTitle, value);
		}

		public string CancelButtonTitle
		{
			get => _cancelButtonTitle;
			set => SetProperty(ref _cancelButtonTitle, value);
		}

		public string Message
		{
			get => _message;
			set => SetProperty(ref _message, value);
		}

		public string Title
		{
			get => _title;
			set => SetProperty(ref _title, value);
		}


		public bool HasMessage => !string.IsNullOrEmpty(Message);

		public bool HasCancelButton => !string.IsNullOrEmpty(CancelButtonTitle);

		private string _title;
		private string _message;
		private string _submitButtonTitle;
		private string _cancelButtonTitle;
		private IMvxAsyncCommand _cancelCommand;
		private IMvxAsyncCommand _submitCommand;

		public override void Prepare(PopupMessageParam parameter)
		{
			_title = parameter.Title;
			_message = parameter.Message;
			_cancelButtonTitle = parameter.CancelTitle;
			_submitButtonTitle = parameter.ConfirmTitle ?? Strings["SubmitTitle"];
		}

		public Task OnSubmit() => NavigationService.Close(this, RequestStatus.Submitted);

		public Task OnCancel() => NavigationService.Close(this, RequestStatus.Cancelled);
	}
}