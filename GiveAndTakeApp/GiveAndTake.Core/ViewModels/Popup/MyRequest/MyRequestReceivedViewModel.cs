﻿using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup.MyRequest
{
	public class MyRequestReceivedViewModel : BaseRequestApprovedViewModel
	{
		//TODO: Refactor code to change the mechanism to determine where to navigate to User's profile or My profile (checking the id vs logged id in dataModel)
		private IMvxCommand _showTakerProfileCommand;
		private IMvxCommand _showGiverProfileCommand;
		public IMvxCommand ShowTakerProfileCommand => _showTakerProfileCommand ?? (_showTakerProfileCommand = new MvxCommand(HandleOnShowTakerProfile));
		public IMvxCommand ShowGiverProfileCommand => _showGiverProfileCommand ?? (_showGiverProfileCommand = new MvxCommand(HandleOnShowGiverProfile));

		private void HandleOnShowTakerProfile() => NavigationService.Close(this, PopupRequestStatusResult.ShowOtherProfile);
		private void HandleOnShowGiverProfile() => NavigationService.Close(this, PopupRequestStatusResult.ShowMyProfile);
	}
}
