﻿using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup
{
	public class PopupAppUpdateViewModel : BaseViewModel
	{
		public IMvxCommand UpdateCommand => _submitCommand ?? (_submitCommand = new MvxCommand(OnUpdate));

		public string Title => Strings["AppUpdateTitle"];

		public string Message => Strings["AppUpdateMessage"];

		public string UpdateButtonTitle => Strings["AppUpdateUpdateButtonText"];

		private IMvxCommand _submitCommand;
		private readonly IUrlHelper _urlHelper;

		public PopupAppUpdateViewModel(IUrlHelper urlHelper)
		{
			_urlHelper = urlHelper;
		}

		private void OnUpdate()
		{
			_urlHelper.OpenStorePage();
		}
	}
}