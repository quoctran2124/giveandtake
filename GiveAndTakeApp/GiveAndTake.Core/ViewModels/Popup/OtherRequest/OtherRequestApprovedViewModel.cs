﻿using System.Threading.Tasks;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup.OtherRequest
{
	public class OtherRequestApprovedViewModel : BaseRequestApprovedViewModel
	{
		private IMvxCommand _showTakerProfileCommand;
		private IMvxCommand _showGiverProfileCommand;
		public IMvxCommand ShowTakerProfileCommand => _showTakerProfileCommand ?? (_showTakerProfileCommand = new MvxCommand(HandleOnShowTakerProfile));
		public IMvxCommand ShowGiverProfileCommand => _showGiverProfileCommand ?? (_showGiverProfileCommand = new MvxCommand(HandleOnShowGiverProfile));

		private void HandleOnShowTakerProfile() => NavigationService.Close(this, PopupRequestStatusResult.ShowOtherProfile);
		private void HandleOnShowGiverProfile() => NavigationService.Close(this, PopupRequestStatusResult.ShowMyProfile);
	}
}
