﻿using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup.OtherRequest
{
	public class OtherRequestReceivedViewModel : BaseRequestApprovedViewModel
	{
		private IMvxCommand _showTakerProfileCommand;
		private IMvxCommand _showGiverProfileCommand;
		public IMvxCommand ShowTakerProfileCommand => _showTakerProfileCommand ?? (_showTakerProfileCommand = new MvxCommand(HandleOnShowTakerProfile));
		public IMvxCommand ShowGiverProfileCommand => _showGiverProfileCommand ?? (_showGiverProfileCommand = new MvxCommand(HandleOnShowGiverProfile));

		private void HandleOnShowTakerProfile() => NavigationService.Close(this, PopupRequestStatusResult.ShowMyProfile);
		private void HandleOnShowGiverProfile() => NavigationService.Close(this, PopupRequestStatusResult.ShowOtherProfile);
	}
}
