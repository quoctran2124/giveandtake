﻿using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using System;
using System.Threading.Tasks;
using GiveAndTake.Core.Exceptions;

namespace GiveAndTake.Core.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        #region Fields

        private readonly ILoadingOverlayService _overlay;
        private bool _isPasswordShown;
        private bool _registerButtonEnabled;
        private bool _termAgreement;
        private bool _isValidationTextVisible;
        private string _password;
        private string _phoneNumber;
        private string _displayName;
        private string _validationText;
        private string _btnTogglePassword;
        private IMvxCommand _showConditionCommand;
        private IMvxCommand _togglePasswordCommand;
        private IMvxCommand _registerCommand;
        private IMvxCommand _navigateToLoginCommand;
        private IMvxCommand _toggleCheckboxCommand;

        #endregion

        #region Constructors

        public RegisterViewModel(ILoadingOverlayService loadingOverlayService)
        {
            _overlay = loadingOverlayService;
            _isPasswordShown = false;
            _termAgreement = false;
            _btnTogglePassword = Strings["BtnShowPassword"];
            _isValidationTextVisible = false;
            _validationText = null;
        }

        #endregion

        #region Properties

        public string TxtRegister => Strings["RegisterScreenTxtRegister"];

        public string TxtRegisterWithPhoneNumber => Strings["RegisterScreenTxtRegisterWithPhoneNumber"];

        public string EdtPhoneNumber => Strings["EdtPhoneNumber"];

        public string EdtDisplayName => Strings["EdtDisplayName"];

        public string EdtPassword => Strings["EdtPassword"];

        public string AcceptTermAndConditionInHtml => Strings["RegisterScreenAcceptTermAndConditionInHtml"];

        public string BtnRegister => Strings["BtnRegister"];

        public string TxtAlreadyHadAnAccount => Strings["RegisterScreenTxtAlreadyHadAnAccount"];

        public string BtnLogin => Strings["BtnLogin"];

        public string PhoneNumber
        {
            get => _phoneNumber;
            set
            {
                SetProperty(ref _phoneNumber, value);
                ValidateLoginForm();
            }
        }

        public string DisplayName
        {
            get => _displayName;
            set
            {
                SetProperty(ref _displayName, value);
                ValidateLoginForm();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                SetProperty(ref _password, value);
                ValidateLoginForm();
                ValidationText = string.IsNullOrEmpty(value) || value.Length >= AppConstants.PasswordMinimumLength ? string.Empty : Strings["PasswordInvalid"];
                IsValidationTextVisible = !string.IsNullOrEmpty(ValidationText);
            }
        }

        public string BtnTogglePassword
        {
            get => _btnTogglePassword;
            set => SetProperty(ref _btnTogglePassword, value);
        }

        public bool IsPasswordShown
        {
            get => _isPasswordShown;
            set
            {
                SetProperty(ref _isPasswordShown, value);
                BtnTogglePassword = value ? Strings["BtnHidePassword"] : Strings["BtnShowPassword"];
            }
        }

        public bool IsValidationTextVisible
        {
            get => _isValidationTextVisible;
            set => SetProperty(ref _isValidationTextVisible, value);
        }

        public string ValidationText
        {
            get => _validationText;
            set => SetProperty(ref _validationText, value);
        }

        public bool RegisterButtonEnabled
        {
            get => _registerButtonEnabled;
            set => SetProperty(ref _registerButtonEnabled, value);
        }
    
        public bool TermAgreement
        {
            get => _termAgreement;
            set
            {
                SetProperty(ref _termAgreement, value);
                ValidateLoginForm();
            }
        }

        public IMvxCommand TogglePasswordCommand =>
            _togglePasswordCommand ?? (_togglePasswordCommand = new MvxCommand(TogglePassword));

        public IMvxCommand ShowConditionCommand =>
            _showConditionCommand ?? (_showConditionCommand = new MvxCommand(NavigateToConditionView));

        public IMvxCommand RegisterCommand =>
            _registerCommand ?? (_registerCommand = new MvxAsyncCommand(RegisterAsync));

        public IMvxCommand NavigateToLoginCommand =>
            _navigateToLoginCommand ?? (_navigateToLoginCommand = new MvxCommand(NavigateToLogin));

        public IMvxCommand ToggleCheckboxCommand =>
            _toggleCheckboxCommand ?? (_toggleCheckboxCommand = new MvxCommand(ToggleCheckbox));

        #endregion

        #region Methods

        private async Task RegisterAsync()
        {
            try
            {
                await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
                var isPhoneExisted = await ManagementService.CheckPhoneNumberExisted(PhoneNumber);
                if (isPhoneExisted)
                {
                    await _overlay.CloseOverlay();
                    await PopupHelper.ShowWarningPopup(Strings["ErrorRegisterBadRequestMessage"]);
                    return;
                }

                var user = new UserRegisterRequest
                {
                    PhoneNumber = PhoneNumber,
                    DisplayName = DisplayName,
                    Password = Password,
                    OtpCode = new Random().Next(100000, 999999).ToString()
                };
                await ManagementService.SendOtpCode(user.PhoneNumber, user.OtpCode);
                await _overlay.CloseOverlay();
                await NavigationService.Navigate<OtpVerificationViewModel, UserRegisterRequest>(user);
            }
            catch (AppException.ApiSendOtpErrorException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["OtpServiceError"]);
            }
            catch (AppException.ApiException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
            }
        }

        private void NavigateToConditionView()
        {
            NavigationService.Navigate<TermAndConditionViewModel>();
        }

        private void TogglePassword()
        {
            IsPasswordShown = !IsPasswordShown;
        }

        private void ValidateLoginForm()
        {
            RegisterButtonEnabled = !string.IsNullOrEmpty(PhoneNumber) && !string.IsNullOrEmpty(Password) && Password.Length >= AppConstants.PasswordMinimumLength && TermAgreement;
        }

        private void NavigateToLogin()
        {
            NavigationService.Navigate<LoginViewModel>();
        }

        private void ToggleCheckbox()
        {
            TermAgreement = !TermAgreement;
        }

        #endregion
    }
}