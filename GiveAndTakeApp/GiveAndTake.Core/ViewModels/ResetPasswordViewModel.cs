﻿using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross;
using MvvmCross.Commands;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels
{
    public class ResetPasswordViewModel : BaseViewModel<UserRegisterRequest>
    {
        #region Fields

        private readonly ILoadingOverlayService _overlay;
        private readonly IStorageHelper _storageHelper; 
        private bool _isPasswordShown;
        private bool _submitButtonEnabled;
        private bool _isValidationTextVisible;
        private string _password;
        private string _validationText;
        private string _btnTogglePassword;
        private IMvxCommand _submitCommand;
        private IMvxCommand _backPressedCommand;
        private IMvxCommand _togglePasswordCommand; 
        private UserRegisterRequest _userRegisterRequest;

        #endregion

        #region Constructors & Initialization

        public ResetPasswordViewModel(ILoadingOverlayService loadingOverlayService, IStorageHelper storageHelper)
        {
            _overlay = loadingOverlayService;
            _storageHelper = storageHelper;
        }


        public override void Prepare(UserRegisterRequest user)
        {
            _userRegisterRequest = user;
        }

        #endregion

        #region Properties

        public string TxtResetPassword => Strings["TxtResetPassword"];

        public string TxtInputYourPassword => Strings["TxtInputYourPassword"];

        public string EdtPassword => Strings["EdtPassword"];

        public string BtnCancelTitle => Strings["CancelTitle"];

        public string BtnSubmit => Strings["SubmitTitle"];

        public string Password
        {
            get => _password;
            set
            {
                SetProperty(ref _password, value);
                SubmitButtonEnabled = !string.IsNullOrEmpty(value);
                ValidationText = string.IsNullOrEmpty(value) || value.Length >= AppConstants.PasswordMinimumLength ? string.Empty : Strings["PasswordInvalid"];
                IsValidationTextVisible = !string.IsNullOrEmpty(ValidationText);
            }
        }

        public bool IsValidationTextVisible
        {
            get => _isValidationTextVisible;
            set => SetProperty(ref _isValidationTextVisible, value);
        }

        public string ValidationText
        {
            get => _validationText;
            set => SetProperty(ref _validationText, value);
        }

        public bool SubmitButtonEnabled
        {
            get => _submitButtonEnabled;
            set => SetProperty(ref _submitButtonEnabled, value);
        }

        public string BtnTogglePassword
        {
            get => _btnTogglePassword;
            set => SetProperty(ref _btnTogglePassword, value);
        }

        public bool IsPasswordShown
        {
            get => _isPasswordShown;
            set
            {
                SetProperty(ref _isPasswordShown, value);
                BtnTogglePassword = value ? Strings["BtnHidePassword"] : Strings["BtnShowPassword"];
            }
        }

        public IMvxCommand SubmitCommand =>
            _submitCommand ?? (_submitCommand = new MvxAsyncCommand(UpdatePasswordAsync));

        public IMvxCommand BackPressedCommand =>
            _backPressedCommand ?? (_backPressedCommand = new MvxCommand(NavigateToLogin));

        public IMvxCommand TogglePasswordCommand =>
            _togglePasswordCommand ?? (_togglePasswordCommand = new MvxCommand(TogglePassword));

        #endregion

        #region Methods

        private async Task UpdatePasswordAsync()
        {
            try
            {
                await _overlay.ShowOverlay(Strings["RegisterScreenProcessOverLayTitle"]);
                var user = new LoginWithPhoneNumberDto {PhoneNumber = _userRegisterRequest.PhoneNumber, Password = Password};
                DataModel.LoginResponse = await ManagementService.ResetPassword(user);
                _storageHelper.SaveCredentials(DataModel.LoginResponse);
                await SendDeviceInfoAndNavigateToHome();
            }
            catch (AppException.ApiException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
            }
        }

        private async Task SendDeviceInfoAndNavigateToHome()
        {
            await ManagementService.SendPushNotificationUserInformation(new PushNotificationUserInformation
            {
                DeviceToken = Mvx.Resolve<IDeviceInfo>().DeviceToken,
                MobilePlatform = Mvx.Resolve<IDeviceInfo>().MobilePlatform,
                Language = DataModel.Language.ToString()
            }, DataModel.LoginResponse.Token);
            await _overlay.CloseOverlay();
            await NavigationService.Navigate<MasterViewModel>();
        }

        private void NavigateToLogin()
        {
            NavigationService.Navigate<LoginViewModel>();
        }

        private void TogglePassword()
        {
            IsPasswordShown = !IsPasswordShown;
        }

        #endregion
    }
}