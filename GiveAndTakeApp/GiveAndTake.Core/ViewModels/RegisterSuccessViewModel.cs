﻿using GiveAndTake.Core.ViewModels.Base;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels
{
    public class RegisterSuccessViewModel : BaseViewModel
    {
        public string TxtRegisterSuccess => Strings["TxtRegisterSuccess"];

        public string TxtRegisterSuccessDetail => Strings["TxtRegisterSuccessDetail"];

        public override async void ViewCreated()
        {
            base.ViewCreated();
            await Task.Delay(1000);
            await NavigationService.Navigate<MasterViewModel>();
        }
    }
}
