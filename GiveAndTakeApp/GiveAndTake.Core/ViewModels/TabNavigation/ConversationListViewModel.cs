﻿using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels.TabNavigation
{
	public class ConversationListViewModel : BaseViewModel
	{
		public string SearchResultTitle => Strings["SearchResultNullTitle"];
		public IMvxCommand SearchCommand => _searchCommand ?? (_searchCommand = new MvxAsyncCommand(OnSearching));
		public IMvxCommand LoadMoreCommand => _loadMoreCommand = _loadMoreCommand ?? new MvxAsyncCommand(OnLoadMore);
		public IMvxCommand RefreshCommand => _refreshCommand = _refreshCommand ?? new MvxCommand(OnRefresh);
		public IMvxCommand CloseSearchBarCommand => _searchCommand ?? (_searchCommand = new MvxAsyncCommand(LoadConversationListData));
		public IMvxCommand BackPressedCommand => _backPressedCommand ?? (_backPressedCommand = new MvxAsyncCommand(OnBackPressedCommand));
		public IMvxCommand ShowBlockedConversationCommand => _showBlockedConversationCommand ?? (_showBlockedConversationCommand = new MvxAsyncCommand(OnShowBlockedConversationCommand));

		public string CurrentQueryString
		{
			get => _currentQueryString;
			set => SetProperty(ref _currentQueryString, value);
		}

		public bool IsSearched
		{
			get => _isSearched;
			set => SetProperty(ref _isSearched, value);
		}

		public bool IsRefreshing
		{
			get => _isRefresh;
			set => SetProperty(ref _isRefresh, value);
		}

		public bool IsClearButtonShown
		{
			get => _isClearButtonShown;
			set
			{
				_isClearButtonShown = value;
				RaisePropertyChanged();
			}
		}

		public bool IsSearchResultNull
		{
			get => _isSearchResultNull;
			set => SetProperty(ref _isSearchResultNull, value);
		}

		public MvxObservableCollection<ConversationItemViewModel> ConversationItemViewModels
		{
			get => _conversationItemViewModels;
			set => SetProperty(ref _conversationItemViewModels, value);
		}

		public string SearchPlaceHolder { get; set; } = Strings["SearchMessagePlaceHolder"];

		private bool _isClearButtonShown;
		private bool _isRefresh;
		private bool _isSearched;
		private bool _isSearchResultNull;
		private int _currentPage;
		private readonly ILoadingOverlayService _overlay;
		private List<Conversation> _conversations;
		private IMvxCommand _searchCommand;
		private IMvxCommand _loadMoreCommand;
		private IMvxCommand _refreshCommand;
		private IMvxCommand _backPressedCommand;
		private IMvxCommand _showBlockedConversationCommand;
		private MvxObservableCollection<ConversationItemViewModel> _conversationItemViewModels;

		private string _currentQueryString;

		public ConversationListViewModel(ILoadingOverlayService loadingOverlayService)
		{
			_overlay = loadingOverlayService;
			DataModel.ConversationBadgeUpdated += OnConversationBadgeUpdated;

		}
		
		public override async void ViewAppeared()
		{
			base.ViewAppeared();
			await UpdateConversationItemViewModelCollection();
		}

		public override void ViewDestroy(bool viewFinishing = true)
		{
			base.ViewDestroy(viewFinishing);

			DataModel.ConversationBadgeUpdated -= OnConversationBadgeUpdated;
		}

		private async void OnConversationBadgeUpdated(object sender, int e)
		{
			await UpdateConversationItemViewModelCollection();
		}

		private async void OnRefresh()
		{
			IsRefreshing = true;
			await UpdateConversationItemViewModelCollection();
			IsRefreshing = false;
		}

		public async Task UpdateConversationItemViewModelCollection()
		{
			try
			{
				DataModel.ApiConversationResponse =
					await ManagementService.GetConversations($"keyword={CurrentQueryString}", DataModel.LoginResponse.Token);

				_conversations = DataModel.ApiConversationResponse.Conversations;

				ConversationItemViewModels = new MvxObservableCollection<ConversationItemViewModel>(DataModel.ApiConversationResponse.Conversations.Select(GenerateConversationItem));
				if (ConversationItemViewModels.Any())
				{
					ConversationItemViewModels.Last().IsLastConversationItemViewModel = false;
				}
				IsSearchResultNull = ConversationItemViewModels.Any();
			}
			catch (AppException.ApiException)
			{
				await _overlay.CloseOverlay(AppConstants.DefaultDelayLoadingTime);
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private ConversationItemViewModel GenerateConversationItem(Conversation conversation)
		{
			var conversationItem = new ConversationItemViewModel(conversation, ReloadData)
			{
				ClickAction = OnItemClicked,
			};
			return conversationItem;
		}

		private async void OnItemClicked(Conversation conversation)
		{
			DataModel.ConversationBadgeUpdated -= OnConversationBadgeUpdated;
			var needToReload = await NavigationService.Navigate<ConversationViewModel, Conversation, bool>(conversation);
			DataModel.ConversationBadgeUpdated += OnConversationBadgeUpdated;
			if (needToReload)
			{
				await UpdateConversationItemViewModelCollection();
			}
		}

		private async void ReloadData()
		{
			await LoadConversationListData();
		}

		private async Task LoadConversationListData()
		{
			var isSucess = true;
			try
			{
				await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
				DataModel.ApiConversationResponse =
					await ManagementService.GetConversations($"keyword={CurrentQueryString}", DataModel.LoginResponse.Token);

				_conversations = DataModel.ApiConversationResponse.Conversations;

				ConversationItemViewModels = new MvxObservableCollection<ConversationItemViewModel>(DataModel.ApiConversationResponse.Conversations.Select(GenerateConversationItem));
				if (ConversationItemViewModels.Any())
				{
					ConversationItemViewModels.Last().IsLastConversationItemViewModel = false;
				}
				IsSearchResultNull = ConversationItemViewModels.Any();
			}
			catch (AppException.ApiException)
			{
				isSucess = false;
			}
			finally
			{
				await _overlay.CloseOverlay(isSucess ? 0 : AppConstants.DefaultDelayLoadingTime);

				if (!isSucess)
				{
					await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
				}
			}
		}

		private async Task OnSearching()
		{
			await LoadConversationListData();
			IsSearched = true;
		}

		private async Task OnLoadMore()
		{
			try
			{
				_currentPage = DataModel.ApiMessagesResponse?.Pagination.Page ?? AppConstants.DefaultApiPageNumber;
				DataModel.ApiConversationResponse = await ManagementService.GetConversations($"page={ _currentPage + AppConstants.DefaultApiPageStep}", DataModel.LoginResponse.Token);

				if (DataModel.ApiConversationResponse.Conversations.Any())
				{
					var conversations = DataModel.ApiConversationResponse.Conversations;
					var conversationItemViewModels = new MvxObservableCollection<ConversationItemViewModel>();

					foreach (var conversation in conversations)
					{
						if (_conversations.Exists(c => c.Id.Equals(conversation.Id)))
						{
							continue;
						}
						_conversations.Add(conversation);
						conversationItemViewModels.Add(GenerateConversationItem(conversation));
					}
					ConversationItemViewModels.AddRange(conversationItemViewModels);
				}
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private async Task OnBackPressedCommand()
		{
			if (IsSearched)
			{
				var isSucess = true;
				try
				{
					await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
					CurrentQueryString = null;
					await UpdateConversationItemViewModelCollection();
					IsSearched = false;
					IsClearButtonShown = false;
				}
				catch (AppException.ApiException)
				{
					isSucess = false;
				}
				finally
				{
					await _overlay.CloseOverlay(isSucess ? 0 : AppConstants.DefaultDelayLoadingTime);

					if (!isSucess)
					{
						await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
					}
				}
			}
		}

		private async Task OnShowBlockedConversationCommand()
		{
			var needToReload = await NavigationService.Navigate<BlockedConversationViewModel, bool>();
			if (needToReload)
			{
				await UpdateConversationItemViewModelCollection();
			}
		}
	}
}
