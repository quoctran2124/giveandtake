using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross;
using MvvmCross.Commands;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GiveAndTake.Core.ViewModels.TabNavigation
{
	public class TabNavigationViewModel : BaseViewModel
	{
		#region Fields and Properties

		private readonly IDataModel _dataModel;
		private const string ApplinksIdentify = "applinks";
		private const int PostIdLength = 36;
		private IMvxAsyncCommand _showInitialViewModelsCommand;
		private IMvxAsyncCommand _showNotificationsCommand;
		private ICommand _showErrorCommand;
		private IMvxCommand _clearNotificationBadgeCommand;
		private IMvxCommand _clearConversationBadgeCommand;
		private int _appBadgeCount;
		private int _notificationCount;
		private int _conversationBadgeCount;
		private bool _applinkReceivedRegistered;

		public ICommand ShowErrorCommand
			=> _showErrorCommand ?? (_showErrorCommand = new MvxCommand(InitErrorResponseAsync));

		public IMvxAsyncCommand ShowInitialViewModelsCommand
			=> _showInitialViewModelsCommand ?? (_showInitialViewModelsCommand = new MvxAsyncCommand(ShowInitialViewModels));

		public IMvxAsyncCommand ShowNotificationsCommand
			=> _showNotificationsCommand ?? (_showNotificationsCommand = new MvxAsyncCommand(ShowNotifications));

		public IMvxCommand ClearNotificationBadgeCommand
			=> _clearNotificationBadgeCommand ?? (_clearNotificationBadgeCommand = new MvxCommand(OnNotificationBadgeCleared));

		public IMvxCommand ClearConversationBadgeCommand
			=> _clearConversationBadgeCommand ?? (_clearConversationBadgeCommand = new MvxCommand(OnConversationBadgeCleared));

		public string AvatarUrl => _dataModel.LoginResponse.Profile.AvatarUrl;

		public int AppBadgeCount
		{
			get => _appBadgeCount;
			set => SetProperty(ref _appBadgeCount, value);
		}

		public int NotificationCount
		{
			get => _notificationCount;
			set => SetProperty(ref _notificationCount, value);
		}

		public int ConversationBadgeCount
		{
			get => _conversationBadgeCount;
			set => SetProperty(ref _conversationBadgeCount, value);
		}

		public int NumberOfTab { get; set; }

		#endregion

		public TabNavigationViewModel(IDataModel dataModel)
		{
			_dataModel = dataModel;
		}

		public override void ViewCreated()
		{
			base.ViewCreated();
			DataModel.NotificationBadgeUpdated += OnNotificationBadgeUpdated;
			DataModel.ConversationBadgeUpdated += OnConversationBadgeUpdated;
			if (!_applinkReceivedRegistered) DataModel.ApplinkReceived += OnApplinkReceived;
			_applinkReceivedRegistered = true;
			if (DataModel.ApplinkUrl != null && DataModel.IsHomeViewBeforeApplink)
			{
				DataModel.OnApplinkReceived();
			}

			// Update the badge values in the bottom tab bar icons
			Task.Run(UpdateBadges);
		}
		
		public override void ViewDestroy(bool viewFinishing = true)
		{
			base.ViewDestroy(viewFinishing);
			DataModel.NotificationBadgeUpdated -= OnNotificationBadgeUpdated;
			DataModel.ConversationBadgeUpdated -= OnConversationBadgeUpdated;
			DataModel.ApplinkReceived -= OnApplinkReceived;
			_applinkReceivedRegistered = false;
			DataModel.ApplinkUrl = null;
		}

		private async Task UpdateBadges()
		{
			try
			{
				var notificationBadge = await ManagementService.GetBadgeFromServer(DataModel.LoginResponse.Token, AppConstants.GetNotificationBadgeFromServer);
				var conversationBadge = await ManagementService.GetBadgeFromServer(DataModel.LoginResponse.Token, AppConstants.GetConversationBadgeFromServer);
				if (notificationBadge != 0)
				{
					DataModel.UpdateNofiticationBadge(notificationBadge);
				}
				if (conversationBadge != 0)
				{
					DataModel.UpdateConversationBadge(conversationBadge);
				}
			}
			catch (AppException.ApiException e)
			{
				Console.WriteLine("Exception" + e.Message);
				// No need to handle exception because it can cause poor user experience
			}
		}

		private async void OnApplinkReceived(object sender, EventArgs e)
		{
			if (_dataModel.ApplinkUrl.Length < AppConstants.ConstAppLinkLength)
			{
				return;
			}

			try
			{
				var post = await ManagementService.GetPostDetail(
					_dataModel.ApplinkUrl.Substring(AppConstants.ConstAppLinkLength, PostIdLength), _dataModel.LoginResponse.Token);
				if (post.PostStatus == PostStatus.Gave.ToString() && post.User.Id != _dataModel.LoginResponse.Profile.Id)
				{
					await PopupHelper.ShowWarningPopup(Strings["PostIsClosed"]);
					return;
				}

				await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(post);
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
			catch (AppException.PostDeletedException)
			{
				//Incase the user click on the link https://chovanhan.vn/, still go to our app but not go to our postdetail
				//If the Post has been deleted, it will diplay popup notify the user
				if (DataModel.ApplinkUrl?.IndexOf(ApplinksIdentify) >= 0)
				{
					await PopupHelper.ShowWarningPopup(Strings["PostIsDeleted"]);
				}
			}
			finally
			{
				DataModel.ApplinkUrl = null;
			}
		}

		private void OnNotificationBadgeUpdated(object sender, int badge)
		{
			NotificationCount = DataModel.NotificationBadge = badge;
		}

		private void OnConversationBadgeUpdated(object sender, int badge)
		{
			ConversationBadgeCount = DataModel.ConversationBadge = badge;
		}

		public async void InitErrorResponseAsync()
		{
			var result = await NavigationService.Navigate<PopupWarningResponseViewModel, string, bool>(Strings["ErrorMessage"]);
			if (result)
			{
				System.Diagnostics.Process.GetCurrentProcess().CloseMainWindow();
			}
		}

		private async Task ShowNotifications()
		{
			await NavigationService.Navigate<NotificationViewModel>();
		}

		private async Task ShowInitialViewModels()
		{
			var tasks = new List<Task>
			{
				NavigationService.Navigate<HomeViewModel>(),
				NavigationService.Navigate<NotificationViewModel>(),
				NavigationService.Navigate<ConversationListViewModel>(),
				NavigationService.Navigate<ProfileViewModel>(),
			};

			NumberOfTab = tasks.Count;
			await Task.WhenAll(tasks);
			//the overlay will be closed here. Because if the overlay closes in LoginViewModel (I mean close early),..
			//.. the LoginView screen still remain appear without loading overlay..
			//..due to the main thread has to wait all of the tasks above to be reloaded.
			await Mvx.Resolve<ILoadingOverlayService>().CloseOverlay();
		}

		private void OnNotificationBadgeCleared()
		{
			Task.Run(async () =>
			{
				try
				{
					await ManagementService.UpdateSeenNotificationStatus(true, DataModel.LoginResponse.Token);
				}
				catch (AppException.ApiException)
				{

				}
			});
			_dataModel.Badge -= _dataModel.NotificationBadge;
			_dataModel.UpdateNofiticationBadge(0);
			AppBadgeCount = _dataModel.Badge;
		}

		private void OnConversationBadgeCleared()
		{
			_dataModel.Badge -= _dataModel.ConversationBadge;
			_dataModel.UpdateConversationBadge(0);
			AppBadgeCount = _dataModel.Badge;
		}
	}
}
