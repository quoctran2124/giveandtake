﻿using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using GiveAndTake.Core.ViewModels.Popup.MyRequest;
using GiveAndTake.Core.ViewModels.Popup.OtherRequest;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace GiveAndTake.Core.ViewModels.TabNavigation
{
	public class NotificationViewModel : BaseViewModel
	{
		public bool IsRefreshing
		{
			get => _isRefresh;
			set => SetProperty(ref _isRefresh, value);
		}

		public MvxObservableCollection<NotificationItemViewModel> NotificationItemViewModels
		{
			get => _notificationItemViewModel;
			set => SetProperty(ref _notificationItemViewModel, value);
		}

		private Conversation _conversation;
		private MvxInteraction _showProfileTab;
		private readonly IDataModel _dataModel;
		private readonly string _token;
		private readonly ILoadingOverlayService _overlay;
		private MvxObservableCollection<NotificationItemViewModel> _notificationItemViewModel;
		private bool _isRefresh;
		private IMvxCommand _refreshCommand;
		private IMvxCommand _loadMoreCommand;

		public IMvxCommand RefreshCommand => _refreshCommand = _refreshCommand ?? new MvxCommand(OnRefresh);
		public IMvxCommand LoadMoreCommand => _loadMoreCommand = _loadMoreCommand ?? new MvxAsyncCommand(OnLoadMore);
		public IMvxInteraction ShowProfileTab => _showProfileTab ?? (_showProfileTab = new MvxInteraction());

		public NotificationViewModel(IDataModel dataModel, ILoadingOverlayService loadingOverlayService)
		{
			_dataModel = dataModel;
			_token = _dataModel.LoginResponse.Token;
			_overlay = loadingOverlayService;
		}

		public override async Task Initialize()
		{
			try
			{
				await base.Initialize();
				await UpdateNotificationViewModels();
				//for iphone when app is destroyed
				if (DataModel.SelectedNotification != null)
				{
					await OnItemClicked(DataModel.SelectedNotification);
					DataModel.SelectedNotification = null;
				}
			}
			catch (AppException.ApiException)
			{
				await _overlay.CloseOverlay(AppConstants.DefaultDelayLoadingTime);
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
			catch (AppException.PostDeletedException)
			{
				await _overlay.CloseOverlay(AppConstants.DefaultDelayLoadingTime);
				await PopupHelper.ShowWarningPopup(Strings["PostIsDeleted"]);
			}
		}

		private async void BadgeReceived(object sender, int badge)
		{
			try
			{
				if (badge != 0)
				{
					await UpdateNotificationViewModels();
				}
			}
			catch (AppException.ApiException)
			{
				await _overlay.CloseOverlay(AppConstants.DefaultDelayLoadingTime);
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}

		}

		public override void ViewCreated()
		{
			base.ViewCreated();
			_dataModel.NotificationReceived += OnNotificationReceived;
			_dataModel.NotificationBadgeUpdated += BadgeReceived;
			Task.Run(async () =>
			{
				try
				{
					await ManagementService.UpdateSeenNotificationStatus(true, DataModel.LoginResponse.Token);
				}
				catch (AppException.ApiException)
				{
					//Inorge
				}
			});
		}

		public override void ViewDisappearing()
		{
			base.ViewDisappearing();
			DataModel.SelectedNotification = null;
		}

		public override void ViewDestroy(bool viewFinishing = true)
		{
			base.ViewDestroy(viewFinishing);
			_dataModel.NotificationBadgeUpdated -= BadgeReceived;
			_dataModel.NotificationReceived -= OnNotificationReceived;
		}

		public async void OnNotificationReceived(object sender, Notification notification)
		{
			try
			{
				if (DataModel.SelectedNotification == null)
				{
					return;
				}

				await OnItemClicked(notification);
				await UpdateNotificationViewModels();
				await UpdateSeenNotificationStatus();
			}
			catch (AppException.ApiException)
			{
				await _overlay.CloseOverlay(AppConstants.DefaultDelayLoadingTime);
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
			catch (AppException.PostDeletedException)
			{
				await _overlay.CloseOverlay(AppConstants.DefaultDelayLoadingTime);
				await PopupHelper.ShowWarningPopup(Strings["PostIsDeleted"]);
			}
		}

		public async Task UpdateSeenNotificationStatus()
		{
			try
			{
				await ManagementService.UpdateSeenNotificationStatus(true, DataModel.LoginResponse.Token);
			}
			catch (Exception)
			{
				//Ignore
			}
		}

		public async Task UpdateNotificationViewModels()
		{
			_dataModel.ApiNotificationResponse =
				await ManagementService.GetNotificationList($"limit={AppConstants.NumberOfRequestPerPage}&language={DataModel.Language}", _token);
			NotificationItemViewModels = new MvxObservableCollection<NotificationItemViewModel>(_dataModel.ApiNotificationResponse.Notifications.Select(GenerateNotificationItem));
		}

		private async Task OnLoadMore()
		{
			try
			{
				_dataModel.ApiNotificationResponse = await ManagementService.GetNotificationList($"limit={AppConstants.NumberOfRequestPerPage}&page={_dataModel.ApiNotificationResponse.Pagination.Page + AppConstants.DefaultApiPageStep}&language={_dataModel.Language}", _token);
				if (_dataModel.ApiNotificationResponse.Notifications.Any())
				{
					NotificationItemViewModels.AddRange(
						_dataModel.ApiNotificationResponse.Notifications.Select(GenerateNotificationItem));
				}
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private NotificationItemViewModel GenerateNotificationItem(Notification notification)
		{
			var notificationItem = new NotificationItemViewModel(notification)
			{
				ClickAction = async noti =>
				{
					try
					{
						await OnItemClicked(noti);
					}
					catch (AppException.ApiException)
					{
						await _overlay.CloseOverlay(AppConstants.DefaultDelayLoadingTime);
						await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
					}
					catch (AppException.PostDeletedException)
					{
						await _overlay.CloseOverlay(AppConstants.DefaultDelayLoadingTime);
						await PopupHelper.ShowWarningPopup(Strings["PostIsDeleted"]);
					}
				},
			};

			return notificationItem;
		}

		private async Task OnItemClicked(Notification notification)
		{
			await Task.Run((() => Thread.Sleep(AppConstants.DefaultDelayLoadingTime)));

			await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);

			switch (notification.Type)
			{
				case "Like":
					var isSuccess = await HandleLikeAndIsRejectedType(notification);

					await _overlay.CloseOverlay();

					if (isSuccess)
					{
						var navigationResult = await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(_dataModel
							.CurrentPost);
						if (navigationResult == null)
						{
							return;
						}
						else if (navigationResult.ShouldNavigateToProfileViewModel)
						{
							_showProfileTab.Raise();
						}
					}

					break;

				case "Comment":
					await HandleCommentType(notification);

					await _overlay.CloseOverlay();

					var viewModeResult =
						await NavigationService.Navigate<CommentViewModel, Post, ViewResult>(DataModel.CurrentPost);
					if (viewModeResult == ViewResult.ShowProfileView)
					{
						await Task.Delay(AppConstants.DefaultDelayLoadingTime);
						_showProfileTab.Raise();
					}

					break;

				case "Request":
					var request = await ManagementService.GetRequestById(notification.RelevantId, _token);
					await _overlay.CloseOverlay();
					await HandleRequestType(request, notification.Id.ToString());
					break;

				case "BlockedPost":
					await PopupHelper.ShowWarningPopup(Strings["DefaultWarningMessage"]);
					break;

				case "Warning":
					await PopupHelper.ShowWarningPopup(Strings["DefaultWarningMessage"]);
					break;
				case "Chat":
					await OpenConversation(notification);
					await _overlay.CloseOverlay();
					await NavigationService.Navigate<ConversationViewModel, Conversation>(_conversation);
					break;
			}

			await UpdateNotificationViewModels();
		}

		private async Task OpenConversation(Notification notification)
		{
			_conversation = await ManagementService.GetConversation(new ConversationRequest
			{
				UserId = DataModel.LoginResponse.Profile.Id,
				FriendId = notification.SourceUserId.ToString(),
			}, DataModel.LoginResponse.Token);
		}

		private async Task HandleRequestType(Request request, string notificationId)
		{
			if (request.Post == null)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string>(Strings["PostIsDeleted"]);
				await ManagementService.UpdateReadStatus(notificationId, true, _token);
				return;
			}
			var requestStatus = request.RequestStatus;
			var popupResult = PopupRequestStatusResult.Cancelled;
			switch (requestStatus)
			{
				case RequestStatus.Pending:
					popupResult = await NavigationService.Navigate<OtherRequestPendingViewModel, Request, PopupRequestStatusResult>(request);
					break;
				case RequestStatus.Received:
					if (DataModel.LoginResponse.Profile.Id == request.Post.User.Id)
					{
						popupResult = await NavigationService.Navigate<MyRequestReceivedViewModel, Request, PopupRequestStatusResult>(request);
					}
					else
					{
						popupResult = await NavigationService.Navigate<OtherRequestReceivedViewModel, Request, PopupRequestStatusResult>(request);
					}
					break;

				case RequestStatus.Approved:
					if (request.Post.User.Id.Equals(_dataModel.LoginResponse.Profile.Id))
					{
						popupResult = await NavigationService.Navigate<OtherRequestApprovedViewModel, Request, PopupRequestStatusResult>(request);

					}
					else
					{
						popupResult = await NavigationService.Navigate<MyRequestApprovedViewModel, Request, PopupRequestStatusResult>(request);
					}

					break;
				case RequestStatus.Rejected:

					await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(request.Post);
					break;
			}

			switch (popupResult)
			{
				case PopupRequestStatusResult.Received:
					await ReceiveGift(request.Id);
					break;

				case PopupRequestStatusResult.Removed:
					await CancelOldRequest(request.Post);
					break;

				case PopupRequestStatusResult.Rejected:
					await Task.Delay(AppConstants.DefaultDelayLoadingTime);
					await OnRequestRejected(request);
					break;

				case PopupRequestStatusResult.Approved:
					await OnRequestAccepted(request);
					break;

				case PopupRequestStatusResult.ShowPostDetail:
					await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(request.Post);
					break;

				case PopupRequestStatusResult.ShowOtherProfile:
					if (DataModel.LoginResponse.Profile.Id == request.Post.User.Id)
					{
						await NavigationService.Navigate<UserProfileViewModel, User>(request.User);
					}
					else
					{
						await NavigationService.Navigate<UserProfileViewModel, User>(request.Post.User);
					}
					break;
				case PopupRequestStatusResult.ShowMyProfile:
					_showProfileTab.Raise();
					break;
			}

			await ManagementService.UpdateReadStatus(notificationId, true, _token);
		}

		private async Task OnRequestRejected(Request request)
		{
			var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["RequestRejectingMessage"]);

			if (result != RequestStatus.Submitted)
			{
				return;
			}

			var isSaved = await ManagementService.ChangeStatusOfRequest(request.Id, "Rejected", _token);
			if (isSaved)
			{
				await PopupHelper.ShowWarningPopup(Strings["SuccessfulRejectionMessage"]);
			}
		}

		private async Task OnRequestAccepted(Request request)
		{
			var result = await NavigationService.Navigate<PopupResponseViewModel, Request, RequestStatus>(request);
			if (result == RequestStatus.Submitted)
			{
				await PopupHelper.ShowWarningPopup(Strings["SuccessfulAcceptanceMessage"]);
			}
		}

		private async Task<bool> HandleLikeAndIsRejectedType(Notification notification)
		{
			await ManagementService.UpdateReadStatus(notification.Id.ToString(), true, _token);
			_dataModel.CurrentPost =
				await ManagementService.GetPostDetail(notification.RelevantId.ToString(), _token);
			_dataModel.CurrentPost.IsMyPost = notification.Type == "Like";

			return true;
		}

		private async Task ReceiveGift(string requestId)
		{
			await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
			await ManagementService.ChangeStatusOfRequest(requestId, RequestStatus.Received.ToString(), _dataModel.LoginResponse.Token);
			await _overlay.CloseOverlay();
		}

		private async Task CancelOldRequest(Post post)
		{
			var popupResult = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["CancelRequestConfirm"]);
			if (popupResult != RequestStatus.Submitted)
			{
				return;
			}
			await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
			await ManagementService.CancelUserRequest(post.PostId, _dataModel.LoginResponse.Token);
			await _overlay.CloseOverlay();
		}

		private async void OnRefresh()
		{
			try
			{
				IsRefreshing = true;
				await UpdateNotificationViewModels();
				await UpdateSeenNotificationStatus();
				IsRefreshing = false;
			}
			catch (AppException.ApiException)
			{
				await _overlay.CloseOverlay(AppConstants.DefaultDelayLoadingTime);
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private async Task HandleCommentType(Notification notification)
		{
			var post = await ManagementService.GetPostDetail(notification.RelevantId.ToString(), _token);
			DataModel.CurrentPost = post;
			await ManagementService.UpdateReadStatus(notification.Id.ToString(), true, _token);
		}
	}
}
