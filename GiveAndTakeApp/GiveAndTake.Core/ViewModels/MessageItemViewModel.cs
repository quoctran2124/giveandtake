﻿using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using System.Collections.Generic;

namespace GiveAndTake.Core.ViewModels
{
	public class MessageItemViewModel : BaseViewModel
	{
		public List<ITransformation> Transformations => new List<ITransformation> { new CircleTransformation() };

		public string MessageContent
		{
			get => _messageContent;
			set => SetProperty(ref _messageContent, value);
		}

		public string AvatarUrl
		{
			get => _avatarUrl;
			set => SetProperty(ref _avatarUrl, value);
		}

		public bool IsOthersMessages
		{
			get => _isOthersMessages;
			set => SetProperty(ref _isOthersMessages, value);
		}

		public bool IsAvatarShown
		{
			get => _isAvatarShown;
			set => SetProperty(ref _isAvatarShown, value);
		}

		public bool IsBlocked
		{
			get => _isBlocked;
			set => SetProperty(ref _isBlocked, value);
		}

		private string _messageContent;
		private string _avatarUrl;
		private bool _isOthersMessages;
		private bool _isAvatarShown;
		private bool _isBlocked;

		public MessageItemViewModel(ChatMessage message, bool isOthersMessages, bool isAvatarShown,bool isBlocked)
		{
			_messageContent = message.MessageContent;
			_avatarUrl = message.Sender.AvatarUrl;
			_isOthersMessages = isOthersMessages;
			_isAvatarShown = isAvatarShown;
			_isBlocked = isBlocked;
		}
	}
}
