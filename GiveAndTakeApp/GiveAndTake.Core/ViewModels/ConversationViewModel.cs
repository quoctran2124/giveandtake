﻿using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using Microsoft.AspNetCore.SignalR.Client;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Services;

namespace GiveAndTake.Core.ViewModels
{
	public class ConversationViewModel : BaseViewModel<Conversation, bool>
	{
		#region Properties

		public string SendMessageHint => Strings["SendMessageHint"];

		public List<ITransformation> Transformations => new List<ITransformation> { new CircleTransformation() };

		private MvxObservableCollection<MessageItemViewModel> _messageItemViewModels;
		public MvxObservableCollection<MessageItemViewModel> MessageItemViewModels
		{
			get => _messageItemViewModels;
			set => SetProperty(ref _messageItemViewModels, value);
		}

		private string _friendAvatarUrl;
		public string FriendAvatarUrl
		{
			get => _friendAvatarUrl;
			set => SetProperty(ref _friendAvatarUrl, value);
		}

		private string _friendUserName;
		public string FriendUserName
		{
			get => _friendUserName;
			set => SetProperty(ref _friendUserName, value);
		}

		private string _message;
		public string Message
		{
			get => _message;
			set
			{
				IsSendIconActivated = !string.IsNullOrEmpty(value);
				SetProperty(ref _message, value);
			}
		}

		private bool _isBlocked;
		public bool IsBLocked
		{
			get => _isBlocked;
			set => SetProperty(ref _isBlocked, value);
		}

		private IMvxCommand _sendMesageCommand;
		public IMvxCommand SendMessageCommand =>
			_sendMesageCommand ?? (_sendMesageCommand = new MvxAsyncCommand(SendMessage));

		private IMvxCommand _backPressedCommand;
		public IMvxCommand BackPressedCommand =>
			_backPressedCommand = _backPressedCommand ?? new MvxAsyncCommand(OnBackPressed);

		private IMvxCommand _showMenuPopupCommand;
		public IMvxCommand ShowMenuPopupCommand =>
			_showMenuPopupCommand ?? (_showMenuPopupCommand = new MvxAsyncCommand(OnShowMenuPopup));

		private IMvxCommand _loadMoreCommand;
		public IMvxCommand LoadMoreCommand =>
			_loadMoreCommand ?? (_loadMoreCommand = new MvxAsyncCommand(OnLoadMore));

		public MvxInteraction<int> ScrollToMessageInteraction { get; } = new MvxInteraction<int>();

		public MvxInteraction HideKeyBoardInteraction { get; } = new MvxInteraction();

		private bool _isSendIconActivated;
		public bool IsSendIconActivated
		{
			get => _isSendIconActivated;
			set => SetProperty(ref _isSendIconActivated, value);
		}

		private User _friend;
		private HubConnection _connection;
		private Conversation _conversation;
		private List<ChatMessage> _chatMessages;
		private readonly ILoadingOverlayService _overlay;
		private bool _isConversationStatusChanged;
		private int _currentPage;
		private bool _isLoadfromfirstTime;

		#endregion

		public ConversationViewModel(ILoadingOverlayService overlay)
		{
			_overlay = overlay;
			MessageItemViewModels = new MvxObservableCollection<MessageItemViewModel>();
		}

		public override void Prepare(Conversation conversation)
		{
			_conversation = conversation;
			_chatMessages = _conversation.Messages;
			_friend = _conversation.User;
			FriendAvatarUrl = _friend.AvatarUrl;
			FriendUserName = _friend.DisplayName;
			IsBLocked = StringHelper.GetEnumFromDescription(conversation.ConversationStatus,
							ConversationItemType.NormalConversation) == ConversationItemType.BlockedConversation;
			_isConversationStatusChanged = false;
			_isLoadfromfirstTime = true;
		}

		public override async Task Initialize()
		{
			await base.Initialize();

			MessageItemViewModels =
				new MvxObservableCollection<MessageItemViewModel>(_conversation.Messages.Select(ConvertToItemViewModel));
			await RegisterHubConnection();
		}

		public override async void ViewDestroy(bool viewFinishing = true)
		{
			await UnRegisterHubConnection();
			base.ViewDestroy(viewFinishing);
		}

		private MessageItemViewModel ConvertToItemViewModel(ChatMessage message) =>
			new MessageItemViewModel(message, !IsOurMessage(message), IsAvatarShown(message), IsBLocked);

		private bool IsOurMessage(ChatMessage message) => message.UserConversationId.Equals(_conversation.UserConversationId,
			StringComparison.InvariantCultureIgnoreCase);

		private bool IsAvatarShown(ChatMessage message)
		{
			if (IsOurMessage(message))
			{
				return false;
			}

			var position = _chatMessages.IndexOf(message);

			if (position == 0)
			{
				return true;
			}

			var result = IsOurMessage(_chatMessages[position - 1]);

			if (position < _chatMessages.Count - 1)
			{
				result = result && (IsOurMessage(_chatMessages[position + 1]));
			}

			return result;
		}

		private async Task OnBackPressed()
		{
			if (DataModel.ApiMessagesResponse != null)
			{
				DataModel.ApiMessagesResponse.Pagination.Page = 1;
			}

			await UnRegisterHubConnection();
			await NavigationService.Close(this, _isConversationStatusChanged);
		}

		private async Task OnShowMenuPopup()
		{
			try
			{
				var conversationItemType = IsBLocked ? ConversationItemType.BlockedConversation : ConversationItemType.NormalConversation;
				var postOptions = MenuOptionHelper.GetMenuPopupList(conversationItemType);
				HideKeyBoardInteraction?.Raise();
				var result = await NavigationService.Navigate<PopupExtensionOptionViewModel, List<string>, string>(postOptions);
				if (string.IsNullOrEmpty(result))
				{
					return;
				}
				if (result == Strings["BlockThisConversation"])
				{
					var confirmResult = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["BlockConversationConfirmationMessage"]);
					if (confirmResult == RequestStatus.Submitted)
					{
						await _overlay.ShowOverlay(Strings["ProcessingDataOverLayTitle"]);
						await ManagementService.ChangeConversationStatus(_conversation.Id, DataModel.LoginResponse.Token, AppConstants.ConversationStatusBlocked);
						await _overlay.CloseOverlay();
						IsBLocked = true;
						Message = string.Empty;
						foreach (var messageItemViewModel in MessageItemViewModels)
						{
							messageItemViewModel.IsBlocked = IsBLocked;
						}
						_isConversationStatusChanged = true;
					}
				}
				else if (result == Strings["DeleteThisConversation"])
				{
					var confirmResult = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["DeleteConversationConfirmationMessage"]);
					if (confirmResult == RequestStatus.Submitted)
					{
						await _overlay.ShowOverlay(Strings["ProcessingDataOverLayTitle"]);
						await ManagementService.DeleteConversation(_conversation.Id, DataModel.LoginResponse.Token);
						await _overlay.CloseOverlay();
						await NavigationService.Close(this, true);
					}
				}
				else if (result == Strings["UnBlockThisConversation"])
				{
					await _overlay.ShowOverlay(Strings["ProcessingDataOverLayTitle"]);
					await ManagementService.ChangeConversationStatus(_conversation.Id, DataModel.LoginResponse.Token, AppConstants.ConversationStatusNormal);
					await _overlay.CloseOverlay();
					IsBLocked = false;

					foreach (var messageItemViewModel in MessageItemViewModels)
					{
						messageItemViewModel.IsBlocked = IsBLocked;
					}
					_isConversationStatusChanged = true;
				}
			}
			catch (AppException.ApiException)
			{
				await _overlay.CloseOverlay();
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private async Task OnLoadMore()
		{
			try
			{
				_currentPage = DataModel.ApiMessagesResponse?.Pagination.Page ?? 1;
				DataModel.ApiMessagesResponse = await ManagementService.GetMessages(_conversation.Id, $"page={ _currentPage + AppConstants.DefaultApiPageStep}", DataModel.LoginResponse.Token);
				if (DataModel.ApiMessagesResponse.Messages.Any())
				{
					var messages = DataModel.ApiMessagesResponse.Messages;
					var messageItemViewModels = new MvxObservableCollection<MessageItemViewModel>();

					foreach (var message in messages)
					{
						if ((int)(_chatMessages.LastOrDefault().CreatedTime.Subtract(new DateTime(1970, 1, 1)))
							.TotalSeconds <= (int)(message.CreatedTime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds)
						{
							continue;
						}
						_chatMessages.Add(message);
						messageItemViewModels.Add(ConvertToItemViewModel(message));
					}
					MessageItemViewModels.AddRange(messageItemViewModels);
					_isConversationStatusChanged = true;
				}
			}
			catch (AppException.ApiException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}

			if (_isLoadfromfirstTime)
			{
				ScrollToMessageInteraction?.Raise(0);
				_isLoadfromfirstTime = false;
			}
		}

		#region ChatHubConnectionInit

		private async Task RegisterHubConnection()
		{
			try
			{
				_connection = new HubConnectionBuilder()
					.WithUrl(AppConstants.ChatHubUrl, options =>
					{
						options.Headers["conversationId"] = _conversation.Id;
						options.Headers["userConversationId"] = _conversation.UserConversationId;
					})
					.Build();

				_connection.On<ChatMessage>("ReceiveMessage", ReceiveMessage);

				_connection.Closed += OnConnectionClosed;

				await _connection.StartAsync();
			}
			catch (Exception e)
			{
				_connection.Closed -= OnConnectionClosed;
				Console.WriteLine(e);
			}
		}

		private async Task UnRegisterHubConnection()
		{
			_connection.Closed -= OnConnectionClosed;
			await _connection.StopAsync();
		}

		private async Task OnConnectionClosed(Exception error)
		{
			Debug.WriteLine(error.Message);
			// avoid continuously reconnecting 
			await Task.Delay(new Random().Next(0, 3) * 1000);
		}

		private async Task SendMessage()
		{
			var message = new ChatMessage
			{
				UserConversationId = _conversation.UserConversationId,
				ConversationId = _conversation.Id,
				MessageContent = Message,
				Sender = DataModel.LoginResponse.Profile,
				ReceiverId = _friend.Id
			};

			try
			{
				await _connection.SendAsync("SendMessage", message);
				Message = string.Empty;
			}
			catch (InvalidOperationException)
			{
				await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
			}
		}

		private void ReceiveMessage(ChatMessage receivedMessage)
		{
			if (!receivedMessage.ConversationId.Equals(_conversation.Id))
			{
				return;
			}

			var latestMessageItemViewModel = MessageItemViewModels.FirstOrDefault();
			if (latestMessageItemViewModel != null && (latestMessageItemViewModel.IsAvatarShown && !IsOurMessage(receivedMessage)))
			{
				latestMessageItemViewModel.IsAvatarShown = false;
			}

			_isConversationStatusChanged = true;

			receivedMessage.Sender = IsOurMessage(receivedMessage) ? DataModel.LoginResponse.Profile : _friend;
			_chatMessages.Insert(0, receivedMessage);
			var messageItemViewModel = ConvertToItemViewModel(receivedMessage);
			MessageItemViewModels.Insert(0, messageItemViewModel);

			ScrollToMessageInteraction?.Raise(0);
		}
		#endregion
	}
}
