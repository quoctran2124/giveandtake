﻿using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using System;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels
{
    public class OtpVerificationViewModel : BaseViewModel<UserRegisterRequest>
    {
        #region Fields

        private readonly ILoadingOverlayService _overlay;
        private readonly IStorageHelper _storageHelper;
        private readonly IDeviceInfo _deviceInfo;
        private bool _registerButtonEnabled;
        private string _otpCode1;
        private string _otpCode3;
        private string _otpCode2;
        private string _otpCode4;
        private string _otpCode5;
        private string _otpCode6;
        private IMvxCommand _resendOtpCommand;
        private IMvxCommand _submitCommand;
        private IMvxCommand _backPressedCommand;
        private UserRegisterRequest _userRegisterRequest;

        #endregion

        #region Constructors & Initilization

        public OtpVerificationViewModel(ILoadingOverlayService overlay, IStorageHelper storageHelper, IDeviceInfo deviceInfo)
        {
            _overlay = overlay;
            _storageHelper = storageHelper;
            _deviceInfo = deviceInfo;
        }

        public override void Prepare(UserRegisterRequest userRegisterRequest)
        {
            _userRegisterRequest = userRegisterRequest;
        }

        #endregion

        #region Properties

        public string TxtOtpTitle => Strings["OtpScreenTxtOtpTitle"];

        public string TxtOtpDetail => Strings["OtpScreenTxtOtpDetail"];

        public string TxtResendOtp => Strings["OtpScreenTxtResendOtp"];

        public string BtnCancelTitle => Strings["CancelTitle"];

        public string BtnSubmit => Strings["SubmitTitle"];

        public bool RegisterButtonEnabled
        {
            get => _registerButtonEnabled;
            set => SetProperty(ref _registerButtonEnabled, value);
        }

        public string OtpCode1
        {
            get => _otpCode1;
            set
            {
                SetProperty(ref _otpCode1, value);
                ValidateOtp();
            }
        }

        public string OtpCode2
        {
            get => _otpCode2;
            set
            {
                SetProperty(ref _otpCode2, value);
                ValidateOtp();
            }
        }

        public string OtpCode3
        {
            get => _otpCode3;
            set
            {
                SetProperty(ref _otpCode3, value);
                ValidateOtp();
            }
        }

        public string OtpCode4
        {
            get => _otpCode4;
            set
            {
                SetProperty(ref _otpCode4, value);
                ValidateOtp();
            }
        }

        public string OtpCode5
        {
            get => _otpCode5;
            set
            {
                SetProperty(ref _otpCode5, value);
                ValidateOtp();
            }
        }

        public string OtpCode6
        {
            get => _otpCode6;
            set
            {
                SetProperty(ref _otpCode6, value);
                ValidateOtp();
            }
        }

        public IMvxCommand ResendOtpCommand => 
            _resendOtpCommand ?? (_resendOtpCommand = new MvxAsyncCommand(ResendOtpAsync));

        public IMvxCommand SubmitCommand =>
            _submitCommand ?? (_submitCommand = new MvxAsyncCommand(SubmitAsync));

        public IMvxCommand BackPressedCommand => _backPressedCommand ?? (_backPressedCommand = new MvxCommand(GoBack));

        #endregion

        #region Methods

        private async Task ResendOtpAsync()
        {
            try
            {
                await _overlay.ShowOverlay(Strings["OtpSendingMessage"]);
                _userRegisterRequest.OtpCode = new Random().Next(100000, 999999).ToString();
                await ManagementService.SendOtpCode(_userRegisterRequest.PhoneNumber, _userRegisterRequest.OtpCode);
                OtpCode1 = "";
                OtpCode2 = "";
                OtpCode3 = "";
                OtpCode4 = "";
                OtpCode5 = "";
                OtpCode6 = "";
                await _overlay.CloseOverlay();
            }
            catch (AppException.ApiException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
            }
        }

        private async Task SubmitAsync()
        {
            try
            {
                await _overlay.ShowOverlay(Strings["RegisterScreenProcessOverLayTitle"]);
                await CheckOtpCode();
                await SubmitAction(_userRegisterRequest.OtpVerifyType);
            }
            catch (AppException.WrongOtpCodeException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["OtpScreenWrongCode"]);
            }
            catch (AppException.ApiLoginBadRequestException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["ErrorRegisterBadRequestMessage"]);
            }
            catch (AppException.ApiException)
            {
                await _overlay.CloseOverlay();
                await PopupHelper.ShowWarningPopup(Strings["ErrorConnectionMessage"]);
            }
        }

        private async Task CheckOtpCode()
        {
            await Task.Delay(500);
            if (!_userRegisterRequest.OtpCode.Equals(GetFullOtpCode()))
            {
                throw new AppException.WrongOtpCodeException(null);
            }
        }

        private async Task SubmitAction(OtpVerifyType otpVerifyType)
        {
            switch (otpVerifyType)
            {
                case OtpVerifyType.Register:
                    await RegisterAndSaveCredentials();
                    await SendDeviceInformationAndNavigateToSuccess();
                    break;
                case OtpVerifyType.ForgotPassword:
                    await NavigationService.Navigate<ResetPasswordViewModel, UserRegisterRequest>(_userRegisterRequest);
                    break;
            }
        }

        private async Task RegisterAndSaveCredentials()
        {
            DataModel.LoginResponse = await ManagementService.RegisterWithPhoneNumber(_userRegisterRequest);
            _storageHelper.SaveCredentials(DataModel.LoginResponse);
        }

        private async Task SendDeviceInformationAndNavigateToSuccess()
        {
            await ManagementService.SendPushNotificationUserInformation(new PushNotificationUserInformation
            {
                DeviceToken = _deviceInfo.DeviceToken,
                MobilePlatform = _deviceInfo.MobilePlatform,
                Language = DataModel.Language.ToString()
            }, DataModel.LoginResponse.Token);

            await NavigationService.Navigate<RegisterSuccessViewModel>();
        }

        private void GoBack()
        {
            NavigationService.Close(this);
        }

        private string GetFullOtpCode()
        {
            return _otpCode1 + _otpCode2 + _otpCode3 + _otpCode4 + _otpCode5 + _otpCode6;
        }

        private void ValidateOtp()
        {
            RegisterButtonEnabled = GetFullOtpCode().Length == 6;
        }

        #endregion
    }
}