﻿using System.ComponentModel;

namespace GiveAndTake.Core
{
	public enum FontType
	{
		Light,
		Regular,
		Medium,
		Bold,
	}
	public enum RequestMethod
	{
		Get,
		Put,
		Post,
		Delete
	}

	public enum NetworkStatus
	{
		Success,
		NoWifi,
		Timeout,
		Exception
	}

	public enum PopupListType
	{
		FilterType,
		MenuType
	}

	public enum RequestStatus
	{
		Pending,
		Approved,
		Received,
		Rejected,
		Cancelled,
		Submitted
	}
	public enum PostStatus
	{
		Giving,
		Gave,
		Deleted
	}

	public enum PopupRequestStatusResult
	{
		Cancelled,
		Removed,
		Rejected,
		Approved,
		Received,
		ShowPostDetail,
		ShowMyProfile,
		ShowOtherProfile
	}

	public enum ListViewItemType
	{
		MyPosts,
		MyRequests
	}

	public enum ViewMode
	{
		CreatePost,
		EditPost,
	}

	public enum PopupReason
	{
	    InappropriateContentReason,
    	InappropriatePictureReason, 
    	FakeContentReason, 
    	OtherReason,
    }
    
	public enum ProfileReloadType
	{
		MyPost,
		MyRequestPost
	}

	public enum BottomTabBar
	{
		Home = 0,
		Notification = 1,
		Conversation = 2,
		Profile = 3
	}

	public enum LanguageType
	{
		vi,
		en
	}

	public enum ConversationItemType
	{
		[Description("Allow")]
		NormalConversation,
		[Description("Block")]
		BlockedConversation
	}
	
	public enum ViewResult
	{
		ReloadData,
		ShowProfileView,
		NoAction
	}

    public enum LoginType
    {
        Facebook,
        PhoneNumber
    }

    public enum OtpVerifyType
    {
        Register,
        ForgotPassword
    }
}