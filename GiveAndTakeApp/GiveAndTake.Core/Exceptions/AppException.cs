﻿using System;

namespace GiveAndTake.Core.Exceptions
{
	public class AppException
	{
		public class ApiException : Exception
		{
			public ApiException(string message) : base(message) { }
		}

		public class ApiServiceUnavailableException : ApiException
		{
			public ApiServiceUnavailableException(string message) : base(message) { }
		}

        public class ApiSendOtpErrorException : ApiException
        {
            public ApiSendOtpErrorException(string message) : base(message) { }
        }

        public class ApiLoginBadRequestException : ApiException
        {
            public ApiLoginBadRequestException(string message) : base(message) { }
        }

		public class PostDeletedException : Exception
		{
			public PostDeletedException(string message) : base(message) { }
		}

        public class WrongOtpCodeException : Exception
        {
            public WrongOtpCodeException(string message) : base(message)
            {
            }
        }
	}
	
}
