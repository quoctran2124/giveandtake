﻿using System.Runtime.Serialization;

namespace GiveAndTake.Core.Models
{
    [DataContract]
    public class LoginWithPhoneNumberDto
    {
        [DataMember(Name = "username")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }
    }
}