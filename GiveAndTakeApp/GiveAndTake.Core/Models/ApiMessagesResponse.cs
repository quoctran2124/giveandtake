﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace GiveAndTake.Core.Models
{
	[DataContract]
	public class ApiMessagesResponse
	{
		[DataMember(Name = "results")]
		public List<ChatMessage> Messages { get; set; }

		[DataMember(Name = "pagination")]
		public Pagination Pagination { get; set; }
	}
}
