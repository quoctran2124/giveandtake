﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace GiveAndTake.Core.Models
{
	[DataContract]
	public class ApiConversationResponse
	{
		[DataMember(Name = "results")]
		public List<Conversation> Conversations { get; set; }

		[DataMember(Name = "pagination")]
		public Pagination Pagination { get; set; }
	}
}
