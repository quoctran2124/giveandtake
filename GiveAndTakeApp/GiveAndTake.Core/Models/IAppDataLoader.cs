﻿using System.Collections.Generic;

namespace GiveAndTake.Core.Models
{
	public interface IAppDataLoader
	{
		AppData AppData { get; }

		void Initialize();

		void SaveData(AppData appData);

		void UpdateLanguage(LanguageType locale);
	}
}
