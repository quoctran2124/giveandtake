using System;
using System.Runtime.Serialization;

namespace GiveAndTake.Core.Models
{
	[DataContract]
	public class ChatMessage
	{
		[DataMember(Name = "Id", EmitDefaultValue = false)]
		public string Id { get; set; }

		[DataMember(Name = "conversationId", EmitDefaultValue = false)]
		public string ConversationId { get; set; }

		[DataMember(Name = "userConversationId", EmitDefaultValue = false)]
		public string UserConversationId { get; set; }

		[DataMember(Name = "messageContent", EmitDefaultValue = false)]
		public string MessageContent { get; set; }

		[DataMember(Name = "sender", EmitDefaultValue = false)]
		public User Sender { get; set; }

		[DataMember(Name = "receiverId", EmitDefaultValue = false)]
		public string ReceiverId { get; set; }

		[DataMember(Name = "createdTime", EmitDefaultValue = false)]
		public DateTime CreatedTime { get; set; }
	}
}
