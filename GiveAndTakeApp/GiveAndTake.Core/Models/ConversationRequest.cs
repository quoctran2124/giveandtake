﻿using System.Runtime.Serialization;

namespace GiveAndTake.Core.Models
{
	[DataContract]
	public class ConversationRequest
	{
		[DataMember(Name = "userId")]
		public string UserId { get; set; }

		[DataMember(Name = "friendId")]
		public string FriendId { get; set; }
	}
}