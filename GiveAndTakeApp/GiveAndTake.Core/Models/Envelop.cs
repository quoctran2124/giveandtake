﻿using System.Xml.Serialization;

namespace GiveAndTake.Core.Models
{
    [XmlRoot(ElementName = "sendSMS", Namespace = "http://tempuri.org/")]
    public class SendSms
    {
        [XmlElement(ElementName = "userID", Namespace = "http://tempuri.org/")]
        public string UserId { get; set; }

        [XmlElement(ElementName = "password", Namespace = "http://tempuri.org/")]
        public string Password { get; set; }

        [XmlElement(ElementName = "phoneNo", Namespace = "http://tempuri.org/")]
        public string PhoneNo { get; set; }

        [XmlElement(ElementName = "content", Namespace = "http://tempuri.org/")]
        public string Content { get; set; }

        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "sendSMSResult", Namespace = "http://tempuri.org/")]
    public class SendSmsResult
    {
        [XmlElement(ElementName = "result", Namespace = "http://tempuri.org/")]
        public string Result { get; set; }

        [XmlElement(ElementName = "message", Namespace = "http://tempuri.org/")]
        public string Message { get; set; }
    }

    [XmlRoot(ElementName = "sendSMSResponse", Namespace = "http://tempuri.org/")]
    public class SendSmsResponse
    {
        [XmlElement(ElementName = "sendSMSResult", Namespace = "http://tempuri.org/")]
        public SendSmsResult SendSmsResult { get; set; }

        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Body
    {
        [XmlElement(ElementName = "sendSMSResponse", Namespace = "http://tempuri.org/")]
        public SendSmsResponse SendSmsResponse { get; set; }

        [XmlElement(ElementName = "sendSMS", Namespace = "http://tempuri.org/")]
        public SendSms SendSms { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelop
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }

        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }

        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }

        [XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soap { get; set; }
    }
}