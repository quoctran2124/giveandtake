﻿using System;
using System.Collections.Generic;

namespace GiveAndTake.Core.Models
{
	public class DataModel : IDataModel
	{
		//Notification
		public event EventHandler<Notification> NotificationReceived;
		public event EventHandler<int> NotificationBadgeUpdated;
		public event EventHandler<int> ConversationBadgeUpdated;
		public ApiNotificationResponse ApiNotificationResponse { get; set; }
		public Notification SelectedNotification { get; set; }
		public int Badge { get; set; }
		public int NotificationBadge { get; set; }
		public int ConversationBadge { get; set; }

		public void RaiseNotificationReceived(Notification notification)
		{
			NotificationReceived?.Invoke(this, notification);
		}

		public void UpdateNofiticationBadge(int badge)
		{
			NotificationBadgeUpdated?.Invoke(this, badge);
		}

		public void UpdateConversationBadge(int badge)
		{
			ConversationBadgeUpdated?.Invoke(this, badge);
		}


		//Applink
		public event EventHandler ApplinkReceived;
		public bool IsHomeViewBeforeApplink { get; set; } 
		public string ApplinkUrl { get; set; }
		public void OnApplinkReceived()
		{
			ApplinkReceived?.Invoke(this, null);
		}

		//Language
		public event EventHandler LanguageUpdated;
		public LanguageType Language { get; set; }
		public void RaiseLanguageUpdated()
		{
			LanguageUpdated?.Invoke(this, null);
		}

		//Post
		public List<Category> Categories { get; set; }
		public List<ProvinceCity> ProvinceCities { get; set; }
		public List<SortFilter> SortFilters { get; set; }
		public ApiPostsResponse ApiPostsResponse { get; set; }
		public ApiPostsResponse ApiMyPostsResponse { get; set; }
		public int PostImageIndex { get; set; }
		public Post CurrentPost { get; set; }

		//Request
		public ApiRequestsResponse ApiRequestsResponse { get; set; }
		public ApiPostsResponse ApiMyRequestedPostResponse { get; set; }

		public ApiCommentResponse ApiCommentResponse { get; set; }

		//Login
		public LoginResponse LoginResponse { get; set; }
		public bool IsLoggedIn { get; set; }

		//Conversation
		public ApiConversationResponse ApiConversationResponse { get; set; }
		public ApiMessagesResponse ApiMessagesResponse { get; set; }
	}
}
