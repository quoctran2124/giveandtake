﻿using System;
using System.Runtime.Serialization;

namespace GiveAndTake.Core.Models
{
	[DataContract]
	public class Comment
	{
		[DataMember(Name = "id")]
		public Guid Id { get; set; }

		[DataMember(Name = "user")]
		public User User { get; set; }

		[DataMember(Name = "postId")]
		public Guid PostId { get; set; }

		[DataMember(Name = "userId")]
		public Guid UserId { get; set; }

		[DataMember(Name = "commentMessage")]
		public string CommentMessage { get; set; }

		[DataMember(Name = "createdTime")]
		public DateTime CreatedTime { get; set; }

		[DataMember(Name = "updatedTime")]
		public DateTime UpdatedTime { get; set; }
	}
}
