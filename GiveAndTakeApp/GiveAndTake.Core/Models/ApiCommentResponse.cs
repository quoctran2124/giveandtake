﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace GiveAndTake.Core.Models
{
	[DataContract]
	public class ApiCommentResponse
	{
		[DataMember(Name = "results")]
		public List<Comment> Comments { get; set; }

		[DataMember(Name = "pagination")]
		public Pagination Pagination { get; set; }
	}
}
