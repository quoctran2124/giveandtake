﻿using System;
using System.Collections.Generic;

namespace GiveAndTake.Core.Models
{
	public interface IDataModel
	{
		//Notification
		event EventHandler<Notification> NotificationReceived;
		event EventHandler<int> NotificationBadgeUpdated;
		event EventHandler<int> ConversationBadgeUpdated;
		ApiNotificationResponse ApiNotificationResponse { get; set; }
		Notification SelectedNotification { get; set; }
		int Badge { get; set; }
		int NotificationBadge { get; set; }
		int ConversationBadge { get; set; }
		void RaiseNotificationReceived(Notification notification);
		void UpdateNofiticationBadge(int badge);
		void UpdateConversationBadge(int badge);

		//Applink
		event EventHandler ApplinkReceived;
		bool IsHomeViewBeforeApplink { get; set; }
		string ApplinkUrl { get; set; }
		void OnApplinkReceived();

		//Language
		event EventHandler LanguageUpdated;
		LanguageType Language { get; set; }
		void RaiseLanguageUpdated();

		//Post
		List<Category> Categories { get; set; }
		List<ProvinceCity> ProvinceCities { get; set; }
		List<SortFilter> SortFilters { get; set; }
		ApiPostsResponse ApiPostsResponse { get; set; }
		ApiPostsResponse ApiMyPostsResponse { get; set; }
		int PostImageIndex { get; set; }
		Post CurrentPost { get; set; }


		//Request
		ApiRequestsResponse ApiRequestsResponse { get; set; }
		ApiPostsResponse ApiMyRequestedPostResponse { get; set; }
		ApiCommentResponse ApiCommentResponse { get; set; }

		//Login
		LoginResponse LoginResponse { get; set; }
		bool IsLoggedIn { get; set; }

		//Conversation
		ApiConversationResponse ApiConversationResponse { get; set; }
		ApiMessagesResponse ApiMessagesResponse { get; set; }
	}
}
