﻿using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using GiveAndTake.Core.Helpers.Interface;
using MvvmCross;
using MvvmCross.Base;
using MvvmCross.Plugin.File;
using Newtonsoft.Json;

namespace GiveAndTake.Core.Models
{
	public class AppDataLoader : IAppDataLoader
	{
		private readonly IMvxFileStore _fileStore;
		private readonly IMvxResourceLoader _resourceLoader;

		private const string SettingFile = "AppData.json";
		private const string SettingFolder = "ChovaNhanAppSetting";

		public LanguageType CurrentLanguage { get; private set; }

		private static readonly object LockObject = new object();
		private readonly string _filePath;

		public AppDataLoader(IMvxFileStore fileStore, IMvxResourceLoader resourceLoader)
		{
			_resourceLoader = resourceLoader;
			_fileStore = fileStore;
			_filePath = _fileStore.PathCombine(SettingFolder, SettingFile);
			_fileStore.EnsureFolderExists(SettingFolder);
		}

		public AppData AppData
		{
			get
			{
				AppData result = null;

				lock (LockObject)
				{
					if (_fileStore.TryReadTextFile(_filePath, out var content))
					{
						if (!string.IsNullOrWhiteSpace(content))
						{
							try
							{
								result = JsonConvert.DeserializeObject<AppData>(content);
							}
							catch
							{
								// ignored
							}
						}
					}
				}

				return result ?? new AppData();
			}
		}

		public void SaveData(AppData appData)
		{
			lock (LockObject)
			{
				_fileStore.WriteFile(_filePath, JsonConvert.SerializeObject(appData));
			}
		}

		public void UpdateLanguage(LanguageType locale)
		{
			CurrentLanguage = locale;
			var appData = AppData;
			appData.CurrentLanguage = locale;

			SaveData(appData);

			Mvx.Resolve<IStorageHelper>().SaveLanguage(locale);
		}

		public void Initialize()
		{
			if (!_fileStore.Exists(SettingFile))
			{
				return;
			}

			lock (LockObject)
			{
				if (!_fileStore.TryReadBinaryFile(SettingFile, LoadFrom))
				{
					_resourceLoader.GetResourceStream(SettingFile, inputStream => LoadFrom(inputStream));
				}
			}
		}

		private bool LoadFrom(Stream inputStream)
		{
			var loadedData = XDocument.Load(inputStream);

			if (loadedData.Root == null)
			{
				return false;
			}

			using (var reader = loadedData.Root.CreateReader())
			{
				var setting = (AppData)new XmlSerializer(typeof(AppData)).Deserialize(reader);
				CurrentLanguage = setting.CurrentLanguage;
				return true;
			}
		}

	}
}
