﻿using System.Runtime.Serialization;

namespace GiveAndTake.Core.Models
{
    [DataContract]
    public class UserRegisterRequest
    {
        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [IgnoreDataMember]
        public string OtpCode { get; set; }

        [IgnoreDataMember] 
        public OtpVerifyType OtpVerifyType { get; set; } = OtpVerifyType.Register;
    }
}