﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GiveAndTake.Core.Models
{
	public class AvatarData
	{
		public string ImageUrl { get; set; }
		public string Base64String { get; set; }
	}
}
