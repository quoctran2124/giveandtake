﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GiveAndTake.Core.Models
{
	[DataContract]
	public class Conversation
	{
		[DataMember(Name = "id")]
		public string Id { get; set; }

		[DataMember(Name = "userConversationId")]
		public string UserConversationId { get; set; }

		[DataMember(Name = "friend")]
		public User User { get; set; }

		[DataMember(Name = "userId")]
		public string UserId { get; set; }

		[DataMember(Name = "message")]
		public string MessageContent { get; set; }

		[DataMember(Name = "lastMessage")]
		public ChatMessage LastMessage { get; set; }

		[DataMember(Name = "createdTime")]
		public DateTime CreatedTime { get; set; }

		[DataMember(Name = "updatedTime")]
		public DateTime UpdatedTime { get; set; }

		[DataMember(Name = "conversationStatus")]
		public string ConversationStatus { get; set; }

		[DataMember(Name = "isAbleToSendMessage")]
		public bool IsAbleToSendMessage { get; set; }

		[DataMember(Name = "isRead")]
		public bool IsRead { get; set; }

		[DataMember(Name = "messages")]
		public List<ChatMessage> Messages { get; set; }
	}
}
