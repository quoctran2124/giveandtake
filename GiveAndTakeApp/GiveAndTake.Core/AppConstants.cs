namespace GiveAndTake.Core
{
	public static class AppConstants
	{
		public const string AppTitle = "Cho và Nhận";

#if DEVELOPMENT
		public const string ApiUrl = "https://api.dev.chovanhan.vn/api/v1/";
		public static string ChatHubUrl = "https://api.dev.chovanhan.vn/chatHub";
#else
		public const string ApiUrl = "https://api.chovanhan.vn/api/v1/";
		public static string ChatHubUrl = "https://api.chovanhan.vn/chatHub";
#endif

		public const int ApiTimeout = 300; // seconds
		public const int ApiVersion = 3; 
		public const int PasswordMinimumLength = 8; 

		//App Url
		public const string PlayStoreUrl = "https://play.google.com/store/apps/details?id=com.sioux.giveandtake";
		public const string AppStoreUrl = "https://itunes.apple.com/vn/app/cho-va-nhan/id1444250710";

        //OTP SMS service 
        public const string OtpApiUrl = "http://49.156.52.24:5993/SmsService.asmx";
        public const string OtpApiContentType = "text/xml";
        public const string UserId = "Sioux_cvn";
        public const string Password = "nctit5sfpmts0d8";
        public const string OtpMessage = "Thong bao - Ma OTP cua ban la {0}. Cam on ban da su dung ung dung cho va nhan";
        public const string OtpServiceSuccessResult = "1";

        //API Url Path
        public const string Version = "version";
        public const string GetCategories = "categories/app/list";
		public const string GetUsers = "user/getList";
		public const string GetPostList = "post/app/list";
		public const string GetMyPostList = "post/app/listPostOfUser";
		public const string GetPostDetail = "post/app/detail";
		public const string GetPostOfUser = "post/app/listPostOfUser";
		public const string GetMyRequestedPosts = "post/app/listRequestedPostOfUser";
		public const string ChangeStatusOfPost = "post/status";
		public const string EditPost = "post/app/update";
		public const string LoginFacebook = "user/login/facebook";
		public const string LoginWithPhoneNumber = "user/login";
		public const string RegisterWithPhoneNumber = "user";
		public const string UpdatePassword = "user/password";
		public const string LogoutApp = "user/logout";
		public const string CreatePost = "post/app/create";
		public const string CreateRequest = "request/create";
		public const string CheckUserRequest = "request/checkUserRequest";
		public const string CancelUserRequest = "request/deleteCurrentUserRequest";
		public const string GetUserProfile = "user";
		public const string BlockUserUrl = "user/block";	
		public const string GetProvinceCities = "provincecity/list";
		public const string GetRequestOfPost = "request/list";
		public const string GetRequestById = "request/getRequestById";
		public const string GetRequestOfCurrentUserByPostId = "request/getRequestOfCurrentUserByPostId";
		public const string ChangeStatusOfRequest = "request/status";
		public const string CheckIfRequestProcessed = "request/checkIfRequestProcessed";
		public const string GetNotificationList = "notification/list";
		public const string UpdateReadStatus = "notification/updateReadStatus";
		public const string GetResponseById = "response/getResponseById";
		public const string RegisterPushNotificationUserInformation = "deviceIdentity/registerDevice";
		public const string GcmNotificationKey = "gcm.notification.notification";
		public const string GcmBadgeKey = "gcm.notification.aps";
		public const string GcmNotificationBadgeKey = "gcm.notification.notificationBadge";
		public const string GcmConversationBadgeKey = "gcm.notification.messageBadge";
		public const string UpdateSeenStatus = "notification/updateSeenStatus";
		public const string GetNotificationBadgeFromServer = "notification/getUnSeenNotificationNumber";
		public const string GetConversationBadgeFromServer = "conversations/badge";
		public const string DeletePushNotificationToken = "deviceIdentity/delete";
		public const string GetMessages = "messages";
		public const string AppreciateAPost = "post/app/appreciate";
		public static string CreateResponse = "response/create";
		public const string CreatePostReport = "report/createReport";
		public const string Conversations = "conversations";
		public const string GetCommentOfPost = "comment/list";
		public const string CreateComment = "comment/create";
		public const string EditComment = "comment/edit";
		public const string DeleteComment = "comment/delete";
		public const string ReportComment = "comment/report";

		public const int DefaultApiPageNumber = 1;
		public const int DefaultApiPageStep = 1;

		public const string ContentType = "application/json";

		//Token
		public const string Token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiOWM4NzIxMGEtM2E2ZC00MGM5LTgwMmItOGRkZWVhN2RlMDU3IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6IkFkbWluIEFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIlVzZXIiLCJBZG1pbiJdLCJuYmYiOjE1MzY2MzM4MTYsImV4cCI6MTUzOTIyNTgxNiwiaXNzIjoiR2l2ZWF3YXkiLCJhdWQiOiJFdmVyeW9uZSJ9.2Jz4t5mnrhXSbr93gtVtSjDdI9nXB412-uwN40xd-aU";
		public const string ApiKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiMjMwMzc5MmItZGI1MC00YzlhLTk3MjAtN2JkMWVjN2QzM2U4IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6IkFkbWluIEFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIkFkbWluIiwiVXNlciJdLCJuYmYiOjE1MzM4ODY3MzMsImV4cCI6MTUzNjQ3ODczMywiaXNzIjoiR2l2ZWF3YXkiLCJhdWQiOiJFdmVyeW9uZSJ9.2A8md3WFGYT-w2Kz0OMAUAj7e20L-wTxIfKL4KyEOko";

		//Navigation Tab
		public const string HomeTab = "Home";
		public const string NotificationTab = "Notification";
		public const string ConversationTab = "Conversation";
		public const string ProfileTab = "Profile";
		public const int NotificationTabIndex = 1;
		public const int ConversationTabIndex = 2;
		public const int ProfileTabIndex = 3;

        //Storage Helper keys
        public const string DeviceLanguage = "chovanhan_language";
        public const string Authenticated = "authenticated";
        public const string Credentials = "credentials";


        public static int NumberOfRequestPerPage = 20;
		public static int NumOfFragmentViewPager = 4;

		public const string DateStringFormat = "dd.MM.yyyy";

		public static string SupportContactValue = "(0236)1022";
		public static string SupportContactPhone = "02361022";
		public const string DefaultFeedbackEmail = "chovanhan.team@gmail.com";

		public static string DefaultPostImage = "default_post";
		public static string NotFound = "Not Found";

		public const string DefaultUserName = "username";

		public const string GiveAndTakeHashtag = "#ChovàNhận";

		public const int ImageMaxSize = 3840;
		public const int ImagePercentQuality = 80;
		public const int DefaultDelayLoadingTime = 777; //For iOS
		public const int ShortDelayTime = 100;

		public const int NumberOfRequestNotDisplayedInIos = 6;
		public const int NumberOfCommentNotDisplayedInIos = 6;

		public const string SelectLanguageVnText = "Lựa chọn ngôn ngữ";
		public const string SelectLanguageEnText = "(Select your language)";
		public const string VnTextTitle = "Tiếng Việt";
		public const string EnTextTitle = "English";

		public const string DefaultCategoryCreatePostId = "0c109358-fae2-42bd-b2f3-45cbe98a5dbd";

		public static string UserStatusBlocked = "Blocked";

		public static string ConversationStatusBlocked = "block";
		public static string ConversationStatusNormal = "allow";

		public const string ShareFacebookLink = "https://chovanhan.vn/applinks/";
		public const string PolicyWebPageLink = "https://chovanhan.vn/chinh-sach-bao-mat/";

		public static int ConstAppLinkLength => ShareFacebookLink.Length;

		public static string FaceBookAvatarUrl = "https://graph.facebook.com/{0}/picture?type=large&width=720&height=720";
	}
}