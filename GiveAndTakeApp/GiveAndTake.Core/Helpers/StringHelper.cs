﻿using System;
using System.ComponentModel;

namespace GiveAndTake.Core.Helpers
{
	public static class StringHelper
	{
		public static T GetEnumFromDescription<T>(string description, T defaultValue) where T : struct
		{
			//Link referenced: https://stackoverflow.com/questions/4367723/get-enum-from-description-attribute/4367868#4367868
			var type = typeof(T);
			if (!type.IsEnum) throw new InvalidOperationException();
			foreach (var field in type.GetFields())
			{
				if (Attribute.GetCustomAttribute(field,
					typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
				{
					if (attribute.Description == description)
						return (T)field.GetValue(null);
				}
				else
				{
					if (field.Name == description)
						return (T)field.GetValue(null);
				}
			}

			return defaultValue;
		}
	}
}
