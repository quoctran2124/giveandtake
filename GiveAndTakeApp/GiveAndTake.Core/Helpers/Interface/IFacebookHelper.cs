﻿using GiveAndTake.Core.Models;

namespace GiveAndTake.Core.Helpers.Interface
{
	public interface IFacebookHelper
	{
		void ShareFacebookContent(string content, string contentUrl);
		bool IsLoggedIn();
		User GetCurrentUser();
		void LogOut();
	}
}
