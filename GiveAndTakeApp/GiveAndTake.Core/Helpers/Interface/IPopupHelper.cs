﻿using System.Threading.Tasks;
using GiveAndTake.Core.ViewModels.Popup;

namespace GiveAndTake.Core.Helpers.Interface
{
	public interface IPopupHelper
	{
		PopupWarningViewModel PopupWarningViewModel { get; set; }
		Task ShowWarningPopup(string message);
	}
}