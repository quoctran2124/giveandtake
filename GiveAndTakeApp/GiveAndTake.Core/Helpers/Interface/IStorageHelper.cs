﻿using GiveAndTake.Core.Models;

namespace GiveAndTake.Core.Helpers.Interface
{
	public interface IStorageHelper
	{
		void SaveLanguage(LanguageType language);
        void SaveCredentials(LoginResponse loginResponse);
        LoginResponse GetCredentials();
        void DeleteCredentials();
    }
}
