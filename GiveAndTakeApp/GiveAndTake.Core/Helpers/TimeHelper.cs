﻿using System;
using I18NPortable;

namespace GiveAndTake.Core.Helpers
{
	public class TimeHelper
	{
		private static II18N Strings => I18N.Current;
		public static string ToTimeAgo(DateTime dateTime)
		{
			var timeSpan = DateTime.Now.Subtract(dateTime);

			if (timeSpan < TimeSpan.FromSeconds(60))
			{
				return Strings["FewSecondsAgo"];
			}

			if (timeSpan < TimeSpan.FromMinutes(60))
			{
				return timeSpan.Minutes == 1
					? Strings["OneMinuteAgo"]
					: $"{timeSpan.Minutes} {Strings["MinutesAgo"]}";
			}

			if (timeSpan.Days > 1)
			{
				return dateTime.ToString(AppConstants.DateStringFormat);
			}

			switch (timeSpan.Hours)
			{
				case 1:
					return Strings["OneHourAgo"];
				case 0:
					return Strings["Yesterday"];
				default:
					return $"{timeSpan.Hours} {Strings["HoursAgo"]}";
			}
		}
	}
}