﻿using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross;
using MvvmCross.Navigation;
using System.Threading.Tasks;
using GiveAndTake.Core.Helpers.Interface;

namespace GiveAndTake.Core.Helpers
{
	/*
	 * Create popup helper for warning popup
	 * Ensure only 1 warning popup will be displayed no matter how many errors occurs
	 * The message of the warning popup will be the latest error message passed in the ShowWarningPopup task
	 */
	public class PopupHelper : IPopupHelper
	{
		public IMvxNavigationService NavigationService => Mvx.Resolve<IMvxNavigationService>();

		public PopupWarningViewModel PopupWarningViewModel { get; set; }

		public async Task ShowWarningPopup(string message)
		{
			if (PopupWarningViewModel != null)
			{
				PopupWarningViewModel.Message = message;
				return;
			}

			await NavigationService.Navigate<PopupWarningViewModel, string>(message);
			await Task.Delay(AppConstants.DefaultDelayLoadingTime);
		}
	}
}