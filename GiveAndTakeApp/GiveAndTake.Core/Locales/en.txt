﻿# key = value (the key will be the same across locales)

Giving = Giving
Gave = Gave
Pending = Pending
Approved = Approved
Rejected = Rejected
Received = Received
ApprovedRequest =  Approved Request
ReceivedRequest =  Received item
PostIsDeleted = This post has been deleted!
PostIsClosed = This post has been closed!

GivingStatus = Giving
GaveStatus = Gave
PendingStatus = Pending
ApprovedStatus = Approved
ReceivedStatus = Received

Normal = Member
Silver = Silver heart
Gold = Golden heart
Platinum = Platinum heart
Diamond = Diamond heart

DefaultWarningMessage = This function is not yet finished.\nPlease come back later
ErrorMessage = Something went wrong.\nPlease try again later
ErrorConnectionMessage = Network connection error.\nPlease try again
ErrorLoginBadRequestMessage = Wrong phone number or password.\nPlease try again
ErrorRegisterBadRequestMessage = The phone number is already registered.\nPlease try another phone number.
ErrorPhoneNumberNotExited = The phone number is not existed\nPlease try another phone number.
RequestRejectingMessage = Are you sure to reject the request?
DeleteConfirmationMessage = Are you sure to delete this post?
SuccessfulAcceptanceMessage = Accepted!
SuccessfulRejectionMessage = Declined!
CancelEditPostConfirmMessage = Do you want to cancel editing?
CancelRequestConfirm = \nAre you sure you want to unsubscribe?\n
ReportPostNotAvailableMessage = Sorry, You have reported this post before!
CancelEditProfileSettingConfirm = The changes have not been saved.\nAre you sure you want to cancel?
BlockUserConfirm = Are you sure you want to block this user?
BlockUserContent = This user will not be able to see your article or request your item until you unlock it

PopupChangeLanguageTitle = Select Your Language
SearchResultNullTitle = No results found
SubmitTitle = Confirm
PopupCategoriesTitle = Category
PopupSortFiltersTitle = Sort by
PopupLocationFiltersTitle = Filter by
PopupRequestDetailTitle = Request detail
PopupResponseTitle = Feedback
ButtonRejectTitle = Reject
ButtonApproveTitle = Approve
RequestsTitle = Request list
RequestApprovalTitle = Information
RankTitle = Rating
SentTitle = Gave
MyPostsTitle = My post
MyRequestsTitle = Sent Request
CreatePostBtnSubmitTitle = Submit
LoginTitle = Login
LoadingDataOverlayTitle = Loading
UploadDataOverLayTitle = Posting
UpdateOverLayTitle = Updating
LoginProcessOverLayTitle = Logging on
ProcessingDataOverLayTitle = Processing
PopupReportTitle = Reason for reporting post:
BlockLabel = Block
SelectedImage = Selected 0 pictures
ImageProcessing = Image Processing ..
PopupTitle = Information
SendTo = Send to:
PopupInputInformationPlaceHolder = Detailed information ...

InappropriateContentReason = Inappropriate content 
InappropriatePictureReason = Contains offensive images
FakeContentReason = Fake Content
OtherReason = Other reasons:

MarkGiven = Change status to "Gave"
MarkGiving = Change status to "Giving"
MarkReceived = Change status to "Received"

Modify = Edit
ViewPostRequests = Manage Requests
Delete = Delete
Report = Report
CancelRequest = Cancel
SaveAPost = Save
ConfirmChangeStatusOfPost = This post has been requested.\nAre you sure you want to close?
ConfirmDeletePost = Are you sure you want to delete this post?
EditProfile = Edit information
SendFeedback = Send feedback
LogOut = Log out
BlockUser = Block this user
TakePhoto = Take a photo
SelectPhoto = Select a photo from the gallery
CancelTitle = Cancel
Done =  Done
NotFound = Not Found
SendTitleButton = Send
RequestReceiver = Send to:
ChangeLanguage = Change language
ConfirmChangeLanguage = Confirm

About = About
AppInfo = Information about "Cho va Nhan"
Department = Department of Information and Communication
DaNangCity = Danang city
MobileApp = "Cho va Nhan" Mobile application
AppVersionLabel = Version:
ReleaseDateLabel = Release date:
ReleaseDateValue = February 14, 2019
SupportContactLabel = Contact support:
DevelopedBy = Developed by:

Member = Member"
Times = Times
Time = Time
UserNameLabel = User name
TermAndCondition = Terms and conditions 

CreatePostDescriptionPlaceHolder = Description (Brand, style, color, ...)
CreatePostTitlePlaceHolder = Title (Brand, category, ...)
PopupReasonHint = The reason I want to report this post is ...

Tất cả (mặc định) = All (Default)
Văn phòng phẩm = Stationery
Đồ dùng cá nhân = Personal belongings
Mẹ và bé = Mother and baby
Nội thất = Furniture
Nội ngoại thất = Interior and Exterior
Thú cưng = Pet
Xe cộ = Vehicle
Đồ điện tử = Electronice device
Thức ăn = Foods
Khác = Other

DefaultCategoryCreatePostName = Stationery
DefaultLocationFilter = Da Nang
Đà Nẵng = Da Nang

SortByNewest = Sort by newest (Default)
SortByOldest = Sort by oldest

FewSecondsAgo = Few seconds ago 
MinutesAgo =  minutes ago
HoursAgo =  hours ago
Yesterday = Yesterday
OneMinuteAgo = 1 minute ago
OneHourAgo = 1 hour ago

Selected1Picture = 1 image selected
SelectedPictures = {0} images selected

ShareTitle = Share

AppUpdateTitle = Newer version is available
AppUpdateMessage = The current version of this application \nis no longer supported
AppUpdateUpdateButtonText = Update now

SearchMessagePlaceHolder = Search by name ..
BlockThisConversation = Block this conversation
UnBlockThisConversation = Re-Open this conversation
DeleteThisConversation = Delete this conversation
DeleteConversationConfirmationMessage = Are you sure you want to delete this conversation?
BlockConversationConfirmationMessage = Are you sure you want to block this conversation?

BlockedConverstationTitle = List of blocked conversations

SendMessageHint = Aa

UnblockTitle = Unblock

SendCommentHint = Send Comment ...
EditCommentTitle =  Edit
DeleteCommentConfirmation = Are you sure you want to delete this comment?

ShareContent = A meaningful gift needs a new owner. Please download "Cho và Nhận" application to receive this gift! \n\nType of gift: {0} \nGift: {1} \nCharacteristics: \n{2} \nCreated on: {3} \nLocation: {4}

WarningBlockedUserMessage = <h style=\"text-align: center;\">Your account has been locked! <br/>Please contact email: <br/><a href=\"temp.com\">chovanhan.team@gmail.com</a> <br/>for more details!</h> 

// Auth Screens -------------------------------------------------------------------------------------------------------------------
AcceptTermAndConditionInHtml = By logging in you agree to <br/><a href =\"./\">our terms and conditions</a>
TermAndConditionContentInHtml = <div>Welcome to <font color='#3fb8ea'><b>Cho va Nhan</b></font> <br/><br/>These Terms will help you adjust your use of "Cho va Nhan" application properly and appropriately, so please read these terms carefully before using. Once you have logged in to the application, it will be understood that you have agreed to be bound by the Terms and Privacy Policy of "Cho va nhan" application . If you do not agree with these terms and conditions of agreement, you should not and must not be albe to "Cho va nhan" application. This agreement applies to all users without exceptions. <br/><br/><font color='#3fb8ea'><b>WHAT IS "CHO VA NHAN" APPLICATION?</font></b> <br/><br/>"Cho va nhan" is a non-profit application where users can share free items to the community, At the same time, you can get what you are looking for. The application allows users to simultaneously donate items and offer to receive items from other users completely free without any financial activities such as cash exchange.<br/><br/><font color='#3fb8ea'><b>PURPOSE OF THE APPLICATION:</font></b> <br/><br/>The mission of "Cho va nhan"  is to give people the ability to build a shared community, help each other and bring people closer together through giving and receiving actions. To promote this mission, "Cho va nhan" application is built with the purpose of:<br/><br/>Summary of information give and receive: The application sends notifications about the donation of valuable items for those who need it.<br/><br/>Connecting benefactors: As a place for charitable organizations, benefactors can meet, organize volunteer programs.<br/><br/>Helping difficult people: Through the application, it is easier to give precious, useful and necessary gifts to people in difficult circumstances.<br/><br/>Raising awareness of the community: Encouraging, recognizing the spirit of support and assistance among individuals and organizations in the community, promoting awareness, building a community of mutual affection.<br/><br/><font color='#3fb8ea'><b>YOUR USER ACCOUNT</font></b> <br/><br/><b>Register user account:</b><br/><br/>You can find out information about the application at the official website of the application without an account but you need to have an account to log in and exchange information for give and receive on the application. We allow you to subscribe to "Cho va nhan" apps using third-party services such as Facebook. If you use a third party service to register for an account, you may be required to provide your login information to that third party service. We do not record or store passwords for such third party services. Please review your security and installation messages for that third party service before using to log in "Cho va nhan" application. Your account is exclusive to you and you are not allowed to share or allow anyone else to use your account. You may not be albe to use someone else's account, create accounts for others, or create and use accounts to impersonate others. You will be responsible for all activities arising from your account.<br/><br/>In addition, "Cho va nhan" also support automatic login after you log in for the first time and you can only log in with another account after logging out of your account from the app. You are responsible for all damages caused by unauthorized access to the application from your account. You must notify us immediately if you know or have reason to suspect any unauthorized use of your account. <br/><br/><b>Policies represent user interests – Your commitment to us:</b><br/><br/>You must ensure that: <b>(1)</b> All information you provide us is truthful, accurate, complete and will always be truthful, accurate and complete; <b>(2)</b> You will comply with the terms and conditions of this Agreement and any other agreements with which you are subject to the use of "Cho va nhan" application from your account; <b>(3)</b> You will not use or access the application for fraudulent or appropriated properties; and <b>(4)</b> Your access and use of your application account will not create violations of any other agreements, contracts, terms of use or any other laws and regulations that you must follow. <b>(5)</b> You must always abide by the laws and regulations of the local government in exchanging information and keeping the traditions and  customs in line with people's morality in the activities of posting  images, information and Item exchange takes place on "Cho va nhan" application. <b>(6)</b> You will not give away items that may endanger people or intentionally commit acts of harming others.  <b>(7)</b> We may limit your ability to use the application, including sending infringement alerts when there are feedback reports from other users about your inappropriate and improper behavior when posting information about images, objectionable content, reactionary, anti-government,  not consistent with fine customs and morality; In addition, we can unilaterally suspend and lock your application account without prior notice if you violate too many times or the level of violation is too great or in the case if we have reasonable grounds to suspect that the information you provide is untrue, inaccurate, outdated, incomplete or in violation of the law. <br/><br/><font color='#3fb8ea'><b>HOW TO USE THE APPLICATION</font></b> <br/><br/><b>When you are a giver:</b><br/><br/>1. When you decide to give an item on "Cho va nhan", you will post an article about the item, which describes the item in detail (image, category, usage status, delivery location ...)<br/><br/>2. You will receive requests for items and you will review these offers.<br/><br/>3. You make a confirmation of agreement with the proposal that you think is appropriate and would like to donate the item or reject inappropriate offers.<br/><br/><b>When you are the person who wants to receive the item:</b><br/><br/>1. When you want to receive an item, you will search for the desired item based on the image, category, usage status ... to determine the appropriate item.<br/><br/>2. After finding the desired item, you will enter the item owner's post and send the request to receive the item for the giver.<br/><br/>3. If the giver agrees to your offer, you will exchange information on how to get the item (exchange details about the time, place, and how to get the item with the owner).<br/><br/><font color='#3fb8ea'><b>OUR RIGHTS AND OBLIGATIONS</font></b> <br/><br/>We do not provide facilities or repositories of items, donations are conducted through the exchange of personal information between giver and taker.<br/><br/>We do not charge any fees related to activities awarded and generated because this is a non-profit service, serving the community and society; therefore, all financial-related actions such as charging for services from individuals and organizations who are not "Cho va nhan" application managers or impersonating "Cho va nhan" application will be considered It is a violation of law and may lead to criminal and civil prosecution. You can report and send feedback if detecting these illegal activities to us via email: <b>chovanhan.team@gmail.com.</b><br/><br/>We reserve the right to add new features to the application, discontinue the application, or any component in the application, or suspend, delete, modify or disable access to the application, or Any part in any time at our discretion and we absolutely may not need to notify you. We will endeavor to send notices about any changes made on the application through the application notification tool, via your account or via email before making changes to the application. In any case, we will not be responsible for deleting or disabling application access or any part thereof.<br/><br/>We may unilaterally delete your item post according to our evaluation. Reasons for refuse may include: Sensitive / offensive images, items that are prohibited, illegal stimulants in the current state law ...<br/><br/>Any Item or exchange activity performed through "Cho va nhan" application is your responsibility and you are solely responsible for ensuring the safety of information, property, and security. All of your own exchange activities for giving or receiving will not interfere by us  and we will not be responsible for this issue.<br/><br/>We welcome all of your comments, feedback, information or materials to contribute to the completion of "Cho va nhan" application. Phản hồi của bạn Your feedback will become our property when you send it to us. By using the feature to send feedback in the application, you are considered to agree to transfer ideas and information from you voluntarily. We will be free to use, copy, distribute, publish and modify your feedback on an unlimited basis and without violating copyright and intellectual property laws.<br/><br/>We respect the privacy of personal information of all users, all personal data collected from the user will be processed according to our Privacy Policy – <span><a href ="{0}">Refer to the link</a></span>. By accepting this Agreement, you also agree to the processing and use of your personal data in accordance with our privacy policy.<br/><br/>We may report your fraudulent activities or violate this Agreement with relevant law enforcement agencies and we also have the right to provide the necessary information to the police or real parties. law enforcement if we discover or be informed of illegal activities, threatening to endanger human property and lives.<br/><br/><font color='#3fb8ea'><b>CONTACT US</font></b> <br/><br/>If you have any questions or concerns regarding this Agreement of "Cho va nhan" application, please contact us via email: <b>chovanhan.team@gmail.com.</b></div>
EdtPhoneNumber = Phone number
EdtDisplayName = Display Name
EdtPassword = Password
BtnShowPassword = Show
BtnHidePassword = Hide
BtnLogin = Login
BtnRegister = Register
PasswordInvalid = Password must contain at least 8 characters
ErrorAccountNotExisted = This account is not existed\nPlease try with another phone number

LoginScreenTxtNotHavingAnAccount = Not having an account?
LoginScreenBtnForgotPassword = Forgot your password?
LoginScreenBtnLoginWithFacebook = Login with Facebook
LoginScreenTxtLoginWithPhoneNumber = Or login via phone number

RegisterScreenTxtRegister = Register
RegisterScreenTxtRegisterWithPhoneNumber = Register an account with your phone number
RegisterScreenTxtAlreadyHadAnAccount = Already had an account?
RegisterScreenProcessOverLayTitle = Registering
RegisterScreenAcceptTermAndConditionInHtml = By registering in you agree to <br/><a href =\"./\">our terms and conditions</a>

OtpScreenTxtOtpTitle = Enter OTP to verify
OtpScreenTxtOtpDetail = The OTP will be sent to your registration phone number with in a few seconds.
OtpScreenTxtResendOtp = Resend OTP
OtpScreenWrongCode = Wrong OTP\nPlease try again
OtpSendingMessage = Sending OTP
OtpServiceError = Can not send OTP code to your phone number.\nPlease check again

TxtRegisterSuccess = You have registered successfully\na new account
TxtRegisterSuccessDetail = The application will automatically login in a few seconds

TxtResetPassword = Reset Password
TxtInputYourPhoneNumber = Enter your registered phone number
TxtInputYourPassword = Enter your new password
